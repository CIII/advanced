package Shared

import java.text.SimpleDateFormat
import java.util.Calendar

import _root_.util._
import akka.actor.{ActorSystem, Props}
import akka.event.LoggingAdapter
import com.github.nscala_time.time.Imports._
import com.google.api.ads.adwords.axis.v201509.mcm.Customer
import com.google.api.ads.adwords.lib.jaxb.v201509.ReportDefinitionReportType
import com.google.gson.GsonBuilder
import com.microsoft.bingads.AuthorizationData
import com.microsoft.bingads.customermanagement.{AccountInfoWithCustomerData, User, Account}
import com.mongodb.casbah.Imports._
import com.mongodb.casbah.MongoClient
import models.mongodb.facebook.Facebook.{NanigansApiAccount, _}
import models.mongodb.google.Google._
import models.mongodb.msn.Msn._
import models.mongodb.uber.Uber._
import models.mongodb.yahoo.Yahoo._
import models.mongodb.{PermissionGroup, SecurityRole, Utilities}
import org.joda.time.format.DateTimeFormat
import play.api.Play.current
import play.api.cache.Cache
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json._
import play.api.libs.ws.WS
import play.api.mvc._
import play.api.{Logger, Play}
import sync.facebook.process.reporting.NanigansReportActor
import sync.google.process.management.mcc.MccActor
import sync.google.process.reporting.GoogleReportActor
import sync.msn.bingads.BingAdsHelper
import sync.msn.process.reporting.MsnReportActor
import sync.scripts.google.ExportKeywordPerformanceReportCsvActor
import sync.scripts.msn.ExportKeywordPerformanceReportRequestCsvActor
import sync.scripts.yahoo.ExportKeywordStatsCsvActor
import sync.shared.Facebook._
import sync.shared.Uber._
import sync.uber.reporting.UberReportActor
import sync.yahoo.reporting.YahooReportActor

import scala.collection.immutable.List
import scala.concurrent.duration.FiniteDuration
import scala.reflect.runtime.universe._

object Shared {

  // Google GSON object for serializing/deserializing API objs
  lazy val gson = new GsonBuilder()
    .addSerializationExclusionStrategy(new PrimaryExclusionStrategy)
    .addDeserializationExclusionStrategy(new PrimaryExclusionStrategy)
    .registerTypeAdapter(classOf[com.google.api.ads.adwords.axis.v201509.cm.Criterion], new CriterionAdapter)
    .registerTypeAdapter(classOf[com.google.api.ads.adwords.axis.v201509.cm.BiddingScheme], new BiddingSchemeAdapter)
    .registerTypeAdapter(classOf[com.google.api.ads.adwords.axis.v201509.cm.Setting], new SettingAdapter)
    .registerTypeAdapter(classOf[com.google.api.ads.adwords.axis.v201509.cm.Ad], new AdAdapter)
    .registerTypeAdapter(classOf[com.google.api.ads.adwords.axis.v201509.cm.Bids], new BidsAdapter)
    .registerTypeAdapter(classOf[com.google.api.ads.adwords.axis.v201509.cm.ComparableValue], new ComparableValueAdapter)
    .registerTypeAdapter(classOf[com.google.api.ads.adwords.axis.v201509.cm.Address], new AddressAdapter)
    .registerTypeAdapter(classOf[PendingCacheStructure], new PendingCacheStructureAdapter)
    .registerTypeAdapter(classOf[TaskStructure], new TaskStructureAdapter)
    .create()

  // connection to mongodb
  val mongoClient = MongoClient(
    Play.current.configuration.getString("mongodb.host").getOrElse(
      throw new Exception("MongoDB application host missing")
    ),
    Play.current.configuration.getInt("mongodb.port").getOrElse(
      throw new Exception("MongoDB application port missing")
    )
  )

  // retrieve advanced db
  val advancedDB = mongoClient(
    Play.current.configuration.getString("mongodb.db").getOrElse(
      throw new Exception("MongoDB Database name missing")
    )
  )

  // create collection when one doesn't already exist
  def advancedCollection(col_name: String): MongoCollection = {
    if (!advancedDB.collectionExists(col_name))
      advancedDB.createCollection(col_name, DBObject())
    advancedDB(col_name)
  }

  // typesafe comparator
  def compare[T](a: Option[T], b: Option[T]) = (a, b) match {
    case (Some(a), Some(b)) => if(a.equals(b)) true else false
    case (None, None)       => true
    case (_, _)          => false
  }


  implicit class MyInstanceOf[U: TypeTag](that: U) {
    def myIsInstanceOf[T: TypeTag] =
      typeOf[U] <:< typeOf[T]
  }

  val cache_ext = "_cache"
  val task_ext = "_task"

  val advantage_google_entities = List(
    ""
  )

  var uber_mapping = List(
    UberReportRequest(
      name="search_auto_overall",
      format="csv",
      preset_id=Some("1964"),
      report_id=None,
      rowParser=searchAutoOverallTVRowParser,
      importer=searchAutoOverallTVImporter
    ),
    UberReportRequest(
      name="search_auto_overall",
      format="csv",
      preset_id=Some("1963"),
      report_id=None,
      rowParser=searchAutoOverallLKRowParser,
      importer=searchAutoOverallLKImporter
    ),
    UberReportRequest(
      name="pricing_by_classification",
      format="csv",
      preset_id=None,
      report_id=Some("1756934"),
      rowParser=pricingByClassificationClickDataRowParser,
      importer=pricingByClassificationClickDataImporter
    ),
    UberReportRequest(
      name="pricing_by_classification",
      format="csv",
      preset_id=None,
      report_id=Some("1756930"),
      rowParser=pricingByClassificationAIQLeadDataRowParser,
      importer=pricingByClassificationAIQLeadDataImporter
    ),
    UberReportRequest(
      name="pricing_by_classification",
      format="csv",
      preset_id=None,
      report_id=Some("1756924"),
      rowParser=pricingByClassificationLeadDataRowParser,
      importer=pricingByClassificationLeadDataImporter
    )
  )

  // standard user definition
  lazy val normalUserID = SecurityRole.securityRoleCollection.findOne(DBObject("RoleName" -> "Standard")) match {
      case Some(role) =>
        role._id.get
      case _ =>
        val _id = new ObjectId
        SecurityRole.securityRoleCollection.insert(
          SecurityRole.securityRoleToDbo(
            SecurityRole(
              Some(_id),
              "Standard",
              Array(
                PermissionGroup.GoogleRead,
                PermissionGroup.GoogleCharts,
                PermissionGroup.MSNRead,
                PermissionGroup.MSNCharts,
                PermissionGroup.NanigansRead,
                PermissionGroup.NanigansCharts,
                PermissionGroup.YahooRead,
                PermissionGroup.YahooCharts
              )
            )
          )
        )
        _id
    }

  // admin user definition
  lazy val adminUserID = SecurityRole.securityRoleCollection.findOne(DBObject("roleName" -> "Admin")) match {
    case Some(role) =>
      role._id.get
    case _ =>
      val _id = new ObjectId
      SecurityRole.securityRoleCollection.insert(
        SecurityRole.securityRoleToDbo(
          SecurityRole(
            Some(_id),
            "Standard",
            Array(
              PermissionGroup.GoogleWrite,
              PermissionGroup.GoogleRead,
              PermissionGroup.GoogleCharts,
              PermissionGroup.MSNWrite,
              PermissionGroup.MSNRead,
              PermissionGroup.MSNCharts,
              PermissionGroup.NanigansWrite,
              PermissionGroup.NanigansRead,
              PermissionGroup.NanigansCharts,
              PermissionGroup.YahooWrite,
              PermissionGroup.YahooRead,
              PermissionGroup.YahooCharts
            )
          )
        )
      )
      _id
  }

  // sync change categories
  object ChangeCategory extends Enumeration {
    type ChangeCategory = Value

    val
    ALERT,
    MCC,
    API_ACCOUNT,
    ACCOUNT_INFO,
    ADVERTISER,
    CUSTOMER,
    ACCOUNT,
    CAMPAIGN,
    CAMPAIGN_AD_SCHEDULE,
    CAMPAIGN_KEYWORD,
    CAMPAIGN_PROXIMITY,
    AD_SCHEDULE,
    AD_GROUP,
    IMAGE_AD,
    MOBILE_AD,
    TEXT_AD,
    SITE_LINK,
    SITE_PLACEMENT,
    KEYWORD,
    BUDGET,
    BIDDING_STRATEGY = Value
  }

  // sync traffic sources
  object TrafficSource extends Enumeration {
    type TrafficSource = Value
    val
    GOOGLE,
    MSN,
    YAHOO,
    NANIGANS = Value
  }

  // sync change types
  object ChangeType extends Enumeration {
    type ChangeType = Value
    val
    DELETE,
    NEW,
    UPDATE = Value
  }

  import Shared.ChangeCategory.ChangeCategory
  import Shared.ChangeType.ChangeType
  import Shared.TrafficSource.TrafficSource

  object ReportFormat extends Enumeration {
    type ReportFormat = Value
    val CSV, JSON, TSV = Value
  }

  // defines yahoo gemini report request obj
  case class MsnReportRequest (
    apiAccount: models.mongodb.msn.Msn.ApiAccount,
    customerId: Long,
    customerAccountId: Long,
    reportType: MsnReportType.Value,
    startDate: DateTime,
    endDate: DateTime,
    reportFormat: ReportFormat.Value=ReportFormat.CSV
  )

  // defines an adwords report request obj
  case class GoogleReportRequest (
    reportType: com.google.api.ads.adwords.lib.jaxb.v201509.ReportDefinitionReportType,
    fields: Option[List[String]],
    predicates: Option[List[com.google.api.ads.adwords.lib.jaxb.v201509.Predicate]],
    dateRange: com.google.api.ads.adwords.lib.jaxb.v201509.ReportDefinitionDateRangeType,
    downloadFormat: com.google.api.ads.adwords.lib.jaxb.v201509.DownloadFormat =
      com.google.api.ads.adwords.lib.jaxb.v201509.DownloadFormat.CSV,
    oAuthData: models.mongodb.google.Google.Mcc,
    customerId: Option[String],
    enableZeroImpressions: Boolean
  )

  // list of nanigans report types
  object NanigansReportType extends Enumeration {
    type NanigansReportType = Value
    val Ad = Value
  }

  // list of attribution types
  object Attribution extends Enumeration {
    type Attribution = Value
    val click = Value
  }

  // defines a nanigans report request obj
  case class NanigansReportRequest (
    reportType: NanigansReportType.Value = NanigansReportType.Ad,
    fields: List[String] = nanigansAdFields,
    metrics: List[String] = nanigansAdMetrics,
    startDate: String,
    attribution: Attribution.Value,
    downloadFormat: ReportFormat.Value=ReportFormat.CSV,
    apiAccount: NanigansApiAccount
  )

  object MsnReportType extends Enumeration {
    type MsnReportType = Value
    val AccountPerformanceReportRequest,
    AdDynamicTextPerformanceReportRequest,
    AdExtensionByAdReportRequest,
    AdExtensionByKeywordReportRequest,
    AdExtensionDetailReportRequest,
    AdExtensionDimensionReportRequest,
    AdGroupPerformanceReportRequest,
    AdPerformanceReportRequest,
    AgeGenderDemographicReportRequest,
    BrandZonePerformanceReportRequest,
    BudgetSummaryReportRequest,
    CallDetailReportRequest,
    CampaignPerformanceReportRequest,
    ConversionPerformanceReportRequest,
    DestinationUrlPerformanceReportRequest,
    GeoLocationPerformanceReportRequest,
    GeographicalLocationReportRequest,
    GoalsAndFunnelsReportRequest,
    KeywordPerformanceReportRequest,
    NegativeKeywordConflictReportRequest,
    PollGenerateReportRequest,
    ProductOfferPerformanceReportRequest,
    ProductPartitionPerformanceReportRequest,
    ProductTargetPerformanceReportRequest,
    SearchQueryPerformanceReportRequest,
    ShareOfVoiceReportRequest,
    SitePerformanceReportRequest,
    SubmitGenerateReportRequest,
    TacticChannelReportRequest,
    TrafficSourcesReportRequest = Value
  }

  // list of yahoo gemini report types
  object GeminiReportType extends Enumeration {
    type GeminiReportType = Value
    val keyword_stats, adjustment_stats, performance_stats, search_stats, ad_extension_details, conversion_rules_stats, audience  = Value
  }

  // list of yahoo gemini report fields
  object GeminiReportField extends Enumeration {
    type GeminiReportField = Value
    val Advertiser_ID,
    Campaign_ID,
    Ad_Group_ID,
    Ad_ID,
    Month,
    Week,
    Day,
    Hour,
    Pricing_Type,
    Source,
    Impressions,
    Clicks,
    Post_Click_Conversions,
    Conversions,
    Total_Conversions,
    Spend,
    Average_Position,
    Max_Bid,
    Ad_Extn_Impressions,
    Ad_Extn_Clicks,
    Ad_Extn_Conversions,
    Ad_Extn_Spend,
    Average_CPC,
    Average_CPM,
    CTR,
    Is_Adjustment,
    Adjustment_Type,
    Keyword_ID,
    Destination_URL,
    Device_Type,
    Post_Impression_Conversions,
    Delivered_Match_Type,
    Search_Term,
    Device,
    Audience_Type,
    Audience_ID,
    Product_Group_ID,
    Category_ID,
    Category_Name,
    Product_Type,
    Brand,
    Item_ID,
    Rule_ID,
    Rule_Name,
    Conversion_Device,
    Price_Type,
    Ad_Extn_ID,
    Keyword_Value,
    Post_View_Conversions,
    Conversion_Value = Value
  }

  // yahoo gemini report filter operations
  object GeminiReportFilterOperation extends Enumeration {
    type GeminiFilterOperations = Value
    val `=`, IN, between = Value
  }

  // yahoo gemini report -> field mapping
  def GeminiReportFieldMapping = Map(
    GeminiReportType.performance_stats -> List(
      GeminiReportField.Advertiser_ID,
      GeminiReportField.Campaign_ID,
      GeminiReportField.Ad_Group_ID,
      GeminiReportField.Ad_ID,
      GeminiReportField.Month,
      GeminiReportField.Week,
      GeminiReportField.Day,
      GeminiReportField.Hour,
      GeminiReportField.Pricing_Type,
      GeminiReportField.Source,
      GeminiReportField.Impressions,
      GeminiReportField.Clicks,
      GeminiReportField.Post_Click_Conversions,
      GeminiReportField.Post_Impression_Conversions,
      GeminiReportField.Conversions,
      GeminiReportField.Total_Conversions,
      GeminiReportField.Spend,
      GeminiReportField.Average_Position,
      GeminiReportField.Max_Bid,
      GeminiReportField.Ad_Extn_Impressions,
      GeminiReportField.Ad_Extn_Clicks,
      GeminiReportField.Ad_Extn_Conversions,
      GeminiReportField.Ad_Extn_Spend,
      GeminiReportField.Average_CPC,
      GeminiReportField.Average_CPM,
      GeminiReportField.CTR
    ),
    GeminiReportType.adjustment_stats -> List(
      GeminiReportField.Advertiser_ID,
      GeminiReportField.Campaign_ID,
      GeminiReportField.Day,
      GeminiReportField.Pricing_Type,
      GeminiReportField.Source,
      GeminiReportField.Is_Adjustment,
      GeminiReportField.Adjustment_Type,
      GeminiReportField.Impressions,
      GeminiReportField.Clicks,
      GeminiReportField.Conversions,
      GeminiReportField.Spend,
      GeminiReportField.Average_Position
    ),
    GeminiReportType.keyword_stats -> List(
      GeminiReportField.Advertiser_ID,
      GeminiReportField.Campaign_ID,
      GeminiReportField.Ad_Group_ID,
      GeminiReportField.Ad_ID,
      GeminiReportField.Keyword_ID,
      GeminiReportField.Destination_URL,
      GeminiReportField.Day,
      GeminiReportField.Device_Type,
      GeminiReportField.Impressions,
      GeminiReportField.Clicks,
      GeminiReportField.Post_Click_Conversions,
      GeminiReportField.Post_Impression_Conversions,
      GeminiReportField.Conversions,
      GeminiReportField.Total_Conversions,
      GeminiReportField.Spend,
      GeminiReportField.Average_Position,
      GeminiReportField.Max_Bid,
      GeminiReportField.Average_CPC,
      GeminiReportField.Average_CPM,
      GeminiReportField.CTR
    ),
    GeminiReportType.search_stats -> List(
      GeminiReportField.Advertiser_ID,
      GeminiReportField.Campaign_ID,
      GeminiReportField.Ad_Group_ID,
      GeminiReportField.Ad_ID,
      GeminiReportField.Keyword_ID,
      GeminiReportField.Delivered_Match_Type,
      GeminiReportField.Search_Term,
      GeminiReportField.Device,
      GeminiReportField.Destination_URL,
      GeminiReportField.Day,
      GeminiReportField.Impressions,
      GeminiReportField.Clicks,
      GeminiReportField.Spend,
      GeminiReportField.Conversions,
      GeminiReportField.Post_Click_Conversions,
      GeminiReportField.Post_Impression_Conversions,
      GeminiReportField.Average_Position,
      GeminiReportField.Max_Bid,
      GeminiReportField.Average_CPC,
      GeminiReportField.CTR
    ),
    GeminiReportType.ad_extension_details -> List(
      GeminiReportField.Advertiser_ID,
      GeminiReportField.Campaign_ID,
      GeminiReportField.Ad_Group_ID,
      GeminiReportField.Ad_ID,
      GeminiReportField.Keyword_ID,
      GeminiReportField.Ad_Extn_ID,
      GeminiReportField.Device,
      GeminiReportField.Month,
      GeminiReportField.Week,
      GeminiReportField.Day,
      GeminiReportField.Pricing_Type,
      GeminiReportField.Destination_URL,
      GeminiReportField.Impressions,
      GeminiReportField.Clicks,
      GeminiReportField.Conversions,
      GeminiReportField.Spend,
      GeminiReportField.Average_CPC,
      GeminiReportField.CTR
    ),
    GeminiReportType.audience -> List(
      GeminiReportField.Advertiser_ID,
      GeminiReportField.Campaign_ID,
      GeminiReportField.Ad_Group_ID,
      GeminiReportField.Ad_ID,
      GeminiReportField.Audience_Type,
      GeminiReportField.Audience_ID,
      GeminiReportField.Day,
      GeminiReportField.Pricing_Type,
      GeminiReportField.Source,
      GeminiReportField.Impressions,
      GeminiReportField.Clicks,
      GeminiReportField.Post_Click_Conversions,
      GeminiReportField.Post_Impression_Conversions,
      GeminiReportField.Conversions,
      GeminiReportField.Total_Conversions,
      GeminiReportField.Spend
    ),
    GeminiReportType.conversion_rules_stats -> List(
      GeminiReportField.Advertiser_ID,
      GeminiReportField.Campaign_ID,
      GeminiReportField.Ad_Group_ID,
      GeminiReportField.Rule_ID,
      GeminiReportField.Rule_Name,
      GeminiReportField.Category_Name,
      GeminiReportField.Conversion_Device,
      GeminiReportField.Keyword_ID,
      GeminiReportField.Keyword_Value,
      GeminiReportField.Source,
      GeminiReportField.Price_Type,
      GeminiReportField.Day,
      GeminiReportField.Post_View_Conversions,
      GeminiReportField.Post_Click_Conversions,
      GeminiReportField.Conversion_Value,
      GeminiReportField.Conversions
    )
  )

  // yahoo gemini report job statuses
  object GeminiReportJobStatus extends Enumeration {
    type GeminiReportJobStatus = Value
    val submitted, running, failed, completed, killed = Value
  }

  // defines yahoo gemini report filter obj
  case class GeminiReportFilter(
    field: GeminiReportField.Value,
    operator: GeminiReportFilterOperation.Value,
    value: Option[String] = None,
    values: Option[List[String]] = None,
    from: Option[String] = None,
    to: Option[String] = None
  )

  // yahoo gemini report filter obj -> mongodb obj
  def geminiReportFilterToDBObject(grf: GeminiReportFilter) = DBObject(
    "field" -> grf.field.toString,
    "operator" -> grf.operator.toString,
    "value" -> grf.value.getOrElse(""),
    "values" -> (grf.values match {case Some(v) => MongoDBList(v: _*) case _ => MongoDBList()}),
    "from" -> grf.from.getOrElse(""),
    "to" -> grf.to.getOrElse("")
  )

  // mongodb object -> yahoo gemini report filter obj
  def dboToGeminiReportFilter(dbo: DBObject) = GeminiReportFilter(
    field = GeminiReportField.withName(dbo.as[String]("field")),
    operator = GeminiReportFilterOperation.withName(dbo.as[String]("operator")),
    value = if(dbo.as[String]("value") == "") None else Some(dbo.as[String]("value")),
    values = if(dbo.as[MongoDBList]("values").isEmpty) None else Some(dbo.as[MongoDBList]("values").asInstanceOf[List[String]]),
    from = if(dbo.as[String]("from") == "") None else Some(dbo.as[String]("from")),
    to = if(dbo.as[String]("to") == "") None else Some(dbo.as[String]("to"))
  )

  // defines yahoo gemini report request obj
  case class GeminiReportRequest (
    reportType: GeminiReportType.Value,
    fields: List[GeminiReportField.Value],
    var filters: Option[List[GeminiReportFilter]],
    oAuthData: models.mongodb.yahoo.Yahoo.ApiAccount,
    reportFormat: ReportFormat.Value=ReportFormat.CSV
  )

  lazy val GeminiReportDateFormat = new SimpleDateFormat("yyyy-MM-dd")

  lazy val ChartDateDimension = List(
    "day",
    "week",
    "month",
    "year"
  )

  // defines uber report request obj
  case class UberReportRequest(
    name: String,
    format: String,
    preset_id: Option[String],
    report_id: Option[String],
    rowParser: (UberReportRequest, DBObject, LoggingAdapter) => DBObject,
    importer: (UberReportRequest, List[DBObject], LoggingAdapter) => Unit
  )

  case class PendingCacheStructure (
    var id: Long,
    var changeCategory: ChangeCategory,
    var trafficSource: TrafficSource,
    var changeType: ChangeType,
    var changeData: DBObject
  )

  // pending cache structure obj -> mongodb obj
  def pendingCacheStructureToDbo(pcs: PendingCacheStructure): DBObject = {
    DBObject(
      "id" -> pcs.id,
      "changeCategory" -> pcs.changeCategory.toString,
      "trafficSource" -> pcs.trafficSource.toString,
      "changeType" -> pcs.changeType.toString,
      "changeData" -> pcs.changeData
    )
  }

  // mongodb obj -> pending cache structure
  def dboToPendingCacheStructure(dbo: DBObject): PendingCacheStructure = {
    PendingCacheStructure(
      id=dbo.getAs[Long]("id").get,
      changeCategory=ChangeCategory.withName(dbo.getAs[String]("changeCategory").get),
      trafficSource=TrafficSource.withName(dbo.getAs[String]("trafficSource").get),
      changeType=ChangeType.withName(dbo.getAs[String]("changeType").get),
      changeData=dbo.getAs[DBObject]("changeData").get
    )
  }

  case class TaskStructure (
    var id: Long,
    var data: List[PendingCacheStructure],
    var startTime: DateTime,
    var complete: Boolean,
    var completeTime: Option[DateTime],
    var processes: List[Process]
  )

  // task structure obj -> mongodb obj
  def taskStructureToDbo(ts: TaskStructure) = DBObject(
    "id" -> ts.id,
    "data" -> ts.data.map(pendingCacheStructureToDbo),
    "startTime" -> ts.startTime.getMillis,
    "complete" -> ts.complete,
    "completeTime" -> (if(ts.completeTime.isEmpty) None else ts.completeTime.get.getMillis),
    "processes" -> ts.processes.map(processToDbo)
  )

  // mongodb obj -> task structure oboj
  def dboToTaskStructure(dbo: DBObject) = TaskStructure(
    id=dbo.as[Long]("id"),
    data=dbo.getAsOrElse[List[DBObject]]("data", List()).map(dboToPendingCacheStructure),
    startTime=new DateTime(dbo.as[Long]("startTime")),
    complete=dbo.getAsOrElse[Boolean]("complete", false),
    completeTime=Some(new DateTime(dbo.getAsOrElse[Option[Long]]("completeTime", None).getOrElse(0L))),
    processes=dbo.getAsOrElse[List[DBObject]]("processes", List()).map(dboToProcess)
  )

  case class Process (
    changeDataId: Long,
    var subProcesses: Int,
    var completedSubProcesses: Int
  )

  // process obj -> mongodb obj
  def processToDbo(p: Process): DBObject = {
    DBObject(
      "changeDataId" -> p.changeDataId,
      "subProcesses" -> p.subProcesses,
      "completedSubProcesses" -> p.completedSubProcesses
    )
  }

  // mongodb obj -> process obj
  def dboToProcess(dbo: DBObject) = Process(
    changeDataId = dbo.getAs[Long]("changeDataId").get,
    subProcesses=dbo.getAsOrElse[Int]("subProcesses", 0),
    completedSubProcesses = dbo.getAsOrElse[Int]("completedSubProcesses", 0)
  )

  case class AlertStructure (
    id: Long,
    var startTime: DateTime,
    var progress: Int,
    var complete: Boolean,
    var completeTime: Option[DateTime]
  )

  // alert structure obj -> mongodb obj
  def alertStructureToDbo(as: AlertStructure) = DBObject(
    "id" -> as.id,
    "startTime" -> as.startTime,
    "progress" -> as.progress,
    "complete" -> as.complete,
    "completeTime" -> as.completeTime
  )

  // mongodb obj -> alert structure obj
  def dboToAlertStructure(dbo: DBObject) = AlertStructure(
    id=dbo.as[Long]("id"),
    startTime=dbo.as[DateTime]("startTime"),
    progress=dbo.getAsOrElse[Int]("progress", 0),
    complete=dbo.getAsOrElse[Boolean]("complete", false),
    completeTime=Some(dbo.getAsOrElse[DateTime]("completeTime", new DateTime))
  )

  case class PendingCacheMessage(
    cache: Option[PendingCacheStructure],
    request: Option[Request[AnyContent]]
  )

  // Akka Async system definitions for reporting and sync processes
  // --------------------------------------------------
  val miscActorSystem = ActorSystem("MiscActorSystem")
  val userActorSystem = ActorSystem("UserActorSystem")
  val googleReportingActorSystem = ActorSystem("GoogleReportingActorSystem")
  val googleManagementActorSystem = ActorSystem("GoogleManagementActorSystem")

  val yahooReportingActorSystem = ActorSystem("YahooReportingActorSystem")
  val yahooManagementActorSystem = ActorSystem("YahooManagementActorSystem")

  val nanigansReportingActorSystem = ActorSystem("NanigansReportingActorSystem")

  val msnReportingActorSystem = ActorSystem("MsnReportingActorSystem")
  val msnManagementActorSystem = ActorSystem("MsnManagementActorSystem")

  val uberReportingActorSystem = ActorSystem("UberReportingActorSystem")

  // ----------------------------------------------------

  // Global Web Service Client
  // todo: remove

  lazy val ws = WS.client

  // Akka actor definitions for reporting and sync processes
  // --------------------------------------------------
  lazy val msnReportActor = msnReportingActorSystem.actorOf(Props(new MsnReportActor))
  lazy val googleReportActor = googleReportingActorSystem.actorOf(Props(new GoogleReportActor))
  lazy val yahooReportActor = yahooReportingActorSystem.actorOf(Props(new YahooReportActor))
  lazy val nanigansReportActor = nanigansReportingActorSystem.actorOf(Props(new NanigansReportActor))
  lazy val uberReportActor = uberReportingActorSystem.actorOf(Props(new UberReportActor))
  lazy val googleManagementActor = googleManagementActorSystem.actorOf(Props(new MccActor))

  // Misc actor definitions
  lazy val exportKeywordPerformanceReportCsvActor = miscActorSystem.actorOf(Props(new ExportKeywordPerformanceReportCsvActor))
  lazy val exportKeywordPerformanceReportRequestCsvActor = miscActorSystem.actorOf(Props(new ExportKeywordPerformanceReportRequestCsvActor))
  lazy val exportKeywordStatsCsvActor = miscActorSystem.actorOf(Props(new ExportKeywordStatsCsvActor))

  // --------------------------------------------------


  // helper to mark a subprocess complete
  def complete_subprocess(task_key: String, cache: PendingCacheStructure, increment: Int=1): Unit = {
    val tasks = taskCache(Right(task_key))
    tasks.foreach(
      x =>
        x.processes.foreach(
          y =>
            if (y.changeDataId == cache.id)
              y.completedSubProcesses += increment
        )
    )
    setTaskCache(Right(task_key), tasks)
  }

  // helper to set number of subprocesses assigned to process
  def set_subprocess_count(task_key: String, cache: PendingCacheStructure, subprocess_count: Int=1): Unit = {
    setTaskCache(
      Right(task_key),
      taskCache(
        Right(task_key)
      ).map { x =>
        x.processes.foreach(y => y.changeDataId match {
          case _ if y.changeDataId == cache.id => y.subProcesses = subprocess_count
        })
        x
      }
    )
  }

  def mongoToCsv(dboList: List[DBObject]): String = {
    dboList.zipWithIndex.map{ case (row, idx) =>
      (if(idx == 0) {
        row.keys
      } else {
        row.values
      }).mkString(",")
    }.mkString("\n")
  }

  // Misc Daemon for utility scripts
  def miscDaemon(initial_delay: FiniteDuration, interval: FiniteDuration) = {
    List(exportKeywordPerformanceReportCsvActor, exportKeywordPerformanceReportRequestCsvActor, exportKeywordStatsCsvActor).foreach{ actor =>
      miscActorSystem.scheduler.schedule(initial_delay, interval, actor, (None, None))
    }
  }

  // Daemon for downloading uber reporting Data
  def uberReportingDaemon(initial_delay: FiniteDuration, interval: FiniteDuration) = {
    uber_mapping.map { reportRequest =>
      Logger.info("Scheduling reporting daemon %s (%s)".format(reportRequest.name, reportRequest.preset_id))
      uberReportingActorSystem.scheduler.schedule(
        initial_delay,
        interval,
        uberReportActor,
        reportRequest
      )
    }
  }

  // Daemon for downloading google reporting Data
  def googleReportingDaemon(
    reportType: com.google.api.ads.adwords.lib.jaxb.v201509.ReportDefinitionReportType,
    fields: Option[List[String]],
    enableZeroImpressions: Boolean,
    initial_delay: FiniteDuration,
    interval: FiniteDuration
   ) = {
    Logger.info("Scheduling google reporting daemon (%s)".format(reportType.value))
    googleMccCollection.find().toList.map { mccObj =>
      val mcc = dboToMcc(mccObj)
      googleCustomerCollection.find(
        DBObject("mccObjId" -> mcc._id),
        DBObject("customer" -> DBObject("$slice" -> -1))
      ).toList.map(acc =>
        dboToGoogleEntity[Customer](acc, "customer", None)
      ).map(account =>
        googleReportingActorSystem.scheduler.schedule(
          initial_delay,
          interval,
          googleReportActor,
          GoogleReportRequest(
            reportType = reportType,
            fields = fields,
            predicates = None,
            dateRange = com.google.api.ads.adwords.lib.jaxb.v201509.ReportDefinitionDateRangeType.YESTERDAY,
            oAuthData = mcc,
            customerId = Some(account.getCustomerId.toString),
            enableZeroImpressions = enableZeroImpressions
          )
        )
      )
    }
  }

  def msnReportingDaemon(
    reportType: MsnReportType.Value,
    initial_delay: FiniteDuration,
    interval: FiniteDuration
  ) = {
    Logger.info("Scheduling msn reporting daemon (%s)".format(reportType))
    msnApiAccountCollection.find().toList.foreach { apiAccountObj =>
      val apiAccount = models.mongodb.msn.Msn.dboToApiAccount(apiAccountObj)
      msnAccountInfoCollection.find(DBObject("apiAccountObjId" -> apiAccount._id), DBObject("account" -> DBObject("$slice" -> -1)))
        .toList.foreach { accountInfoObj =>
        val account = dboToMsnEntity[Account](accountInfoObj, "account", Some("accountObj"))
        val accountInfoWithCustomerData = dboToMsnEntity[AccountInfoWithCustomerData](accountInfoObj, "account", Some("accountInfoWithCustomerData"))
        try {
          msnReportingActorSystem.scheduler.schedule(
            initial_delay,
            interval,
            msnReportActor,
            MsnReportRequest(
              apiAccount,
              accountInfoWithCustomerData.getCustomerId,
              account.getId,
              MsnReportType.KeywordPerformanceReportRequest,
              startDate = DateTime.now.minusDays(1),
              endDate = DateTime.now
            )
          )
        } catch {
          case e: Exception =>
            e.printStackTrace()
            Logger.info("Error Running report for %s - %s".format(apiAccount.name, e.toString))
        }
      }
    }
  }

  //Daemon for downloading google reporting Data
  def yahooReportingDaemon(
    reportType: Shared.GeminiReportType.Value,
    fields: List[Shared.GeminiReportField.Value],
    initial_delay: FiniteDuration,
    interval: FiniteDuration
  ) = {
    Logger.info("Scheduling yahoo reporting daemon (%s)".format(reportType))
    yahooApiAccountCollection.find().toList.map { apiAccountObj =>
      val apiAccount = models.mongodb.yahoo.Yahoo.dboToApiAccount(apiAccountObj)
      val yesterday = Calendar.getInstance
      yesterday.add(Calendar.DATE, -1)
      val today = Calendar.getInstance

      yahooAdvertiserCollection.find(
        DBObject("apiAccountObjId" -> apiAccount._id.get),
        DBObject("advertiser" -> DBObject("$slice" -> -1))
      ).toList.map(adv =>
        yahooReportingActorSystem.scheduler.schedule(
          initial_delay,
          interval,
          yahooReportActor,
          GeminiReportRequest(
            reportType = reportType,
            fields = fields,
            filters = Some(List(GeminiReportFilter(
              field = GeminiReportField.Day,
              operator = GeminiReportFilterOperation.between,
              value = None,
              values = None,
              from = Some(GeminiReportDateFormat.format(yesterday.getTime)),
              to = Some(GeminiReportDateFormat.format(today.getTime))
            ))),
            oAuthData = apiAccount,
            reportFormat = ReportFormat.CSV
          )
        )
      )
    }
  }


  //Daemon for downloading Facebook (nanigans) reporting Data
  def nanigansReportingDaemon(
    reportType: Shared.NanigansReportType.Value,
    fields: Option[List[String]],
    initial_delay: FiniteDuration,
    interval: FiniteDuration
  ) = {
    Logger.info("Scheduling yahoo reporting daemon (%s)".format(reportType))
    nanigansApiAccountCollection.find().toList.map { apiAccountObj =>
      val apiAccount = models.mongodb.facebook.Facebook.DBObjectToNanigansApiAccount(apiAccountObj)
      nanigansReportingActorSystem.scheduler.schedule(
        initial_delay,
        interval,
        googleReportActor,
        NanigansReportRequest(
          reportType = reportType,
          fields = nanigansAdFields,
          metrics = nanigansAdMetrics,
          startDate = "",
          attribution = Attribution.click,
          downloadFormat = ReportFormat.CSV,
          apiAccount
        )
      )
    }
  }

  //Daemon for downloading google reporting Data
  def googleCustomerDaemon(
    reportType: com.google.api.ads.adwords.lib.jaxb.v201509.ReportDefinitionReportType,
    fields: Option[List[String]],
    enableZeroImpressions: Boolean,
    initial_delay: FiniteDuration,
    interval: FiniteDuration
  ) = {
    Logger.info("Scheduling reporting daemon (%s)".format(reportType.value))
    googleMccCollection.find().toList.map { mccObj =>
      val mcc = dboToMcc(mccObj)
      googleCustomerCollection.find(
        DBObject("mccObjId" -> mcc._id),
        DBObject("customer" -> DBObject("$slice" -> -1))
      ).toList.map(dboToGoogleEntity[Customer](_, "customer", None)).map(account =>
        googleReportingActorSystem.scheduler.schedule(
          initial_delay,
          interval,
          googleReportActor,
          GoogleReportRequest(
            reportType = reportType,
            fields = fields,
            predicates = None,
            dateRange = com.google.api.ads.adwords.lib.jaxb.v201509.ReportDefinitionDateRangeType.YESTERDAY,
            oAuthData = mcc,
            customerId = Some(account.getCustomerId.toString),
            enableZeroImpressions = enableZeroImpressions
          )
        )
      )
    }
  }

  // check if object is a case class
  def isCaseClass(obj: Any): Boolean = {
    val typeMirror = runtimeMirror(obj.getClass.getClassLoader)
    val instanceMirror = typeMirror.reflect(obj)
    val symbol = instanceMirror.symbol
    symbol.isCaseClass
  }

  def objectIdToFormString(objId: Option[ObjectId]) = objId match {case Some(id) => Some(id.toString) case None => None}

  def formStringToObjectId(objIdStr: Option[String]) = objIdStr match {case Some(id) => Some(new ObjectId(id)) case None => None}

  //todo: should be refactored for less redundancy should replace the individual *ToDbo methods
  def caseClassToDbo(c: Any): DBObject = c match {
    case x if isCaseClass(x) =>
      var dbo = DBObject.newBuilder
      val typeMirror = runtimeMirror(c.getClass.getClassLoader)
      val instanceMirror = typeMirror.reflect(c)
      for((name, idx) <- Utilities.getCaseClassParameter[instanceMirror.type].zipWithIndex) {
        dbo += (Utilities.getMethodName(name) -> (
          if(isCaseClass(c))
            caseClassToDbo(c.asInstanceOf[Product].productElement(idx))
          else
            c.asInstanceOf[Product].productElement(idx)
        ))
      }
      dbo.result
    case _ => throw new RuntimeException("Invalid Object passed to caseClassToDbo")
  }

  //Daemon for downloading Google MCC data (Campaign, Adgroup, Ad, Criterion...etc)
  def googleManagementDaemon(
    initial_delay: FiniteDuration,
    interval: FiniteDuration

  ) = {
      Logger.info("Scheduling MCC daemon (%s)")
      googleManagementActorSystem.scheduler.schedule(initial_delay, interval, googleManagementActor, None)
  }

  // get saved task key
  def taskKey(arg: Either[Request[Any], String]): String = {
    if(arg.isLeft)
      arg.left.get.session.get(Security.username).get + task_ext
    else
      arg.right.get + task_ext
  }

  // get pending cache key
  def pendingCacheKey(arg: Either[Request[Any], String]): String = {
    if(arg.isLeft)
      arg.left.get.session.get(Security.username).get + cache_ext
    else
      arg.right.get + cache_ext
  }

  // retrieve pending cache
  def pendingCache(arg: Either[Request[Any], String]): List[PendingCacheStructure] = {
    Cache.get(
      pendingCacheKey(
        arg.isLeft match {
          case true => Left(arg.left.get)
          case _ => Right(arg.right.get)
        }
      )
    ).getOrElse(List()).asInstanceOf[List[PendingCacheStructure]]
  }

  // retrieve saved task cache
  def taskCache(arg: Either[Request[Any], String]): List[TaskStructure] = {
    Cache.get(
      taskKey(
        arg.isLeft match {
          case true => Left(arg.left.get)
          case _ => Right(arg.right.get)
        }
      )
    ).getOrElse(
        List[TaskStructure]()
      ).asInstanceOf[List[TaskStructure]]
  }

  // set saved cache
  def setTaskCache(arg: Either[Request[Any], String], task: List[TaskStructure]) = {
    Cache.set(
      taskKey(
        arg.isLeft match {
          case true => Left(arg.left.get)
          case _ => Right(arg.right.get)
        }
      ),
      task
    )
  }

  // set pending cache
  def setPendingCache(arg: Either[Request[Any], String], pending_cache: List[PendingCacheStructure]) = {
    Cache.set(
      pendingCacheKey(
        arg.isLeft match {
          case true => Left(arg.left.get)
          case _ => Right(arg.right.get)
        }
      ),
      pending_cache
    )
  }

  def titleCase(s: String): String = {
    s(0).toUpper + s.substring(1).toLowerCase
  }

  def doubleToMoney(v: Double): Double = {
    Math.round(v*100.0)/100.0
  }

  def stringToDateTime(v: Option[String], dateFormat: String="yyyy-MM-dd", defaultValue: String="2000-01-01"): DateTime = {
    DateTimeFormat.forPattern(dateFormat).parseDateTime(v.getOrElse(defaultValue))
  }

  // pull together dashboard chart data
  def dashboardChartData: List[JsObject] = {
    searchAutoOverallChartData ++
      pricingByClassificationAutoFormVolumeLeadValueChartData
  }

  def pricingByClassificationAutoFormVolumeLeadValueChartData: List[JsObject] = {
    val previousMonth = DateTimeFormat.forPattern("yyyy-MM-dd").print(DateTime.now.minusDays(31))
    ChartDateDimension.filter(_ != "year").map { dateDimension =>
      val pricing_by_classification_data = uberReportCollection("pricing_by_classification", dateDimension)
        .find().toList.filter(_.getAsOrElse[String]("classification", "") != "Email")

      val pricing_by_classification_data_grouped = pricing_by_classification_data.groupBy(x => x.getAsOrElse[String]("classification", ""))

      val form_volume_values = pricing_by_classification_data.flatMap(x => List(x.getAsOrElse[Int]("forms", 0)))

      val maxValue1 = if(form_volume_values.isEmpty) 0 else form_volume_values.max
      val minValue1 = if(form_volume_values.isEmpty) 0 else form_volume_values.min

      val lead_values = pricing_by_classification_data.flatMap(x => List(x.getAsOrElse[Double]("lead-form", 0.0))
      )

      val maxValue2 = if (lead_values.isEmpty) 0.0 else lead_values.max
      val minValue2 = if (lead_values.isEmpty) 0.0 else lead_values.min

      var series_options = Json.obj()

      pricing_by_classification_data_grouped
        .zipWithIndex
        .foreach{ x =>
          series_options += ((pricing_by_classification_data_grouped.size + x._2).toString -> Json.obj("targetAxisIndex" -> 1, "type" -> "line"))
        }

      Json.obj(
        "name" -> "chart",
        "options" -> Json.obj(
          "title" -> "Pricing By Classification - Auto Form Volume & Lead Value - %s".format(titleCase(dateDimension)),
          "pointSize" -> "14",
          "dataOpacity" -> "0.3",
          "legend" -> Json.obj("position" -> "top", "alignment" -> "start", "maxLines" -> "5", "width" -> "1030"),
          "width" -> "1030",
          "height" -> "500",
          "vAxes" -> Json.arr(
            Json.obj(
              "title" -> "Forms",
              "gridlines" -> Json.obj(
                "color" -> "black"
              ),
              "viewWindowMode" -> "explicit",
              "viewWindow" -> Json.obj(
                "max" -> maxValue1,
                "min" -> minValue1
              )
            ),
            Json.obj(
              "title" -> "Lead dollars per form",
              "format" -> "$#,###",
              "gridlines" -> Json.obj(
                "color" -> "transparent"
              ),
              "viewWindowMode" -> "explicit",
              "viewWindow" -> Json.obj(
                "max" -> maxValue2,
                "min" -> minValue2
              )
            )
          ),
          "hAxis" -> Json.obj("title" -> dateDimension),
          "seriesType" -> "bars",
          "series" -> series_options
        ),
        "data" -> (
          List(JsArray(
            List(JsString(dateDimension)) ++
            pricing_by_classification_data_grouped.keys.map(pbc => JsString("%s Forms".format(pbc))) ++
            pricing_by_classification_data_grouped.keys.map(pbc => JsString("%s $L/Form".format(pbc)))
          )) ::: pricing_by_classification_data.groupBy(_.getAsOrElse[Long](dateDimension, 0L)).toList.map{ data =>
            JsArray(
              List(JsString(new DateTime(data._1).toString("yyyy-MM-dd"))) ++
              data._2.map(x => JsNumber(x.getAsOrElse[Int]("forms", 0))) ++
              data._2.map(x => JsNumber(x.getAsOrElse[Int]("lead-form", 0)))
            )
          }
        )
      )
    }
  }

  def searchAutoOverallChartData: List[JsObject] = {
    val previousMonth = DateTimeFormat.forPattern("yyyy-MM-dd").print(DateTime.now.minusDays(31))
    ChartDateDimension.filter(_ != "year").map { dateDimension =>
      val search_auto_overall_data = uberReportCollection("search_auto_overall", dateDimension).find().toList

      val total_values = search_auto_overall_data.flatMap(x =>
        List(
          x.getAsOrElse[Double]("lk_total_profit", 0.0),
          x.getAsOrElse[Double]("tv_total_profit", 0.0),
          x.getAsOrElse[Double]("lk_total_revenue", 0.0),
          x.getAsOrElse[Double]("tv_total_revenue", 0.0)
        )
      )

      val maxValue1 = if (total_values.isEmpty) 0.0 else total_values.max
      val minValue1 = if (total_values.isEmpty) 0.0 else total_values.min

      val lead_values = search_auto_overall_data.flatMap(x =>
        List(
          x.getAsOrElse[Int]("lk_leads", 0),
          x.getAsOrElse[Int]("tv_leads", 0)
        )
      )

      val maxValue2 = if (lead_values.isEmpty) 0 else lead_values.max
      val minValue2 = if(lead_values.isEmpty) 0 else lead_values.min

      Json.obj(
        "name" -> "chart",
        "options" -> Json.obj(
          "title" -> "Search Auto Overall - %s".format(titleCase(dateDimension)),
          "pointSize" -> "14",
          "dataOpacity" -> "0.3",
          "legend" -> Json.obj("position" -> "top", "maxLines" -> "5", "width" -> "1030"),
          "width" -> "1030",
          "height" -> "500",
          "vAxes" -> Json.arr(
            Json.obj(
              "title" -> "Totals",
              "format" -> "$#,###",
              "gridlines" -> Json.obj(
                "color" -> "black"
              ),
              "viewWindowMode" -> "explicit",
              "viewWindow" -> Json.obj(
                "max" -> maxValue1,
                "min" -> minValue1
              )
            ),
            Json.obj(
              "title" -> "Leads",
              "gridlines" -> Json.obj(
                "color" -> "transparent"
              ),
              "viewWindowMode" -> "explicit",
              "viewWindow" -> Json.obj(
                "max" -> maxValue2,
                "min" -> minValue2
              )
            )
          ),
          "hAxis" -> Json.obj("title" -> dateDimension),
          "seriesType" -> "bars",
          "series" -> Json.obj(
            "4" -> Json.obj("targetAxisIndex" -> 1, "type" -> "line"),
            "5" -> Json.obj("targetAxisIndex" -> 1, "type" -> "line")
          )
        ),
        "data" -> (
          List(Json.arr(
            dateDimension,
            "LK Total Profit",
            "TV Total Profit",
            "LK Total Revenue",
            "TV Total Revenue",
            "LK Leads",
            "TV Leads"
          )) ::: search_auto_overall_data.map(data =>
          Json.arr(
            data.getAs[String](dateDimension).get,
            Math.round(data.getAsOrElse[Double]("lk_total_profit", 0.0)*100.0)/100.0,
            Math.round(data.getAsOrElse[Double]("tv_total_profit", 0.0)*100.0)/100.0,
            Math.round(data.getAsOrElse[Double]("lk_total_revenue", 0.0)*100.0)/100.0,
            Math.round(data.getAsOrElse[Double]("tv_total_revenue", 0.0)*100.0)/100.0,
            data.getAsOrElse[Int]("lk_leads", 0),
            data.getAsOrElse[Int]("tv_leads", 0)
          ))
        )
      )
    }
  }
}