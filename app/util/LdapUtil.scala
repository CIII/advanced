package util

import Shared.Shared._
import com.mongodb.casbah.Imports._
import com.unboundid.ldap.sdk.{Filter, LDAPConnection, SearchRequest, SearchScope}
import models.mongodb.UserAccount

case class UserNotFoundException(error: String)  extends Exception(error)

object LdapUtil {
  def connect(username: String, password: String): LDAPConnection = new LDAPConnection("10.50.20.4", 389, "svc_uberauth", "4*s9!dlwoX")

  def authenticate(username: String, password: String): Option[UserAccount] = {
    val conn = connect(username, password)

    val filter = Filter.create("sAMAccountName=" + username)
    val search_request = new SearchRequest("OU=People,DC=nq,DC=corp", SearchScope.SUB, filter)
    val search_results = conn.search(search_request).getSearchEntries
    if (search_results.size() <= 0) {
      return None
    }
    val entry_dn = search_results.get(0).getDN
    new LDAPConnection("10.60.20.186", 389).bind(entry_dn, password)
    UserAccount.userAccountCollection.findOne(DBObject("userName" -> username)) match {
      case Some(userAccount) =>
        Some(UserAccount.dboToUserAccount(userAccount))
      case None =>
        val userAccount = UserAccount(
          Some(new ObjectId),
          username,
          search_results.get(0).getAttribute("mail").getValue,
          Array(username match {
            case "williamsc" | "ellen" | "ismand" =>
              adminUserID
            case _ =>
              normalUserID
          })
        )
        UserAccount.userAccountCollection.insert(UserAccount.userAccountToDbo(userAccount))
        Some(userAccount)
    }
  }

  def toOption[T](value: T): Option[T] = Option(value)

}
