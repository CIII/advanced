package sync.google.process.reporting

import Shared.Shared.AdWordsReportRequest
import akka.actor.Actor
import akka.event.Logging
import models.mongodb.Utilities._
import models.mongodb.google.collections._
import play.api.mvc.Controller
import play.modules.reactivemongo.MongoController
import sync.google.adwords.AdWordsHelper
import sync.google.adwords.account.AccountHelper
import scala.async.Async.async

import scala.concurrent.ExecutionContext.Implicits.global

class ReportActor extends Actor with Controller with MongoController {
  val log = Logging(context.system, this)

  def receive = {
    case msg =>
      async {
        val reportRequest = msg.asInstanceOf[AdWordsReportRequest]

        log.info("Processing %s -> Date Range: %s -> Download Format: %s".format(
          reportRequest.reportType.value,
          reportRequest.dateRange.value,
          reportRequest.downloadFormat.value
        ))

        val adWordsHelper = new AdWordsHelper(
          clientId = reportRequest.oAuthData.oAuthClientId,
          clientSecret = reportRequest.oAuthData.oAuthClientSecret,
          refreshToken = reportRequest.oAuthData.oAuthRefreshToken,
          developerToken = reportRequest.oAuthData.DeveloperToken,
          customerId = reportRequest.customerId,
          enablePartialFailure = false
        )

        val accountHelper = new AccountHelper(adWordsHelper)

        accountHelper.downloadReport(
          reportRequest.reportType,
          reportRequest.fields,
          reportRequest.predicates,
          reportRequest.dateRange,
          reportRequest.downloadFormat,
          reportRequest.enableZeroImpressions
        ) match {
          case None =>
          case Some(report_csv) =>
            for (item <- csvToJson(
              report_csv
                .split("\n")
                .drop(1)
                .dropRight(1)
                .mkString("\n")
            )) {
              reportCollection(reportRequest.reportType).insert(item)
            }
        }
      }
      Thread.sleep(5000)
  }
}
