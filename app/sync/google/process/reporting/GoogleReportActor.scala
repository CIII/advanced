package sync.google.process.reporting

import Shared.Shared.GoogleReportRequest
import akka.actor.Actor
import akka.event.Logging
import com.mongodb.DBObject
import models.mongodb.Utilities._
import models.mongodb.google.Google._
import sync.google.adwords.AdWordsHelper
import sync.google.adwords.account.CustomerHelper
import util.MongoJson

class GoogleReportActor extends Actor {
  val log = Logging(context.system, this)

  def receive = {
    case reportRequest: GoogleReportRequest =>
      try {
        log.info("Processing %s -> Date Range: %s -> Download Format: %s".format(
          reportRequest.reportType.value,
          reportRequest.dateRange.value,
          reportRequest.downloadFormat.value
        ))

        val adWordsHelper = new AdWordsHelper(
          clientId = reportRequest.oAuthData.oAuthClientId,
          clientSecret = reportRequest.oAuthData.oAuthClientSecret,
          refreshToken = reportRequest.oAuthData.oAuthRefreshToken,
          developerToken = reportRequest.oAuthData.developerToken,
          customerId = reportRequest.customerId,
          enablePartialFailure = false
        )

        val accountHelper = new CustomerHelper(adWordsHelper, log)

        accountHelper.downloadReport(
          reportRequest.reportType,
          reportRequest.fields,
          reportRequest.predicates,
          reportRequest.dateRange,
          reportRequest.downloadFormat,
          reportRequest.enableZeroImpressions
        ) match {
          case None =>
          case Some(report_csv) =>
            for (item <- csvToJson(
              report_csv
                .split("\n")
                .drop(1)
                .dropRight(1)
                .mkString("\n")
            )) {
              var sanitized_item = item
              sanitized_item.keys.foreach{ k =>
                if(k.contains(".")) {
                  val value = sanitized_item.\(k).get
                  sanitized_item = sanitized_item.-(k)
                  sanitized_item.+(k.replace(".", "") -> value)
                }
              }
              googleReportCollection(reportRequest.reportType).insert(MongoJson.fromJson(sanitized_item))
            }
        }
      } catch {
        case e: Exception =>
          e.printStackTrace()
          log.info("Error Processing Report (%s) - %s".format(
            reportRequest.reportType.value,
            e.toString
          ))
      }
  }
}
