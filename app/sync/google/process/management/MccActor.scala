package sync.google.process.management

import Shared.Shared._
import akka.actor.Actor
import akka.event.Logging
import com.google.api.ads.adwords.axis.v201409.cm.{Predicate, PredicateOperator}
import models.mongodb.google.collections._
import play.api.libs.json.{JsObject, Json}
import play.api.mvc._
import play.modules.reactivemongo.MongoController
import reactivemongo.bson.BSONObjectID
import sync.google.adwords.AdWordsHelper
import sync.google.adwords.account._
import sync.shared.Google._

import scala.async.Async.async
import scala.concurrent.ExecutionContext.Implicits.global

class MccActor(request: Request[AnyContent]) extends Actor with Controller with MongoController {
  val log = Logging(context.system, this)

  def receive = {
    case msg =>
      val cache = msg.asInstanceOf[pending_cache_structure]
      val filter_object_id = Json.obj("_id" -> 1)
      val mcc_data = cache.change_data.asInstanceOf[controllers.google.mcc.MccController.MccForm]
      log.info("Processing %s -> %s -> %s -> %s".format(
        cache.change_category,
        cache.change_type,
        cache.traffic_source,
        cache.id
      ))
      val adWordsHelper = new AdWordsHelper(
        clientId = mcc_data.oauth_client_id,
        clientSecret = mcc_data.oauth_client_secret,
        refreshToken = mcc_data.oauth_refresh_token,
        developerToken = mcc_data.developer_token,
        customerId = None
      )
      val accountHelper = new AccountHelper(adWordsHelper)
      val accounts = accountHelper.getAccounts(managedCustomerFields, None)

      set_subprocess_count(taskKey(request), cache, accounts.length + 1)
      complete_subprocess(taskKey(request), cache)

      val mccObjectID = BSONObjectID.generate
      val accountObjectID = BSONObjectID.generate

      mccCollection.insert(
        Json.obj("_id" -> mccObjectID.toString) ++
          Json.parse(
            gson.toJson(
              models.mongodb.google.Mcc(
                mcc_data.name,
                mcc_data.developer_token,
                mcc_data.oauth_client_id,
                mcc_data.oauth_client_secret,
                mcc_data.oauth_refresh_token
              )
            )
          ).as[JsObject]
      )

      log.info("Accounts to iterate over: " + accounts.map(_.getName).mkString(", "))
      for (acc <- accounts) {
        async {
          val json = gson.toJsonTree(acc)
          json.getAsJsonObject.addProperty("mcc", mccObjectID.stringify)
          accountCollection.find(Json.obj("customerId" -> acc.getCustomerId.toString), filter_object_id).one[JsObject].map {
            case Some(doc) =>
              accountCollection.update(
                Json.obj("customerId" -> doc.\("customerId")),
                Json.obj("$set" -> Json.parse(gson.toJson(json)))).map { _ =>
                log.info("Updated Account")
              }
            case None =>
              accountCollection.insert(Json.parse(gson.toJson(json))).map(lastError =>
                log.info("Inserted (%s) -> %s".format(json, lastError))
              )
          }

          adWordsHelper.adWordsSession.setClientCustomerId(acc.getCustomerId.toString)
          val budgets = accountHelper.getBudgets(budgetFields, None)

          if (budgets.length > 0) {
            var budgetStats: (Int, Int) = (0, 0)
            for (budget <- budgets) {
              val json = gson.toJson(budget)
              budgetCollection.find(
                Json.obj(
                  "budgetId" -> budget.getBudgetId.toString
                ),
                filter_object_id
              ).one[JsObject]
              .map {
                case Some(doc) =>
                  budgetCollection.update(
                    Json.obj("budgetId" -> doc.\("budgetId")),
                    Json.obj("$set" -> (
                      Json.obj("mcc" -> mccObjectID.stringify) ++
                        Json.obj("account" -> accountObjectID.stringify) ++
                        Json.parse(json).as[JsObject]))).map { _ =>
                    budgetStats = (budgetStats._1, budgetStats._2 + 1)
                  }
                case None =>

                  budgetCollection.insert(
                    Json.obj("mcc" -> mccObjectID.stringify) ++
                      Json.obj("account" -> accountObjectID.stringify) ++
                      Json.parse(json).as[JsObject]).map(lastError =>
                    budgetStats = (budgetStats._1 + 1, budgetStats._2)
                    )
              }
            }
          }

          val sharedBiddingStrategies = accountHelper.getSharedBiddingStrategy(sharedBiddingStrategyFields, None)

          if (sharedBiddingStrategies.length > 0) {
            var sharedBiddingStrategyStats: (Int, Int) = (0, 0)
            for (sharedBiddingStrategy <- sharedBiddingStrategies) {
              val json = gson.toJson(sharedBiddingStrategy)
              sharedBiddingStrategyCollection.find(
                Json.obj("id" -> sharedBiddingStrategy.getId.toString),
                filter_object_id
              ).one[JsObject]
              .map {
                case Some(doc) =>
                  sharedBiddingStrategyCollection.update(
                    Json.obj("id" -> doc.\("id")),
                    Json.obj("$set" -> (Json.obj("mcc" -> mccObjectID.stringify) ++
                      Json.obj("account" -> accountObjectID.stringify) ++
                      Json.parse(json).as[JsObject]))).map { _ =>
                    sharedBiddingStrategyStats = (sharedBiddingStrategyStats._1, sharedBiddingStrategyStats._2 + 1)
                  }
                case None =>
                  sharedBiddingStrategyCollection.insert(
                    Json.obj("mcc" -> mccObjectID.stringify) ++
                      Json.obj("account" -> accountObjectID.stringify) ++
                      Json.parse(json).as[JsObject]).map(lastError =>
                    sharedBiddingStrategyStats = (sharedBiddingStrategyStats._1 + 1, sharedBiddingStrategyStats._2)
                    )
              }
            }
            log.info("Inserted %d Shared Bidding Strategies".format(sharedBiddingStrategyStats._1))
            log.info("Updated %d Shared Bidding Strategies".format(sharedBiddingStrategyStats._2))
          }

          val campaignHelper = new CampaignHelper(adWordsHelper)
          val campaigns = campaignHelper.getCampaigns(campaignFields, None)

          if (campaigns.length > 0) {
            log.info("Campaigns in " + acc.getName + " -> " + campaigns.map(_.getName).mkString(", "))

            var campaignStats: (Int, Int) = (0, 0)
            for (campaign <- campaigns) {
              val json = gson.toJson(campaign)
              campaignCollection.find(Json.obj("id" -> campaign.getId.toString), filter_object_id).one[JsObject].map {
                case Some(doc) =>
                  campaignCollection.update(
                    Json.obj("id" -> doc.\("id")),
                    Json.obj("$set" -> (Json.obj("mcc" -> mccObjectID.stringify) ++
                      Json.obj("account" -> accountObjectID.stringify) ++
                      Json.parse(json).as[JsObject]))).map { _ =>
                    campaignStats = (campaignStats._1, campaignStats._2 + 1)
                  }
                case None =>
                  campaignCollection.insert(
                    Json.obj("mcc" -> mccObjectID.stringify) ++
                      Json.obj("account" -> accountObjectID.stringify) ++
                      Json.parse(json).as[JsObject]).map(lastError =>
                    campaignStats = (campaignStats._1 + 1, campaignStats._2)
                    )
              }


              log.info("Inserted %d Campaigns".format(campaignStats._1))
              log.info("Updated %d Campaigns".format(campaignStats._2))

              val adGroupPredicate = new Predicate()
              adGroupPredicate.setField("CampaignId")
              adGroupPredicate.setValues(Array(campaign.getId.toString))
              adGroupPredicate.setOperator(PredicateOperator.EQUALS)
              val campaignCriterionList = campaignHelper.getCampaignCriterion(campaignCriterionFields, Some(Array(adGroupPredicate)))

              log.info("Campaign Criterion -> " + campaignCriterionList.map(x => x.getCampaignId + " -> " + x.getCampaignCriterionType + ": " + x.getBidModifier).mkString(", "))

              var campaignCriterionStats: (Int, Int) = (0, 0)
              for (campaignCriterion <- campaignCriterionList) {
                val json = gson.toJson(campaignCriterion)
                campaignCriterionCollection.find(Json.obj("criterion.properties.id" -> campaignCriterion.getCriterion.getId.toString), filter_object_id).one[JsObject].map {
                  case Some(doc) =>
                    campaignCriterionCollection.update(
                      Json.obj("criterion.id" -> doc.\("criterion.id")),
                      Json.obj("$set" -> (Json.obj("mcc" -> mccObjectID.stringify) ++
                        Json.obj("account" -> accountObjectID.stringify) ++
                        Json.parse(json).as[JsObject]))).map { _ =>
                      campaignCriterionStats = (campaignCriterionStats._1, campaignCriterionStats._2 + 1)
                    }
                  case None =>
                    campaignCriterionCollection.insert(
                      Json.obj("mcc" -> mccObjectID.stringify) ++
                        Json.obj("account" -> accountObjectID.stringify) ++
                        Json.parse(json).as[JsObject]).map(lastError =>
                      campaignCriterionStats = (campaignCriterionStats._1 + 1, campaignCriterionStats._2)
                      )
                }
              }

              log.info("Inserted %d Campaign Criterion".format(campaignCriterionStats._1))
              log.info("Updated %d Campaign Criterion".format(campaignCriterionStats._2))

              val adGroupHelper = new AdGroupHelper(adWordsHelper)

              val adGroupBidModifiers = adGroupHelper.getAdGroupBidModifiers(adGroupBidModifierFields, Some(Array(adGroupPredicate)))
              var adGroupBidModifierStats: (Int, Int) = (0, 0)
              for (bidModifier <- adGroupBidModifiers) {
                val json = gson.toJson(bidModifier)
                adGroupBidModifierCollection.find(Json.obj("criterion.properties.id" -> bidModifier.getCriterion.getId.toString), filter_object_id).one[JsObject].map {
                  case Some(doc) =>
                    campaignCollection.update(
                      Json.obj("criterion.properties.id" -> doc.\("criterion.id")),
                      Json.obj("$set" -> (Json.obj("mcc" -> mccObjectID.stringify) ++
                        Json.obj("account" -> accountObjectID.stringify) ++
                        Json.parse(json).as[JsObject]))).map { _ =>
                      adGroupBidModifierStats = (adGroupBidModifierStats._1, adGroupBidModifierStats._2 + 1)
                    }
                  case None =>
                    adGroupBidModifierCollection.insert(
                      Json.obj("mcc" -> mccObjectID.stringify) ++
                        Json.obj("account" -> accountObjectID.stringify) ++
                        Json.parse(json).as[JsObject]).map(lastError =>
                      adGroupBidModifierStats = (adGroupBidModifierStats._1 + 1, adGroupBidModifierStats._2)
                      )
                }
              }

              log.info("Inserted %d AdGroup Bid Modifiers".format(adGroupBidModifierStats._1))
              log.info("Updated %d AdGroup Bid Modifiers".format(adGroupBidModifierStats._2))

              val adGroups = adGroupHelper.getAdGroups(adGroupFields, Some(Array(adGroupPredicate)))

              var adGroupStats: (Int, Int) = (0, 0)
              for (adGroup <- adGroups) {
                val json = gson.toJson(adGroup)
                adGroupCollection.find(Json.obj("id" -> adGroup.getId.toString), filter_object_id).one[JsObject].map {
                  case Some(doc) =>
                    adGroupCollection.update(
                      Json.obj("id" -> doc.\("id")),
                      Json.obj("$set" -> (Json.obj("mcc" -> mccObjectID.stringify) ++
                        Json.obj("account" -> accountObjectID.stringify) ++
                        Json.parse(json).as[JsObject]))).map { _ =>
                      adGroupStats = (adGroupStats._1, adGroupStats._2 + 1)
                    }
                  case None =>
                    adGroupCollection.insert(
                      Json.obj("mcc" -> mccObjectID.stringify) ++
                        Json.obj("account" -> accountObjectID.stringify) ++
                        Json.parse(json).as[JsObject]).map(lastError =>
                      adGroupStats = (adGroupStats._1 + 1, adGroupStats._2)
                      )
                }

                log.info("Inserted %d AdGroups".format(adGroupStats._1))
                log.info("Updated %d AdGroups".format(adGroupStats._2))

                if (adGroups.length > 0) {

                  if (adGroupBidModifiers.length > 0)
                    log.info("AdGroupBidModifiers -> " + adGroupBidModifiers.map(x => x.getCriterion.getCriterionType + ": " + x.getBidModifier).mkString(", "))

                  val adHelper = new AdHelper(adWordsHelper)
                  val adPredicate = new Predicate()
                  adPredicate.setField("AdGroupId")
                  adPredicate.setValues(Array(adGroup.getId.toString))
                  adPredicate.setOperator(PredicateOperator.EQUALS)
                  val ads = adHelper.getAds(adGroupAdFields, Some(Array(adPredicate)))

                  var adStats: (Int, Int) = (0, 0)
                  for (ad <- ads) {
                    val json = gson.toJson(ad)
                    adGroupAdCollection.find(Json.obj("ad.properties.id" -> ad.getAd.getId.toString), filter_object_id).one[JsObject].map {
                      case Some(doc) =>
                        adGroupCollection.update(
                          Json.obj("ad.properties.id" -> doc.\("ad.id")),
                          Json.obj("$set" -> (Json.obj("mcc" -> mccObjectID.stringify) ++
                            Json.obj("account" -> accountObjectID.stringify) ++
                            Json.parse(json).as[JsObject]))).map { _ =>
                          adStats = (adStats._1, adStats._2 + 1)
                        }
                      case None =>
                        adGroupAdCollection.insert(
                          Json.obj("mcc" -> mccObjectID.stringify) ++
                            Json.obj("account" -> accountObjectID.stringify) ++
                            Json.parse(json).as[JsObject]).map(lastError =>
                          adStats = (adStats._1 + 1, adStats._2)
                          )
                    }
                  }

                  log.info("Inserted %d Ads".format(adStats._1))
                  log.info("Updated %d Ads".format(adStats._2))

                  val keywordHelper = new AdGroupCriterionHelper(adWordsHelper)
                  val keywordPredicate = new Predicate()
                  keywordPredicate.setField("AdGroupId")
                  keywordPredicate.setValues(Array(adGroup.getId.toString))
                  keywordPredicate.setOperator(PredicateOperator.EQUALS)
                  val keywords = keywordHelper.getKeywords(adGroupCriterionFields, Some(Array(keywordPredicate)))

                  var keywordStats: (Int, Int) = (0, 0)

                  for (keyword <- keywords) {
                    val json = gson.toJson(keyword)
                    adGroupCriterionCollection.find(
                      Json.obj("criterion.properties.id" -> keyword.getCriterion.getId.toString),
                      filter_object_id
                    ).one[JsObject].map {
                      case Some(doc) =>
                        adGroupCriterionCollection.update(
                          Json.obj("_id" -> doc.\("_id")),
                          Json.obj("$set" -> (Json.obj("mcc" -> mccObjectID.stringify) ++
                            Json.obj("account" -> accountObjectID.stringify) ++
                            Json.parse(json).as[JsObject]))).map { _ =>
                          keywordStats = (keywordStats._1, keywordStats._2 + 1)
                        }
                      case None =>
                        adGroupCriterionCollection.insert(
                          Json.obj("mcc" -> mccObjectID.stringify) ++
                            Json.obj("account" -> accountObjectID.stringify) ++
                            Json.parse(json).as[JsObject]).map(lastError =>
                          keywordStats = (keywordStats._1 + 1, keywordStats._2)
                          )
                    }
                  }

                  log.info("Inserted %d AdGroupCriterion".format(keywordStats._1))
                  log.info("Updated %d AdGroupCriterion".format(keywordStats._2))

                  if (keywords.length > 0)
                    log.info("Keywords -> " + keywords.map(_.getCriterion.getId).mkString(", "))
                }
              }
            }
          }
          complete_subprocess(taskKey(request), cache)
        }
        Thread.sleep(5000)
      }
  }
}