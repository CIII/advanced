package sync.google.process.management.campaign

import Shared.Shared._
import akka.actor.Actor
import akka.event.Logging
import play.api.libs.json.{JsObject, Json}
import play.api.mvc.{AnyContent, Controller, Request, Security}
import play.modules.reactivemongo.MongoController
import sync.google.adwords.AdWordsHelper
import models.mongodb.google.collections._
import sync.google.adwords.account.CampaignHelper
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

class CampaignActor(request: Request[AnyContent]) extends Actor with Controller with MongoController {
  val log = Logging(context.system, this)

  def receive = {
    case msg =>
      val task_key = request.session.get(Security.username).get + task_ext
      val cache = msg.asInstanceOf[pending_cache_structure]
      val campaign_change = cache.change_data.asInstanceOf[controllers.google.campaign.CampaignController.CampaignForm]
      log.info("Processing %s -> %s -> %s -> %s".format(
        cache.change_category,
        cache.change_type,
        cache.traffic_source,
        cache.id
      ))

      val current_campaign_json_data = Await.result(campaignCollection.find(
        Json.obj("id" -> campaign_change.api_id)
      ).one[JsObject], 60 seconds).get

      val mcc_json_data = Await.result(mccCollection.find(
        Json.obj("id" -> current_campaign_json_data.\("mcc"))
      ).one[JsObject], 60 seconds).get

      val account_json_data = Await.result(accountCollection.find(
        Json.obj("id" -> current_campaign_json_data.\("account"))
      ).one[JsObject], 60 seconds).get

      val mcc_data = gson.fromJson(
        Json.stringify(mcc_json_data),
        classOf[models.mongodb.google.Mcc]
      )

      val account_data = gson.fromJson(
        Json.stringify(account_json_data),
        classOf[com.google.api.ads.adwords.axis.v201409.mcm.ManagedCustomer]
      )

      val adWordsHelper = new AdWordsHelper(
        clientId=mcc_data.oAuthClientId,
        clientSecret=mcc_data.oAuthClientSecret,
        refreshToken=mcc_data.oAuthRefreshToken,
        developerToken=mcc_data.DeveloperToken,
        customerId=Some(account_data.getCustomerId.toString)
      )

      val campaignHelper = new CampaignHelper(adWordsHelper)

      cache.change_type match {
        case change_type.DELETE =>
          //DELETE CAMPAIGN
        case change_type.NEW =>
          //CREATE CAMPAIGN
        case change_type.UPDATE =>
          //UPDATE CAMPAIGN
      }
  }
}