package sync.google.process.management.mcc.account.campaign.adgroup

import Shared.Shared._
import akka.actor.Actor
import akka.event.Logging
import com.google.api.ads.adwords.axis.v201509.cm.AdGroupBidModifier
import com.mongodb.casbah.Imports._
import com.mongodb.util.JSON
import models.mongodb.google.Google._
import sync.shared.Google.GoogleAdGroupBidModifierDataPullRequest

class AdGroupBidModifierActor extends Actor {
  val log = Logging(context.system, this)

  def receive = {
    case adGroupBidModifierDataPullRequest: GoogleAdGroupBidModifierDataPullRequest =>
      try {
        log.info("Processing Incoming Ad Group Bid Modifier (%s)".format(
          adGroupBidModifierDataPullRequest.adGroupBidModifierObject.adGroupBidModifier.getCriterion.getId
        ))


        val qry = DBObject(
          "mccObjId" -> adGroupBidModifierDataPullRequest.adGroupBidModifierObject.campaignObject.customerObject.mccObject.mccObjId,
          "customerObjId" -> adGroupBidModifierDataPullRequest.adGroupBidModifierObject.campaignObject.customerObject.customerObjId,
          "customerApiId" -> adGroupBidModifierDataPullRequest.adGroupBidModifierObject.campaignObject.customerObject.managedCustomer.getCustomerId,
          "campaignObjId" -> adGroupBidModifierDataPullRequest.adGroupBidModifierObject.campaignObject.campaignObjId,
          "campaignApiId" -> adGroupBidModifierDataPullRequest.adGroupBidModifierObject.campaignObject.campaign.getId,
          "ApiId" -> adGroupBidModifierDataPullRequest.adGroupBidModifierObject.adGroupBidModifier.getCriterion.getId,
          "bidModifierType" -> "adGroup"
        )

        val newData = DBObject(
          "startTsecs" -> java.lang.System.currentTimeMillis() / 1000,
          "endTsecs" -> -1,
          "classPath" -> adGroupBidModifierDataPullRequest.adGroupBidModifierObject.adGroupBidModifier.getClass.getCanonicalName,
          "object" -> JSON.parse(gson.toJson(adGroupBidModifierDataPullRequest.adGroupBidModifierObject.adGroupBidModifier)).asInstanceOf[DBObject]
        )

        var matchFound = true
        googleBidModifierCollection.findOne(
          qry ++ ("bidModifier.endTsecs" -> -1)
        ) match {
          case Some(bidModifierRs) =>
            if (
              !gson.toJson(dboToGoogleEntity[AdGroupBidModifier](bidModifierRs, "bidModifier", None))
                .equals(gson.toJson(adGroupBidModifierDataPullRequest.adGroupBidModifierObject.adGroupBidModifier))
            ) {
              googleBidModifierCollection.update(
                qry ++ ("bidModifier.endTsecs" -> -1),
                DBObject("$set" -> DBObject("bidModifier.0.endTsecs" -> java.lang.System.currentTimeMillis() / 1000))
              )
              matchFound = false
              log.debug("Google AdGroupBidModifier match found. Changes detected. Updating...")
            }
          case _ =>
            matchFound = false
            log.debug("No Google AdGroupBidModifier record Found. Inserting...")
        }
        if(!matchFound)
          googleBidModifierCollection.update(qry, DBObject("$push" -> DBObject("bidModifier" -> newData)), upsert = true)
      } catch {
        case e: Exception =>
          log.info("Error Retrieving Data for AdGroup Bid Modifier (%s) - %s".format(
            adGroupBidModifierDataPullRequest.adGroupBidModifierObject.adGroupBidModifier.getCriterion.getId,
            e.getMessage
          ))
          e.printStackTrace()
      }
  }
}