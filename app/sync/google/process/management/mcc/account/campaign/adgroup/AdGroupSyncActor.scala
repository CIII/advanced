package sync.google.process.management.mcc.account.campaign.adgroup

import Shared.Shared._
import akka.actor.Actor
import akka.event.Logging
import models.agile_production_db.google.{AdGroupThem, AdGroupThemHistory, GoogleDB}
import org.squeryl.PrimitiveTypeMode._
import sync.shared.Google._

class AdGroupSyncActor extends Actor {
  val log = Logging(context.system, this)

  def receive = {
    case (
      adGroupDataPullRequest: GoogleAdGroupDataPullRequest,
      tsecs: Long
    ) =>
      try {
        log.info("Processing Sync for AdGroup Data (%s) to Uber".format(
          adGroupDataPullRequest.adGroupObject.adGroup.getId
        ))
        val previousData = using(GoogleDB.session) {
          from(GoogleDB.adGroupThem)(agt =>
            where(agt.api_id === adGroupDataPullRequest.adGroupObject.adGroup.getId.toLong)
              select agt
          ).toIndexedSeq
        }

        if (previousData.nonEmpty) {
          for (adgroup <- previousData) {
            if (
              !adgroup.status.equals(adGroupDataPullRequest.adGroupObject.adGroup.getStatus.getValue) ||
                !compare[String](adgroup.name, Some(adGroupDataPullRequest.adGroupObject.adGroup.getStatus.getValue))
            ) {
              using(GoogleDB.session) {
                GoogleDB.adGroupThemHistory.insert(new AdGroupThemHistory(
                  them_id = adgroup.id,
                  campaign_them_id = adgroup.campaign_them_id,
                  tsecs = tsecs,
                  job_id = None,
                  operation = None,
                  us_id = adgroup.us_id,
                  client_id = adgroup.client_id,
                  api_id = adgroup.api_id,
                  max_content_cpc_cents = adgroup.max_content_cpc_cents,
                  max_cpc_cents = adgroup.max_cpc_cents,
                  max_cpm_cents = adgroup.max_cpm_cents,
                  name = adgroup.name,
                  proxy_max_cpc_cents = adgroup.proxy_max_cpc_cents,
                  status = adgroup.status,
                  removed_bool = adgroup.removed_bool,
                  max_cpa_cents = adgroup.max_cpa_cents,
                  site_max_cpc_cents = adgroup.site_max_cpc_cents
                ))
              }

              adgroup.name = Some(adGroupDataPullRequest.adGroupObject.adGroup.getName)
              adgroup.status = adGroupDataPullRequest.adGroupObject.adGroup.getStatus.getValue

              using(GoogleDB.session) { GoogleDB.adGroupThem.update(adgroup) }
              log.info("UPDATING GOOGLE ADGROUP! %s for %s".format(
                adgroup.toString,
                gson.toJson(adGroupDataPullRequest.adGroupObject.adGroup)
              ))
            }
          }
        } else {
          val accountInfo = using(GoogleDB.session) {
            from(GoogleDB.accountInfo)(ai =>
              where(ai.customer_id === Some(
                adGroupDataPullRequest.adGroupObject.campaignObject.customerObject.customer.get.getCustomerId.toString
              )) select ai
            ).toIndexedSeq
          }

          if (accountInfo.nonEmpty) {
            using(GoogleDB.session) {
              GoogleDB.adGroupThem.insert(new AdGroupThem(
                campaign_them_id = None,
                us_id = None,
                client_id = accountInfo.head.client_id,
                api_id = Some(adGroupDataPullRequest.adGroupObject.adGroup.getId),
                max_content_cpc_cents = None,
                max_cpc_cents = None,
                max_cpm_cents = None,
                name = Some(adGroupDataPullRequest.adGroupObject.adGroup.getName),
                proxy_max_cpc_cents = None,
                status = adGroupDataPullRequest.adGroupObject.adGroup.getStatus.getValue,
                removed_bool = Some(0),
                max_cpa_cents = None,
                site_max_cpc_cents = None
              ))
            }
          } else {
            //throw new Exception("Cannot find matching clientId to create new entry")
          }
        }
      } catch {
        case e: Exception =>
          e.printStackTrace()
          log.info("Error Syncing Google AdGroup (%s) to Uber - %s".format(
            adGroupDataPullRequest.adGroupObject.adGroup.getName,
            e.getMessage
          ))
      }
  }
}
