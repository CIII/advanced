package sync.google.process.management.mcc.account.campaign.adgroup

import akka.actor.Actor
import akka.event.Logging
import sync.shared.Google.GoogleAdGroupBidModifierDataPullRequest

class AdGroupBidModifierSyncActor extends Actor {
  val log = Logging(context.system, this)

  def receive = {
    case adGroupBidModifierDataPullRequest: GoogleAdGroupBidModifierDataPullRequest =>
      try {
        log.info("Processing Sync for Ad Group Bid Modifier (%s) to Uber".format(
          adGroupBidModifierDataPullRequest.adGroupBidModifierObject.adGroupBidModifier.getCriterion.getId
        ))

      } catch {
        case e: Exception =>
          log.info("Error Syncing AdGroup Bid Modifier (%s) to Uber - %s".format(
            adGroupBidModifierDataPullRequest.adGroupBidModifierObject.adGroupBidModifier.getCriterion.getId,
            e.getMessage
          ))
          e.printStackTrace()
      }
  }
}