package sync.google.process.management.mcc.account.campaign.adgroup.criterion.keyword

import Shared.Shared._
import akka.actor.Actor
import akka.event.Logging
import com.google.api.ads.adwords.axis.v201509.cm._
import models.agile_production_db.google.{KeywordThem, KeywordThemHistory, GoogleDB}
import org.squeryl.PrimitiveTypeMode._
import sync.shared.Google.{GoogleAdGroupCriterionDataPullRequest, _}

class AdGroupCriterionSyncActor extends Actor {
  val log = Logging(context.system, this)

  def receive = {
    case (
      adGroupCriterionDataPullRequest: GoogleAdGroupCriterionDataPullRequest,
      tsecs: Long
    ) =>
      try {
        log.info("Processing Sync for AdGroupCriterion Data (%s) to Uber".format(
          adGroupCriterionDataPullRequest.adGroupCriterionObject.adGroupCriterion.getCriterion.getId
        ))
        val previousData = using(GoogleDB.session) {
          from(GoogleDB.keywordThem)(kt =>
            where(kt.api_id === adGroupCriterionDataPullRequest.adGroupCriterionObject.adGroupCriterion.getCriterion.getId.toLong)
              select kt
          ).toIndexedSeq
        }

        val bagc = adGroupCriterionDataPullRequest.adGroupCriterionObject.adGroupCriterion.asInstanceOf[BiddableAdGroupCriterion]

        if (previousData.nonEmpty) {
          for (adgroup_criterion <- previousData) {
            val keyword = bagc.getCriterion.asInstanceOf[Keyword]
            if (
              !compare[String](adgroup_criterion.text, Some(keyword.getText)) ||
                !adgroup_criterion.`type`.equals(keyword.getMatchType.getValue) ||
                bagc.getFinalUrls.getUrls.count(url => compare[String](adgroup_criterion.destination_url, Some(url))) == 0 ||
                !compare[Int](adgroup_criterion.quality_score, Some(bagc.getQualityInfo.getQualityScore.intValue))
            ) {
              using(GoogleDB.session) {
                GoogleDB.keywordThemHistory.insert(new KeywordThemHistory(
                  ad_group_them_id = adgroup_criterion.ad_group_them_id,
                  them_id = adgroup_criterion.id,
                  tsecs = tsecs,
                  job_id = None,
                  operation = None,
                  us_id = adgroup_criterion.us_id,
                  client_id = adgroup_criterion.client_id,
                  api_id = adgroup_criterion.api_id,
                  destination_url = adgroup_criterion.destination_url,
                  exemption_request = adgroup_criterion.exemption_request,
                  language = adgroup_criterion.language,
                  max_cpc_cents = adgroup_criterion.max_cpc_cents,
                  negative_bool = adgroup_criterion.negative_bool,
                  paused_bool = adgroup_criterion.paused_bool,
                  proxy_max_cpc_cents = adgroup_criterion.proxy_max_cpc_cents,
                  status = adgroup_criterion.status,
                  text = adgroup_criterion.text,
                  `type` = adgroup_criterion.`type`,
                  removed_bool = adgroup_criterion.removed_bool,
                  quality_score = adgroup_criterion.quality_score,
                  first_page_cpc_cents = adgroup_criterion.first_page_cpc_cents
                ))
              }

              adgroup_criterion.`type` = keyword.getMatchType.getValue
              adgroup_criterion.text = Some(keyword.getText)

              var dest_url = adGroupCriterionDataPullRequest
                .adGroupCriterionObject
                .adGroupObject
                .campaignObject
                .customerObject
                .customer
                .get
                .getTrackingUrlTemplate.replace(
                landingPageTag,
                bagc.getFinalUrls.getUrls.headOption.getOrElse("")
              )

              bagc.getUrlCustomParameters.getParameters.foreach(
                p =>
                  dest_url = dest_url.replace(p.getKey, p.getValue)
              )

              adgroup_criterion.destination_url = Some(dest_url)

              adgroup_criterion.status = bagc.getUserStatus.toString

              using(GoogleDB.session) {
                GoogleDB.keywordThem.update(adgroup_criterion)
              }
              log.info("UPDATING GOOGLE KEYWORD! %s for %s".format(
                adgroup_criterion.toString,
                gson.toJson(adGroupCriterionDataPullRequest.adGroupCriterionObject.adGroupCriterion)
              ))
            }
          }
        } else {
          val accountInfo = using(GoogleDB.session) {
            from(GoogleDB.accountInfo)(ai =>
              where(ai.customer_id === Some(
                adGroupCriterionDataPullRequest.adGroupCriterionObject.adGroupObject.campaignObject.customerObject.customer.get.getCustomerId.toString
              )) select ai
            ).toIndexedSeq
          }

          if (
            accountInfo.nonEmpty &&
            (
              try { bagc.getFinalUrls.getUrls.nonEmpty }
              catch { case _: Throwable => false }
            )
          ) {
            var dest_url = adGroupCriterionDataPullRequest
              .adGroupCriterionObject
              .adGroupObject
              .campaignObject
              .customerObject
              .customer
              .get
              .getTrackingUrlTemplate.replace(
              landingPageTag,
              bagc.getFinalUrls.getUrls.head
            )

            bagc.getUrlCustomParameters.getParameters.foreach(
              p =>
                dest_url = dest_url.replace(p.getKey, p.getValue)
            )

            using(GoogleDB.session) {
              GoogleDB.keywordThem.insert(new KeywordThem(
                ad_group_them_id = None,
                us_id = None,
                client_id = accountInfo.head.client_id,
                api_id = Some(adGroupCriterionDataPullRequest.adGroupCriterionObject.adGroupCriterion.getCriterion.getId),
                destination_url = Some(dest_url),
                exemption_request = None,
                language = None,
                max_cpc_cents = None,
                negative_bool = None,
                paused_bool = None,
                proxy_max_cpc_cents = None,
                status = adGroupCriterionDataPullRequest.adGroupCriterionObject.adGroupCriterion.asInstanceOf[BiddableAdGroupCriterion].getApprovalStatus.getValue,
                text = None,
                `type` = adGroupCriterionDataPullRequest.adGroupCriterionObject.adGroupCriterion.getCriterion.getCriterionType,
                removed_bool = 0,
                quality_score = None,
                first_page_cpc_cents = None
              ))
            }
          } else {
            //throw new Exception("Cannot find matching clientId to create new entry")
          }
        }
      } catch {
        case e: Exception =>
          e.printStackTrace()
          log.info("Error Syncing Google AdGroup Criterion (%s) to Uber - %s".format(
            adGroupCriterionDataPullRequest.adGroupCriterionObject.adGroupCriterion.getCriterion.getId,
            e.getMessage
          ))
      }
  }
}