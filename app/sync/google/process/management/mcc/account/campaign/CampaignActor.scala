package sync.google.process.management.mcc.account.campaign

import Shared.Shared._
import akka.actor.Actor
import akka.event.Logging
import com.google.api.ads.adwords.axis.utils.v201509.SelectorBuilder
import com.google.api.ads.adwords.axis.v201509.cm._
import com.google.api.ads.adwords.lib.selectorfields.v201509.cm.{AdGroupBidModifierField, AdGroupField, CampaignCriterionField, CampaignField}
import com.mongodb.casbah.Imports._
import com.mongodb.util.JSON
import helpers.google.mcc.account.campaign.CampaignControllerHelper._
import models.mongodb.google.Google._
import sync.google.adwords.AdWordsHelper
import sync.google.adwords.account.{AdGroupHelper, CampaignHelper}
import sync.shared.Google._

import scala.collection.mutable.ListBuffer

class CampaignActor extends Actor {
  val log = Logging(context.system, this)

  def receive = {
    case campaignDataPullRequest: GoogleCampaignDataPullRequest =>
        try {
          log.info("Processing Incoming Data for Campaign (%s)".format(
            campaignDataPullRequest.campaignObject.campaign.getId
          ))

          val qry = DBObject(
            "mccObjId" -> campaignDataPullRequest.campaignObject.customerObject.mccObject.mccObjId,
            "customerObjId" -> campaignDataPullRequest.campaignObject.customerObject.customerObjId,
            "customerApiId" -> campaignDataPullRequest.campaignObject.customerObject.managedCustomer.getCustomerId,
            "apiId" -> campaignDataPullRequest.campaignObject.campaign.getId
          )

          val startTsecs = java.lang.System.currentTimeMillis() / 1000
          val endTsecs = java.lang.System.currentTimeMillis() / 1000

          val newData = DBObject(
            "startTsecs" -> startTsecs,
            "endTsecs" -> -1,
            "classPath" -> campaignDataPullRequest.campaignObject.campaign.getClass.getCanonicalName,
            "object" -> JSON.parse(gson.toJson(campaignDataPullRequest.campaignObject.campaign)).asInstanceOf[DBObject]
          )

          var matchFound = true

          googleCampaignCollection.findOne(
            qry ++ ("campaign.endTsecs" -> -1)
          ) match {
            case Some(campaignRs) =>
              if (
                !gson.toJson(dboToGoogleEntity[Campaign](campaignRs, "campaign", None))
                  .equals(gson.toJson(campaignDataPullRequest.campaignObject.campaign))
              ) {
                googleCampaignCollection.update(
                  qry ++ ("campaign.endTsecs" -> -1),
                  DBObject("$set" -> DBObject("campaign.0.endTsecs" -> endTsecs))
                )
                matchFound = false
                log.debug("Google Campaign match found. Changes detected. Updating...")
              }
            case _ =>
              matchFound = false
              log.debug("No Google Campaign record Found. Inserting...")
          }
          if(!matchFound) {
            googleCampaignCollection.update(qry, DBObject("$push" -> DBObject("campaign" -> newData)), upsert = true)

            if (campaignDataPullRequest.pushToUber)
              googleCampaignSyncActor ! (campaignDataPullRequest, endTsecs)
          }

          if(campaignDataPullRequest.recursivePull) {
            campaignDataPullRequest.campaignObject.campaignObjId = googleCampaignCollection.findOne(qry).get._id

            val campaignCriterionSelector = (new SelectorBuilder)
              .offset(0)
              .fields(campaignCriterionFields: _*)
              .equals(CampaignCriterionField.CampaignId, campaignDataPullRequest.campaignObject.campaign.getId.toString)
              .limit(campaignDataPullRequest.adWordsHelper.PAGE_SIZE)
              .build()

            val campaignHelper = new CampaignHelper(campaignDataPullRequest.adWordsHelper, log)

            campaignHelper.getCampaignCriterion(
              campaignCriterionFields,
              None,
              Some(campaignCriterionSelector)
            ).foreach(criterion => googleCampaignCriterionActor ! GoogleCampaignCriterionDataPullRequest(
              campaignDataPullRequest.adWordsHelper,
              CampaignCriterionObject(
                campaignDataPullRequest.campaignObject,
                None,
                criterion
              ),
              pushToUber = true
            ))

            val adGroupHelper = new AdGroupHelper(campaignDataPullRequest.adWordsHelper, log)

            val adGroupBidModifierSelector = (new SelectorBuilder)
              .offset(0)
              .fields(adGroupBidModifierFields: _*)
              .equals(AdGroupBidModifierField.CampaignId, campaignDataPullRequest.campaignObject.campaign.getId.toString)
              .limit(campaignDataPullRequest.adWordsHelper.PAGE_SIZE)
              .build()

            adGroupHelper.getAdGroupBidModifiers(adGroupBidModifierFields, None, Some(adGroupBidModifierSelector)).foreach(bidModifier =>
              googleAdGroupBidModifierActor ! GoogleAdGroupBidModifierDataPullRequest(
                campaignDataPullRequest.adWordsHelper,
                AdGroupBidModifierObject(
                  campaignDataPullRequest.campaignObject,
                  None,
                  bidModifier
                ),
                pushToUber = true
              )
            )

            val adGroupSelector = (new SelectorBuilder)
              .offset(0)
              .fields(adGroupFields: _*)
              .equals(AdGroupField.CampaignId, campaignDataPullRequest.campaignObject.campaign.getId.toString)
              .limit(campaignDataPullRequest.adWordsHelper.PAGE_SIZE)
              .build()

            adGroupHelper.getAdGroups(adGroupFields, None, Some(adGroupSelector)).foreach(adGroup =>
              googleAdGroupActor ! GoogleAdGroupDataPullRequest(
                adWordsHelper = campaignDataPullRequest.adWordsHelper,
                adGroupObject = AdGroupObject(
                  campaignDataPullRequest.campaignObject,
                  None,
                  adGroup
                ),
                recursivePull = true,
                pushToUber = true
              )
            )
          }
        } catch {
          case e: Exception =>
            log.info("Error Retrieving Data for Google Campaign (%s) - %s".format(
              campaignDataPullRequest.campaignObject.campaign.getName,
              e.getMessage
            ))
            e.printStackTrace()
        }
    case cache: PendingCacheStructure =>
      val campaign_change = dboToCampaignForm(cache.changeData.asDBObject)
      log.info("Processing %s -> %s -> %s -> %s".format(
        cache.changeCategory,
        cache.changeType,
        cache.trafficSource,
        cache.id
      ))

      val account_data = gson.fromJson(
        googleCustomerCollection.findOne(
          DBObject(
            "mccObjId" -> new ObjectId(campaign_change.parent.mccObjId.get),
            "apiId" -> campaign_change.parent.customerApiId
          ),
          DBObject("$slice" -> -1)
        ).get.getAs[String]("customer").get,
        classOf[com.google.api.ads.adwords.axis.v201509.mcm.Customer]
      )

      val mcc_data = dboToMcc(
        googleMccCollection.findOne(
          DBObject("_id" -> new ObjectId(campaign_change.parent.mccObjId.get))
        ).get.asDBObject
      )

      val adWordsHelper = new AdWordsHelper(
        clientId=mcc_data.oAuthClientId,
        clientSecret=mcc_data.oAuthClientSecret,
        refreshToken=mcc_data.oAuthRefreshToken,
        developerToken=mcc_data.developerToken,
        customerId=Some(account_data.getCustomerId.toString)
      )

      val campaignHelper = new CampaignHelper(adWordsHelper, log)

      var campaigns = ListBuffer[Campaign]()

      if (cache.changeType != ChangeType.NEW) {
        campaigns = campaigns ++ campaignHelper
          .getCampaigns(
            List(CampaignField.Id, CampaignField.Name),
            None,
            Some(
              (new SelectorBuilder)
                .equals(CampaignField.Id, campaign_change.apiId.getOrElse("").toString)
                .build
            )
          )
      }

      cache.changeType match {
        case ChangeType.DELETE =>
          campaigns.map(campaign => campaignHelper.deleteCampaign(campaign))
        case ChangeType.NEW =>
          val campaign = new Campaign()
          campaign.setName(campaign_change.name)
          campaign.setStatus(CampaignStatus.fromString(campaign_change.status.getOrElse(campaign.getStatus.toString)))
          campaign.setStartDate(campaign_change.startDate.getOrElse(campaign.getStartDate))
          campaign.setEndDate(campaign_change.endDate.getOrElse(campaign.getEndDate))
          campaign.setAdvertisingChannelType(
            AdvertisingChannelType
              .fromString(
                campaign_change.advertisingChannelType.getOrElse(campaign.getAdvertisingChannelType.toString)
              )
          )
        case ChangeType.UPDATE =>
          campaigns.foreach{campaign =>
            campaign.setName(campaign_change.name)
            campaign.setStatus(CampaignStatus.fromString(campaign_change.status.getOrElse(campaign.getStatus.toString)))
            campaign.setStartDate(campaign_change.startDate.getOrElse(campaign.getStartDate))
            campaign.setEndDate(campaign_change.endDate.getOrElse(campaign.getEndDate))
            campaign.setAdvertisingChannelType(
              AdvertisingChannelType
                .fromString(
                  campaign_change.advertisingChannelType.getOrElse(campaign.getAdvertisingChannelType.toString)
                )
            )
            campaignHelper.updateCampaign(campaign)
          }
      }
  }
}