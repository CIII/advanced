package sync.google.process.management.mcc.account.campaign.criterion

import akka.actor.Actor
import akka.event.Logging
import sync.shared.Google.GoogleCampaignCriterionDataPullRequest

class CampaignCriterionSyncActor extends Actor {
  val log = Logging(context.system, this)

  def receive = {
    case (
      campaignCriterionDataPullRequest: GoogleCampaignCriterionDataPullRequest,
      tsecs: Long
    )=>
      try {
      log.info("Nothing to do...")
      } catch {
        case e: Exception =>
          log.info("Error Syncing Google Campaign Criterion (%s) to Uber - %s".format(
            campaignCriterionDataPullRequest.campaignCriterionObject.campaignCriterion.getCriterion.getId,
            e.getMessage
          ))
          e.printStackTrace()
      }
  }
}