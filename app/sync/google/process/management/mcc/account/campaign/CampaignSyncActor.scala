package sync.google.process.management.mcc.account.campaign

import Shared.Shared._
import akka.actor.Actor
import akka.event.Logging
import models.agile_production_db.google.{CampaignThem, CampaignThemHistory, GoogleDB}
import org.squeryl.PrimitiveTypeMode._
import sync.shared.Google._

class CampaignSyncActor extends Actor {
  val log = Logging(context.system, this)

  def receive = {
    case (
      campaignDataPullRequest: GoogleCampaignDataPullRequest,
      tsecs: Long
    ) =>
      try {
        log.info("Processing Sync for Campaign (%s) to Uber".format(
          campaignDataPullRequest.campaignObject.campaign.getId
        ))
        val previousData = using(GoogleDB.session) {
          from(GoogleDB.campaignThem)(ct =>
            where(ct.api_id === campaignDataPullRequest.campaignObject.campaign.getId.toLong)
              select ct
          ).toIndexedSeq
        }

        if (previousData.nonEmpty) {
          for (campaign <- previousData) {
            if (
              !campaign.budget_amount_cents.getOrElse(0).equals(campaignDataPullRequest.campaignObject.campaign.getBudget.getAmount.getMicroAmount) ||
                !campaign.budget_period.equals(campaignDataPullRequest.campaignObject.campaign.getBudget.getPeriod.getValue) ||
                !campaign.delivery_method.getOrElse("").equals(campaignDataPullRequest.campaignObject.campaign.getBudget.getDeliveryMethod.getValue) ||
                !campaign.enable_bid_optimization_bool.getOrElse(0).equals(
                  if (campaignDataPullRequest.campaignObject.campaign.getAdServingOptimizationStatus.getValue.equals("OPTIMIZE")) 1 else 0
                ) ||
                !compare[java.util.Date](
                campaign.end_date,
                if(campaignDataPullRequest.campaignObject.campaign.getEndDate.isEmpty)
                  None
                else Some(googleDateFormatter.parse(campaignDataPullRequest.campaignObject.campaign.getEndDate))
              ) ||
                !compare[String](campaign.name, Some(campaignDataPullRequest.campaignObject.campaign.getName)) ||
                !campaign.status.equals(campaignDataPullRequest.campaignObject.campaign.getStatus.getValue) ||
                !compare[Int](campaign.target_content_network_bool, Some(campaignDataPullRequest.campaignObject.campaign.getNetworkSetting.getTargetContentNetwork.asInstanceOf[Int])) ||
                !compare[Int](campaign.target_google_search_bool, Some(campaignDataPullRequest.campaignObject.campaign.getNetworkSetting.getTargetGoogleSearch.asInstanceOf[Int])) ||
                !compare[Int](campaign.target_search_network_bool, Some(campaignDataPullRequest.campaignObject.campaign.getNetworkSetting.getTargetSearchNetwork.asInstanceOf[Int]))
            ) {
              using(GoogleDB.session) {
                GoogleDB.campaignThemHistory.insert(new CampaignThemHistory(
                  account_them_id = campaign.account_them_id,
                  them_id = campaign.id,
                  tsecs = tsecs,
                  job_id = None,
                  operation = None,
                  us_id = Some(campaign.us_id.getOrElse(0)),
                  client_id = campaign.client_id,
                  tid = campaign.tid,
                  api_id = campaign.api_id,
                  name = campaign.name,
                  status = campaign.status,
                  start_date = campaign.start_date,
                  end_date = campaign.end_date,
                  target_google_search_bool = campaign.target_google_search_bool,
                  target_search_network_bool = campaign.target_search_network_bool,
                  target_content_network_bool = campaign.target_content_network_bool,
                  target_all_geo_bool = campaign.target_all_geo_bool,
                  budget_amount_cents = campaign.budget_amount_cents,
                  budget_period = Some(campaign.budget_period),
                  content_targeting = Some(campaign.content_targeting),
                  delivery_method = campaign.delivery_method,
                  bid_ceiling_cents = campaign.bid_ceiling_cents,
                  enable_bid_optimization_bool = campaign.enable_bid_optimization_bool,
                  ad_schedule_status = campaign.ad_schedule_status,
                  removed_bool = campaign.removed_bool,
                  exclude_error_pages_bool = campaign.exclude_error_pages_bool,
                  exclude_parked_domains_bool = campaign.exclude_parked_domains_bool,
                  optimize_ad_serving_bool = campaign.optimize_ad_serving_bool
                ))
              }

              campaign.name = Some(campaignDataPullRequest.campaignObject.campaign.getName)
              campaign.status = campaignDataPullRequest.campaignObject.campaign.getStatus.getValue
              campaign.start_date = if (campaignDataPullRequest.campaignObject.campaign.getStartDate.isEmpty) None
              else Some(googleDateFormatter.parse(campaignDataPullRequest.campaignObject.campaign.getStartDate))
              campaign.end_date = if (campaignDataPullRequest.campaignObject.campaign.getEndDate.isEmpty) None
              else Some(googleDateFormatter.parse(campaignDataPullRequest.campaignObject.campaign.getEndDate))
              campaign.target_google_search_bool = Some(campaignDataPullRequest.campaignObject.campaign.getNetworkSetting.getTargetGoogleSearch.asInstanceOf[Int])
              campaign.target_search_network_bool = Some(campaignDataPullRequest.campaignObject.campaign.getNetworkSetting.getTargetSearchNetwork.asInstanceOf[Int])
              campaign.target_content_network_bool = Some(campaignDataPullRequest.campaignObject.campaign.getNetworkSetting.getTargetContentNetwork.asInstanceOf[Int])
              campaign.budget_amount_cents = Some(campaignDataPullRequest.campaignObject.campaign.getBudget.getAmount.getMicroAmount.intValue())
              campaign.budget_period = campaignDataPullRequest.campaignObject.campaign.getBudget.getPeriod.getValue
              campaign.delivery_method = Some(campaignDataPullRequest.campaignObject.campaign.getBudget.getDeliveryMethod.getValue)

              using(GoogleDB.session) { GoogleDB.campaignThem.update(campaign) }

              log.info("UPDATING CAMPAIGN! %s for %s".format(
                campaign.toString,
                gson.toJson(campaignDataPullRequest.campaignObject.campaign)
              ))
            }
          }
        } else {
          val accountInfo = using(GoogleDB.session) {
            from(GoogleDB.accountInfo)(ai =>
              where(ai.customer_id === Some(campaignDataPullRequest.campaignObject.customerObject.customer.get.getCustomerId.toString)) select ai
            ).toIndexedSeq
          }

          if (accountInfo.nonEmpty) {
            using(GoogleDB.session) {
              GoogleDB.campaignThem.insert(new CampaignThem(
                us_id = None,
                account_them_id = None,
                client_id = accountInfo.head.client_id,
                tid = accountInfo.head.tid,
                api_id = Some(campaignDataPullRequest.campaignObject.campaign.getId),
                name = Some(campaignDataPullRequest.campaignObject.campaign.getName),
                status = campaignDataPullRequest.campaignObject.campaign.getStatus.getValue,
                start_date = None,
                end_date = None,
                target_google_search_bool = None,
                target_search_network_bool = None,
                target_content_network_bool = None,
                target_all_geo_bool = None,
                budget_amount_cents = None,
                budget_period = "",
                content_targeting = "",
                delivery_method = None,
                bid_ceiling_cents = None,
                enable_bid_optimization_bool = None,
                ad_schedule_status = None,
                removed_bool = 0,
                exclude_error_pages_bool = 0,
                exclude_parked_domains_bool = 0,
                optimize_ad_serving_bool = 0
              ))
            }
          } else {
            //throw new Exception("Cannot find matching clientId to create new entry")
          }
        }
      } catch {
        case e: Exception =>
          e.printStackTrace()
          log.info("Error Syncing Google Campaign (%s) to Uber - %s".format(
            campaignDataPullRequest.campaignObject.campaign.getName,
            e.getMessage
          ))
      }
  }
}