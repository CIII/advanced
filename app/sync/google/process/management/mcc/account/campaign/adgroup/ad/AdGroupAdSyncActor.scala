package sync.google.process.management.mcc.account.campaign.adgroup.ad

import Shared.Shared._
import akka.actor.Actor
import akka.event.Logging
import com.google.api.ads.adwords.axis.v201509.cm._
import models.agile_production_db.google.{AdThem, AdThemHistory, GoogleDB}
import org.squeryl.PrimitiveTypeMode._
import sync.shared.Google.{GoogleAdGroupAdDataPullRequest, _}

class AdGroupAdSyncActor extends Actor {
  val log = Logging(context.system, this)

  def receive = {
    case (
      adGroupAdDataPullRequest: GoogleAdGroupAdDataPullRequest,
      tsecs: Long
    ) =>
      try {
        log.info("Processing Sync for AdGroupAd (%s) to Uber".format(
          adGroupAdDataPullRequest.adGroupAdObject.adGroupAd.getAd.getId
        ))
        val previousData = using(GoogleDB.session) {
          from(GoogleDB.adThem)(agat =>
            where(agat.api_id === adGroupAdDataPullRequest.adGroupAdObject.adGroupAd.getAd.getId.toLong)
              select agat
          ).toIndexedSeq
        }

        if (previousData.nonEmpty) {
          for (adgroupad <- previousData) {
            // compare all adgroup ad fields to determine if we need to update or insert
            if (
              !adgroupad.status.equals(adGroupAdDataPullRequest.adGroupAdObject.adGroupAd.getStatus.getValue) ||
                !compare[String](adgroupad.ad_type, Some(adGroupAdDataPullRequest.adGroupAdObject.adGroupAd.getAd.getAdType)) ||
                !(adGroupAdDataPullRequest.adGroupAdObject.adGroupAd.getAd match {
                  case textAd: TextAd =>
                    !compare[String](adgroupad.description1, Some(textAd.getDescription1)) ||
                      !compare[String](adgroupad.description2, Some(textAd.getDescription2)) ||
                      !compare[String](adgroupad.headline, Some(textAd.getHeadline))
                  case _ => false
                }) ||
                adGroupAdDataPullRequest.adGroupAdObject.adGroupAd.getAd.getFinalUrls.count(x => compare[String](adgroupad.destination_url, Some(x))) == 0
            ) {
              using(GoogleDB.session) {
                GoogleDB.adThemHistory.insert(new AdThemHistory(
                  ad_group_them_id = adgroupad.ad_group_them_id,
                  them_id = adgroupad.id,
                  tsecs = tsecs,
                  job_id = None,
                  operation = None,
                  us_id = adgroupad.us_id,
                  client_id = adgroupad.client_id,
                  api_id = adgroupad.api_id,
                  ad_type = adgroupad.ad_type,
                  description1 = adgroupad.description1,
                  description2 = adgroupad.description2,
                  destination_url = adgroupad.destination_url,
                  disapproved_bool = adgroupad.disapproved_bool,
                  display_url = adgroupad.display_url,
                  exemption_request = adgroupad.exemption_request,
                  headline = adgroupad.headline,
                  status = adgroupad.status,
                  removed_bool = adgroupad.removed_bool
                ))
              }

              adGroupAdDataPullRequest.adGroupAdObject.adGroupAd.getAd match {
                case textAd: TextAd =>
                  adgroupad.description1 = Some(textAd.getDescription1)
                  adgroupad.description2 = Some(textAd.getDescription2)
                  adgroupad.headline = Some(textAd.getHeadline)
                case _ => false
              }

              if (
                try { adGroupAdDataPullRequest.adGroupAdObject.adGroupAd.getAd.getFinalUrls.nonEmpty }
                catch { case _: Throwable => false }
              ) {
                var dest_url = adGroupAdDataPullRequest
                  .adGroupAdObject
                  .adGroupObject
                  .campaignObject
                  .customerObject
                  .customer
                  .get
                  .getTrackingUrlTemplate.replace(
                  landingPageTag,
                  adGroupAdDataPullRequest.adGroupAdObject.adGroupAd.getAd.getFinalUrls.headOption.getOrElse("")
                )

                adGroupAdDataPullRequest.adGroupAdObject.adGroupAd.getAd.getUrlCustomParameters.getParameters.foreach(
                  p =>
                    dest_url = dest_url.replace(p.getKey, p.getValue)
                )

                adgroupad.destination_url = Some(dest_url)
              }
              adgroupad.status = adGroupAdDataPullRequest.adGroupAdObject.adGroupAd.getStatus.getValue

              using(GoogleDB.session) {
                GoogleDB.adThem.update(adgroupad)
              }
              log.info("UPDATING GOOGLE AD! %s for %s".format(
                adgroupad.toString,
                gson.toJson(adGroupAdDataPullRequest.adGroupAdObject.adGroupAd)
              ))
            }
          }
        } else {
          val accountInfo = using(GoogleDB.session) {
            from(GoogleDB.accountInfo)(ai =>
              where(ai.customer_id === Some(
                adGroupAdDataPullRequest.adGroupAdObject.adGroupObject.campaignObject.customerObject.customer.get.getCustomerId.toString
              )) select ai
            ).toIndexedSeq
          }

          var dest_url = adGroupAdDataPullRequest
            .adGroupAdObject
            .adGroupObject
            .campaignObject
            .customerObject
            .customer
            .get
            .getTrackingUrlTemplate.replace(
            landingPageTag,
            adGroupAdDataPullRequest.adGroupAdObject.adGroupAd.getAd.getFinalUrls.head
          )

          adGroupAdDataPullRequest.adGroupAdObject.adGroupAd.getAd.getUrlCustomParameters.getParameters.foreach(
            p => dest_url = dest_url.replace(p.getKey, p.getValue)
          )

          if (accountInfo.nonEmpty) {
            using(GoogleDB.session) {
              GoogleDB.adThem.insert(new AdThem(
                ad_group_them_id = None,
                us_id = None,
                client_id = accountInfo.head.client_id,
                api_id = Some(adGroupAdDataPullRequest.adGroupAdObject.adGroupAd.getAd.getId),
                ad_type = Some(adGroupAdDataPullRequest.adGroupAdObject.adGroupAd.getAd.getAdType),
                description1 = None,
                description2 = None,
                destination_url = Some(dest_url),
                disapproved_bool = Some(0),
                display_url = None,
                exemption_request = None,
                headline = None,
                status = adGroupAdDataPullRequest.adGroupAdObject.adGroupAd.getApprovalStatus.getValue,
                removed_bool = 0
              ))
            }
          } else {
            //throw new Exception("Cannot find matching clientId to create new entry")
          }
        }
      } catch {
        case e: Exception =>
          e.printStackTrace()
          log.info("Error Syncing Google AdGroup Ad Actor (%s) to Uber - %s".format(
            adGroupAdDataPullRequest.adGroupAdObject.adGroupAd.getAd.getId,
            e.getMessage
          ))
      }
  }
}