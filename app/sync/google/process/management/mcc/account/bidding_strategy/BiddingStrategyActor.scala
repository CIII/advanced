package sync.google.process.management.mcc.account.bidding_strategy

import Shared.Shared._
import akka.actor.Actor
import akka.event.Logging
import com.google.api.ads.adwords.axis.v201509.cm.SharedBiddingStrategy
import com.mongodb.casbah.Imports._
import com.mongodb.util.JSON
import models.mongodb.google.Google._
import sync.shared.Google.{GoogleBiddingStrategyDataPullRequest, _}

class BiddingStrategyActor extends Actor {
  val log = Logging(context.system, this)

  def receive = {
    case biddingStrategyDataPullRequest: GoogleBiddingStrategyDataPullRequest =>
        try {
          log.info("Processing Incoming Data for Shared Bidding Strategy (%s)".format(
            biddingStrategyDataPullRequest.sharedBiddingStrategyObject.sharedBiddingStrategy.getId
          ))

          val qry = DBObject(
            "mccObjId" -> biddingStrategyDataPullRequest.sharedBiddingStrategyObject.customerObject.mccObject.mccObjId,
            "customerObjId" -> biddingStrategyDataPullRequest.sharedBiddingStrategyObject.customerObject.customerObjId,
            "apiId" -> biddingStrategyDataPullRequest.sharedBiddingStrategyObject.sharedBiddingStrategy.getId
          )

          val newData = DBObject(
            "startTsecs" -> java.lang.System.currentTimeMillis() / 1000,
            "endTsecs" -> -1,
            "classPath" -> biddingStrategyDataPullRequest.sharedBiddingStrategyObject.sharedBiddingStrategy.getClass.getCanonicalName,
            "object" -> JSON.parse(gson.toJson(biddingStrategyDataPullRequest.sharedBiddingStrategyObject.sharedBiddingStrategy)).asInstanceOf[DBObject]
          )

          var matchFound = true

          googleBiddingStrategyCollection.findOne(
            qry ++ ("biddingStrategy.endTsecs" -> -1)
          ) match {
            case Some(biddingStrategyRs) =>
              if (
                !gson.toJson(dboToGoogleEntity[SharedBiddingStrategy](biddingStrategyRs, "biddingStrategy", None))
                  .equals(gson.toJson(biddingStrategyDataPullRequest.sharedBiddingStrategyObject.sharedBiddingStrategy))
              ) {
                googleBiddingStrategyCollection.update(
                  qry ++ ("biddingStrategy.endTsecs" -> -1),
                  DBObject("$set" -> DBObject("biddingStrategy.0.endTsecs" -> java.lang.System.currentTimeMillis() / 1000))
                )
                matchFound = false
              }
            case _ =>
              matchFound = false
          }
          if(!matchFound) {
            googleBiddingStrategyCollection.update(qry, DBObject("$push" -> DBObject("biddingStrategy" -> newData)), upsert = true)
          }
        } catch {
          case e: Exception =>
            log.info("Error Retrieving Data for Google Shared Bidding Strategy (%s) - %s".format(
              biddingStrategyDataPullRequest.sharedBiddingStrategyObject.sharedBiddingStrategy.getName,
              e.getMessage
            ))
        }
    case cache: PendingCacheStructure =>
      log.info("Processing Shared Bidding Strategy %s -> %s -> %s -> %s".format(
        cache.changeCategory,
        cache.changeType,
        cache.trafficSource,
        cache.id
      ))
  }
}