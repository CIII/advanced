package sync.google.process.management.mcc.account.bidding_strategy

import akka.actor.Actor
import akka.event.Logging
import sync.shared.Google.GoogleBiddingStrategyDataPullRequest

class BiddingStrategySyncActor extends Actor {
  val log = Logging(context.system, this)

  def receive = {
    case biddingStrategyDataPullRequest: GoogleBiddingStrategyDataPullRequest =>
      try {
        log.info("Processing Sync for Shared Bidding Strategy (%s) to Uber".format(
          biddingStrategyDataPullRequest.sharedBiddingStrategyObject.sharedBiddingStrategy.getId
        ))
        log.info("Nothing to do...")
      } catch {
        case e: Exception =>
          log.info("Error Syncing Google Shared Bidding Strategy (%s) to Uber - %s".format(
            biddingStrategyDataPullRequest.sharedBiddingStrategyObject.sharedBiddingStrategy.getName,
            e.getMessage
          ))
      }
  }
}