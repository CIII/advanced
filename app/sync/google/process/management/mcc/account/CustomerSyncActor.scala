package sync.google.process.management.mcc.account

import java.sql.ResultSet

import Shared.Shared._
import akka.actor.Actor
import akka.event.Logging
import models.agile_production_db.google.{AccountThem, AccountThemHistory, GoogleDB}
import org.squeryl.PrimitiveTypeMode._
import org.squeryl.Query
import sync.shared.Google.GoogleCustomerDataPullRequest


class CustomerSyncActor extends Actor {
  val log = Logging(context.system, this)

  def receive = {
    case (
      customerDataPullRequest: GoogleCustomerDataPullRequest,
      tsecs: Long
    ) =>
      try {
        log.info("Processing Sync for Account (%s) to Uber".format(
          customerDataPullRequest.customerObject.customer.get.getCustomerId
        ))

        val previousData = using(GoogleDB.session) {
          from(GoogleDB.accountThem)(at =>
            where(at.customer_id === Some(customerDataPullRequest.customerObject.customer.get.getCustomerId.toString)) select at
          ).toIndexedSeq
        }

        if (previousData.nonEmpty) {
          for (account <- previousData) {
            if (
              !account.customer_id.getOrElse("").equals(customerDataPullRequest.customerObject.customer.get.getCustomerId.toString) ||
                !account.currency_code.getOrElse("").equals(customerDataPullRequest.customerObject.customer.get.getCurrencyCode) ||
                !account.name.getOrElse("").equals(customerDataPullRequest.customerObject.customer.get.getCompanyName) ||
                !account.time_zone_id.getOrElse("").equals(customerDataPullRequest.customerObject.customer.get.getDateTimeZone)
            ) {
              using(GoogleDB.session) {
                GoogleDB.accountThemHistory.insert(new AccountThemHistory(
                  them_id = account.id,
                  tsecs = tsecs,
                  job_id = None,
                  operation = None,
                  us_id = Some(account.us_id.getOrElse("0").toInt),
                  client_id = account.client_id,
                  tid = account.tid,
                  account_id = account.account_id,
                  name = account.name,
                  customer_id = account.customer_id,
                  currency_code = account.currency_code,
                  default_network_targeting_google_search_bool = account.default_network_targeting_google_search_bool,
                  default_network_targeting_search_network_bool = account.default_network_targeting_search_network_bool,
                  default_network_targeting_content_network_bool = account.default_network_targeting_content_network_bool,
                  email_promotions_preferences_account_performance_enabled_bool = account.email_promotions_preferences_account_performance_enabled_bool,
                  email_promotions_preferences_disapproved_ads_enabled_bool = account.email_promotions_preferences_disapproved_ads_enabled_bool,
                  email_promotions_preferences_marketResearch_enabled_bool = account.email_promotions_preferences_marketResearch_enabled_bool,
                  email_promotions_preferences_newsletter_enabled_bool = account.email_promotions_preferences_newsletter_enabled_bool,
                  email_promotions_preferences_promotions_enabled_bool = account.email_promotions_preferences_promotions_enabled_bool,
                  language_preference = account.language_preference,
                  primary_business_category = account.primary_business_category,
                  time_zone_effective_date = account.time_zone_effective_date,
                  time_zone_id = account.time_zone_id,
                  client_email = account.client_email,
                  removed_bool = account.removed_bool
                ))
              }

              account.currency_code = Some(customerDataPullRequest.customerObject.customer.get.getCurrencyCode)
              account.name = Some(customerDataPullRequest.customerObject.managedCustomer.getName)
              account.time_zone_id = Some(customerDataPullRequest.customerObject.customer.get.getDateTimeZone)
              using(GoogleDB.session) {
                GoogleDB.accountThem.update(account)
              }
              log.info("UPDATING GOOGLE ACCOUNT! %s for %s".format(
                account.toString,
                gson.toJson(customerDataPullRequest.customerObject.customer.get)
              ))
            }
          }
        } else {
          //FIND CLIENT ID
          val accountInfo = using(GoogleDB.session) {
            from(GoogleDB.accountInfo)(ai =>
              where(ai.customer_id === Some(customerDataPullRequest.customerObject.customer.get.getCustomerId.toString)) select ai
            ).toIndexedSeq
          }

          if (accountInfo.nonEmpty) {
            //INSERT NEW RECORD
            using(GoogleDB.session) {
              GoogleDB.accountThem.insert(new AccountThem(
                us_id = None,
                client_id = accountInfo.head.client_id,
                tid = accountInfo.head.tid,
                account_id = accountInfo.head.id,
                name = Some(customerDataPullRequest.customerObject.customer.get.getCompanyName),
                customer_id = Some(customerDataPullRequest.customerObject.customer.get.getCustomerId.toString),
                currency_code = Some(customerDataPullRequest.customerObject.customer.get.getCurrencyCode),
                default_network_targeting_google_search_bool = None,
                default_network_targeting_content_network_bool = None,
                default_network_targeting_search_network_bool = None,
                email_promotions_preferences_account_performance_enabled_bool = None,
                email_promotions_preferences_disapproved_ads_enabled_bool = None,
                email_promotions_preferences_marketResearch_enabled_bool = None,
                email_promotions_preferences_newsletter_enabled_bool = None,
                email_promotions_preferences_promotions_enabled_bool = None,
                client_email = None,
                language_preference = None,
                primary_business_category = None,
                time_zone_effective_date = None,
                time_zone_id = None,
                removed_bool = 0
              ))
            }
          } else {
            //throw new Exception("Cannot find matching clientId to create new entry")
          }
        }
      } catch {
        case e: Exception =>
          e.printStackTrace()
          log.info("Error Syncing Google Account (%s) to Uber - %s".format(
            customerDataPullRequest.customerObject.customer.get.getCompanyName,
            e.toString
          ))
      }
  }
}