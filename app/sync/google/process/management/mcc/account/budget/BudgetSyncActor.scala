package sync.google.process.management.mcc.account.budget

import akka.actor.Actor
import akka.event.Logging
import sync.shared.Google.GoogleBudgetDataPullRequest

class BudgetSyncActor extends Actor {
  val log = Logging(context.system, this)

  def receive = {
    case budgetDataPullRequest: GoogleBudgetDataPullRequest =>
      try {
        log.info("Processing Sync for Budget (%s) to Uber".format(
          budgetDataPullRequest.budgetObject.budget.getBudgetId
        ))
        log.info("Nothing to do...")
      } catch {
        case e: Exception =>
          log.info("Error Syncing Google Budget(%s) to Uber - %s".format(
            budgetDataPullRequest.budgetObject.budget.getName,
            e.getMessage
          ))
      }
  }
}