package sync.google.process.management.mcc.account.budget

import Shared.Shared._
import akka.actor.Actor
import akka.event.Logging
import com.google.api.ads.adwords.axis.v201509.cm.Budget
import com.mongodb.casbah.Imports._
import com.mongodb.util.JSON
import models.mongodb.google.Google._
import sync.shared.Google.{GoogleBudgetDataPullRequest, _}


class BudgetActor extends Actor {
  val log = Logging(context.system, this)

  def receive = {
    case budgetDataPullRequest: GoogleBudgetDataPullRequest =>
        try {
          log.info("Processing Incoming Data for Budget (%s)".format(
            budgetDataPullRequest.budgetObject.budget.getBudgetId
          ))

          val qry = DBObject(
            "mccObjId" -> budgetDataPullRequest.budgetObject.customerObject.mccObject.mccObjId,
            "customerObjId" -> budgetDataPullRequest.budgetObject.customerObject.customerObjId
          )

          val newData = DBObject(
            "startTsecs" -> java.lang.System.currentTimeMillis() / 1000,
            "endTsecs" -> -1,
            "classPath" -> budgetDataPullRequest.budgetObject.budget.getClass.getCanonicalName,
            "object" -> JSON.parse(gson.toJson(budgetDataPullRequest.budgetObject.budget)).asInstanceOf[DBObject]
          )

          var matchFound = true

          googleCriterionCollection.findOne(
            qry ++ ("budget.endTsecs" -> -1)
          ) match {
            case Some(budgetRs) =>
              if (
                !gson.toJson(dboToGoogleEntity[Budget](budgetRs, "budget", None))
                  .equals(gson.toJson(budgetDataPullRequest.budgetObject.budget))
              ) {
                googleBudgetCollection.update(
                  qry ++ ("budget.endTsecs" -> -1),
                  DBObject("$set" -> DBObject("budget.0.endTsecs" -> java.lang.System.currentTimeMillis() / 1000))
                )
                matchFound = false
              }
            case _ =>
              matchFound = false
          }
          if(matchFound) {
            googleBudgetCollection.update(qry, DBObject("$push" -> DBObject("budget" -> newData)), upsert = true)
          }
        } catch {
          case e: Exception =>
            log.info("Error Retrieving Data for Google Budget(%s) - %s".format(
              budgetDataPullRequest.budgetObject.budget.getName,
              e.getMessage
            ))
        }
    case cache: PendingCacheStructure =>
      log.info("Processing Budget %s -> %s -> %s -> %s".format(
        cache.changeCategory,
        cache.changeType,
        cache.trafficSource,
        cache.id
      ))
  }
}