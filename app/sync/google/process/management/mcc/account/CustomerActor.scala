package sync.google.process.management.mcc.account

import Shared.Shared._
import akka.actor.Actor
import akka.event.Logging
import com.google.api.ads.adwords.axis.v201509.mcm.{Customer, CustomerServiceInterface}
import com.mongodb.casbah.Imports._
import com.mongodb.util.JSON
import models.mongodb.google.Google._
import sync.google.adwords.AdWordsHelper
import sync.google.adwords.account._
import sync.shared.Google._

class CustomerActor extends Actor {
  val log = Logging(context.system, this)

  def receive = {
    case customerDataPullRequest: GoogleCustomerDataPullRequest =>
      try {
        log.info("Processing Incoming Data for Account (%s)".format(
          customerDataPullRequest.customerObject.managedCustomer.getCustomerId
        ))

        customerDataPullRequest.adWordsHelper = Some(
          new AdWordsHelper(
            customerDataPullRequest.customerObject.mccObject.mcc.oAuthClientId,
            customerDataPullRequest.customerObject.mccObject.mcc.oAuthClientSecret,
            customerDataPullRequest.customerObject.mccObject.mcc.oAuthRefreshToken,
            customerDataPullRequest.customerObject.mccObject.mcc.developerToken,
            Some(customerDataPullRequest.customerObject.managedCustomer.getCustomerId.toString),
            enablePartialFailure = false
          )
        )

        customerDataPullRequest.customerObject.customer = Some(
          customerDataPullRequest.adWordsHelper.get.adWordsServices.get(
            customerDataPullRequest.adWordsHelper.get.adWordsSession,
            classOf[CustomerServiceInterface]
          ).get
        )

        val qry = DBObject(
          "mccObjId" -> customerDataPullRequest.customerObject.mccObject.mccObjId,
          "apiId" -> customerDataPullRequest.customerObject.managedCustomer.getCustomerId
        )

        val startTsecs = java.lang.System.currentTimeMillis() / 1000
        val endTsecs = java.lang.System.currentTimeMillis() / 1000

        val newData = DBObject(
          "startTsecs" -> startTsecs,
          "endTsecs" -> -1,
          "classPath" -> classOf[Customer].getCanonicalName,
          "object" -> JSON.parse(
            gson.toJson(customerDataPullRequest.customerObject.customer.get)
          ).asInstanceOf[DBObject]
        )

        var matchFound = true

        googleCustomerCollection.findOne(
          qry ++ ("customer.endTsecs" -> -1)
        ) match {
          case Some(customerRs) =>
            if (
              !gson.toJson(dboToGoogleEntity[Customer](customerRs, "customer", None))
                .equals(gson.toJson(customerDataPullRequest.customerObject.managedCustomer))
            ) {
              googleCustomerCollection.update(
                qry ++ ("customer.endTsecs" -> -1),
                DBObject("$set" -> DBObject("customer.0.endTsecs" -> endTsecs))
              )
              matchFound = false
              log.debug("Google Customer match found. Changes detected. Updating...")
            }
          case _ =>
            matchFound = false
            log.debug("No Google Customer record Found. Inserting...")
        }

        if(!matchFound) {
          googleCustomerCollection.update(qry, DBObject("$push" -> DBObject("customer" -> newData)), upsert = true)

          if (customerDataPullRequest.pushToUber)
            googleAccountSyncActor ! (customerDataPullRequest, endTsecs)
        }

        if(customerDataPullRequest.recursivePull) {

          customerDataPullRequest.customerObject.customerObjId = googleCustomerCollection.findOne(qry).get._id

          val accountHelper = new CustomerHelper(customerDataPullRequest.adWordsHelper.get, log)
          val campaignHelper = new CampaignHelper(customerDataPullRequest.adWordsHelper.get, log)

          accountHelper.getBudgets(budgetFields).foreach(budget => googleBudgetActor ! GoogleBudgetDataPullRequest(
            customerDataPullRequest.adWordsHelper.get,
            BudgetObject(
              customerDataPullRequest.customerObject,
              None,
              budget
            ),
            pushToUber = true
          ))
          accountHelper.getSharedBiddingStrategies(sharedBiddingStrategyFields).foreach(strategy => googleBiddingStrategyActor ! GoogleBiddingStrategyDataPullRequest(
            customerDataPullRequest.adWordsHelper.get,
            SharedBiddingStrategyObject(
              customerDataPullRequest.customerObject,
              None,
              strategy
            ),
            pushToUber = true
          ))

          campaignHelper.getCampaigns(campaignFields).foreach(campaign => googleCampaignActor ! GoogleCampaignDataPullRequest(
            adWordsHelper = customerDataPullRequest.adWordsHelper.get,
            campaignObject = CampaignObject(
              customerDataPullRequest.customerObject,
              None,
              campaign
            ),
            recursivePull = customerDataPullRequest.recursivePull,
            pushToUber = true
          ))
        }
      } catch {
        case e: Exception =>
          log.info("Error Retrieving Data for Google Account (%s) - %s".format(
            customerDataPullRequest.customerObject.managedCustomer.getName,
            e.toString
          ))
          e.printStackTrace()
      }
  }
}