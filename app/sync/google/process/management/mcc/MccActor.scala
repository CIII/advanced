package sync.google.process.management.mcc

import Shared.Shared._
import akka.actor.Actor
import akka.event.Logging
import com.mongodb.casbah.Imports.{ObjectId, _}
import models.mongodb.google.Google._
import sync.google.adwords.AdWordsHelper
import sync.google.adwords.account._
import sync.shared.Google._

class MccActor extends Actor {
  val log = Logging(context.system, this)

  def receive = {
    case cache_msg: PendingCacheMessage =>
      val cache: PendingCacheStructure = cache_msg.cache.get
      try {
        val mcc_data = dboToMcc(cache.changeData.asDBObject)
        log.info("Processing MCC %s -> %s -> %s -> %s".format(
          cache.changeCategory,
          cache.changeType,
          cache.trafficSource,
          cache.id
        ))

        var mcc: Option[models.mongodb.google.Google.Mcc] = None
        googleMccCollection.findOne(DBObject("oAuthRefreshToken" -> mcc_data.oAuthRefreshToken)) match {
          case Some(existingMcc) =>
            mcc = Some(models.mongodb.google.Google.Mcc(
              existingMcc._id,
              mcc_data.name,
              mcc_data.developerToken,
              mcc_data.oAuthClientId,
              mcc_data.oAuthClientSecret,
              mcc_data.oAuthRefreshToken
            ))
          case None =>
            mcc = Some(models.mongodb.google.Google.Mcc(
              Some(new ObjectId),
              mcc_data.name,
              mcc_data.developerToken,
              mcc_data.oAuthClientId,
              mcc_data.oAuthClientSecret,
              mcc_data.oAuthRefreshToken
            ))
        }

        googleMccCollection.update(DBObject("oAuthRefreshToken" -> mcc.get.oAuthRefreshToken), mccToDBObject(mcc.get), upsert=true)

        val adWordsHelper = new AdWordsHelper(
          clientId = mcc_data.oAuthClientId,
          clientSecret = mcc_data.oAuthClientSecret,
          refreshToken = mcc_data.oAuthRefreshToken,
          developerToken = mcc_data.developerToken,
          customerId = None
        )
        val customerHelper = new CustomerHelper(adWordsHelper, log)
        val accounts = customerHelper.getManagedCustomers(managedCustomerFields, None)

        set_subprocess_count(taskKey(Left(cache_msg.request.get)), cache, 1)

        log.info("Accounts to iterate over: " + accounts.map(_.getName).mkString(", "))

        accounts.foreach(acc =>
          googleAccountActor ! GoogleCustomerDataPullRequest(
            None,
            CustomerObject(MccObject(mcc.get._id.get, mcc.get), None, acc, None),
            recursivePull = true,
            pushToUber = true
          )
        )
        complete_subprocess(taskKey(Left(cache_msg.request.get)), cache)
      } catch {
        case e: Exception =>
          log.info("Error Retrieving Data for MCC (%s) - %s".format(
            cache.id,
            e.getMessage
          ))
      }
    case _ =>
      try {
        googleMccCollection.find().toArray.foreach {
          mcc_obj =>
            val mcc = dboToMcc(mcc_obj.asDBObject)
            val adWordsHelper = new AdWordsHelper(
              clientId = mcc.oAuthClientId,
              clientSecret = mcc.oAuthClientSecret,
              refreshToken = mcc.oAuthRefreshToken,
              developerToken = mcc.developerToken,
              customerId = None
            )
            val accountHelper = new CustomerHelper(adWordsHelper, log)
            val accounts = accountHelper.getManagedCustomers(managedCustomerFields, None)

            log.info("Accounts to iterate over: " + accounts.map(_.getName).mkString(", "))

            accounts.foreach(acc => googleAccountActor ! GoogleCustomerDataPullRequest(
              None,
              CustomerObject(MccObject(mcc._id.get, mcc), None, acc, None),
              recursivePull = true,
              pushToUber = true
            ))
        }
      } catch {
        case e: Exception =>
          log.info("Error Retrieving Incremental Data for MCC - %s".format(
            e.toString
          ))
          e.printStackTrace()
      }
  }
}