package sync.google.adwords.account

import com.google.api.ads.adwords.axis.utils.v201409.SelectorBuilder
import com.google.api.ads.adwords.axis.v201409.cm._
import sync.google.adwords.AdWordsHelper
import scala.collection.mutable.ListBuffer
import scala.util.control.Breaks._

class AdHelper(AdWords: AdWordsHelper) {

  def getAds(fields: List[String], predicates: Option[Array[Predicate]]): List[AdGroupAd] = {
    val adGroupAdService = AdWords.adWordsServices.get(AdWords.adWordsSession, classOf[AdGroupAdServiceInterface])
    var offset: Int = 0
    val builder = new SelectorBuilder()

    val paging = new Paging {
      setNumberResults(AdWords.PAGE_SIZE)
      setStartIndex(offset)
    }

    val selector: Selector = builder
      .offset(offset)
      .limit(AdWords.PAGE_SIZE)
      .fields(fields: _*)
      .build()

    selector.setPredicates(predicates.orNull)
    selector.setPaging(paging)
    var morePages: Boolean = true
    var results = ListBuffer[AdGroupAd]()
    breakable {
      while (morePages) {
        var success = false
        var page: AdGroupAdPage = null
        while(!success) {
          try {
            page = adGroupAdService.get(selector)
            success = true
          } catch {
            case e: Exception =>
              println("Error Retrieving AdGroupAdPage (" + e.toString + ")...retrying after 30 secs.")
              Thread.sleep(31000)
          }
        }
        if (page.getEntries != null) {
          results = results ++ page.getEntries.toList
          offset += AdWords.PAGE_SIZE
          paging.setStartIndex(offset)
          selector.setPaging(paging)
          morePages = offset < page.getTotalNumEntries
        } else {
          break()
        }
      }
    }
      results.toList
    }
}
