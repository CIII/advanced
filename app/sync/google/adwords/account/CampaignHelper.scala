package sync.google.adwords.account

import akka.event.LoggingAdapter
import com.google.api.ads.adwords.axis.utils.v201509.SelectorBuilder
import com.google.api.ads.adwords.axis.v201509.cm.{Selector, _}
import com.google.api.ads.adwords.lib.selectorfields.v201509.cm.{CampaignCriterionField, CampaignField}
import sync.google.adwords.AdWordsHelper

import scala.collection.mutable._
import scala.util.control.Breaks._

class CampaignHelper(AdWords: AdWordsHelper, log: LoggingAdapter) {
  private val campaignService = AdWords.adWordsServices.get(AdWords.adWordsSession, classOf[CampaignServiceInterface])
  private val campaignCriterionService = AdWords.adWordsServices.get(AdWords.adWordsSession, classOf[CampaignCriterionServiceInterface])

  def getCampaigns(fields: List[CampaignField], predicates: Option[Array[Predicate]]=None, sel: Option[Selector]=None): List[Campaign] = {
    var offset = 0
    val paging = new Paging()

    paging.setNumberResults(AdWords.PAGE_SIZE)
    paging.setStartIndex(offset)
    val selector = sel match {
      case Some(s) => s
      case _ =>
        (new SelectorBuilder)
          .offset(offset)
          .fields(fields: _*)
          .limit(AdWords.PAGE_SIZE)
          .build()
    }

    predicates match {
      case Some(p) =>
        selector.setPredicates(p)
      case _ =>
    }

    var morePages = true
    var page: CampaignPage = null
    var results = ListBuffer[Campaign]()
    breakable {
      while (morePages) {
        var success = false
        var page: CampaignPage = null
        while(!success) {
          try {
            page = campaignService.get(selector)
            success = true
          } catch {
            case ae: ApiException =>
              AdWords.sleepIfRequired(ae, log, "CampaignService")
          }
        }
        if (page.getEntries != null) {
          results = results ++ page.getEntries.toList
          offset += AdWords.PAGE_SIZE
          paging.setStartIndex(offset)
          selector.setPaging(paging)
          morePages = offset < page.getTotalNumEntries
        } else {
          break()
        }
      }
    }
    results.toList
  }

  def getCampaignCriterion(fields: List[CampaignCriterionField], predicates: Option[Array[Predicate]]=None, sel: Option[Selector]=None): List[CampaignCriterion] = {
    var offset = 0
    val paging = new Paging()
    paging.setNumberResults(AdWords.PAGE_SIZE)
    paging.setStartIndex(offset)
    val selector = sel match {
      case Some(s) => s
      case _ =>
        (new SelectorBuilder)
          .offset(offset)
          .fields(fields: _*)
          .limit(AdWords.PAGE_SIZE)
          .build()
    }

    predicates match {
      case Some(p) =>
        selector.setPredicates(p)
      case _ =>
    }

    var morePages = true
    var results = ListBuffer[CampaignCriterion]()
    breakable {
      while (morePages) {
        var success = false
        var page: CampaignCriterionPage = null
        while(!success) {
          try {
            page = campaignCriterionService.get(selector)
            success = true
          } catch {
            case ae: ApiException =>
              AdWords.sleepIfRequired(ae, log, "CampaignCriterionService")
          }
        }
        if (page.getEntries != null) {
          results = results ++ page.getEntries.toList
          offset += AdWords.PAGE_SIZE
          paging.setStartIndex(offset)
          selector.setPaging(paging)
          morePages = offset < page.getTotalNumEntries
        } else {
          break()
        }
      }
    }
    results.toList
  }

  def createCampaign(campaign: Campaign): CampaignReturnValue = {
    val campaignOperation = new CampaignOperation
    campaignOperation.setOperator(Operator.ADD)
    campaignOperation.setOperand(campaign)
    campaignService.mutate(Array(campaignOperation))
  }

  def deleteCampaign(campaign: Campaign): CampaignReturnValue = {
    val campaignOperation = new CampaignOperation
    campaignOperation.setOperator(Operator.REMOVE)
    campaignOperation.setOperand(campaign)
    campaignService.mutate(Array(campaignOperation))
  }

  def updateCampaign(campaign: Campaign): CampaignReturnValue = {
    val campaignOperation = new CampaignOperation
    campaignOperation.setOperator(Operator.SET)
    campaignOperation.setOperand(campaign)
    campaignService.mutate(Array(campaignOperation))
  }
}
