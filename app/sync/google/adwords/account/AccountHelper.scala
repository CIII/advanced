package sync.google.adwords.account

import java.text.SimpleDateFormat
import java.util.Date

import com.google.api.ads.adwords.axis.utils.v201409.SelectorBuilder
import com.google.api.ads.adwords.axis.v201409.ch.{CustomerChangeData, CustomerSyncSelector, CustomerSyncServiceInterface}
import com.google.api.ads.adwords.axis.v201409.cm.{Campaign, CampaignPage, CampaignServiceInterface, DateTimeRange, Selector, _}
import com.google.api.ads.adwords.axis.v201409.mcm.{CustomerServiceInterface, ManagedCustomer, ManagedCustomerPage, ManagedCustomerServiceInterface}
import com.google.api.ads.adwords.lib.utils.v201409.ReportDownloader
import org.apache.commons.lang.ArrayUtils
import play.api.Logger
import sync.google.adwords.AdWordsHelper

import scala.collection.JavaConverters._
import scala.collection.mutable.ListBuffer
import scala.util.control.Breaks._

/**
 * Helper Class for AdWords Account
 */
class AccountHelper(AdWords: AdWordsHelper) {

  def getAccountChanges: Option[CustomerChangeData] = {
    val campaignService: CampaignServiceInterface = AdWords.adWordsServices.get(AdWords.adWordsSession, classOf[CampaignServiceInterface])
    val customerSyncService = AdWords.adWordsServices.get(AdWords.adWordsSession, classOf[CustomerSyncServiceInterface])

    val campaignIds = List[java.lang.Long]()
    val selector: Selector = new SelectorBuilder()
      .fields("Id")
      .build()
    val campaigns: CampaignPage = campaignService.get(selector)
    if (campaigns.getEntries != null) {
      for (campaign: Campaign <- campaigns.getEntries) {
        campaignIds :+ campaign.getId
      }
    }

    val dateTimeRange: DateTimeRange = new DateTimeRange()
    dateTimeRange.setMin(new SimpleDateFormat("yyyyMMdd HHmmss").format(new Date(System.currentTimeMillis() - 1000L * 60 * 60 * 24)))
    dateTimeRange.setMax(new SimpleDateFormat("yyyyMMdd HHmmss").format(new Date()))

    val customerSyncSelector: CustomerSyncSelector = new CustomerSyncSelector()
    customerSyncSelector.setDateTimeRange(dateTimeRange)
    customerSyncSelector.setCampaignIds(ArrayUtils.toPrimitive(campaignIds.toArray))

    val accountChanges: CustomerChangeData = customerSyncService.get(customerSyncSelector)


    if (accountChanges != null && accountChanges.getChangedCampaigns != null) {
      Some(accountChanges)
    } else {
      None
    }
  }

  def getAccounts(fields: List[String], predicates: Option[Array[Predicate]]): List[ManagedCustomer] = {
    val customerService = AdWords.adWordsServices.get(AdWords.adWordsSession, classOf[CustomerServiceInterface])

    val customer = customerService.get

    AdWords.adWordsSession.setClientCustomerId(customer.getCustomerId.toString)

    val managedCustomerService = AdWords.adWordsServices.get(AdWords.adWordsSession, classOf[ManagedCustomerServiceInterface])
    var offset: Int = 0
    val builder = new SelectorBuilder()
    var selector: Selector = builder
      .offset(offset)
      .fields(fields: _*)
      .limit(AdWords.PAGE_SIZE)
      .build()

    selector.setPredicates(predicates.getOrElse(Array()))

    var morePages: Boolean = true
    var page: ManagedCustomerPage = null
    var results = ListBuffer[ManagedCustomer]()
    breakable {
      while (morePages) {
        page = managedCustomerService.get(selector)
        if(page.getEntries != null) {
          results = results ++ page.getEntries
          offset += AdWords.PAGE_SIZE
          selector = builder.increaseOffsetBy(AdWords.PAGE_SIZE).build()
          morePages = offset < page.getTotalNumEntries
        } else {
          break()
        }
      }
    }
    results.toList
  }

  def getBudgets(fields: List[String], predicates: Option[Array[Predicate]]): List[Budget] = {
    val budgetService = AdWords.adWordsServices.get(AdWords.adWordsSession, classOf[BudgetServiceInterface])
    var offset: Int = 0
    val builder = new SelectorBuilder()
    var selector: Selector = builder
      .offset(offset)
      .fields(fields: _*)
      .limit(AdWords.PAGE_SIZE)
      .build()

    selector.setPredicates(predicates.getOrElse(Array()))

    var morePages: Boolean = true
    var page: BudgetPage = null
    var results = ListBuffer[Budget]()
    breakable {
      while (morePages) {
        page = budgetService.get(selector)
        if (page.getEntries != null) {
          results = results ++ page.getEntries
          offset += AdWords.PAGE_SIZE
          selector = builder.increaseOffsetBy(AdWords.PAGE_SIZE).build()
          morePages = offset < page.getTotalNumEntries
        } else {
          break()
        }
      }
    }
    results.toList
  }

  def getSharedBiddingStrategy(fields: List[String], predicates: Option[Array[Predicate]]): List[SharedBiddingStrategy] = {
    val biddingStrategyService = AdWords.adWordsServices.get(AdWords.adWordsSession, classOf[BiddingStrategyServiceInterface])
    var offset: Int = 0
    val builder = new SelectorBuilder()
    var selector: Selector = builder
      .offset(offset)
      .fields(fields: _*)
      .limit(AdWords.PAGE_SIZE)
      .build()

    selector.setPredicates(predicates.getOrElse(Array()))

    var morePages: Boolean = true
    var page: BiddingStrategyPage = null
    var results = ListBuffer[SharedBiddingStrategy]()
    breakable {
      while (morePages) {
        page = biddingStrategyService.get(selector)
        if(page.getEntries != null) {
          results = results ++ page.getEntries
          offset += AdWords.PAGE_SIZE
          selector = builder.increaseOffsetBy(AdWords.PAGE_SIZE).build()
          morePages = offset < page.getTotalNumEntries
        } else {
          break()
        }
      }
    }
    results.toList
  }

  def downloadReport(
    reportType: com.google.api.ads.adwords.lib.jaxb.v201409.ReportDefinitionReportType,
    fields: Option[List[String]],
    predicates: Option[List[com.google.api.ads.adwords.lib.jaxb.v201409.Predicate]],
    dateRange: com.google.api.ads.adwords.lib.jaxb.v201409.ReportDefinitionDateRangeType,
    downloadFormat: com.google.api.ads.adwords.lib.jaxb.v201409.DownloadFormat,
    enableZeroImpressions: Boolean = false
  ): Option[String] = {
    try {
      var reportFields = fields.getOrElse(List())
      val reportPredicates = predicates.getOrElse(List())
      fields match {
        case Some(x) =>
        case None =>
          val reportDefinitionService = AdWords.adWordsServices.get(AdWords.adWordsSession, classOf[ReportDefinitionServiceInterface])
          reportDefinitionService.getReportFields(
            com.google.api.ads.adwords.axis.v201409.cm.ReportDefinitionReportType.fromValue(reportType.value)
          ).map(x =>
            x.getFieldName match {
              case "ConversionRateManyPerClickSignificance" =>
              case "ConversionRateSignificance" =>
              case "ViewThroughConversionsSignificance" =>
              case "AdvertiserExperimentSegmentationBin" =>
              case _ =>
                reportFields = reportFields :+ x.getFieldName
            })
      }
      val selector = new com.google.api.ads.adwords.lib.jaxb.v201409.Selector()
      selector.getFields.addAll(reportFields.asJava)
      selector.getPredicates.addAll(reportPredicates.asJava)

      val reportDefinition = new com.google.api.ads.adwords.lib.jaxb.v201409.ReportDefinition()
      reportDefinition.setReportName("Criteria performance report # " + System.currentTimeMillis())
      reportDefinition.setReportType(reportType)
      reportDefinition.setDownloadFormat(downloadFormat)
      reportDefinition.setDateRangeType(dateRange)
      reportDefinition.setIncludeZeroImpressions(enableZeroImpressions)
      reportDefinition.setSelector(selector)

      val response = new ReportDownloader(AdWords.adWordsSession).downloadReport(reportDefinition)
      val scanner = new java.util.Scanner(response.getInputStream, "UTF-8").useDelimiter("\\A")
      scanner.hasNext match {
        case true =>
          Some(scanner.next)
        case _ => None
      }
    } catch {
      case rde: ReportDefinitionError =>
        Logger.error(rde.getMessage)
        None
    }
  }
}