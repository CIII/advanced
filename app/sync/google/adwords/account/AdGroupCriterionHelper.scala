package sync.google.adwords.account

import akka.event.LoggingAdapter
import com.google.api.ads.adwords.axis.utils.v201509.SelectorBuilder
import com.google.api.ads.adwords.axis.v201509.cm._
import com.google.api.ads.adwords.axis.v201509.o.{AttributeType, IdeaType, LanguageSearchParameter, RelatedToQuerySearchParameter, RequestType, SearchParameter, SeedAdGroupIdSearchParameter, TargetingIdea, TargetingIdeaPage, TargetingIdeaSelector, TargetingIdeaServiceInterface}
import com.google.api.ads.adwords.lib.selectorfields.v201509.cm.AdGroupCriterionField
import sync.google.adwords.AdWordsHelper

import scala.collection.mutable._
import scala.util.control.Breaks._


class AdGroupCriterionHelper(AdWords: AdWordsHelper, log: LoggingAdapter) {

  private val dataService: DataServiceInterface = AdWords.adWordsServices.get(AdWords.adWordsSession, classOf[DataServiceInterface])
  private val targetingIdeaService: TargetingIdeaServiceInterface = AdWords.adWordsServices.get(AdWords.adWordsSession, classOf[TargetingIdeaServiceInterface])
  private val adGroupCriterionService: AdGroupCriterionServiceInterface = AdWords.adWordsServices.get(AdWords.adWordsSession, classOf[AdGroupCriterionServiceInterface])


  def getKeywordBidSimulations(fields: List[AdGroupCriterionField], predicates: Option[Array[Predicate]]=None, sel: Option[Selector]=None):  List[CriterionBidLandscape] = {
    var offset = 0
    val paging = new Paging {
      setNumberResults(AdWords.PAGE_SIZE)
      setStartIndex(offset)
    }

    val selector = sel match {
      case Some(s) => s
      case _ =>
        (new SelectorBuilder)
          .offset(offset)
          .fields(fields: _*)
          .limit(AdWords.PAGE_SIZE)
          .build()
    }

    predicates match {
      case Some(p) =>
        selector.setPredicates(p)
      case _ =>
    }

    selector.setPaging(paging)
    var morePages: Boolean = true
    var results = ListBuffer[CriterionBidLandscape]()
    breakable {
      while (morePages) {
        var success = false
        var page: CriterionBidLandscapePage = null
        while(!success) {
          try {
            page = dataService.getCriterionBidLandscape(selector)
            success = true
          } catch {
            case ae: ApiException =>
              AdWords.sleepIfRequired(ae, log, "DataService")
          }
        }
        if (page.getEntries != null) {
          results = results ++ page.getEntries.toList
          offset += AdWords.PAGE_SIZE
          paging.setStartIndex(offset)
          selector.setPaging(paging)
          morePages = offset < page.getTotalNumEntries
        } else {
          break()
        }
      }
    }
    results.toList
  }


  def getKeywordIdeas(adGroupId: Long = 0, seedKeywords: Array[String], requestType: RequestType = RequestType.IDEAS, ideaType: IdeaType = IdeaType.KEYWORD): Option[Array[TargetingIdea]] = {
    val selector: TargetingIdeaSelector = new TargetingIdeaSelector()
    selector.setRequestType(requestType)
    selector.setIdeaType(ideaType)
    selector.setRequestedAttributeTypes(Array(
      AttributeType.KEYWORD_TEXT,
      AttributeType.SEARCH_VOLUME,
      AttributeType.CATEGORY_PRODUCTS_AND_SERVICES))

    val paging: Paging = new Paging()
    paging.setStartIndex(0)
    paging.setNumberResults(10)
    selector.setPaging(paging)

    val searchParameters = List[SearchParameter]()

    if (seedKeywords != null) {
      val relatedToQuerySearchParameter: RelatedToQuerySearchParameter = new RelatedToQuerySearchParameter()
      relatedToQuerySearchParameter.setQueries(seedKeywords)
      searchParameters :+ relatedToQuerySearchParameter
    }

    val languageParameter: LanguageSearchParameter = new LanguageSearchParameter()
    val english: Language = new Language()
    english.setId(1000L)
    languageParameter.setLanguages(Array(english))

    searchParameters :+ languageParameter

    if (adGroupId != 0) {
      val seedAdGroupIdSearchParameter: SeedAdGroupIdSearchParameter = new SeedAdGroupIdSearchParameter()
      seedAdGroupIdSearchParameter.setAdGroupId(adGroupId)
      searchParameters :+ seedAdGroupIdSearchParameter
    }

    selector.setSearchParameters(searchParameters.toArray)

    val page: TargetingIdeaPage = targetingIdeaService.get(selector)

    if (page.getEntries != null && page.getEntries.nonEmpty) {
      Some(page.getEntries)
    } else {
      None
    }
  }

  def getKeywords(fields: List[AdGroupCriterionField], predicates: Option[Array[Predicate]]=None, sel: Option[Selector]=None): List[AdGroupCriterion] = {
    var offset: Int = 0
    val selector = sel match {
      case Some(s) => s
      case _ =>
        (new SelectorBuilder)
          .offset(offset)
          .fields(fields: _*)
          .limit(AdWords.PAGE_SIZE)
          .build()
    }

    val paging = new Paging()

    paging.setNumberResults(AdWords.PAGE_SIZE)
    paging.setStartIndex(offset)

    predicates match {
      case Some(p) =>
        selector.setPredicates(p)
      case _ =>
    }
    var morePages: Boolean = true
    var page: AdGroupCriterionPage = null
    var results = ListBuffer[AdGroupCriterion]()
    breakable {
      while (morePages) {
        var success = false
        var page: AdGroupCriterionPage = null
        while(!success) {
          try {
            page = adGroupCriterionService.get(selector)
            success = true
          } catch {
            case ae: ApiException =>
              AdWords.sleepIfRequired(ae, log, "AdGroupCriterionService")
          }
        }
        if (page.getEntries != null) {
          results = results ++ page.getEntries.toList
          offset += AdWords.PAGE_SIZE
          paging.setStartIndex(offset)
          selector.setPaging(paging)
          morePages = offset < page.getTotalNumEntries
        } else {
          break()
        }
      }
    }
    results.toList
  }

  def getAdGroupCriterion(fields: List[AdGroupCriterionField], predicates: Option[Array[Predicate]]=None, sel: Option[Selector]=None): List[AdGroupCriterion] = {
    var offset: Int = 0

    val paging = new Paging()

    paging.setNumberResults(AdWords.PAGE_SIZE)
    paging.setStartIndex(offset)

    val selector = sel match {
      case Some(s) => s
      case _ =>
        (new SelectorBuilder)
          .offset(offset)
          .fields(fields: _*)
          .limit(AdWords.PAGE_SIZE)
          .build()
    }

    predicates match {
      case Some(p) =>
        selector.setPredicates(p)
      case _ =>
    }

    selector.setPaging(paging)
    var morePages: Boolean = true
    var results = ListBuffer[AdGroupCriterion]()
    breakable {
      while (morePages) {
        var success = false
        var page: AdGroupCriterionPage = null
        while(!success) {
          try {
            page = adGroupCriterionService.get(selector)
            success = true
          } catch {
            case ae: ApiException =>
              AdWords.sleepIfRequired(ae, log, "AdGroupCriterionService")
          }
        }
        if (page.getEntries != null) {
          results = results ++ page.getEntries.toList
          offset += AdWords.PAGE_SIZE
          paging.setStartIndex(offset)
          selector.setPaging(paging)
          morePages = offset < page.getTotalNumEntries
        } else {
          break()
        }
      }
    }
    results.toList
  }

  def createAdGroupCriterion(criterion: AdGroupCriterion): AdGroupCriterionReturnValue = {
    val adGroupCriterionOperation = new AdGroupCriterionOperation
    adGroupCriterionOperation.setOperator(Operator.ADD)
    adGroupCriterionOperation.setOperand(criterion)
    adGroupCriterionService.mutate(Array(adGroupCriterionOperation))
  }

  def deleteAdGroupCriterion(criterion: AdGroupCriterion): AdGroupCriterionReturnValue = {
    val adGroupCriterionOperation = new AdGroupCriterionOperation
    adGroupCriterionOperation.setOperator(Operator.REMOVE)
    adGroupCriterionOperation.setOperand(criterion)
    adGroupCriterionService.mutate(Array(adGroupCriterionOperation))
  }

  def updateAdGroupCriterion(criterion: AdGroupCriterion): AdGroupCriterionReturnValue = {
    val adGroupCriterionOperation = new AdGroupCriterionOperation
    adGroupCriterionOperation.setOperator(Operator.SET)
    adGroupCriterionOperation.setOperand(criterion)
    adGroupCriterionService.mutate(Array(adGroupCriterionOperation))
  }
}