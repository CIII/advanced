package sync.facebook.nanigans

import Shared.Shared.Attribution
import com.google.common.base.Charsets
import com.google.common.io.BaseEncoding
import com.mongodb.casbah.Imports._
import org.joda.time.DateTime
import play.api.Play.current
import play.api.libs.json.Json
import play.api.libs.ws.{WS, WSAuthScheme}
import sync.shared.Facebook._

import scala.concurrent.Await
import scala.concurrent.duration.Duration

class NanigansHelper(
  partner_id: String,
  partner_name: String,
  secret_key: String,
  ba_username: String,
  ba_password: String,
  method_name: String = "generateAdLevelReport"
) {
  val endPoint = "%s/%s/%s".format(baseEndPoint, partner_name, method_name)
  val gmtTimestamp = (new DateTime).toString(gmtFormat)

  val signatureParams = List(partner_id, gmtTimestamp, secret_key).mkString("&")
  var signature = BaseEncoding.base64().encode(
    md.digest(signatureParams.getBytes("UTF-8")).map("%02x".format(_)).mkString.stripSuffix(" ").getBytes
  ).stripSuffix(" ")

  signature = "\\=".r.replaceAllIn(signature, "-")
  signature = "\\+".r.replaceAllIn(signature, ".")
  signature = "\\/".r.replaceAllIn(signature, "_")
  endPoint

  var AuthSignature = BaseEncoding.base64().encode("%s:%s".format(ba_username, ba_password).getBytes(Charsets.UTF_8)).stripSuffix("=")
  val authHeader = "Basic %s".format(AuthSignature)

  def downloadReport(startDate: String, attribution: Attribution.Value=Attribution.click): Option[DBObject] = {
    val params = Json.obj(
      "method" -> method_name,
      "params" -> Json.obj(
        "partner_id" -> partner_id,
        "tms" -> gmtTimestamp,
        "sig" -> signature,
        "date" -> startDate,
        "attribution" -> attribution.toString,
        "fields" -> Json.toJson(nanigansAdFields),
        "metrics" -> Json.toJson(nanigansAdMetrics)
      )
    )
    val header = Seq("Content-Type" -> "application/json")

    val res = Await.result(
      WS.url(endPoint)
        .withHeaders(header: _*)
        .withAuth(ba_username, ba_password, WSAuthScheme.BASIC)
        .post(params),
      Duration.Inf
    )

    if(res.status != 200) {
      None
    } else {
      Some(com.mongodb.util.JSON.serialize(res.body).asInstanceOf[DBObject])
    }
  }
}
