package sync.facebook.process.reporting

import Shared.Shared._
import akka.actor.Actor
import akka.event.Logging
import com.mongodb.casbah.Imports._
import models.mongodb.facebook.Facebook._
import sync.facebook.nanigans.NanigansHelper

class NanigansReportActor extends Actor {
  val log = Logging(context.system, this)

  def receive = {
    case reportRequest: NanigansReportRequest =>
      try {
        log.info("Processing %s -> Start Date: %s -> Download Format: %s".format(
          reportRequest.reportType.toString,
          reportRequest.startDate,
          reportRequest.downloadFormat.toString
        ))

        val nanigansHelper = new NanigansHelper(
          partner_id = reportRequest.apiAccount.partnerId,
          partner_name = reportRequest.apiAccount.partnerName,
          secret_key = reportRequest.apiAccount.secretKey,
          ba_username = reportRequest.apiAccount.baUserName,
          ba_password = reportRequest.apiAccount.baPassword
        )

        nanigansHelper.downloadReport(
          startDate=reportRequest.startDate
        ) match {
          case None =>
          case Some(report) =>
              nanigansReportCollection(reportRequest.reportType).insert(
                DBObject(
                  "partnerId" -> reportRequest.apiAccount.partnerId,
                  "partnerName" -> reportRequest.apiAccount.partnerName,
                  "startDate" -> reportRequest.startDate,
                  "reportType" -> reportRequest.reportType.toString,
                  "attribution" -> reportRequest.attribution.toString,
                  "reportData" -> report
                )
              )
        }
      } catch {
        case e: Exception =>
          log.info("Error Processing Nanigans Report (%s - %s) - %s".format(
            reportRequest.apiAccount.partnerName,
            reportRequest.reportType.toString,
            e.toString
          ))
      }
  }
}