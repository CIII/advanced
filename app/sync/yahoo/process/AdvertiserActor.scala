package sync.yahoo.process

import Shared.Shared._
import akka.actor.Actor
import akka.event.Logging
import com.google.api.ads.adwords.axis.v201509.mcm.{Customer, CustomerServiceInterface}
import com.mongodb.casbah.Imports._
import com.mongodb.util.JSON
import models.mongodb.google.Google._
import play.airbrake.Airbrake
import sync.google.adwords.AdWordsHelper
import sync.google.adwords.account.{CampaignHelper, CustomerHelper}
import sync.shared.Google._
import sync.shared.Yahoo.YahooAdvertiserDataPullRequest
import models.mongodb.yahoo.Yahoo._

class AdvertiserActor extends Actor {
  val log = Logging(context.system, this)

  def receive = {
    case advertiserDataPullRequest: YahooAdvertiserDataPullRequest =>
      try {
        log.info("Processing Incoming Data for Yahoo Advertiser (%s)".format(
          advertiserDataPullRequest.advertiserObject.advertiser.advertiserName
        ))

        val qry = DBObject(
          "mccObjId" -> advertiserDataPullRequest.advertiserObject.apiAccountObject.apiAccountObjId,
          "apiId" -> advertiserDataPullRequest.advertiserObject.advertiser.apiId
        )

        val startTsecs = java.lang.System.currentTimeMillis()
        val endTsecs = java.lang.System.currentTimeMillis()

        val newData = DBObject(
          "startTsecs" -> startTsecs,
          "endTsecs" -> -1,
          "classPath" -> classOf[Customer].getCanonicalName,
          "object" -> advertiserToDBObject(advertiserDataPullRequest.advertiserObject.advertiser)
        )

        var matchFound = true

        yahooAdvertiserCollection.findOne(
          qry ++ ("advertiser.endTsecs" -> -1)
        ) match {
          case Some(advertiserRs) =>
            if (
              !gson.toJson(advertiserRs.as[String]("advertiser"))
                .equals(gson.toJson(advertiserDataPullRequest.advertiserObject.advertiser))
            ) {
              yahooAdvertiserCollection.update(
                qry ++ ("advertiser.endTsecs" -> -1),
                DBObject("$set" -> DBObject("advertiser.0.endTsecs" -> endTsecs))
              )
              matchFound = false
              log.debug("Yahoo Advertiser match found. Changes detected. Updating...")
            }
          case _ =>
            matchFound = false
            log.debug("No Yahoo Advertiser record Found. Inserting...")
        }
        if(!matchFound) {
          yahooAdvertiserCollection.update(qry, DBObject("$push" -> DBObject("advertiser" -> newData)), upsert = true)

          if (advertiserDataPullRequest.pushToUber) {
            //yahooAdvertiserSyncActor ! (advertiserDataPullRequest, endTsecs)
          }
        }

        if(advertiserDataPullRequest.recursivePull) {
          //todo: recursive pull
        }
      } catch {
        case e: Exception =>
          log.info("Error Retrieving Data for Google Account (%s) - %s".format(
            advertiserDataPullRequest.advertiserObject.advertiser.advertiserName,
            e.toString
          ))
          e.printStackTrace()
          Airbrake.notify()
      }
  }
}