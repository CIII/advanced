package sync.msn.process.reporting

import akka.actor.Actor
import akka.event.Logging
import play.api.mvc.Controller
import play.modules.reactivemongo.MongoController

class ReportActor extends Actor with Controller with MongoController {
  val log = Logging(context.system, this)

  def receive = {
    case msg =>

  }

}
