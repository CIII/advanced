package sync.msn.process.reporting

import java.io.File
import java.util.Calendar
import java.util.concurrent.TimeUnit
import scala.util.control.Breaks._

import Shared.Shared.{MsnReportRequest, MsnReportType}
import akka.actor.Actor
import akka.event.Logging
import com.microsoft.bingads.AsyncCallback
import com.microsoft.bingads.reporting._
import models.mongodb.Utilities._
import models.mongodb.msn.Msn._
import sync.msn.bingads.BingAdsHelper
import util.MongoJson
import scala.concurrent.Await
import scala.io.Source
import scala.concurrent.duration.Duration

class MsnReportActor extends Actor {
  val log = Logging(context.system, this)

  def receive = {
    case msnReportRequest: MsnReportRequest =>
      try {
        log.info("Processing %s -> Date Range: %s to %s -> Download Format: %s".format(
          msnReportRequest.reportType,
          msnReportRequest.startDate,
          msnReportRequest.endDate,
          msnReportRequest.reportFormat
        ))

        val bingAdsHelper = new BingAdsHelper(
          msnReportRequest.apiAccount.userName,
          msnReportRequest.apiAccount.password,
          msnReportRequest.apiAccount.developerToken,
          Some(msnReportRequest.customerId),
          Some(msnReportRequest.customerAccountId)
        )

        val reportingServiceManager = new ReportingServiceManager(bingAdsHelper.authData)

        var reportRequest: Option[ReportRequest] = None

        if(msnReportRequest.reportType == MsnReportType.KeywordPerformanceReportRequest) {
          reportRequest = Some(getKeywordPerformanceReportRequest(msnReportRequest, bingAdsHelper))
        }

        val reportingDownloadOperation = reportingServiceManager.submitDownloadAsync(
          reportRequest.get,
          null
        ).get

        var reportingOperationStatus: Option[ReportingOperationStatus] = None
        breakable {
          for (i <- 0 to 30) {
            Thread.sleep(5000)
            reportingOperationStatus = Some(reportingDownloadOperation.getStatusAsync(null).get())
            if (reportingOperationStatus.get.getStatus == ReportRequestStatusType.SUCCESS) {
              break()
            }
          }
        }

        val resultFile = reportingDownloadOperation.downloadResultFileAsync(null, "msn_tmp.csv", true, true, null).get()
        val lines = Source.fromFile(resultFile).getLines().toList
        if (lines.length > 13) {
          for (item <- csvToJson(lines.slice(10, lines.length - 1).mkString("\n"))) {
            var sanitized_item = item
            sanitized_item.keys.foreach { k =>
              if (k.contains(".")) {
                val value = sanitized_item.\(k).get
                sanitized_item = sanitized_item.-(k)
                sanitized_item.+(k.replace(".", "") -> value)
              }
            }
            msnReportCollection(msnReportRequest.reportType).insert(MongoJson.fromJson(sanitized_item))
          }
        }
        resultFile.delete()
      } catch {
        case e: Exception =>
          e.printStackTrace()
          log.info("Error Processing Report (%s) - %s".format(
            msnReportRequest.reportType,
            e.toString
          ))
      }
  }

  def getKeywordPerformanceReportRequest(msnReportRequest: MsnReportRequest, bingAdsHelper: BingAdsHelper): KeywordPerformanceReportRequest = {
    val report = new KeywordPerformanceReportRequest
    report.setFormat(ReportFormat.CSV)
    report.setReportName("Keyword Performance Report Request")
    report.setReturnOnlyCompleteData(false)

    val accountIds = new ArrayOflong()
    accountIds.getLongs.add(bingAdsHelper.authData.getAccountId)

    val accountThroughAdGroupReportScope = new AccountThroughAdGroupReportScope
    accountThroughAdGroupReportScope.setAccountIds(accountIds)
    report.setScope(accountThroughAdGroupReportScope)
    report.getScope.setCampaigns(null)
    report.getScope.setAdGroups(null)

    val reportTime = new ReportTime
    val startDate = new com.microsoft.bingads.reporting.Date
    startDate.setDay(msnReportRequest.startDate.getDayOfMonth)
    startDate.setMonth(msnReportRequest.startDate.getMonthOfYear)
    startDate.setYear(msnReportRequest.startDate.getYear)

    val endDate = new com.microsoft.bingads.reporting.Date
    endDate.setDay(msnReportRequest.startDate.getDayOfMonth)
    endDate.setMonth(msnReportRequest.startDate.getMonthOfYear)
    endDate.setYear(msnReportRequest.startDate.getYear)

    reportTime.setCustomDateRangeStart(startDate)
    reportTime.setCustomDateRangeEnd(endDate)
    report.setTime(reportTime)

    val keywordPerformanceReportColumns = new ArrayOfKeywordPerformanceReportColumn()
    keywordPerformanceReportColumns.getKeywordPerformanceReportColumns.add(KeywordPerformanceReportColumn.TIME_PERIOD)
    keywordPerformanceReportColumns.getKeywordPerformanceReportColumns.add(KeywordPerformanceReportColumn.ACCOUNT_ID)
    keywordPerformanceReportColumns.getKeywordPerformanceReportColumns.add(KeywordPerformanceReportColumn.CAMPAIGN_ID)
    keywordPerformanceReportColumns.getKeywordPerformanceReportColumns.add(KeywordPerformanceReportColumn.KEYWORD)
    keywordPerformanceReportColumns.getKeywordPerformanceReportColumns.add(KeywordPerformanceReportColumn.KEYWORD_ID)
    keywordPerformanceReportColumns.getKeywordPerformanceReportColumns.add(KeywordPerformanceReportColumn.DEVICE_TYPE)
    keywordPerformanceReportColumns.getKeywordPerformanceReportColumns.add(KeywordPerformanceReportColumn.BID_MATCH_TYPE)
    keywordPerformanceReportColumns.getKeywordPerformanceReportColumns.add(KeywordPerformanceReportColumn.CLICKS)
    keywordPerformanceReportColumns.getKeywordPerformanceReportColumns.add(KeywordPerformanceReportColumn.IMPRESSIONS)
    keywordPerformanceReportColumns.getKeywordPerformanceReportColumns.add(KeywordPerformanceReportColumn.CTR)
    keywordPerformanceReportColumns.getKeywordPerformanceReportColumns.add(KeywordPerformanceReportColumn.AVERAGE_CPC)
    keywordPerformanceReportColumns.getKeywordPerformanceReportColumns.add(KeywordPerformanceReportColumn.SPEND)
    keywordPerformanceReportColumns.getKeywordPerformanceReportColumns.add(KeywordPerformanceReportColumn.QUALITY_SCORE)

    report.setAggregation(ReportAggregation.DAILY)

    report.setColumns(keywordPerformanceReportColumns)

    report
  }
}
