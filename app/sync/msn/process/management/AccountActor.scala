package sync.msn.process.management

import Shared.Shared._
import akka.actor.Actor
import akka.event.Logging
import com.microsoft.bingads.customermanagement.{AccountInfoWithCustomerData, FindAccountsOrCustomersInfoRequest, GetCurrentUserRequest, User}
import com.mongodb.casbah.Imports._
import helpers.msn.customer.account.AccountControllerHelper
import models.mongodb.msn.Msn._
import play.api.libs.json.Json
import play.api.mvc.{AnyContent, Request}
import sync.msn.bingads.BingAdsHelper
import sync.msn.bingads.account.{AccountHelper, CampaignHelper}
import sync.shared.Msn.MsnAccountDataPullRequest

class AccountActor(request: Request[AnyContent]) extends Actor {
  val log = Logging(context.system, this)

  def receive = {

    case accountDataPullRequest: MsnAccountDataPullRequest =>
      try {
        log.info("Processing Incoming Data for Account (%s)".format(
          accountDataPullRequest.accountApiId
        ))

        val qry = DBObject(
          "apiId" -> accountDataPullRequest.accountApiId
        )

        val newData = DBObject(
          "startTsecs" -> java.lang.System.currentTimeMillis(),
          "endTsecs" -> -1,
          "object" -> DBObject(
            "User" -> Json.parse(gson.toJson(accountDataPullRequest.user)),
            "AccountInfoWithCustomerData" -> Json.parse(gson.toJson(accountDataPullRequest.accountInfoWithCustomerData))
          )
        )
        var matchFound = true

        msnAccountCollection.findOne(
          qry ++ ("account.endTsecs" -> -1)
        ) match {
          case Some(accountRs) =>
            if (
              !dboToMsnEntity[User](accountRs, "account", Some("user")).equals(accountDataPullRequest.user) ||
              !gson.toJson(dboToMsnEntity[AccountInfoWithCustomerData](accountRs, "account", Some("accountInfoWithCustomerData")))
                .equals(gson.toJson(accountDataPullRequest.accountInfoWithCustomerData))
            ) {
              msnAccountCollection.update(
                qry ++ ("account.endTsecs" -> -1),
                DBObject("$set" -> DBObject("account.0.endTsecs" -> java.lang.System.currentTimeMillis()))
              )
              matchFound = false
            }
          case _ =>
            matchFound = false
        }
        if(!matchFound)
          msnAccountCollection.update(qry, DBObject("$push" -> DBObject("account" -> newData)), upsert = true)

        val accountHelper = new AccountHelper(accountDataPullRequest.bingAdsHelper)
        val campaignHelper = new CampaignHelper(accountDataPullRequest.bingAdsHelper)

        //todo: FILTER UP TO OTHER BINGADS SYNC LOADERS
      } catch {
        case e: Exception =>
          log.info("Error Retrieving Data for Google Account (%s) - %s".format(
            accountDataPullRequest.accountInfoWithCustomerData.getAccountName,
            e.toString
          ))
      }
    case cache: PendingCacheStructure =>
      val account_data = AccountControllerHelper.dboToAccountForm(cache.changeData.asDBObject)
      val bingAdsHelper = new BingAdsHelper(
        account_data.userName,
        account_data.password,
        account_data.developerToken,
        account_data.customerApiId,
        account_data.customerAccountApiId
      )
      val findAccountsOrCustomersInfoResponse = bingAdsHelper.customerManagementService.getService.findAccountsOrCustomersInfo(
        new FindAccountsOrCustomersInfoRequest
      )
      val accountInfoWithCustomerData = findAccountsOrCustomersInfoResponse.getAccountInfoWithCustomerData
      val getCurrentUserResponse = bingAdsHelper.customerManagementService.getService.getCurrentUser(new GetCurrentUserRequest)
      val user = getCurrentUserResponse.getUser
  }
}