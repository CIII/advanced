package sync.msn.process.management.api_account.account_info

import Shared.Shared._
import akka.actor.Actor
import akka.event.Logging
import com.microsoft.bingads.customermanagement._
import com.mongodb.casbah.Imports._
import com.mongodb.util.JSON
import helpers.msn.api_account.ApiAccountControllerHelper
import models.mongodb.msn.Msn._
import play.api.libs.json.Json
import sync.msn.bingads.BingAdsHelper
import sync.msn.bingads.account.{AccountHelper, CampaignHelper}
import sync.shared.Msn.MsnAccountInfoDataPullRequest

class AccountInfoActor extends Actor {
  val log = Logging(context.system, this)

  def receive = {
    case accountDataPullRequest: MsnAccountInfoDataPullRequest =>
      try {
        log.info("Processing Incoming Data for Account (%s)".format(
          accountDataPullRequest.account.getId
        ))

        val qry = DBObject(
          "apiAccountObjId" -> accountDataPullRequest.apiAccountObjId,
          "apiId" -> accountDataPullRequest.account.getId
        )

        val newData = DBObject(
          "startTsecs" -> java.lang.System.currentTimeMillis() / 1000,
          "endTsecs" -> -1,
          "object" -> DBObject(
            "accountObj" -> JSON.parse(gson.toJson(accountDataPullRequest.account)),
            "accountInfoWithCustomerData" -> JSON.parse(gson.toJson(accountDataPullRequest.accountInfoWithCustomerData))
          )
        )
        var matchFound = true

        msnAccountInfoCollection.findOne(
          qry ++ ("account.endTsecs" -> -1)
        ) match {
          case Some(accountRs) =>
            if (
              !dboToMsnEntity[Account](accountRs, "account", Some("accountObj")).equals(accountDataPullRequest.account) ||
              !gson.toJson(dboToMsnEntity[AccountInfoWithCustomerData](accountRs, "account", Some("accountInfoWithCustomerData")))
                .equals(gson.toJson(accountDataPullRequest.accountInfoWithCustomerData))
            ) {
              msnAccountInfoCollection.update(
                qry ++ ("account.endTsecs" -> -1),
                DBObject("$set" -> DBObject("account.0.endTsecs" -> java.lang.System.currentTimeMillis() / 1000))
              )
              matchFound = false
            }
          case _ =>
            matchFound = false
        }
        if(!matchFound)
          msnAccountInfoCollection.update(qry, DBObject("$push" -> DBObject("account" -> newData)), upsert = true)

        //val accountHelper = new AccountHelper(accountDataPullRequest.bingAdsHelper)
        //val campaignHelper = new CampaignHelper(accountDataPullRequest.bingAdsHelper)

        //todo: FILTER UP TO OTHER BINGADS SYNC LOADERS
      } catch {
        case e: Exception =>
          e.printStackTrace()
          log.info("Error Retrieving Data for Msn Account Info (%s) - %s".format(
            accountDataPullRequest.accountInfoWithCustomerData.getAccountName,
            e.toString
          ))
      }
    case _ =>
  }
}