package sync.shared

import java.text.SimpleDateFormat

import Shared.Shared._
import akka.actor.Props
import akka.event.LoggingAdapter
import com.google.api.ads.adwords.axis.v201509.cm._
import com.google.api.ads.adwords.axis.v201509.mcm.{Customer, ManagedCustomer}
import com.google.api.ads.adwords.lib.selectorfields.v201509.cm._
import com.microsoft.bingads.customermanagement.{Account, AccountInfoWithCustomerData}
import com.mongodb.casbah.Imports._
import models.mongodb.google.Google.Mcc
import models.mongodb.uber.Uber._
import models.mongodb.yahoo.Yahoo.{Advertiser, ApiAccount}
import org.joda.time.format.DateTimeFormat
import org.joda.time.{DateTimeConstants, DateTimeZone}
import play.api.Play
import sync.google.adwords.AdWordsHelper
import sync.google.process.management.mcc.account.bidding_strategy.BiddingStrategyActor
import sync.google.process.management.mcc.account.budget.BudgetActor
import sync.google.process.management.mcc.account.campaign.adgroup.ad.{AdGroupAdActor, AdGroupAdSyncActor}
import sync.google.process.management.mcc.account.campaign.adgroup.criterion.keyword.{AdGroupCriterionActor, AdGroupCriterionSyncActor}
import sync.google.process.management.mcc.account.campaign.adgroup.{AdGroupActor, AdGroupBidModifierActor, AdGroupSyncActor}
import sync.google.process.management.mcc.account.campaign.criterion.CampaignCriterionActor
import sync.google.process.management.mcc.account.campaign.{CampaignActor, CampaignSyncActor}
import sync.google.process.management.mcc.account.{CustomerActor, CustomerSyncActor}
import sync.msn.bingads.BingAdsHelper
import sync.yahoo.gemini.GeminiHelper
import sync.yahoo.process._
import sync.yahoo.process.advertiser.AdvertiserActor

import scala.collection.immutable.List

object Google {

  def googleDateFormatter = new SimpleDateFormat("yyyyMMdd")

  lazy val landingPageTag = "{lpurl}"

  def campaignFields = List(
    CampaignField.AdServingOptimizationStatus,
    CampaignField.AdvertisingChannelType,
    CampaignField.Amount,
    CampaignField.BidCeiling,
    CampaignField.BidType,
    CampaignField.BiddingStrategyId,
    CampaignField.BiddingStrategyName,
    CampaignField.BiddingStrategyType,
    CampaignField.BudgetId,
    CampaignField.BudgetName,
    CampaignField.BudgetReferenceCount,
    CampaignField.BudgetStatus,
    CampaignField.DeliveryMethod,
    CampaignField.Eligible,
    CampaignField.EndDate,
    CampaignField.EnhancedCpcEnabled,
    CampaignField.FrequencyCapMaxImpressions,
    CampaignField.Id,
    CampaignField.IsBudgetExplicitlyShared,
    CampaignField.Labels,
    CampaignField.Level,
    CampaignField.Name,
    CampaignField.Period,
    CampaignField.PricingMode,
    CampaignField.RejectionReasons,
    CampaignField.ServingStatus,
    CampaignField.Settings,
    CampaignField.StartDate,
    CampaignField.Status,
    CampaignField.TargetContentNetwork,
    CampaignField.TargetGoogleSearch,
    CampaignField.TargetPartnerSearchNetwork,
    CampaignField.TargetSearchNetwork,
    CampaignField.TimeUnit,
    CampaignField.TrackingUrlTemplate,
    CampaignField.UrlCustomParameters
  )

  def campaignCriterionFields = List(
    CampaignCriterionField.Address,
    CampaignCriterionField.AgeRangeType,
    CampaignCriterionField.BidModifier,
    CampaignCriterionField.CampaignId,
    CampaignCriterionField.CarrierCountryCode,
    CampaignCriterionField.CarrierName,
    CampaignCriterionField.ContentLabelType,
    CampaignCriterionField.CriteriaType,
    CampaignCriterionField.DayOfWeek,
    CampaignCriterionField.DeviceName,
    CampaignCriterionField.DeviceType,
    CampaignCriterionField.Dimensions,
    CampaignCriterionField.DisplayName,
    CampaignCriterionField.DisplayType,
    CampaignCriterionField.EndHour,
    CampaignCriterionField.EndMinute,
    CampaignCriterionField.GenderType,
    CampaignCriterionField.GeoPoint,
    CampaignCriterionField.Id,
    CampaignCriterionField.IsNegative,
    CampaignCriterionField.KeywordMatchType,
    CampaignCriterionField.LanguageCode,
    CampaignCriterionField.LanguageName,
    CampaignCriterionField.LocationName,
    CampaignCriterionField.ManufacturerName,
    CampaignCriterionField.MatchingFunction,
    CampaignCriterionField.MobileAppCategoryId,
    CampaignCriterionField.OperatingSystemName,
    CampaignCriterionField.OperatorType,
    CampaignCriterionField.OsMajorVersion,
    CampaignCriterionField.OsMinorVersion,
    CampaignCriterionField.Path,
    CampaignCriterionField.PlacementUrl,
    CampaignCriterionField.PlatformName,
    CampaignCriterionField.RadiusDistanceUnits,
    CampaignCriterionField.RadiusInUnits,
    CampaignCriterionField.StartHour,
    CampaignCriterionField.StartHour,
    CampaignCriterionField.TargetingStatus,
    CampaignCriterionField.KeywordText,
    CampaignCriterionField.UserInterestId,
    CampaignCriterionField.UserInterestName,
    CampaignCriterionField.UserListId,
    CampaignCriterionField.UserListMembershipStatus,
    CampaignCriterionField.UserListName,
    CampaignCriterionField.VerticalId,
    CampaignCriterionField.VerticalParentId
  )

  def adGroupBidModifierFields = List(
    AdGroupBidModifierField.AdGroupId,
    AdGroupBidModifierField.BidModifier,
    AdGroupBidModifierField.BidModifierSource,
    AdGroupBidModifierField.CampaignId,
    AdGroupBidModifierField.CriteriaType,
    AdGroupBidModifierField.Id,
    AdGroupBidModifierField.PlatformName
  )

  def adGroupCriterionFields = List(
    AdGroupCriterionField.AdGroupId,
    AdGroupCriterionField.AgeRangeType,
    AdGroupCriterionField.ApprovalStatus,
    AdGroupCriterionField.BidModifier,
    AdGroupCriterionField.BidType,
    AdGroupCriterionField.BiddingStrategyId,
    AdGroupCriterionField.BiddingStrategyName,
    AdGroupCriterionField.BiddingStrategySource,
    AdGroupCriterionField.BiddingStrategyType,
    AdGroupCriterionField.CaseValue,
    AdGroupCriterionField.CpcBid,
    AdGroupCriterionField.CpcBidSource,
    AdGroupCriterionField.CpmBid,
    AdGroupCriterionField.CpmBidSource,
    AdGroupCriterionField.CriteriaCoverage,
    AdGroupCriterionField.CriteriaSamples,
    AdGroupCriterionField.CriteriaType,
    AdGroupCriterionField.CriterionUse,
    AdGroupCriterionField.DestinationUrl,
    AdGroupCriterionField.DisapprovalReasons,
    AdGroupCriterionField.DisplayName,
    AdGroupCriterionField.EnhancedCpcEnabled,
    AdGroupCriterionField.ExperimentBidMultiplier,
    AdGroupCriterionField.ExperimentDataStatus,
    AdGroupCriterionField.ExperimentDeltaStatus,
    AdGroupCriterionField.ExperimentId,
    AdGroupCriterionField.FinalAppUrls,
    AdGroupCriterionField.FinalMobileUrls,
    AdGroupCriterionField.FinalUrls,
    AdGroupCriterionField.FirstPageCpc,
    AdGroupCriterionField.GenderType,
    AdGroupCriterionField.Id,
    AdGroupCriterionField.KeywordMatchType,
    AdGroupCriterionField.Labels,
    AdGroupCriterionField.MobileAppCategoryId,
    AdGroupCriterionField.Parameter,
    AdGroupCriterionField.ParentCriterionId,
    AdGroupCriterionField.PartitionType,
    AdGroupCriterionField.Path,
    AdGroupCriterionField.PlacementUrl,
    AdGroupCriterionField.QualityScore,
    AdGroupCriterionField.Status,
    AdGroupCriterionField.SystemServingStatus,
    AdGroupCriterionField.TopOfPageCpc,
    AdGroupCriterionField.TrackingUrlTemplate,
    AdGroupCriterionField.UrlCustomParameters,
    AdGroupCriterionField.UserInterestId,
    AdGroupCriterionField.UserInterestName,
    AdGroupCriterionField.UserListId,
    AdGroupCriterionField.UserListMembershipStatus,
    AdGroupCriterionField.UserListName,
    AdGroupCriterionField.VerticalId,
    AdGroupCriterionField.VerticalParentId
  )

  def adGroupFields = List(
    AdGroupField.BidType,
    AdGroupField.BiddingStrategyId,
    AdGroupField.BiddingStrategyName,
    AdGroupField.BiddingStrategySource,
    AdGroupField.BiddingStrategyType,
    AdGroupField.CampaignId,
    AdGroupField.CampaignName,
    AdGroupField.ContentBidCriterionTypeGroup,
    AdGroupField.CpcBid,
    AdGroupField.CpmBid,
    AdGroupField.EnhancedCpcEnabled,
    AdGroupField.Id,
    AdGroupField.Labels,
    AdGroupField.MaxContentCpcMultiplier,
    AdGroupField.MaxCpcMultiplier,
    AdGroupField.MaxCpmMultiplier,
    AdGroupField.Name,
    AdGroupField.Settings,
    AdGroupField.Status,
    AdGroupField.TargetCpa,
    AdGroupField.TargetCpaBid,
    AdGroupField.TrackingUrlTemplate,
    AdGroupField.UrlCustomParameters
  )

  def sharedBiddingStrategyFields = List(
    BiddingStrategyField.BiddingScheme,
    BiddingStrategyField.Id,
    BiddingStrategyField.Name,
    BiddingStrategyField.Status,
    BiddingStrategyField.Type
  )

  def budgetFields = List(
    BudgetField.Amount,
    BudgetField.BudgetId,
    BudgetField.BudgetName,
    BudgetField.BudgetReferenceCount,
    BudgetField.BudgetStatus,
    BudgetField.DeliveryMethod,
    BudgetField.IsBudgetExplicitlyShared,
    BudgetField.Period
  )

  def managedCustomerFields = List(
    ManagedCustomerField.CanManageClients,
    ManagedCustomerField.CompanyName,
    ManagedCustomerField.CurrencyCode,
    ManagedCustomerField.CustomerId,
    ManagedCustomerField.DateTimeZone,
    ManagedCustomerField.Name,
    ManagedCustomerField.TestAccount
  )

  def adGroupAdFields = List(
    AdGroupAdField.AdGroupAdDisapprovalReasons,
    AdGroupAdField.AdGroupAdTrademarkDisapproved,
    AdGroupAdField.AdGroupCreativeApprovalStatus,
    AdGroupAdField.AdGroupId,
    AdGroupAdField.AdvertisingId,
    AdGroupAdField.CreationTime,
    AdGroupAdField.CreativeFinalMobileUrls,
    AdGroupAdField.CreativeFinalUrls,
    AdGroupAdField.CreativeTrackingUrlTemplate,
    AdGroupAdField.CreativeUrlCustomParameters,
    AdGroupAdField.Description1,
    AdGroupAdField.Description1,
    AdGroupAdField.Description2,
    AdGroupAdField.Description2,
    AdGroupAdField.DevicePreference,
    AdGroupAdField.Dimensions,
    AdGroupAdField.DisplayUrl,
    AdGroupAdField.ExpandingDirections,
    AdGroupAdField.ExperimentDataStatus,
    AdGroupAdField.ExperimentDeltaStatus,
    AdGroupAdField.ExperimentId,
    AdGroupAdField.FileSize,
    AdGroupAdField.Headline,
    AdGroupAdField.Height,
    AdGroupAdField.Id,
    AdGroupAdField.ImageCreativeName,
    AdGroupAdField.IndustryStandardCommercialIdentifier,
    AdGroupAdField.IsCookieTargeted,
    AdGroupAdField.IsTagged,
    AdGroupAdField.IsUserInterestTargeted,
    AdGroupAdField.Labels,
    AdGroupAdField.MediaId,
    AdGroupAdField.MimeType,
    AdGroupAdField.PromotionLine,
    AdGroupAdField.ReadyToPlayOnTheWeb,
    AdGroupAdField.ReadyToPlayOnTheWeb,
    AdGroupAdField.ReferenceId,
    AdGroupAdField.RichMediaAdCertifiedVendorFormatId,
    AdGroupAdField.RichMediaAdDuration,
    AdGroupAdField.RichMediaAdImpressionBeaconUrl,
    AdGroupAdField.RichMediaAdName,
    AdGroupAdField.RichMediaAdSnippet,
    AdGroupAdField.RichMediaAdSourceUrl,
    AdGroupAdField.RichMediaAdType,
    AdGroupAdField.SourceUrl,
    AdGroupAdField.Status,
    AdGroupAdField.TemplateAdDuration,
    AdGroupAdField.TemplateAdName,
    AdGroupAdField.TemplateAdUnionId,
    AdGroupAdField.TemplateElementFieldName,
    AdGroupAdField.TemplateElementFieldText,
    AdGroupAdField.TemplateElementFieldType,
    AdGroupAdField.TemplateId,
    AdGroupAdField.TemplateOriginAdId,
    AdGroupAdField.UniqueName,
    AdGroupAdField.Url,
    AdGroupAdField.Urls,
    AdGroupAdField.VideoTypes,
    AdGroupAdField.Width,
    AdGroupAdField.YouTubeVideoIdString
  )

  val googleAccountActor = userActorSystem.actorOf(Props(new CustomerActor))
  val googleAccountSyncActor = userActorSystem.actorOf(Props(new CustomerSyncActor))
  val googleCampaignActor = userActorSystem.actorOf(Props(new CampaignActor))
  val googleCampaignSyncActor = userActorSystem.actorOf(Props(new CampaignSyncActor))
  val googleBudgetActor = userActorSystem.actorOf(Props(new BudgetActor))
  val googleBiddingStrategyActor = userActorSystem.actorOf(Props(new BiddingStrategyActor))
  val googleCampaignCriterionActor = userActorSystem.actorOf(Props(new CampaignCriterionActor))
  val googleAdGroupBidModifierActor = userActorSystem.actorOf(Props(new AdGroupBidModifierActor))
  val googleAdGroupActor = userActorSystem.actorOf(Props(new AdGroupActor))
  val googleAdGroupSyncActor = userActorSystem.actorOf(Props(new AdGroupSyncActor))
  val googleAdGroupAdActor = userActorSystem.actorOf(Props(new AdGroupAdActor))
  val googleAdGroupAdSyncActor = userActorSystem.actorOf(Props(new AdGroupAdSyncActor))
  val googleAdGroupCriterionActor = userActorSystem.actorOf(Props(new AdGroupCriterionActor))
  val googleAdGroupCriterionSyncActor = userActorSystem.actorOf(Props(new AdGroupCriterionSyncActor))

  case class MccObject(
    mccObjId: ObjectId,
    mcc: Mcc
  )

  case class CustomerObject(
    mccObject: MccObject,
    var customerObjId: Option[ObjectId],
    managedCustomer: ManagedCustomer,
    var customer: Option[Customer]
  )

  case class CampaignObject(
    customerObject: CustomerObject,
    var campaignObjId: Option[ObjectId],
    campaign: Campaign
  )

  case class AdGroupObject(
    campaignObject: CampaignObject,
    var adGroupObjId: Option[ObjectId],
    adGroup: AdGroup
  )

  case class SharedBiddingStrategyObject(
    customerObject: CustomerObject,
    var sharedBiddingStrategyObjId: Option[ObjectId],
    sharedBiddingStrategy: SharedBiddingStrategy
  )

  case class AdGroupBidModifierObject(
    campaignObject: CampaignObject,
    var adGroupBidModifierObjId: Option[ObjectId],
    adGroupBidModifier: AdGroupBidModifier
  )

  case class CampaignCriterionObject(
    campaignObject: CampaignObject,
    var campaignCriterionObjId: Option[ObjectId],
    campaignCriterion: CampaignCriterion
  )

  case class AdGroupAdObject(
    adGroupObject: AdGroupObject,
    var adGroupAdObjId: Option[ObjectId],
    adGroupAd: AdGroupAd
  )

  case class BudgetObject(
    customerObject: CustomerObject,
    var budgetObjId: Option[ObjectId],
    budget: Budget
  )

  case class AdGroupCriterionObject(
    adGroupObject: AdGroupObject,
    var adGroupCriterionObjId: Option[ObjectId],
    adGroupCriterion: AdGroupCriterion
  )

  abstract class GoogleDataPullRequest{
    val pushToUber: Boolean
  }

  case class GoogleCustomerDataPullRequest(
    var adWordsHelper: Option[AdWordsHelper],
    customerObject: CustomerObject,
    recursivePull: Boolean,
    pushToUber: Boolean
  ) extends GoogleDataPullRequest

  case class GoogleCampaignDataPullRequest(
    adWordsHelper: AdWordsHelper,
    campaignObject: CampaignObject,
    recursivePull: Boolean,
    pushToUber: Boolean
  ) extends GoogleDataPullRequest

  case class GoogleAdGroupDataPullRequest(
    adWordsHelper: AdWordsHelper,
    adGroupObject: AdGroupObject,
    recursivePull: Boolean,
    pushToUber: Boolean
  ) extends GoogleDataPullRequest

  case class GoogleAdGroupBidModifierDataPullRequest(
    adWordsHelper: AdWordsHelper,
    adGroupBidModifierObject: AdGroupBidModifierObject,
    pushToUber: Boolean
  ) extends GoogleDataPullRequest

  case class GoogleCampaignCriterionDataPullRequest(
    adWordsHelper: AdWordsHelper,
    campaignCriterionObject: CampaignCriterionObject,
    pushToUber: Boolean
  ) extends GoogleDataPullRequest

  case class GoogleAdGroupCriterionDataPullRequest(
    adWordsHelper: AdWordsHelper,
    adGroupCriterionObject: AdGroupCriterionObject,
    pushToUber: Boolean
  ) extends GoogleDataPullRequest

  case class GoogleAdGroupAdDataPullRequest(
    adWordsHelper: AdWordsHelper,
    adGroupAdObject: AdGroupAdObject,
    pushToUber: Boolean
  ) extends GoogleDataPullRequest

  case class GoogleBudgetDataPullRequest(
    adWordsHelper: AdWordsHelper,
    budgetObject: BudgetObject,
    pushToUber: Boolean
  ) extends GoogleDataPullRequest

  case class GoogleBiddingStrategyDataPullRequest(
    adWordsHelper: AdWordsHelper,
    sharedBiddingStrategyObject: SharedBiddingStrategyObject,
    pushToUber: Boolean
  ) extends GoogleDataPullRequest
}

object Facebook {
  def nanigansAdMetrics = List(
    "fb_spend",
    "fb_spend_wfees",
    "impressions",
    "clicks",
    "purchase_value",
    "revenue",
    "purchase_events",
    "purchase_users",
    "reach"
  ) ::: (1 to 17).map("a%d".format(_)).toList ::: (1 to 17).map("a%dval".format(_)).toList

  def nanigansAdFields = List(
    "countries",
    "genders",
    "age_min",
    "age_max",
    "regions",
    "cities",
    "interests_group_id",
    "interests_group",
    "interests",
    "audience",
    "demographic_set_id",
    "user_adclusters",
    "user_adclusters_excluded",
    "interested_in",
    "relationship_statuses",
    "education_statuses",
    "college_networks",
    "connections",
    "locales",
    "delivery_location",
    "user_device",
    "user_os",
    "wireless_carrier",
    "product_category",
    "fbx_retargeting_group_id",
    "fbx_retargeting_group",
    "ad_type",
    "image_id",
    "image_file_name",
    "creative_set_id",
    "network_config_set_id",
    "title",
    "body",
    "caption",
    "call_to_action",
    "upp_name",
    "upp_body",
    "upp_message",
    "upp_description",
    "destination_id",
    "destination",
    "destination_url",
    "creative_group_name",
    "site_id",
    "site",
    "placement_id",
    "ad_group_id",
    "ad_group",
    "campaign_id",
    "campaign",
    "client_id",
    "client",
    "strategy_group_id",
    "strategy_group",
    "ad_plan_id",
    "ad_plan",
    "budget_pool_id",
    "budget_pool",
    "date_created",
    "bid_type",
    "hour"
  )

  lazy val gmtFormat = DateTimeFormat.forPattern("YmdHis").withZone(DateTimeZone.UTC)
  lazy val baseEndPoint = Play.current.configuration.getString("facebook.nanigans.endpoint.baseurl").get
  lazy val md = java.security.MessageDigest.getInstance("SHA-1")
}

object Yahoo {

  val yahooApiAccountActor = userActorSystem.actorOf(Props(new sync.yahoo.process.ApiAccountActor))
  val yahooAdvertiserActor = userActorSystem.actorOf(Props(new AdvertiserActor))
  val yahooCampaignActor = userActorSystem.actorOf(Props(new advertiser.campaign.CampaignActor))
  val yahooAdGroupActor = userActorSystem.actorOf(Props(new advertiser.campaign.adgroup.AdGroupActor))

  case class ApiAccountObject(
    apiAccountObjId: ObjectId,
    apiAccount: ApiAccount
  )

  case class AdvertiserObject(
    apiAccountObject: ApiAccountObject,
    var advertiserObjId: Option[ObjectId],
    advertiser: Advertiser
  )

  case class AdGroupObject(
    campaignObject: CampaignObject,
    var adGroupObjId: Option[ObjectId],
    adGroup: models.mongodb.yahoo.Yahoo.AdGroup
  )

  case class AdObject(
    adGroupObject: AdGroupObject,
    var adObjId: Option[ObjectId],
    ad: models.mongodb.yahoo.Yahoo.Ad
  )

  abstract class YahooDataPullRequest{
    val pushToUber: Boolean
  }

  case class YahooAdvertiserDataPullRequest(
    var geminiHelper: Option[GeminiHelper],
    advertiserObject: AdvertiserObject,
    recursivePull: Boolean,
    pushToUber: Boolean
  ) extends YahooDataPullRequest

  case class CampaignObject(
    advertiserObject: AdvertiserObject,
    var campaignObjId: Option[ObjectId],
    campaign: models.mongodb.yahoo.Yahoo.Campaign
  )

  case class YahooCampaignDataPullRequest(
    geminiHelper: GeminiHelper,
    campaignObject: CampaignObject,
    recursivePull: Boolean,
    pushToUber: Boolean
  ) extends YahooDataPullRequest

  case class YahooAdGroupDataPullRequest(
    geminiHelper: GeminiHelper,
    adGroupObject: AdGroupObject,
    recursivePull: Boolean,
    pushToUber: Boolean
  ) extends YahooDataPullRequest

  case class YahooAdDataPullRequest(
    adWordsHelper: AdWordsHelper,
    adObject: AdObject,
    pushToUber: Boolean
  ) extends YahooDataPullRequest
}

object Msn {
  val addAdGroupsLimit = 1000
  val getAdsByIdsLimit = 20
  val addCampaignsLimit = 1000
  val addAdsLimit = 50
  val addKeywordsLimit = 1000

  val msnApiAccountActor = userActorSystem.actorOf(Props(new sync.msn.process.management.api_account.ApiAccountActor))
  val msnAccountInfoActor = userActorSystem.actorOf(Props(new sync.msn.process.management.api_account.account_info.AccountInfoActor))

  case class Bid(
    var amount: Double
  )

  def bidToDbo(b: Bid) = DBObject(
    "amount" -> b.amount
  )

  def dboToBid(dbo: DBObject) = Bid(
    amount=dbo.getAsOrElse[Double]("amount", 0.0)
  )

  case class MsnAccountInfoDataPullRequest(
    bingAdsHelper: BingAdsHelper,
    apiAccountObjId: ObjectId,
    account: Account,
    accountInfoWithCustomerData: AccountInfoWithCustomerData,
    pushToUber: Boolean
  )
}

object Uber {

  def refby_classifications = Map(
    "806609" -> "Affiliate",
    "807383" -> "Affiliate",
    "807513" -> "Affiliate",
    "800912" -> "AIQ",
    "808522" -> "AIQ",
    "800917" -> "AIQ",
    "800919" -> "AIQ",
    "801052" -> "AIQ",
    "801053" -> "AIQ",
    "808173" -> "AIQ",
    "808893" -> "Display",
    "809356" -> "Display",
    "809461" -> "Email",
    "809462" -> "Email",
    "809000" -> "Email",
    "808807" -> "Email",
    "808808" -> "Email",
    "14176" -> "Email",
    "20464" -> "Email",
    "803391" -> "GCIQ",
    "809061" -> "GCIQ",
    "809182" -> "GCIQ",
    "809199" -> "GCIQ",
    "807716" -> "H/S",
    "807841" -> "H/S",
    "808011" -> "H/S",
    "808012" -> "H/S",
    "808360" -> "H/S",
    "808361" -> "H/S",
    "808362" -> "H/S",
    "808363" -> "H/S",
    "809080" -> "H/S",
    "809478" -> "H/S",
    "809479" -> "H/S",
    "809480" -> "H/S",
    "809482" -> "H/S",
    "809417" -> "H/S",
    "809418" -> "H/S",
    "809419" -> "H/S",
    "809420" -> "H/S",
    "809421" -> "H/S",
    "809422" -> "H/S",
    "809423" -> "H/S",
    "809424" -> "H/S",
    "809425" -> "H/S",
    "809426" -> "H/S",
    "809427" -> "H/S",
    "809428" -> "H/S",
    "809429" -> "H/S",
    "809430" -> "H/S",
    "809431" -> "H/S",
    "809432" -> "H/S",
    "809433" -> "H/S",
    "809434" -> "H/S",
    "809001" -> "Mobile",
    "806761" -> "Mobile",
    "807717" -> "Mobile",
    "807842" -> "Mobile",
    "809303" -> "Mobile",
    "809304" -> "Mobile",
    "809305" -> "Mobile",
    "806823" -> "Mobile",
    "806709" -> "Other",
    "808456" -> "Other",
    "809166" -> "Other",
    "809167" -> "Other",
    "809168" -> "Other",
    "809169" -> "Other",
    "809513" -> "Email",
    "809514" -> "Email",
    "809551" -> "Email",
    "809552" -> "Email",
    "809555" -> "Email",
    "809556" -> "Email",
    "809581" -> "Email",
    "809582" -> "Email"
  )

  def searchAutoOverallTVImporter(reportRequest: UberReportRequest, data: List[DBObject], log: LoggingAdapter): Unit = {
    data.map { item =>
      Array(
        "day" -> item.getAs[String]("day").get,
        "week" -> item.getAs[String]("week").get,
        "month" -> item.getAs[String]("month").get
      ).map { date_dimension =>
        val docs = uberReportCollection(reportRequest.name, date_dimension._1).find(
          DBObject(date_dimension._1 -> date_dimension._2)
        ).toList
        if (docs.nonEmpty) {
          val doc = docs.head
          uberReportCollection(reportRequest.name, date_dimension._1).update(
            DBObject("_id" -> doc._id.get),
            DBObject(
              "$set" -> DBObject(
                "tv_onsite_profit" -> (
                  doc.getAs[Double]("tv_onsite_profit").getOrElse(0.0) + (
                    item.getAsOrElse[Double]("onsite rev", 0.0) - item.getAsOrElse[Double]("cost", 0.0)
                    )
                  ),
                "tv_total_profit" -> (
                  //todo: should also include TV Call data
                  doc.getAsOrElse[Double]("tv_total_profit", 0.0) + (
                    item.getAsOrElse[Double]("onsite rev", 0.0) -
                      item.getAsOrElse[Double]("cost", 0.0)
                    )
                  ),
                "tv_onsite_revenue" -> (
                  doc.getAsOrElse[Double]("tv_onsite_revenue", 0.0) + item.getAsOrElse[Double]("onsite rev", 0.0)
                  ),
                "tv_total_revenue" -> (
                  //todo: should also include TV Call data
                  doc.getAsOrElse[Double]("tv_total_revenue", 0.0) + item.getAsOrElse[Double]("onsite rev", 0.0)
                  ),
                "tv_forms" -> (
                  doc.getAsOrElse[Int]("tv_forms", 0) + item.getAsOrElse[Int]("forms", 0)
                  ),
                "tv_leads" -> (
                  //todo: should also include TV Transfers
                  doc.getAsOrElse[Int]("tv_leads", 0) + item.getAsOrElse[Int]("forms", 0)
                  )
              )
            )
          )
        } else {
          uberReportCollection(reportRequest.name, date_dimension._1).insert(
            DBObject(
              date_dimension._1 -> date_dimension._2,
              "tv_onsite_profit" -> (item.getAsOrElse[Double]("onsite rev", 0.0) - item.getAsOrElse[Double]("cost", 0.0)),
              "tv_total_profit" -> (item.getAsOrElse[Double]("onsite rev", 0.0) - item.getAsOrElse[Double]("cost", 0.0)),
              "tv_onsite_revenue" -> item.getAsOrElse[Double]("onsite rev", 0.0),
              "tv_total_revenue" -> item.getAsOrElse[Double]("onsite rev", 0.0),
              "tv_forms" -> item.getAsOrElse[Int]("forms", 0),
              "tv_leads" -> item.getAsOrElse[Int]("forms", 0)
            ))
        }
      }
    }
  }

  def searchAutoOverallLKImporter(reportRequest: UberReportRequest, data: List[DBObject], log: LoggingAdapter): Unit = {
    data.map { item =>
      Array(
        "day" -> item.getAs[String]("day").get,
        "week" -> item.getAs[String]("week").get,
        "month" -> item.getAs[String]("month").get
      ).map { date_dimension =>
        uberReportCollection(reportRequest.name, date_dimension._1).findOne(
          DBObject(date_dimension._1 -> date_dimension._2)
        ) match {
          case Some(doc) =>
            uberReportCollection(reportRequest.name, date_dimension._1).update(
              DBObject("_id" -> doc._id.get),
              DBObject(
                "$set" -> DBObject(
                  "lk_onsite_profit" -> (
                    doc.getAsOrElse[Double]("lk_onsite_profit", 0.0) + (
                      (
                        item.getAsOrElse[Double]("lead-revenue", 0.0) +
                          item.getAsOrElse[Double]("co-revenue", 0.0)
                      ) - item.getAsOrElse[Double]("cost", 0.0)
                    )
                ),
                "lk_total_profit" -> (
                  //todo: should also include TV Call data
                  doc.getAsOrElse[Double]("lk_total_profit", 0.0) + (
                    (
                      item.getAsOrElse[Double]("lead-revenue", 0.0) +
                        item.getAsOrElse[Double]("co-revenue", 0.0)
                      ) - item.getAsOrElse[Double]("cost", 0.0)
                    )
                  ),
                "lk_onsite_revenue" -> (
                  doc.getAsOrElse[Double]("lk_onsite_revenue", 0.0) + (
                    item.getAsOrElse[Double]("lead-revenue", 0.0) +
                      item.getAsOrElse[Double]("co-revenue", 0.0)
                    )
                  ),
                "lk_total_revenue" -> (
                  //todo: should also include TV Call data
                  doc.getAsOrElse[Double]("lk_total_revenue", 0.0) + (
                    item.getAsOrElse[Double]("lead-revenue", 0.0) +
                      item.getAsOrElse[Double]("co-revenue", 0.0)
                    )
                  ),
                "lk_forms" -> (
                  doc.getAsOrElse[Int]("lk_forms", 0) + item.getAsOrElse[Int]("forms", 0)
                  ),
                "lk_leads" -> (
                  //todo: should also include TV Transfers
                  doc.getAsOrElse[Int]("lk_leads", 0) + item.getAsOrElse[Int]("forms", 0)
                  )
              )
            )
          )
          case _ =>
            log.info(item.toString)
            uberReportCollection(reportRequest.name, date_dimension._1).insert(
              DBObject(
                date_dimension._1 -> date_dimension._2,
                "lk_onsite_profit" -> (
                  (
                    item.getAsOrElse[Double]("lead-revenue", 0.0) +
                      item.getAsOrElse[Double]("co-revenue", 0.0)
                    ) - item.getAsOrElse[Double]("cost", 0.0)
                  ),
                "lk_total_profit" -> (
                  (
                    item.getAsOrElse[Double]("lead-revenue", 0.0) +
                      item.getAsOrElse[Double]("co-revenue", 0.0)
                    ) - item.getAsOrElse[Double]("cost", 0.0)
                  ),
                "lk_onsite_revenue" -> (
                  item.getAsOrElse[Double]("lead-revenue", 0.0) +
                    item.getAsOrElse[Double]("co-revenue", 0.0)
                  ),
                "lk_total_revenue" -> (
                  item.getAsOrElse[Double]("lead-revenue", 0.0) +
                    item.getAsOrElse[Double]("co-revenue", 0.0)
                  ),
                "lk_forms" -> item.getAsOrElse[Int]("forms", 0),
                "lk_leads" -> item.getAsOrElse[Int]("forms", 0)
              )
            )
        }
      }
    }
  }

  def searchAutoOverallTVRowParser(reportRequest: UberReportRequest, item: DBObject, log: LoggingAdapter): DBObject = {
    val day = stringToDateTime(item.getAs[String]("date"))
    val weekDay = day.getDayOfWeek
    val week = day.withDayOfWeek(DateTimeConstants.MONDAY)
    val month = day.withDayOfMonth(1)
    val year = day.withDayOfYear(1)

    item ++ DBObject(
      "day" -> DateTimeFormat.forPattern("yyyy-MM-dd").print(day),
      "weekday" -> weekDay,
      "week" -> DateTimeFormat.forPattern("yyyy-MM-dd").print(week),
      "month" -> DateTimeFormat.forPattern("yyyy-MM-dd").print(month),
      "year" -> DateTimeFormat.forPattern("yyyy-MM-dd").print(year)
    )
  }

  def searchAutoOverallLKRowParser(reportRequest: UberReportRequest, item: DBObject, log: LoggingAdapter): DBObject = {
    //todo: Yahoo Cost needed?
    val totalCost = item.getAsOrElse[Double]("cost", 0.0)
    val day = stringToDateTime(item.getAs[String]("date"))
    val weekDay = day.getDayOfWeek
    val week = day.withDayOfWeek(DateTimeConstants.MONDAY)
    val month = day.withDayOfMonth(1)
    val year = day.withDayOfYear(1)

    item ++ DBObject(
      "total cost" -> totalCost,
      "day" -> DateTimeFormat.forPattern("yyyy-MM-dd").print(day),
      "weekday" -> weekDay,
      "week" -> DateTimeFormat.forPattern("yyyy-MM-dd").print(week),
      "month" -> DateTimeFormat.forPattern("yyyy-MM-dd").print(month),
      "year" -> DateTimeFormat.forPattern("yyyy-MM-dd").print(year)
    )
  }

  def pricingByClassificationLeadDataRowParser(reportRequest: UberReportRequest, item: DBObject, log: LoggingAdapter): DBObject = {
    val day = stringToDateTime(item.getAs[String]("date"), "yyyy-MM-dd")
    val week = stringToDateTime(item.getAs[String]("week"), "yyyy-MM-dd")
    val month = stringToDateTime(item.getAs[String]("month"), "yyyy-MM")
    val head_shoulder_terms = item.getAs[String]("head & shoulder terms").get
    val device_type = item.getAs[String]("device type").get
    val forms = item.getAs[String]("forms").get.replaceAll("\\D+", "").toInt
    val lead_revenue = item.getAsOrElse[Double]("lead-revenue", 0.0)
    val classification = head_shoulder_terms.toLowerCase match {
      case "head terms" =>
        "H/S"
      case "shoulder terms" =>
        "H/S"
      case "the general" =>
        "GCIQ"
      case _ =>
        "Other"
    }

    DBObject(
      "day" -> day,
      "week" -> week,
      "month" -> month,
      "head_and_shoulder_terms" -> head_shoulder_terms,
      "device_type" -> device_type,
      "forms" -> forms,
      "lead-revenue" -> lead_revenue,
      "classification" -> classification
    )
  }

  def pricingByClassificationLeadDataImporter(reportRequest: UberReportRequest, data: List[DBObject], log: LoggingAdapter): Unit = {
    val date_dimension_data: List[List[DBObject]] = List("day", "week", "month").map { date_dimension =>
      data.groupBy(_.getAs[String](date_dimension).get).flatMap { x =>
        x._2.groupBy(_.getAs[String]("classification").get).+(
          "Mobile" -> x._2.groupBy(_.getAs[String]("device_type").get)("Chosen Device Type")
        ).map { classification_grouped =>
          DBObject(
            date_dimension -> x._1,
            "classification" -> classification_grouped._1,
            "forms" -> classification_grouped._2.map(_.getAsOrElse[Int]("forms", 0)).sum,
            "lead-revenue" -> classification_grouped._2.map(_.getAsOrElse[Double]("lead-revenue", 0.0)).sum,
            "lead-form" -> (
              classification_grouped._2.map(_.getAsOrElse[Double]("lead-revenue", 0.0)).sum / classification_grouped._2.map(_.getAsOrElse[Int]("forms", 0)).sum
              )
          )
        }
      }.toList
    }.asInstanceOf[List[List[DBObject]]]

    Map(
      "day" -> date_dimension_data.head,
      "week" -> date_dimension_data(1),
      "month" -> date_dimension_data(2)
    ).foreach { x =>
      x._2.foreach { item =>
        val docs = uberReportCollection(reportRequest.name, x._1).find(
          DBObject(x._1 -> item.getAs[String](x._1).get, "classification" -> item.getAs[String]("classification").get)
        ).toList
        if (docs.nonEmpty) {
          uberReportCollection(reportRequest.name, x._1).update(
            DBObject("_id" -> docs.head._id.get),
            DBObject(
              "$set" -> (docs ++ item ++ (docs.head.getAs[Double]("co-revenue") match {
                case Some(co_revenue) =>
                  DBObject("vpimp" -> (co_revenue / item.getAsOrElse[Int]("forms", 0)))
                case _ =>
                  DBObject()
              }) ++ (docs.head.getAs[Int]("co") match {
                case Some(co) =>
                  DBObject("ad-imp-c-rate" -> (co / item.getAsOrElse[Int]("forms", 0)))
                case _ =>
                  DBObject()
              }) ++ (item.getAs[Double]("vpimp") match {
                case Some(vpimp) =>
                  DBObject("value_form" -> (vpimp + item.getAsOrElse[Double]("lead-form", 0.0)))
                case _ =>
                  DBObject()
              })
            )
          ))
        } else {
          uberReportCollection(reportRequest.name, x._1).insert(item)
        }
      }
    }
  }

  def pricingByClassificationAIQLeadDataRowParser(reportRequest: UberReportRequest, item: DBObject, log: LoggingAdapter): DBObject = {
    val day = stringToDateTime(item.getAs[String]("date"), "yyyy-MM-dd")
    val week = stringToDateTime(item.getAs[String]("week"), "yyyy-MM-dd")
    val month = stringToDateTime(item.getAs[String]("month"), "yyyy-MM")
    val device_type = item.getAs[String]("device type").get
    val forms = item.getAs[String]("forms").get.replaceAll("\\D+", "").toInt
    val lead_revenue = item.getAsOrElse[Double]("lead-revenue", 0.0)
    val classification = "AIQ"

    DBObject(
      "day" -> day,
      "week" -> week,
      "month" -> month,
      "device_type" -> device_type,
      "forms" -> forms,
      "lead-revenue" -> lead_revenue,
      "classification" -> classification
    )
  }

  def pricingByClassificationAIQLeadDataImporter(reportRequest: UberReportRequest, data: List[DBObject], log: LoggingAdapter): Unit = {
    val day_grouped = data.groupBy(_.getAs[String]("day").get)
    val week_grouped = data.groupBy(_.getAs[String]("week").get)
    val month_grouped = data.groupBy(_.getAs[String]("month").get)

    val day_data = day_grouped.flatMap { x =>
      x._2.groupBy(_.getAs[String]("classification")).map { classification_grouped =>
        DBObject(
          "day" -> x._1,
          "classification" -> classification_grouped._1,
          "forms" -> classification_grouped._2.map(_.getAsOrElse[Int]("forms", 0)).sum,
          "lead-revenue" -> classification_grouped._2.map(_.getAsOrElse[Double]("lead-revenue", 0.0)).sum,
          "lead-form" -> (
            classification_grouped._2.map(
              _.getAsOrElse[Double]("lead-revenue", 0.0)
            ).sum / classification_grouped._2.map(
              _.getAsOrElse[Int]("forms", 0)
            ).sum
            )
        )
      }
    }

    val week_data = week_grouped.flatMap { x =>
      x._2.groupBy(_.getAs[String]("classification")).map { classification_grouped =>
        DBObject(
          "week" -> x._1,
          "classification" -> classification_grouped._1,
          "forms" -> classification_grouped._2.map(_.getAsOrElse[Int]("forms", 0)).sum,
          "lead-revenue" -> classification_grouped._2.map(_.getAsOrElse[Double]("lead-revenue", 0.0)).sum,
          "lead-form" -> (
            classification_grouped._2.map(
              _.getAsOrElse[Double]("lead-revenue", 0.0)
            ).sum / classification_grouped._2.map(
              _.getAsOrElse[Int]("forms", 0)
            ).sum
            )
        )
      }
    }

    val month_data = month_grouped.flatMap { x =>
      x._2.groupBy(_.getAs[String]("classification")).map { classification_grouped =>
        DBObject(
          "month" -> x._1,
          "classification" -> classification_grouped._1,
          "forms" -> classification_grouped._2.map(_.getAsOrElse[Int]("forms", 0)).sum,
          "lead-revenue" -> classification_grouped._2.map(_.getAsOrElse[Double]("lead-revenue", 0)).sum,
          "lead-form" -> (
            classification_grouped._2.map(
              _.getAsOrElse[Double]("lead-revenue", 0.0)
            ).sum / classification_grouped._2.map(
              _.getAsOrElse[Int]("forms", 0)
            ).sum
            )
        )
      }
    }

    Map(
      "day" -> day_data,
      "week" -> week_data,
      "month" -> month_data
    ).foreach { x =>
      x._2.foreach { item =>
        val docs = uberReportCollection(reportRequest.name, x._1).find(
          DBObject(x._1 -> item.getAs[String](x._1).get, "classification" -> item.getAs[String]("classification").get)
        ).toList
        if (docs.nonEmpty) {
          uberReportCollection(reportRequest.name, x._1).update(
            DBObject("_id" -> docs.head._id.get),
            DBObject(
              "$set" -> (docs.head ++ item ++ (docs.head.getAs[Double]("co-revenue") match {
                case Some(co_revenue) =>
                  DBObject("vpimp" -> (co_revenue / item.getAsOrElse[Int]("forms", 0)))
                case _ =>
                  DBObject()
              }) ++ (docs.head.getAs[Int]("co") match {
                case Some(co) =>
                  DBObject("ad-imp-c-rate" -> (co / item.getAsOrElse[Int]("forms", 0)))
                case _ =>
                  DBObject()
              }) ++ (item.getAs[Double]("vpimp") match {
                case Some(vpimp) =>
                  DBObject("value_form" -> (vpimp + item.getAsOrElse[Double]("lead-form", 0.0)))
                case _ =>
                  DBObject()
              }))
            )
          )
        } else {
          uberReportCollection(reportRequest.name, x._1).insert(item.asDBObject)
        }
      }
    }
  }

  def pricingByClassificationClickDataRowParser(reportRequest: UberReportRequest, item: DBObject, log: LoggingAdapter): DBObject = {
    val date = stringToDateTime(item.getAs[String]("date"), "yyyy-MM-dd")
    val week = stringToDateTime(item.getAs[String]("week"), "yyyy-MM-dd")
    val month = stringToDateTime(item.getAs[String]("month"), "yyyy-MM")
    val refby = item.getAs[String]("refby").get
    val ad_impressions = item.getAs[Int]("ad-imp").getOrElse(0)
    val co = item.getAs[Int]("co").getOrElse(0)
    val co_revenue = item.getAsOrElse[Double]("co-revenue", 0.0)
    val classification = refby_classifications.keySet.contains(item.getAsOrElse[String]("refby", "")) match {
      case true =>
        refby_classifications(item.getAs[String]("refby").get)
      case false =>
        "Other"
    }

    DBObject(
      "day" -> date,
      "week" -> week,
      "month" -> month,
      "refby" -> refby,
      "ad-imp" -> ad_impressions,
      "co" -> co,
      "co-revenue" -> co_revenue,
      "classification" -> classification
    )
  }

  def pricingByClassificationClickDataImporter(reportRequest: UberReportRequest, data: List[DBObject], log: LoggingAdapter): Unit = {
    val date_grouped = data.groupBy(_.getAs[String]("day").get)
    val week_grouped = data.groupBy(_.getAs[String]("week").get)
    val month_grouped = data.groupBy(_.getAs[String]("month").get)

    val date_data = date_grouped.flatMap { x =>
      x._2.groupBy(_.getAs[String]("classification").get).map { classification_grouped =>
        DBObject(
          "day" -> x._1,
          "classification" -> classification_grouped._1,
          "co" -> classification_grouped._2.map(_.getAsOrElse[Int]("co", 0)).sum,
          "co-revenue" -> classification_grouped._2.map(_.getAsOrElse[Double]("co-revenue", 0.0)).sum
        )
      }
    }

    val week_data = week_grouped.flatMap { x =>
      x._2.groupBy(_.getAs[String]("classification")).map { classification_grouped =>
        DBObject(
          "week" -> x._1,
          "classification" -> classification_grouped._1,
          "co" -> classification_grouped._2.map(_.getAsOrElse[Int]("co", 0)).sum,
          "co-revenue" -> classification_grouped._2.map(_.getAsOrElse[Double]("co-revenue", 0.0)).sum
        )
      }
    }

    val month_data = month_grouped.flatMap { x =>
      x._2.groupBy(_.getAs[String]("classification")).map { classification_grouped =>
        DBObject(
          "month" -> x._1,
          "classification" -> classification_grouped._1,
          "co" -> classification_grouped._2.map(_.getAsOrElse[Int]("co", 0)).sum,
          "co-revenue" -> classification_grouped._2.map(_.getAsOrElse[Double]("co-revenue", 0.0)).sum
        )
      }
    }

    Map(
      "day" -> date_data,
      "week" -> week_data,
      "month" -> month_data
    ).foreach { x =>
      x._2.foreach { item =>
        val docs = uberReportCollection(reportRequest.name, x._1).find(
          DBObject(x._1 -> item.getAs[String](x._1).get, "classification" -> item.getAs[String]("classification").get)
        ).toList
        if (docs.nonEmpty) {
          uberReportCollection(reportRequest.name, x._1).update(
            DBObject("_id" -> docs.head._id.get),
            DBObject(
              "$set" -> (docs.head ++ item ++ (docs.head.getAs[Int]("forms") match {
                case Some(forms) =>
                  DBObject(
                    "vpimp" -> (item.getAsOrElse[Double]("co-revenue", 0.0) / forms),
                    "vpoc" -> (item.getAsOrElse[Double]("co-revenue", 0.0) / item.getAsOrElse[Int]("co", 0)),
                    "ad-imp-c-rate" -> (item.getAsOrElse[Int]("co", 0) / forms)
                  )
                case _ =>
                  DBObject()
              }))
            )
          )
        } else {
          uberReportCollection(reportRequest.name, x._1).insert(item)
        }
      }
    }
  }
}