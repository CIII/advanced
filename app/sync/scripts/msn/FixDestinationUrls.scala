/**
 * Name - FixAdDestinationUrls
 * Checks each destination url for semreporting=uber, and xid query string parameters.
 * If not present it updates msn.
 *
 * @return      std-out
 *
 * @author Clarence Williams III
 * @date 2014-10-16
 */

package sync.scripts.msn

import java.net.URLEncoder

import akka.actor.{Actor, Props}
import akka.event.Logging
import com.microsoft.bingads.campaignmanagement._
import models.agile_production_db.msn.MsnDB
import org.squeryl.PrimitiveTypeMode._
import sync.msn.bingads.BingAdsHelper
import sync.scripts.shared.Shared._

import scala.collection.mutable.ListBuffer

object FixDestinationUrls {

  case class ProcessAdGroupOptions(
                                    entityMap: List[EntityMap],
                                    prefix: String,
                                    tids: Map[Long, Int],
                                    customerId: Int,
                                    userName: String,
                                    password: String,
                                    accessKey: String,
                                    customerAccountId: Long
                                    )

  def updateKeywordDestinationUrl(
                                   BingAds: BingAdsHelper,
                                   destinationUrlMap: List[destinationUrlMapping],
                                   mutate: Boolean = true
                                   ): Either[Map[Long, ListBuffer[Keyword]], List[UpdateKeywordsResponse]] = {
    var keywordOperations = Map[Long, ListBuffer[Keyword]]()
    for (destUrlMap <- destinationUrlMap) {
      val keyword = new Keyword()
      keyword.setId(destUrlMap.entityId)
      keyword.setParam1(destUrlMap.destinationUrl)

      if (keywordOperations.get(destUrlMap.adGroupId).isDefined)
        keywordOperations(destUrlMap.adGroupId) += keyword
      else
        keywordOperations += destUrlMap.adGroupId -> ListBuffer[Keyword](keyword)
    }

    if (mutate) {
      Right(
        keywordOperations.map {
          case (key, value) =>
            val keywordRequest = new UpdateKeywordsRequest
            keywordRequest.setAdGroupId(key)
            val keywordArr = new ArrayOfKeyword
            value.foreach(keywordArr.getKeywords.add)
            keywordRequest.setKeywords(keywordArr)
            BingAds.campaignManagementService.getService.updateKeywords(keywordRequest)
        }.toList
      )
    } else {
      Left(keywordOperations)
    }
  }

  class ProcessAdGroupActor extends Actor {
    val log = Logging(context.system, this)

    def receive = {
      case msg =>
        try {
          val processAdGroupOptions = msg.asInstanceOf[ProcessAdGroupOptions]
          val prefix = processAdGroupOptions.prefix.replace("=", "")
          val bingAdsHelper = new BingAdsHelper(
            processAdGroupOptions.userName,
            processAdGroupOptions.password,
            processAdGroupOptions.accessKey,
            Some(processAdGroupOptions.customerId),
            Some(processAdGroupOptions.customerAccountId)
          )

          val destinationUrlMap = new ListBuffer[destinationUrlMapping]()
          for (entityMap <- processAdGroupOptions.entityMap) {
            val destinationUrlParsed = parseUriParameters(entityMap.destination_url)
            val oldDestinationUrl = entityMap.destination_url

            val destinationUrlSplit = oldDestinationUrl.split("\\?")

            val newQsParams = new ListBuffer[String]()

            if (destinationUrlParsed.keys.count(_ == prefix) == 0) {
              newQsParams += processAdGroupOptions.prefix + (entityMap.internal_keyword_id * 1000 + processAdGroupOptions.tids(entityMap.ad_group_api_id))
            } else {
              newQsParams += processAdGroupOptions.prefix + destinationUrlParsed(prefix)
            }

            var newDestinationUrl = destinationUrlSplit(0) + "?"

            val destinationUrlParsedKeysMatcher = destinationUrlParsed.keys.toList.map(_.toLowerCase)

            if (!(destinationUrlParsedKeysMatcher contains qsKey(semReporting))) {
              newQsParams += semReporting
            }

            if (!(destinationUrlParsedKeysMatcher contains qsKey(matchType))) {
              newQsParams += matchType
            }

            if (!(destinationUrlParsedKeysMatcher contains qsKey(device))) {
              newQsParams += device
            }

            if (!(destinationUrlParsedKeysMatcher contains qsKey(msnCreative))) {
              newQsParams += msnCreative
            }

            for ((key, value) <- destinationUrlParsed) {
              if (key != prefix && key != "ap") {
                newQsParams += "%s=%s".format(key, if (!value.contains("{")) URLEncoder.encode(value, "UTF-8") else value)
              }
            }
            newDestinationUrl += newQsParams.toArray.mkString("&")

            destinationUrlMap += destinationUrlMapping(adGroupId = entityMap.ad_group_api_id, entityId = entityMap.keyword_api_id, destinationUrl = newDestinationUrl)
          }
          if (destinationUrlMap.length > 0) {
            log.info("Running batch AdWords update (%s) keywords)...".format(destinationUrlMap.size))
            //val result = updateKeywordDestinationUrl(bingAdsHelper, destinationUrlMap.toList, true)
            //checkErrors(result.right.get.map(_.getPartialErrors).flatten.toArray, result.right.get.length, context.self.path.name)
          }
        } catch {
          case e: Throwable => log.info("Error occurred %s".format(e.toString))
        } finally {
          running_processes = running_processes - 1
        }
    }
  }

  def main(args: Array[String]) {
    /*if (args.length == 0) {
      println(usage)
      sys.exit(0)
    }

    val options = nextOption(Map(), args.toList)*/

    using(MsnDB.session) {
      val resultset = join(
        MsnDB.keywordThem,
        MsnDB.keywordUs,
        MsnDB.adGroupUs,
        MsnDB.accountInfo,
        MsnDB.apiAccount
      )((kt, ku, agu, ai, aa) =>
        where(
          kt.client_id === 89
            and kt.removed_bool === 0
            and not(kt.param1.isNull)
            and (ku.account_id === 2 or ku.account_id === 4)
        ) select(
          ai.customer_id,
          aa.access_key,
          ai.username,
          ai.password,
          ai.tid,
          kt.api_id,
          kt.param1,
          kt.client_id,
          ku.ad_group_us_id,
          agu.api_id,
          ku.keyword_id,
          ai.api_id
          ) on(
          ku.us_id === kt.us_id,
          agu.us_id === ku.ad_group_us_id,
          ai.id_val === ku.account_id and ai.client_id === ku.client_id,
          aa.id === ai.api_account_id
          )
      )

      var offset = 0

      while (resultset.page(batch_limit * offset, batch_limit).nonEmpty) {
        var username: String = ""
        var password: String = ""
        var access_key: String = ""
        var customer_id: Option[Int] = None
        var customer_account_id: Option[Long] = None
        var prefix: Option[String] = None
        var updateMap = ListBuffer[EntityMap]()
        var tids = Map[Long, Int]()

        val processAdGroup = system.actorOf(Props[ProcessAdGroupActor], name = "MsnProcessAdGroupActor_" + offset)

        for (result <- resultset.page(batch_limit * offset, batch_limit)) {
          try {
            username = result._3
            password = result._4
            access_key = result._2
            customer_id = Some(result._1)
            customer_account_id = Some(result._12)

            updateMap += EntityMap(
              ad_group_api_id = result._10,
              keyword_api_id = result._6,
              internal_keyword_id = result._11,
              destination_url = result._7
            )

            if (result._7 == null || result._7 == "") {
              var baseUrl = from(MsnDB.keywordUs)((ku) =>
                where(
                  ku.param1.isNotNull
                    and ku.param1 <> Some("{param1}")
                    and ku.client_id === result._9
                    and ku.ad_group_us_id === result._10
                    and ku.removed_bool === 0
                )
                  select ku.param1
              ).headOption.orNull

              if (baseUrl == null)
                baseUrl = from(MsnDB.textAdUs)((tau) =>
                  where(
                    tau.client_id === result._8
                      and tau.ad_group_us_id === result._9
                      and tau.destination_url.isNotNull
                      and tau.removed_bool === 0
                      and tau.destination_url <> Some("{param1}")
                  )
                    select tau.destination_url
                ).headOption.orNull

              if (baseUrl == null)
                throw new Exception("BaseUrl Not Found in keywordUs or adUs...Skipping (AdGroupId: %s, Destination_Url: %s)".format(
                  result._10,
                  result._7
                ))

              updateMap.last.destination_url = baseUrl
            }

            val prefixArr = prefixes.filter(x => x.client_id == result._8)
            assert(prefixArr.nonEmpty, "ClientID (%s) has no prefix defined.".format(result._9.toString))
            prefix = Some(prefixArr.head.prefix)
            tids += (result._10 -> result._5.toInt)
          } catch {
            case e: Throwable => println("Error -> %s".format(e.toString))
          }
        }
        println("Processing %s - %s".format(batch_limit * offset, batch_limit * offset + batch_limit))
        processAdGroup ! ProcessAdGroupOptions(
          updateMap.toList,
          prefix.getOrElse("id="),
          tids,
          customer_id.get,
          username,
          password,
          access_key,
          customer_account_id.get
        )
        offset += 1
        running_processes += 1
      }

      while (running_processes > 0) {
        println("Status: %s Threads still running...".format(running_processes))
        Thread.sleep(5000)
      }
      sys.exit(1)
    }
  }
}
