package sync.scripts.msn

import java.io.{FileWriter, BufferedWriter, File}

import Shared.Shared._
import akka.actor.Actor
import akka.event.Logging
import helpers.msn.reporting.ReportingControllerHelper._
import helpers.google.reporting.ReportingControllerHelper.googleReportDateFormatter
import models.mongodb.msn.Msn._
import org.joda.time.DateTime

class ExportKeywordPerformanceReportRequestCsvActor extends Actor {
  val log = Logging(context.system, this)

  def receive = {
    case dateRange: (Option[String], Option[String]) =>
      try {
        val yesterday = DateTime.now.minusDays(1)
        val filename = "/tmp/msn_keyword_performance_%s.csv".format(
          dateRange._1.getOrElse(googleReportDateFormatter.print(yesterday))
        )
        val file = new File(filename)
        val bw = new BufferedWriter(new FileWriter(file))
        bw.write(mongoToCsv(
          msnReportCollection(MsnReportType.KeywordPerformanceReportRequest)
            .find(buildQry("", "", "", "", dateRange._1.getOrElse(""), dateRange._2.getOrElse("")))
            .toList
        ))
        bw.close()
      } catch {
        case e: Exception =>
          log.error("Error exporting msn keyword performance report CSV - %s".format(e.toString))
      }
  }
}