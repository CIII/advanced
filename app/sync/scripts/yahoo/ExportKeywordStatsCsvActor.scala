package sync.scripts.yahoo

import java.io.{BufferedWriter, File, FileWriter}

import Shared.Shared._
import akka.actor.Actor
import akka.event.Logging
import helpers.yahoo.reporting.ReportingControllerHelper._
import models.mongodb.yahoo.Yahoo._
import org.joda.time.DateTime

class ExportKeywordStatsCsvActor extends Actor {
  val log = Logging(context.system, this)

  def receive = {
    case dateRange: (Option[String], Option[String]) =>
      try {
        val yesterday = DateTime.now.minusDays(1)
        val filename = "/tmp/yahoo_keyword_stats_%s.csv".format(
          dateRange._1.getOrElse(yahooReportDateFormatter.print(yesterday))
        )
        val file = new File(filename)
        val bw = new BufferedWriter(new FileWriter(file))
        bw.write(mongoToCsv(
          yahooReportCollection(GeminiReportType.keyword_stats)
            .find(buildQry("", "", "", "", "", dateRange._1.getOrElse(""), dateRange._2.getOrElse("")))
            .toList
        ))
        bw.close()
      } catch {
        case e: Exception =>
          log.error("Error exporting yahoo keyword stats report CSV - %s".format(e.toString))
      }
  }
}
