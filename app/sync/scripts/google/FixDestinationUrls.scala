/**
 * Name - FixAdDestinationUrls
 * Checks each destination url for semreporting=uber, and xid query string parameters.
 * If not present it updates google.
 *
 * @param  --client-id  adverplex specific clientid.
 * @return      std-out
 *
 * @author Clarence Williams III
 * @date 2014-08-12
 */

package sync.scripts.google

import java.net._

import akka.actor._
import akka.event.Logging
import com.google.api.ads.adwords.axis.v201509.cm._
import models.agile_production_db.google.GoogleDB
import org.squeryl.PrimitiveTypeMode._
import sync.google.adwords.AdWordsHelper
import sync.scripts.shared.Google._
import sync.scripts.shared.Shared._

import scala.collection.mutable.ListBuffer

object FixDestinationUrls {

  case class ProcessAdGroupOptions(
                                    entityMap: List[EntityMap],
                                    prefix: String,
                                    tids: Map[Long, Int],
                                    customerId: String,
                                    developerToken: String,
                                    oAuthClientId: String,
                                    oAuthClientSecret: String,
                                    oAuthRefreshToken: String
                                    )

  def updateKeywordDestinationUrl(
                                   AdWords: AdWordsHelper,
                                   destinationUrlMap: List[destinationUrlMapping],
                                   mutate: Boolean = true
                                   ): Either[Array[AdGroupCriterionOperation], AdGroupCriterionReturnValue] = {
    val adGroupCriterionService = AdWords.adWordsServices.get(AdWords.adWordsSession, classOf[AdGroupCriterionServiceInterface])
    val adGroupCriterionOperations = new ListBuffer[AdGroupCriterionOperation]()
    for (destUrlMap <- destinationUrlMap) {
      val criterion = new Criterion()
      criterion.setId(destUrlMap.entityId)
      val biddableAdGroupCriterion = new BiddableAdGroupCriterion()
      biddableAdGroupCriterion.setAdGroupId(destUrlMap.adGroupId)
      biddableAdGroupCriterion.setCriterion(criterion)
      biddableAdGroupCriterion.setDestinationUrl(destUrlMap.destinationUrl)

      val adGroupCriterionOperation: AdGroupCriterionOperation = new AdGroupCriterionOperation()
      adGroupCriterionOperation.setOperand(biddableAdGroupCriterion)
      adGroupCriterionOperation.setOperator(Operator.SET)


      adGroupCriterionOperations += adGroupCriterionOperation
    }

    if (mutate) {
      Right(adGroupCriterionService.mutate(adGroupCriterionOperations.toArray))
    } else {
      Left(adGroupCriterionOperations.toArray)
    }
  }

  class ProcessAdGroupActor extends Actor {
    val log = Logging(context.system, this)

    def receive = {
      case msg =>
        try {
          val processAdGroupOptions = msg.asInstanceOf[ProcessAdGroupOptions]
          val prefix = processAdGroupOptions.prefix.replace("=", "")
          val adWordsHelper = new AdWordsHelper(
            processAdGroupOptions.oAuthClientId,
            processAdGroupOptions.oAuthClientSecret,
            processAdGroupOptions.oAuthRefreshToken,
            processAdGroupOptions.developerToken,
            Some(processAdGroupOptions.customerId)
          )

          val destinationUrlMap = new ListBuffer[destinationUrlMapping]()
          for (entityMap <- processAdGroupOptions.entityMap) {
            val destinationUrlParsed = parseUriParameters(entityMap.destination_url)
            val oldDestinationUrl = entityMap.destination_url

            val destinationUrlSplit = oldDestinationUrl.split("\\?")

            val newQsParams = new ListBuffer[String]()

            if (destinationUrlParsed.keys.count(_ == prefix) == 0) {
              newQsParams += processAdGroupOptions.prefix + (entityMap.internal_keyword_id * 1000 + processAdGroupOptions.tids(entityMap.ad_group_api_id))
            } else {
              newQsParams += processAdGroupOptions.prefix + destinationUrlParsed(prefix)(0)
            }

            var newDestinationUrl = destinationUrlSplit(0) + "?"

            if (!(destinationUrlParsed contains qsKey(semReporting))) {
              newQsParams += semReporting
            }

            if (!(destinationUrlParsed contains qsKey(matchType))) {
              newQsParams += matchType
            }

            if (!(destinationUrlParsed contains qsKey(device))) {
              newQsParams += device
            }

            if (!(destinationUrlParsed contains qsKey(adPosition))) {
              newQsParams += adPosition
            }

            if (!(destinationUrlParsed contains qsKey(googleCreative))) {
              newQsParams += googleCreative
            }

            for ((key, value) <- destinationUrlParsed) {
              if (key != prefix) {
                newQsParams += "%s=%s".format(key, if (!value.contains("{")) URLEncoder.encode(value, "UTF-8") else value)
              }
            }
            newDestinationUrl += newQsParams.toArray.mkString("&")

            checkUrl(newDestinationUrl) match {
              case true =>
                destinationUrlMap += destinationUrlMapping(adGroupId = entityMap.ad_group_api_id, entityId = entityMap.keyword_api_id, destinationUrl = newDestinationUrl)
              case false =>
                log.info("Invalid URL Skipping -> %s".format(newDestinationUrl))
            }
          }
          if (destinationUrlMap.nonEmpty) {
            log.info("Running batch AdWords update (%s) keywords)...".format(destinationUrlMap.size))
            val result = updateKeywordDestinationUrl(adWordsHelper, destinationUrlMap.toList, true)
            checkErrors(result.right.get.getPartialFailureErrors, result.right.get.getValue.length, context.self.path.name)
          }
        } catch {
          case e: Throwable => log.info("Error occurred %s".format(e.toString))
        } finally {
          running_processes = running_processes - 1
        }
    }
  }

  def main(args: Array[String]) {
    if (args.length == 0) {
      println(usage)
      sys.exit(0)
    }

    val options = nextOption(Map(), args.toList)

    client_id = options.get(Symbol("clientid"))

    using(GoogleDB.session) {
      val resultset = join(
        GoogleDB.keywordThem,
        GoogleDB.keywordUs,
        GoogleDB.adGroupUs,
        GoogleDB.accountInfo,
        GoogleDB.apiAccount
      )((kt, ku, agu, ai, aa) =>
        where(
          (kt.client_id === client_id.get.toString.toInt).inhibitWhen(client_id.isEmpty)
            and kt.removed_bool === 0
            and (not(kt.destination_url like "%semreporting%") or kt.destination_url.isNull)
            and (ku.account_id <> 20 and ku.account_id <> 21).inhibitWhen(client_id.get.toString != "93")
        ) select(
          ai.customer_id,
          aa.developer_token,
          aa.oauth_client_id,
          aa.oauth_client_secret,
          aa.oauth_refresh_token,
          ai.tid,
          kt.api_id,
          kt.destination_url,
          kt.client_id,
          ku.ad_group_us_id,
          agu.api_id,
          ku.keyword_id
          ) on(
          ku.id === kt.id,
          agu.id === ku.ad_group_us_id,
          ai.id === ku.account_id and ai.client_id === ku.client_id,
          aa.id === ai.api_account_id
          )
      )

      var offset = 0

      while (resultset.page(batch_limit * offset, batch_limit).nonEmpty) {
        var oauth_client_id: Option[String] = None
        var oauth_client_secret: Option[String] = None
        var oauth_refresh_token: Option[String] = None
        var developer_token: Option[String] = None
        var customer_id: Option[String] = None
        var prefix: Option[String] = None
        var updateMap = ListBuffer[EntityMap]()
        var tids = Map[Long, Int]()

        val googleProcessAdGroup = system.actorOf(Props[ProcessAdGroupActor], name = "GoogleProcessAdGroupActor_" + offset)

        for (result <- resultset.page(batch_limit * offset, batch_limit)) {
          try {
            oauth_client_id = Some(result._3.get)
            oauth_client_secret = Some(result._4.get)
            oauth_refresh_token = Some(result._5.get)
            developer_token = Some(result._2)
            customer_id = Some(result._1.get)

            updateMap += EntityMap(
              ad_group_api_id = result._11.get,
              keyword_api_id = result._7.get,
              internal_keyword_id = result._12.get,
              destination_url = result._8.getOrElse("")
            )

            if (result._8.isEmpty) {
              var baseUrl = from(GoogleDB.keywordUs)((ku) =>
                where(ku.destination_url.isNotNull and ku.client_id === result._9 and ku.ad_group_us_id === result._10)
                  select ku.destination_url
              ).headOption.orNull

              if (baseUrl == null)
                baseUrl = from(GoogleDB.adUs)((au) =>
                  where(au.client_id === result._9 and au.ad_group_us_id === result._10 and au.destination_url.isNotNull)
                    select au.destination_url
                ).headOption.orNull

              if (baseUrl == null)
                throw new Exception("BaseUrl Not Found in keywordUs or adUs...Skipping (AdGroupId: %s, Destination_Url: %s)".format(
                  result._10,
                  result._8
                ))

              updateMap.last.destination_url = baseUrl.get

            }

            val prefixArr = prefixes.filter(x => x.client_id == result._9)
            assert(prefixArr.nonEmpty, "ClientID (%s) has no prefix defined.".format(result._9.toString))
            prefix = Some(prefixArr.head.prefix)
            tids += (result._11.get -> result._6.toInt)
          } catch {
            case e: Throwable => println("Error -> %s".format(e.toString))
          }
        }
        println("Processing %s - %s".format(batch_limit * offset, batch_limit * offset + batch_limit))
        googleProcessAdGroup ! ProcessAdGroupOptions(
          updateMap.toList,
          prefix.getOrElse("id="),
          tids,
          customer_id.getOrElse(""),
          developer_token.getOrElse(""),
          oauth_client_id.getOrElse(""),
          oauth_client_secret.getOrElse(""),
          oauth_refresh_token.getOrElse("")
        )
        offset += 1
        running_processes += 1
      }

      while (running_processes > 0) {
        println("Status: %s Threads still running...".format(running_processes))
        Thread.sleep(5000)
      }
      sys.exit(1)
    }
  }
}