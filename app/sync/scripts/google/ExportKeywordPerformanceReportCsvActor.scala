package sync.scripts.google

import java.io.{BufferedWriter, File, FileWriter}

import Shared.Shared._
import akka.actor.Actor
import akka.event.Logging
import com.google.api.ads.adwords.lib.jaxb.v201509.ReportDefinitionReportType
import helpers.google.reporting.ReportingControllerHelper._
import models.mongodb.google.Google._
import org.joda.time.DateTime

class ExportKeywordPerformanceReportCsvActor extends Actor {
  val log = Logging(context.system, this)

  def receive = {
    case dateRange: (Option[String], Option[String]) =>
      try {
        val yesterday = DateTime.now.minusDays(1)
        val filename = "/tmp/google_keyword_performance_%s.csv".format(
          dateRange._1.getOrElse(googleReportDateFormatter.print(yesterday))
        )
        val file = new File(filename)
        val bw = new BufferedWriter(new FileWriter(file))
        bw.write(mongoToCsv(
          googleReportCollection(ReportDefinitionReportType.KEYWORDS_PERFORMANCE_REPORT)
            .find(buildQry("", "", "", "", dateRange._1.getOrElse(""), dateRange._2.getOrElse("")))
            .toList
        ))
        bw.close()
      } catch {
        case e: Exception =>
          log.error("Error exporting google keyword performance report CSV - %s".format(e.toString))
      }
  }
}
