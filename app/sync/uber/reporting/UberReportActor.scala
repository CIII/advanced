package sync.uber.reporting

import Shared.Shared._
import akka.actor.Actor
import akka.event.Logging
import com.mongodb.casbah.Imports._
import com.ning.http.client.AsyncHttpClientConfigBean
import models.mongodb.Utilities._
import play.api.Play
import play.api.Play.current
import play.api.libs.ws.ning.NingWSClient
import play.api.libs.ws.{WS, WSResponse}

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration

class UberReportActor extends Actor {
  val log = Logging(context.system, this)

  def receive = {
    case reportRequest: UberReportRequest =>
      try {
        log.info("Processing %s (%s) - format %s".format(
          reportRequest.name,
          reportRequest.preset_id.getOrElse(reportRequest.report_id.getOrElse("N/A")),
          reportRequest.format
        ))

        val uber_login_url = Play.current.configuration.getString("uber.login_url").getOrElse(
          throw new Exception("uber login url configuration missing")
        )

        val username = Play.current.configuration.getString("uber.credentials.username").getOrElse(
          throw new Exception("uber login credentials missing")
        )
        val password = Play.current.configuration.getString("uber.credentials.password").getOrElse(
          throw new Exception("uber login credentials missing")
        )

        var processedData = List[DBObject]()

        // Workaround for 'acceptAllCertificates' bug
        val config=new AsyncHttpClientConfigBean()
        config.setAcceptAnyCertificate(true)
        config.setFollowRedirect(true)
        config.setConnectionTimeOut(10000)
        config.setRequestTimeout(10000)
        val client=new NingWSClient(config)

        Await.result(
          client.url(uber_login_url)
          .withHeaders("Accept" -> "application/%s".format(reportRequest.format))
          .withQueryString("r" -> "/reports_uber/%s?%s".format(
            reportRequest.format,
            reportRequest.preset_id match {
              case Some(id) =>
                "preset=%s".format(id)
              case _ =>
                "report_id=%s".format(reportRequest.report_id.getOrElse(""))
            }
          ))
          .post(
              Map(
                "username" -> Seq(username),
                "password" -> Seq(password)
              )
            ).map { report =>
            for (item <- csvToJson(report.body.replace("$", ""))) {
              processedData = processedData :+ reportRequest.rowParser(
                reportRequest,
                com.mongodb.util.JSON.parse(item.toString()).asInstanceOf[DBObject],
                log
              )
            }
          }, Duration.Inf
        )
        reportRequest.importer(reportRequest, processedData, log)
      } catch {
        case e: Exception =>
          log.info("Error Processing Uber Report %s (%s) - %s".format(
            reportRequest.name,
            reportRequest.preset_id.getOrElse(reportRequest.report_id.getOrElse("")),
            e.toString
          ))
          e.getStackTrace.foreach{x =>
            log.info(x.toString)
          }
      }
  }
}
