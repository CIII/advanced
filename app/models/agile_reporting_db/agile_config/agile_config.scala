package models.agile_reporting_db.agile_config

import com.typesafe.config.ConfigFactory
import org.squeryl.PrimitiveTypeMode._
import org.squeryl.adapters.MySQLAdapter
import org.squeryl.dsl._
import org.squeryl.{KeyedEntity, Schema}

/**
 * agile-reporting-db/agile_config (mysql).
 */

class DestinationUrlPatterns(
  val client_id: Int,
  val prefix: String,
  val qs_only: Int
) extends KeyedEntity[CompositeKey2[Int, String]] {
  def id = compositeKey(client_id, prefix)
}


object AgileConfigDB extends Schema {
  Class.forName("com.mysql.jdbc.Driver")
  val config = ConfigFactory.load()
  val session = org.squeryl.Session.create(
    java.sql.DriverManager.getConnection(
      config.getString("db.agile_reporting_db_agile_config.url"),
      config.getString("db.agile_reporting_db_agile_config.user"),
      config.getString("db.agile_reporting_db_agile_config.password")
    ),
    new MySQLAdapter
  )

  val destinationUrlPatterns = table[DestinationUrlPatterns]("destination_url_patterns")
}