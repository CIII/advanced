package models.mongodb.facebook

import Shared.Shared.NanigansReportType.NanigansReportType
import Shared.Shared._
import com.mongodb.casbah.Imports._
import org.bson.types.ObjectId

object Facebook {
  def nanigansApiAccountCollection = advancedCollection("nanigans_api_account")
  def nanigansReportCollection(reportType: NanigansReportType) = advancedCollection(
    "facebook_nanigans_%s_report".format(reportType.toString.toLowerCase)
  )

  case class NanigansApiAccount(
    _id: Option[ObjectId],
    partnerId: String,
    partnerName: String,
    secretKey: String,
    baUserName: String,
    baPassword: String
  )

  def nanigansApiAccountToDBObject(naa: NanigansApiAccount): DBObject = {
    DBObject(
      "_id" -> naa._id.getOrElse(new ObjectId),
      "partnerId" -> naa.partnerId,
      "partnerName" -> naa.partnerName,
      "secretKey" -> naa.secretKey,
      "baUserName" -> naa.baUserName,
      "baPassword" -> naa.baPassword
    )
  }

  def DBObjectToNanigansApiAccount(dbo: DBObject): NanigansApiAccount = {
    NanigansApiAccount(
      _id = dbo._id,
      partnerId = dbo.as[String]("partnerId"),
      partnerName = dbo.as[String]("partnerName"),
      secretKey = dbo.as[String]("secretKey"),
      baUserName = dbo.as[String]("baUserName"),
      baPassword = dbo.as[String]("baPassword")
    )
  }
}
