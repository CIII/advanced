package models.mongodb

import Shared.Shared._
import com.mongodb.casbah.Imports._
import org.joda.time.DateTime

object Task extends {
  def taskCollection = advancedCollection("task")

  def taskToDbo(task: TaskStructure): DBObject = {
    DBObject(
      "" -> task.id,
      "complete" -> task.complete,
      "completeTime" -> task.completeTime,
      "data" -> task.data.map(pendingCacheStructureToDbo),
      "processes" -> task.processes.map(processToDbo),
      "startTime" -> task.startTime
    )
  }

  def dboToTask(dbo: DBObject) = TaskStructure(
      id=dbo.getAs[Long]("id").get,
      complete=dbo.getAs[Boolean]("complete").get,
      completeTime=Some(new DateTime(dbo.getAsOrElse[Long]("completeTime", 0))),
      data=dbo.getAsOrElse[List[DBObject]]("data", List()).map(dboToPendingCacheStructure),
      processes=dbo.getAsOrElse[List[DBObject]]("processes", List()).map(dboToProcess),
      startTime= new DateTime(dbo.getAs[Long]("startTime").getOrElse(0))
  )
}