package models.mongodb

import Shared.Shared._
import be.objectify.deadbolt.core.models.{Permission, Role, Subject}
import com.mongodb.casbah.Imports._
import models.mongodb.SecurityRole._
import org.bson.types.ObjectId
import play.libs.Scala

import scala.collection.mutable.ListBuffer

case class UserAccount(
  _id: Option[ObjectId],
  var userName: String,
  var email: String,
  var securityRoles: Array[ObjectId]
) extends Subject {

  def getRoles: java.util.List[_ <: Role] = {
    var userSecurityRoles: ListBuffer[SecurityRole] = new ListBuffer()
    this.securityRoles.foreach(
      roleId =>
        userSecurityRoles += dboToSecurityRole(
          SecurityRole.securityRoleCollection.findOne(DBObject("_id" -> roleId)).get.asDBObject
        )
    )
    Scala.asJava(userSecurityRoles)
  }

  def getPermissions: java.util.List[_ <: Permission] = {
    var userSecurityRoles: ListBuffer[PermissionGroup] = new ListBuffer()
    this.securityRoles.foreach(
      roleId =>
        userSecurityRoles = userSecurityRoles ++ dboToSecurityRole(SecurityRole.securityRoleCollection
          .findOne(DBObject("_id" -> roleId)).get.asDBObject).permissions
    )
    Scala.asJava(userSecurityRoles)
  }

  def getIdentifier: String = userName
}

object UserAccount {
  def userAccountCollection = advancedCollection("user_account")

  def userAccountToDbo(userAccount: UserAccount): DBObject = {
    DBObject(
      "_id" -> userAccount._id.getOrElse(new ObjectId),
      "email" -> userAccount.email,
      "securityRoles" -> userAccount.securityRoles,
      "userName" -> userAccount.userName
    )
  }

  def dboToUserAccount(dbo: DBObject): UserAccount = {
    UserAccount(
      _id=dbo._id,
      email=dbo.getAs[String]("email").get,
      securityRoles=dbo.getAsOrElse[List[ObjectId]]("securityRoles", List()).toArray,
      userName=dbo.getAs[String]("userName").get
    )
  }
}