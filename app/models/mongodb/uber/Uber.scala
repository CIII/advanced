package models.mongodb.uber

import Shared.Shared._

object Uber {
  def uberReportCollection(reportName: String, frequency: String) = advancedCollection(
    "uber_%s_%s".format(reportName, frequency)
  )
}