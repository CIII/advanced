package models.agile_production_db.msn

import com.typesafe.config.ConfigFactory
import org.squeryl.PrimitiveTypeMode._
import org.squeryl.adapters.MySQLAdapter
import org.squeryl.annotations.Column
import org.squeryl.dsl._
import org.squeryl.{KeyedEntity, Schema}


/**
 * agile-production-db/msn (mysql).
 */


class ApiAccount(
  @Column("id")
  val id_val: Int,
  val username: String,
  val password: String,
  val access_key: String,
  val sandbox_bool: Int,
  val third_party_bool: Int
) extends KeyedEntity[Int] {
  def id = id_val
}

class AccountInfo(
  val client_id: Int,
  @Column("id")
  val id_val: Int,
  val username: String,
  val password: String,
  val tid: Int,
  val start_date: java.util.Date,
  val end_date: java.util.Date,
  val api_account_id: Int,
  val api_id: Long,
  val customer_id: Int
 ) extends KeyedEntity[CompositeKey2[Int, Int]] {
  def id = compositeKey(client_id, id_val)
}

class AccountUs(
  val us_id: Int,
  val base_name: String,
  val upload_tag: String,
  val client_id: Int,
  val tid: Int,
  val account_id: Int,
  val name: String,
  val api_id: Long,
  val account_number: String,
  val status: String,
  val preferred_language_type: String,
  val sales_house_customer_id: Int,
  val bill_to_customer_id: Int,
  val agency_contact_name: String,
  val payment_options_type: String,
  val preferred_currency_type: String,
  val removed_bool: Int
) extends KeyedEntity[Int] {
  def id = us_id
}

class AccountThem(
  val them_id: Int,
  val us_id: Int,
  val client_id: Int,
  val tid: Int,
  val account_id: Int,
  val name: String,
  val api_id: Long,
  val account_number: String,
  val status: String,
  val preferred_language_type: String,
  val sales_house_customer_id: Int,
  val bill_to_customer_id: Int,
  val agency_contact_name: String,
  val payment_options_type: String,
  val preferred_currency_type: String,
  val removed_bool: Int
) extends KeyedEntity[Int] {
  def id = them_id
}

class AccountThemHistory(
  val them_history_id: Long,
  val them_id: Int,
  val tsecs: Int,
  val job_id: Option[Int],
  val operation: Option[String],
  val us_id: Int,
  val client_id: Int,
  val tid: Int,
  val account_id: Int,
  val name: String,
  val api_id: Long,
  val account_number: String,
  val status: String,
  val preferred_language_type: String,
  val sales_house_customer_id: Int,
  val bill_to_customer_id: Int,
  val agency_contact_name: String,
  val payment_options_type: String,
  val preferred_currency_type: String,
  val removed_bool: Int
) extends KeyedEntity[CompositeKey3[Long, Option[Int], Int]] {
  def id = compositeKey(them_history_id, job_id, client_id)
}

class CampaignUs(
  val us_id: Int,
  val account_us_id: Int,
  val account_id: Int,
  val base_name: String,
  val upload_tag: String,
  val client_id: Int,
  val tid: Int,
  val api_id: Long,
  val name: String,
  val description: String,
  val status: String,
  val monthly_budget_cents: Int,
  val daily_budget_cents: Int,
  val budget_type: String,
  val v8_budget_type: String,
  val daylight_saving_bool: Int,
  val time_zone: String,
  val conversion_tracking_enabled_bool: Int,
  val conversion_tracking_script: String,
  val removed_bool: Int,
  val cash_back: Int,
  val paused_bool: Int
) extends KeyedEntity[Int] {
  def id = us_id
}

class CampaignThem(
  val them_id: Int,
  val account_them_id: Int,
  val us_id: Int,
  val client_id: Int,
  val tid: Int,
  val api_id: Long,
  val name: String,
  val description: String,
  val status: String,
  val monthly_budget_cents: Int,
  val daily_budget_cents: Int,
  val budget_type: String,
  val v8_budget_type: String,
  val daylight_saving_bool: Int,
  val time_zone: String,
  val conversion_tracking_enabled_bool: Int,
  val conversion_tracking_script: String,
  val removed_bool: Int,
  val cash_back: Int,
  val paused_bool: Int
) extends KeyedEntity[Int] {
  def id = them_id
}

class CampaignThemHistory(
  val them_history_id: Long,
  val account_them_id: Int,
  val them_id: Int,
  val tsecs: Int,
  val job_id: Option[Int],
  val operation: Option[String],
  val us_id: Option[Int],
  val client_id: Int,
  val tid: Int,
  val api_id: Long,
  val name: String,
  val description: String,
  val status: String,
  val monthly_budget_cents: Int,
  val daily_budget_cents: Int,
  val budget_type: String,
  val v8_budget_type: String,
  val daylight_saving_bool: Int,
  val time_zone: String,
  val conversion_tracking_enabled_bool: Int,
  val conversion_tracking_script: String,
  val removed_bool: Int,
  val cash_back: Int,
  val paused_bool: Int
) extends KeyedEntity[CompositeKey4[Long, Option[Int], Option[Int], Int]] {
  def id = compositeKey(them_history_id, job_id, us_id, client_id)
}

class AdGroupUs (
  val us_id: Int,
  val campaign_us_id: Int,
  val account_id: Int,
  val base_name: String,
  val upload_tag: String,
  val client_id: Int,
  val tid: Int,
  val api_id: Long,
  val name: String,
  val start_date: java.util.Date,
  val end_date: java.util.Date,
  val status: String,
  val language_and_region: String,
  val network: String,
  val target_search_network_bool: Int,
  val target_content_network_bool: Int,
  val removed_bool: Int,
  val content_bid_cents: Int,
  val pricing_model: String,
  val exact_match_bid_cents: Int,
  val phrase_match_bid_cents: Int,
  val broad_match_bid_cents: Int,
  val search_bid_cents: Int,
  val content_match_bid_cents: Int,
  val paused_bool: Int
) extends KeyedEntity[Int] {
def id = us_id
}

class AdGroupThem (
  val them_id: Int,
  val campaign_them_id: Int,
  val us_id: Int,
  val client_id: Int,
  val tid: Int,
  val api_id: Long,
  val name: String,
  val start_date: java.util.Date,
  val end_date: java.util.Date,
  val status: String,
  val language_and_region: String,
  val network: String,
  val target_search_network_bool: Int,
  val target_content_network_bool: Int,
  val removed_bool: Int,
  val content_bid_cents: Int,
  val pricing_model: String,
  val exact_match_bid_cents: Int,
  val phrase_match_bid_cents: Int,
  val broad_match_bid_cents: Int,
  val search_bid_cents: Int,
  val content_match_bid_cents: Int,
  val paused_bool: Int
) extends KeyedEntity[Int] {
  def id = them_id
}

class AdGroupThemHistory (
  val them_history_id: Long,
  val account_them_id: Option[Int],
  val them_id: Int,
  val tsecs: Int,
  val job_id: Option[Int],
  val operation: Option[String],
  val campaign_them_id: Int,
  val us_id: Option[Int],
  val client_id: Int,
  val tid: Int,
  val api_id: Long,
  val name: String,
  val start_date: java.util.Date,
  val end_date: java.util.Date,
  val status: String,
  val language_and_region: String,
  val network: String,
  val target_search_network_bool: Int,
  val target_content_network_bool: Int,
  val removed_bool: Int,
  val content_bid_cents: Int,
  val pricing_model: String,
  val exact_match_bid_cents: Int,
  val phrase_match_bid_cents: Int,
  val broad_match_bid_cents: Int,
  val search_bid_cents: Int,
  val content_match_bid_cents: Int,
  val paused_bool: Int
) extends KeyedEntity[CompositeKey4[Long, Option[Int], Option[Int], Int]] {
  def id = compositeKey(them_history_id, job_id, us_id, client_id)
}

class TextAdUs(
  val us_id: Int,
  val ad_group_us_id: Int,
  val account_id: Int,
  val ad_master_id: Int,
  val upload_tag: String,
  val client_id: Int,
  val api_id: Long,
  val text: String,
  val destination_url: String,
  val display_url: String,
  val title: String,
  val status: String,
  val removed_bool: Int,
  val paused_bool: Int,
  val editorial_status: String
) extends KeyedEntity[Int] {
  def id = us_id
}

class TextAdThem(
  val them_id: Int,
  val ad_group_them_id: Int,
  val us_id: Int,
  val client_id: Int,
  val api_id: Long,
  val text: String,
  val destination_url: String,
  val display_url: String,
  val title: String,
  val status: String,
  val removed_bool: Int,
  val paused_bool: Int,
  val editorial_status: String
) extends KeyedEntity[Int] {
  def id = them_id
}

class TextAdThemHistory(
  val them_history_id: Long,
  val ad_group_them_id: Option[Int],
  val them_id: Int,
  val tsecs: Int,
  val job_id: Option[Int],
  val operation: Option[String],
  val us_id: Option[Int],
  val client_id: Int,
  val api_id: Long,
  val text: String,
  val destination_url: String,
  val display_url: String,
  val title: String,
  val status: String,
  val removed_bool: Int,
  val paused_bool: Int,
  val editorial_status: String
) extends KeyedEntity[CompositeKey4[Long, Option[Int], Option[Int], Int]] {
  def id = compositeKey(them_history_id, job_id, us_id, client_id)
}

class KeywordUs(
  val us_id: Int,
  val ad_group_us_id: Int,
  val keyword_id: Int,
  val account_id: Int,
  val upload_tag: String,
  val max_cpc_upper_cents: Int,
  val max_cpc_lower_cents: Int,
  val text: String,
  val client_id: Int,
  val api_id: Long,
  val exact_match_bid_cents: Int,
  val phrase_match_bid_cents: Int,
  val broad_match_bid_cents: Int,
  val content_match_bid_cents: Int,
  val param1: String,
  val param2: String,
  val param3: String,
  val paused_bool: Int,
  val status: String,
  val removed_bool: Int,
  val editorial_status: String
) extends KeyedEntity[Int] {
  def id = us_id
}

class KeywordThem(
  val them_id: Int,
  val ad_group_them_id: Int,
  val us_id: Int,
  val text: String,
  val client_id: Int,
  val api_id: Long,
  val exact_match_bid_cents: Int,
  val phrase_match_bid_cents: Int,
  val broad_match_bid_cents: Int,
  val content_match_bid_cents: Int,
  val param1: String,
  val param2: String,
  val param3: String,
  val paused_bool: Int,
  val status: String,
  val removed_bool: Int,
  val editorial_status: String
) extends KeyedEntity[Int] {
  def id = them_id
}

class KeywordThemHistory(
  val them_history_id: Long,
  val ad_group_them_id: Option[Int],
  val them_id: Int,
  val tsecs: Int,
  val job_id: Option[Int],
  val operation: Option[String],
  val us_id: Option[Int],
  val text: String,
  val client_id: Int,
  val api_id: Long,
  val exact_match_bid_cents: Int,
  val phrase_match_bid_cents: Int,
  val broad_match_bid_cents: Int,
  val content_match_bid_cents: Int,
  val param1: String,
  val param2: String,
  val param3: String,
  val paused_bool: Int,
  val status: String,
  val removed_bool: Int,
  val editorial_status: String
) extends KeyedEntity[CompositeKey4[Long, Option[Int], Option[Int], Int]] {
  def id = compositeKey(them_history_id, job_id, us_id, client_id)
}

object MsnDB extends Schema {
  Class.forName("com.mysql.jdbc.Driver")
  val config = ConfigFactory.load()
  val session = org.squeryl.Session.create(
    java.sql.DriverManager.getConnection(
      config.getString("db.agile_production_db_msn.url"),
      config.getString("db.agile_production_db_msn.user"),
      config.getString("db.agile_production_db_msn.password")
    ),
    new MySQLAdapter
  )

  val apiAccount = table[ApiAccount]("api_account")
  val accountInfo = table[AccountInfo]("account_info")
  val accountUs = table[AccountUs]("account_us")
  val accountThem = table[AccountThem]("account_them")
  val accountThemHistory = table[AccountThemHistory]("account_them_history")
  val campaignUs = table[CampaignUs]("campaign_us")
  val campaignThem = table[CampaignThem]("campaign_them")
  val campaignThemHistory = table[CampaignThemHistory]("campaign_them_history")
  val adGroupUs = table[AdGroupUs]("ad_group_us")
  val adGroupThem = table[AdGroupThem]("ad_group_them")
  val adGroupThemHistory = table[AdGroupThemHistory]("ad_group_them_history")
  val textAdUs = table[TextAdUs]("text_ad_us")
  val textAdThem = table[TextAdThem]("text_ad_them")
  val textAdThemHistory = table[TextAdThemHistory]("text_ad_them_history")
  val keywordUs = table[KeywordUs]("keyword_us")
  val keywordThem = table[KeywordThem]("keyword_them")
  val keywordThemHistory = table[KeywordThemHistory]("keyword_them_history")

}