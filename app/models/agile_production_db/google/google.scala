package models.agile_production_db.google

import com.typesafe.config.ConfigFactory
import org.squeryl.PrimitiveTypeMode._
import org.squeryl.adapters.MySQLAdapter
import org.squeryl.annotations.Column
import org.squeryl.dsl._
import org.squeryl.{Optimistic, KeyedEntity, Schema}


/**
 * agile-production-db/google (mysql).
 */

class primaryKey extends KeyedEntity[Long] {
  val id = 0L
}

class ThemTable extends KeyedEntity[Long] {
  @Column("them_id")
  val id = 0L
}

class ThemHistoryTable extends KeyedEntity[Long] {
  @Column("them_history_id")
  val id = 0L
}

class UsTable extends KeyedEntity[Long] {
  @Column("us_id")
  val id = 0L
}

class UsHistoryTable extends KeyedEntity[Long] {
  @Column("us_history_id")
  val id = 0L
}


class AccountInfo(
  var client_id: Int,
  var username: String,
  var password: String,
  var api_account_id: Option[Long],
  var tid: Int,
  var start_date: Option[java.util.Date],
  var end_date: Option[java.util.Date],
  var uncapped_bool: Option[Int],
  var username_alias: Option[String],
  var client_center_id: Option[Int],
  var customer_id: Option[String]
) extends primaryKey

class ClientCenter(
  var Username: String,
  var Password: String,
  var api_account_id: Int,
  var client_center_client_id: Option[Int]
) extends primaryKey

class ApiAccount(
  var user_agent: String,
  var developer_token: String,
  var application_token: String,
  var oauth_refresh_token: Option[String],
  var oauth_client_id: Option[String],
  var oauth_client_secret: Option[String]
) extends primaryKey

class AccountUs(
  var base_name: Option[String],
  var upload_tag: Option[String],
  var client_id: Int,
  var tid: Int,
  var account_id: Long,
  var name: Option[String],
  var customer_id: Option[String],
  var currency_code: Option[String],
  var default_network_targeting_google_search_bool: Option[Int],
  var default_network_targeting_search_network_bool: Option[Int],
  var default_network_targeting_content_network_bool: Option[Int],
  var email_promotions_preferences_account_performance_enabled_bool: Option[Int],
  var email_promotions_preferences_disapproved_ads_enabled_bool: Option[Int],
  var email_promotions_preferences_marketResearch_enabled_bool: Option[Int],
  var email_promotions_preferences_newsletter_enabled_bool: Option[Int],
  var email_promotions_preferences_promotions_enabled_bool: Option[Int],
  var language_preference: Option[String],
  var primary_business_category: Option[String],
  var time_zone_effective_date: Option[Int],
  var time_zone_id: Option[String],
  var client_email: Option[String],
  var removed_bool: Int,
  var last_sync_tsecs: Option[Long]
) extends UsTable

class AccountThem(
  var us_id: Option[String],
  var client_id: Int,
  var tid: Int,
  var account_id: Long,
  var name: Option[String],
  var customer_id: Option[String],
  var currency_code: Option[String],
  var default_network_targeting_google_search_bool: Option[Int],
  var default_network_targeting_search_network_bool: Option[Int],
  var default_network_targeting_content_network_bool: Option[Int],
  var email_promotions_preferences_account_performance_enabled_bool: Option[Int],
  var email_promotions_preferences_disapproved_ads_enabled_bool: Option[Int],
  var email_promotions_preferences_marketResearch_enabled_bool: Option[Int],
  var email_promotions_preferences_newsletter_enabled_bool: Option[Int],
  var email_promotions_preferences_promotions_enabled_bool: Option[Int],
  var language_preference: Option[String],
  var primary_business_category: Option[String],
  var time_zone_effective_date: Option[Int],
  var time_zone_id: Option[String],
  var client_email: Option[String],
  var removed_bool: Int
) extends ThemTable

class AccountThemHistory(
  var them_id: Long,
  var tsecs: Long,
  var job_id: Option[Long],
  var operation: Option[String],
  var us_id: Option[Long],
  var client_id: Int,
  var tid: Int,
  var account_id: Long,
  var name: Option[String],
  var customer_id: Option[String],
  var currency_code: Option[String],
  var default_network_targeting_google_search_bool: Option[Int],
  var default_network_targeting_search_network_bool: Option[Int],
  var default_network_targeting_content_network_bool: Option[Int],
  var email_promotions_preferences_account_performance_enabled_bool: Option[Int],
  var email_promotions_preferences_disapproved_ads_enabled_bool: Option[Int],
  var email_promotions_preferences_marketResearch_enabled_bool: Option[Int],
  var email_promotions_preferences_newsletter_enabled_bool: Option[Int],
  var email_promotions_preferences_promotions_enabled_bool: Option[Int],
  var language_preference: Option[String],
  var primary_business_category: Option[String],
  var time_zone_effective_date: Option[Int],
  var time_zone_id: Option[String],
  var client_email: Option[String],
  var removed_bool: Int
) extends ThemHistoryTable

class CampaignUs(
  var account_us_id: Option[Long],
  var account_id: Long,
  var base_name: Option[String],
  var upload_tag: Option[String],
  var client_id: Int,
  var tid: Int,
  var api_id: Option[Long],
  var name: Option[String],
  var status: Option[String],
  var start_date: Option[java.util.Date],
  var end_date: Option[java.util.Date],
  var target_google_search_bool: Option[Int],
  var target_search_network_bool: Option[Int],
  var target_content_network_bool: Option[Int],
  var target_all_geo_bool: Option[Int],
  var bid_ceiling_cents: Option[Int],
  var enable_bid_optimization_bool: Option[Int],
  var ad_schedule_status: Option[String],
  var removed_bool: Int,
  var delivery_method: Option[String],
  var budget_amount_cents: Option[Int],
  var budget_period: Option[String],
  var content_targeting: Option[String],
  var exclude_error_pages_bool: Int,
  var exclude_parked_domains_bool: Int,
  var optimize_ad_serving_bool: Int
) extends UsTable

class CampaignThem(
  var account_them_id: Option[Long],
  var us_id: Option[Long],
  var client_id: Int,
  var tid: Int,
  var api_id: Option[Long],
  var `name`: Option[String],
  var status: String,
  var start_date: Option[java.util.Date],
  var end_date: Option[java.util.Date],
  var target_google_search_bool: Option[Int],
  var target_search_network_bool: Option[Int],
  var target_content_network_bool: Option[Int],
  var target_all_geo_bool: Option[Int],
  var bid_ceiling_cents: Option[Int],
  var enable_bid_optimization_bool: Option[Int],
  var ad_schedule_status: Option[String],
  var removed_bool: Int,
  var delivery_method: Option[String],
  var budget_amount_cents: Option[Long],
  var budget_period: String,
  var content_targeting: String,
  var exclude_error_pages_bool: Int,
  var exclude_parked_domains_bool: Int,
  var optimize_ad_serving_bool: Int
) extends ThemTable

class CampaignThemHistory(
  var account_them_id: Option[Long],
  var them_id: Long,
  var tsecs: Long,
  var job_id: Option[Long],
  var operation: Option[String],
  var us_id: Option[Long],
  var client_id: Int,
  var tid: Int,
  var api_id: Option[Long],
  var name: Option[String],
  var status: String,
  var start_date: Option[java.util.Date],
  var end_date: Option[java.util.Date],
  var target_google_search_bool: Option[Int],
  var target_search_network_bool: Option[Int],
  var target_content_network_bool: Option[Int],
  var target_all_geo_bool: Option[Int],
  var budget_amount_cents: Option[Long],
  var budget_period: Option[String],
  var content_targeting: Option[String],
  var delivery_method: Option[String],
  var bid_ceiling_cents: Option[Int],
  var enable_bid_optimization_bool: Option[Int],
  var ad_schedule_status: Option[String],
  var removed_bool: Int,
  var exclude_error_pages_bool: Int,
  var exclude_parked_domains_bool: Int,
  var optimize_ad_serving_bool: Int
) extends ThemHistoryTable

class AdGroupUs(
  var campaign_us_id: Option[Long],
  var account_id: Long,
  var base_name: Option[String],
  var upload_tag: Option[String],
  var client_id: Int,
  var api_id: Option[Long],
  var max_content_cpc_cents: Option[Int],
  var max_cpc_center: Option[Int],
  var max_cpm_cents: Option[Int],
  var name: Option[String],
  var proxy_max_cpc_cents: Option[Int],
  var status: String,
  var removed_bool: Int,
  var max_content_cpc_upper_cents: Option[Int],
  var max_content_cpc_lower_cents: Option[Int],
  var max_cpc_upper_cents: Option[Int],
  var max_cpc_lower_cents: Option[Int],
  var max_cpm_upper_cents: Option[Int],
  var max_cpm_lower_cents: Option[Int],
  var max_cpa_cents: Option[Int],
  var site_max_cpc_cents: Option[Int]
) extends UsTable

class AdGroupThem(
  var campaign_them_id: Option[Long],
  var us_id: Option[Long],
  var client_id: Int,
  var api_id: Option[Long],
  var max_content_cpc_cents: Option[Int],
  var max_cpc_cents: Option[Int],
  var max_cpm_cents: Option[Int],
  var name: Option[String],
  var proxy_max_cpc_cents: Option[Int],
  var status: String,
  var removed_bool: Option[Int],
  var max_cpa_cents: Option[Int],
  var site_max_cpc_cents: Option[Int]
) extends ThemTable

class AdGroupThemHistory(
  var campaign_them_id: Option[Long],
  var them_id: Long,
  var tsecs: Long,
  var job_id: Option[Long],
  var operation: Option[String],
  var us_id: Option[Long],
  var client_id: Int,
  var api_id: Option[Long],
  var max_content_cpc_cents: Option[Int],
  var max_cpc_cents: Option[Int],
  var max_cpm_cents: Option[Int],
  var name: Option[String],
  var proxy_max_cpc_cents: Option[Int],
  var status: String,
  var removed_bool: Option[Int],
  var max_cpa_cents: Option[Int],
  var site_max_cpc_cents: Option[Int]
) extends ThemHistoryTable

class AdUs(
  var ad_group_us_id: Option[Long],
  var account_id: Long,
  var ad_master_id: Option[Long],
  var upload_tag: Option[String],
  var client_id: Int,
  var api_id: Option[Long],
  var ad_type: Option[String],
  var description1: Option[String],
  var description2: Option[String],
  var destination_url: Option[String],
  var disapproved_bool: Option[Int],
  var display_url: Option[String],
  var exemption_request: Option[String],
  var headline: Option[String],
  var status: Option[String],
  var removed_bool: Int
) extends UsTable

class AdThem(
  var ad_group_them_id: Option[Long],
  var us_id: Option[Long],
  var client_id: Int,
  var api_id: Option[Long],
  var ad_type: Option[String],
  var description1: Option[String],
  var description2: Option[String],
  var destination_url: Option[String],
  var disapproved_bool: Option[Int],
  var display_url: Option[String],
  var exemption_request: Option[String],
  var headline: Option[String],
  var status: String,
  var removed_bool: Int
) extends ThemTable

class AdThemHistory(
  var ad_group_them_id: Option[Long],
  var them_id: Long,
  var tsecs: Long,
  var job_id: Option[Long],
  var operation: Option[String],
  var us_id: Option[Long],
  var client_id: Int,
  var api_id: Option[Long],
  var ad_type: Option[String],
  var description1: Option[String],
  var description2: Option[String],
  var destination_url: Option[String],
  var disapproved_bool: Option[Int],
  var display_url: Option[String],
  var exemption_request: Option[String],
  var headline: Option[String],
  var status: String,
  var removed_bool: Int
) extends ThemHistoryTable

class KeywordUs(
  var ad_group_us_id: Option[Long],
  var keyword_id: Option[Long],
  var account_id: Long,
  var upload_tag: Option[String],
  var client_id: Int,
  var api_id: Option[Long],
  var destination_url: Option[String],
  var exemption_request: Option[String],
  var language: Option[String],
  var max_cpc_cents: Option[Int],
  var negative_bool: Option[Int],
  var paused_bool: Option[Int],
  var proxy_max_cpc_cents: Option[Int],
  var status: Option[String],
  var text: Option[String],
  var `type`: Option[String],
  var removed_bool: Int,
  var max_cpc_upper_cents: Option[Int],
  var max_cpc_lower_cents: Option[Int],
  var quality_score: Option[Int],
  var first_page_cpc_cents: Option[Int]
) extends UsTable

class KeywordThem(
  var ad_group_them_id: Option[Long],
  var us_id: Option[Long],
  var client_id: Int,
  var api_id: Option[Long],
  var destination_url: Option[String],
  var exemption_request: Option[String],
  var language: Option[String],
  var max_cpc_cents: Option[Int],
  var negative_bool: Option[Int],
  var paused_bool: Option[Int],
  var proxy_max_cpc_cents: Option[Int],
  var status: String,
  var text: Option[String],
  var `type`: String,
  var removed_bool: Int,
  var quality_score: Option[Int],
  var first_page_cpc_cents: Option[Int]
) extends ThemTable

class KeywordThemHistory(
  var ad_group_them_id: Option[Long],
  var them_id: Long,
  var tsecs: Long,
  var job_id: Option[Long],
  var operation: Option[String],
  var us_id: Option[Long],
  var client_id: Int,
  var api_id: Option[Long],
  var destination_url: Option[String],
  var exemption_request: Option[String],
  var language: Option[String],
  var max_cpc_cents: Option[Int],
  var negative_bool: Option[Int],
  var paused_bool: Option[Int],
  var proxy_max_cpc_cents: Option[Int],
  var status: String,
  var text: Option[String],
  var `type`: String,
  var removed_bool: Int,
  var quality_score: Option[Int],
  var first_page_cpc_cents: Option[Int]
) extends ThemHistoryTable

object GoogleDB extends Schema {
  Class.forName("com.mysql.jdbc.Driver")
  val config = ConfigFactory.load()
  val session = org.squeryl.Session.create(
    java.sql.DriverManager.getConnection(
      config.getString("db.agile_production_db_google.url"),
      config.getString("db.agile_production_db_google.user"),
      config.getString("db.agile_production_db_google.password")
    ),
    new MySQLAdapter
  )

  val accountInfo = table[AccountInfo]("account_info")
  val clientCenter = table[ClientCenter]("client_center")
  val apiAccount = table[ApiAccount]("api_account")
  val accountUs = table[AccountUs]("account_us")
  val accountThem = table[AccountThem]("account_them")
  val accountThemHistory = table[AccountThemHistory]("account_them_history")
  val campaignUs = table[CampaignUs]("campaign_us")
  val campaignThem = table[CampaignThem]("campaign_them")
  val campaignThemHistory = table[CampaignThemHistory]("campaign_them_history")
  val adGroupUs = table[AdGroupUs]("ad_group_us")
  val adGroupThem = table[AdGroupThem]("ad_group_them")
  val adGroupThemHistory = table[AdGroupThemHistory]("ad_group_them_history")
  val adUs = table[AdUs]("ad_us")
  val adThem = table[AdThem]("ad_them")
  val adThemHistory = table[AdThemHistory]("ad_them_history")
  val keywordUs = table[KeywordUs]("keyword_us")
  val keywordThem = table[KeywordThem]("keyword_them")
  val keywordThemHistory = table[KeywordThemHistory]("keyword_them_history")
}