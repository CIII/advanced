package controllers

import javax.inject.Inject

import play.api.data.Forms._
import play.api.data._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc._
import util._

class LoginController @Inject() (val messagesApi: MessagesApi) extends Controller with I18nSupport {

  def login = Action {
    implicit request =>
      Ok(views.html.login(login_form))
  }

  case class LoginForm(
    var username: String,
    var password: String
  )

  val login_form = Form(
    mapping(
      "username" -> text,
      "password" -> text
    )(LdapUtil.authenticate)(_.map(u => (u.userName, ""))).verifying("Invalid username or password.", result => result.isDefined)
  )

  def authenticate = Action {
    implicit request =>
      login_form.bindFromRequest.fold(
        formWithErrors => BadRequest(views.html.login(formWithErrors)),
        user => Redirect(routes.DashboardController.dashboard()).withSession(request.session + (Security.username -> user.get.userName))
      )
  }

  def logout = Action {
    Redirect(routes.LoginController.login()).withNewSession.flashing(
      "success" -> "You are now logged out."
    )
  }
}
