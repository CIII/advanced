package controllers

import javax.inject.Inject

import be.objectify.deadbolt.scala.cache.HandlerCache
import be.objectify.deadbolt.scala.{ActionBuilders, DeadboltActions}
import com.mongodb.casbah.Imports._
import helpers.UserProfileControllerHelper
import models.mongodb.{SecurityRole, UserAccount}
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc.{Action, Security, _}

class UserProfileController @Inject()(val messagesApi: MessagesApi, deadbolt: DeadboltActions, handlers: HandlerCache, actionBuilder: ActionBuilders) extends Controller with I18nSupport {
  import UserProfileControllerHelper._

  def user_profile = Action {
    implicit request =>
      UserAccount.userAccountCollection
        .findOne(DBObject("userName" -> request.session.get(Security.username).get)) match {
        case Some(user_account_obj) =>
          val user_account = UserAccount.dboToUserAccount(user_account_obj.asDBObject)
          val security_roles = SecurityRole.securityRoleCollection.find.toList.map(x => SecurityRole.dboToSecurityRole(x.asDBObject))
          Ok(
            views.html.user_profile(
              userProfileForm.fill(
                UserProfileForm(
                  _id = Some(user_account_obj._id.toString),
                  username = user_account.userName,
                  email = user_account.email,
                  security_roles
                    .filter(role => user_account.securityRoles.contains(role._id))
                    .map(_.roleName)
                )
              ),
              security_roles
            )
          )
      }
  }

  def save = Action {
    implicit request =>
      val security_roles = SecurityRole.securityRoleCollection.find.toList.map(x => SecurityRole.dboToSecurityRole(x.asDBObject))
      userProfileForm.bindFromRequest.fold(
        formWithErrors => BadRequest(views.html.user_profile(formWithErrors, security_roles)),
        user_profile_form => {
          UserAccount.userAccountCollection.update(
            DBObject("userName" -> request.session.get(Security.username)),
            UserAccount.userAccountToDbo(
              UserAccount(
                _id = user_profile_form._id match {
                  case id =>
                    Some(new ObjectId(id.get))
                  case None =>
                    Some(new ObjectId)
                },
                userName = user_profile_form.username,
                email = user_profile_form.email,
                securityRoles = user_profile_form.security_roles.map(role_id => new ObjectId(role_id)).toArray
              )
            )
          )
          Redirect(routes.DashboardController.dashboard())
        }
      )
  }
}

