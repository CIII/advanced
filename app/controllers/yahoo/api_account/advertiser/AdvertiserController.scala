package controllers.yahoo.api_account.advertiser

import javax.inject.Inject

import Shared.Shared._
import be.objectify.deadbolt.scala.cache.HandlerCache
import be.objectify.deadbolt.scala.{ActionBuilders, DeadboltActions}
import com.mongodb.casbah.Imports._
import helpers.yahoo.api_account.advertiser.AdvertiserControllerHelper._
import models.mongodb.yahoo.Yahoo._
import models.mongodb.{PermissionGroup, Utilities, yahoo}
import play.api.Play.current
import play.api.cache.Cache
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc.{Action, Controller, Security}
import security.HandlerKeys

import scala.collection.mutable.ListBuffer

class AdvertiserController @Inject()(val messagesApi: MessagesApi, deadbolt: DeadboltActions, handlers: HandlerCache, actionBuilder: ActionBuilders) extends Controller with I18nSupport {
  def json = Action {
    implicit request =>
      Ok(controllers.json(
        request,
        List(
          "apiAccountObjId",
          "advertiserObjId",
          "advertiserApiId",
          "tsecs"
        ),
        "advertiser",
        yahooAdvertiserCollection
      ))
  }

  def advertiser(page: Int, pageSize: Int, orderBy: Int, filter: String) = deadbolt.Dynamic(name=PermissionGroup.YahooRead.entryName, handler=handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        Ok(views.html.yahoo.api_account.advertiser.advertiser(
          yahooAdvertiserCollection.find(
            DBObject(),
            DBObject("advertiser" -> DBObject("$slice" -> -1))
          ).skip(page * pageSize).limit(pageSize).toList.map(x => yahoo.Yahoo.dboToAdvertiser(x.as[List[DBObject]]("advertiser").head.as[DBObject]("object"))),
          page,
          pageSize,
          orderBy,
          filter,
          yahooAdvertiserCollection.count(),
          pendingCache(Left(request))
            .filter(
              x =>
                x.trafficSource == TrafficSource.YAHOO && x.changeCategory == ChangeCategory.ADVERTISER
            )
        ))
    }
  }


  def newAdvertiser = deadbolt.Dynamic(name=PermissionGroup.YahooWrite.entryName, handler=handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request => Ok(views.html.yahoo.api_account.advertiser.new_advertiser(advertiserForm, List()))
    }
  }


  def editAdvertiser(id: String) = deadbolt.Dynamic(name=PermissionGroup.YahooWrite.entryName, handler=handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        yahooApiAccountCollection.findOne(DBObject("_id" -> id)) match {
          case Some(advertiser_obj) =>
            def advertiser = yahoo.Yahoo.dboToAdvertiser(advertiser_obj)
            Ok(views.html.yahoo.api_account.advertiser.edit_advertiser(id, advertiserForm.fill(advertiser)))
          case None =>
            BadRequest("Not Found")
        }
    }
  }


  def createAdvertiser = deadbolt.Dynamic(name=PermissionGroup.YahooWrite.entryName, handler=handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        advertiserForm.bindFromRequest.fold(
          formWithErrors => BadRequest(views.html.yahoo.api_account.advertiser.new_advertiser(formWithErrors, List())),
          advertiser => {
            setPendingCache(
              Left(request),
              pendingCache(Left(request)) :+ PendingCacheStructure(
                id = pendingCache(Left(request)).length + 1,
                changeType = ChangeType.NEW,
                trafficSource = TrafficSource.YAHOO,
                changeCategory = ChangeCategory.ADVERTISER,
                changeData = advertiserToDBObject(advertiser)
              )
            )
            Redirect(controllers.yahoo.api_account.advertiser.routes.AdvertiserController.advertiser(0, 10, 2, ""))
          }
        )
    }
  }


  def saveAdvertiser(id: String) = deadbolt.Dynamic(name=PermissionGroup.YahooWrite.entryName, handler=handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        val pending_cache_key = request.session.get(Security.username).get + cache_ext
        val current_cache = Cache.get(pending_cache_key).getOrElse(List()).asInstanceOf[List[PendingCacheStructure]]
        advertiserForm.bindFromRequest.fold(
          formWithErrors => {
            BadRequest(
              views.html.yahoo.api_account.advertiser.edit_advertiser(
                id,
                formWithErrors
              )
            )
          },
          advertiser => {
            Cache.set(
              pending_cache_key,
              current_cache :+ PendingCacheStructure(
                id = current_cache.length + 1,
                changeType = ChangeType.UPDATE,
                trafficSource = TrafficSource.YAHOO,
                changeCategory = ChangeCategory.ADVERTISER,
                changeData = com.mongodb.util.JSON.serialize(advertiser).asInstanceOf[DBObject]
              )
            )
            Redirect(controllers.yahoo.api_account.advertiser.routes.AdvertiserController.advertiser())
          }
        )
    }
  }


  def deleteAdvertiser(id: String) = deadbolt.Dynamic(name=PermissionGroup.YahooWrite.entryName, handler=handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        val pending_cache_key = request.session.get(Security.username).get + cache_ext
        val current_cache = Cache.get(pending_cache_key).getOrElse(List()).asInstanceOf[List[PendingCacheStructure]]
        Cache.set(
          pending_cache_key,
          current_cache :+ PendingCacheStructure(
            id = current_cache.length + 1,
            changeType = ChangeType.DELETE,
            trafficSource = TrafficSource.YAHOO,
            changeCategory = ChangeCategory.ADVERTISER,
            changeData = DBObject("advertiserObjId" -> id)
          )
        )
        Redirect(controllers.yahoo.api_account.advertiser.routes.AdvertiserController.advertiser())
    }
  }


  def bulkNewAdvertiser = deadbolt.Dynamic(name=PermissionGroup.YahooWrite.entryName, handler=handlers(HandlerKeys.defaultHandler)) {
    Action(parse.multipartFormData) {
      implicit request => {
        var error_list = new ListBuffer[String]()
        val pending_cache_key = request.session.get(Security.username).get + cache_ext
        val current_cache = Cache.get(pending_cache_key).getOrElse(List()).asInstanceOf[List[PendingCacheStructure]]
        request.body.file("bulk").foreach {
          bulk => {
            val field_names = Utilities.getCaseClassParameter[ApiAccount]
            val advertiser_data_list = Utilities.bulkImport(bulk, field_names)
            for (((advertiser_data, action), index) <- advertiser_data_list.zipWithIndex) {
              advertiserForm.bind(advertiser_data.map(kv => (kv._1, kv._2)).toMap).fold(
                formWithErrors => {
                  error_list += "Row " + index.toString + ": " + formWithErrors.errorsAsJson.toString
                },
                advertiser => {
                  Cache.set(
                    pending_cache_key,
                    current_cache :+ PendingCacheStructure(
                      id = current_cache.length + 1,
                      changeType = ChangeType.withName(action.toUpperCase),
                      trafficSource = TrafficSource.YAHOO,
                      changeCategory = ChangeCategory.API_ACCOUNT,
                      changeData = advertiserToDBObject(advertiser)
                    )
                  )
                }
              )
            }
          }
        }
        if (error_list.nonEmpty) {
          BadRequest(views.html.yahoo.api_account.advertiser.new_advertiser(
            advertiserForm,
            error_list.toList
          ))
        } else {
          Redirect(controllers.yahoo.api_account.advertiser.routes.AdvertiserController.advertiser())
        }
      }
    }
  }
}