package controllers

import javax.inject.Inject

import Shared.Shared._
import be.objectify.deadbolt.scala.cache.HandlerCache
import be.objectify.deadbolt.scala.{ActionBuilders, DeadboltActions}
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc._

class DashboardController @Inject()(val messagesApi: MessagesApi, deadbolt: DeadboltActions, handlers: HandlerCache, actionBuilder: ActionBuilders) extends Controller with I18nSupport {
  def dashboard = Action {
      implicit request =>
        Ok(views.html.dashboard(dashboardChartData))
  }

  def permission_denied = Action {
    implicit request =>
      Ok(views.html.permission_denied())
  }
}