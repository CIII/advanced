package controllers

import javax.inject.Inject

import be.objectify.deadbolt.scala.cache.HandlerCache
import be.objectify.deadbolt.scala.{ActionBuilders, DeadboltActions}
import helpers.NotificationConfigurationControllerHelper
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc._

class NotificationConfigurationController @Inject()(val messagesApi: MessagesApi, deadbolt: DeadboltActions, handlers: HandlerCache, actionBuilder: ActionBuilders) extends Controller with I18nSupport {
  import NotificationConfigurationControllerHelper._

  def notification_configuration = Action {
    implicit request =>
      Ok(views.html.notification_configuration(notificationForm(request.session.get(Security.username).get)))
  }

  def save(id: String) = Action {
    implicit request =>
      Redirect(routes.DashboardController.dashboard())
  }
}

