package controllers.msn.customer.account

import javax.inject.Inject

import Shared.Shared._
import be.objectify.deadbolt.scala.cache.HandlerCache
import be.objectify.deadbolt.scala.{ActionBuilders, DeadboltActions}
import com.microsoft.bingads.customermanagement.{AccountInfoWithCustomerData, User}
import com.mongodb.casbah.Imports._
import helpers.msn.customer.account.AccountControllerHelper._
import models.mongodb._
import models.mongodb.msn.Msn._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc._
import security.HandlerKeys

import scala.collection.immutable.List
import scala.collection.mutable.ListBuffer

class AccountController @Inject()(val messagesApi: MessagesApi, deadbolt: DeadboltActions, handlers: HandlerCache, actionBuilder: ActionBuilders) extends Controller with I18nSupport {

  def accounts(page: Int, pageSize: Int, orderBy: Int, filter: String) = deadbolt.Dynamic(name=PermissionGroup.MSNRead.entryName, handler=handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        Ok(views.html.msn.customer.account.accounts(
          msnAccountCollection.find(
            DBObject(),
            DBObject("account" -> DBObject("$slice" -> -1))
          ).toList.map { x =>
            Tuple3(
              dboToMsnEntity[User](x, "account", Some("user")),
              dboToMsnEntity[AccountInfoWithCustomerData](x, "account", Some("accountInfoWithCustomerData")),
              x.as[MongoDBList]("account").head.asInstanceOf[DBObject].as[String]("developerToken")
            )
          },
          page,
          pageSize,
          orderBy,
          filter,
          msnAccountCollection.count(),
          pendingCache(Left(request))
            .filter(
              x =>
                x.trafficSource == TrafficSource.MSN && x.changeCategory == ChangeCategory.ACCOUNT
            )
        ))
    }
  }


  def newAccount = deadbolt.Dynamic(name=PermissionGroup.MSNWrite.entryName, handler=handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request => Ok(views.html.msn.customer.account.new_account(accountForm, List()))
    }
  }


  def editAccount(id: String) = deadbolt.Dynamic(name=PermissionGroup.MSNWrite.entryName, handler=handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        msnAccountCollection.findOne(DBObject("AccountApiId" -> id)) match {
          case Some(account_obj) =>
            val user = dboToMsnEntity[User](account_obj, "account", Some("user"))
            val account_info_with_customer_data = dboToMsnEntity[AccountInfoWithCustomerData](account_obj, "account", Some("accountInfoWithCustomerData"))
            Ok(views.html.msn.customer.account.edit_account(
              id,
              accountForm.fill(
                AccountForm(
                  userName = user.getUserName,
                  password = user.getPassword,
                  developerToken = account_obj.as[MongoDBList]("account").head.asInstanceOf[DBObject].as[String]("developerToken"),
                  customerApiId = Some(account_info_with_customer_data.getCustomerId),
                  customerAccountApiId = Some(account_info_with_customer_data.getAccountId)
                )
              )
            ))
          case None =>
            BadRequest("Not Found")
        }
    }
  }


  def createAccount = deadbolt.Dynamic(name=PermissionGroup.MSNWrite.entryName, handler=handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        accountForm.bindFromRequest.fold(
          formWithErrors => BadRequest(views.html.msn.customer.account.new_account(formWithErrors, List())),
          account => {
            setPendingCache(
              Left(request),
              pendingCache(Left(request)) :+ PendingCacheStructure(
                id = pendingCache(Left(request)).length + 1,
                changeType = ChangeType.NEW,
                trafficSource = TrafficSource.MSN,
                changeCategory = ChangeCategory.ACCOUNT,
                changeData = accountFormToDbo(account)
              )
            )
            Redirect(controllers.msn.customer.account.routes.AccountController.accounts())
          }
        )
    }
  }


  def saveAccount(id: String) = deadbolt.Dynamic(name=PermissionGroup.MSNWrite.entryName, handler=handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        accountForm.bindFromRequest.fold(
          formWithErrors => {
            BadRequest(
              views.html.msn.customer.account.edit_account(
                id,
                formWithErrors
              )
            )
          },
          account => {
            setPendingCache(
              Left(request),
              pendingCache(Left(request)) :+ PendingCacheStructure(
                id = pendingCache(Left(request)).length + 1,
                changeType = ChangeType.UPDATE,
                trafficSource = TrafficSource.MSN,
                changeCategory = ChangeCategory.ACCOUNT,
                changeData = accountFormToDbo(account)
              )
            )
            Redirect(controllers.msn.customer.account.routes.AccountController.accounts())
          }
        )
    }
  }

  def deleteAccount(id: String) = deadbolt.Dynamic(name=PermissionGroup.MSNWrite.entryName, handler=handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        setPendingCache(
          Left(request),
          pendingCache(Left(request)) :+ PendingCacheStructure(
            id = pendingCache(Left(request)).length + 1,
            changeType = ChangeType.DELETE,
            trafficSource = TrafficSource.MSN,
            changeCategory = ChangeCategory.ACCOUNT,
            changeData = DBObject("apiId" -> id)
          )
        )
        Redirect(controllers.msn.customer.account.routes.AccountController.accounts())
    }
  }


  def bulkNewAccount = deadbolt.Dynamic(name = PermissionGroup.MSNWrite.entryName, handler = handlers(HandlerKeys.defaultHandler)) {
    Action(parse.multipartFormData) {
      implicit request => {
        var error_list = new ListBuffer[String]()
        request.body.file("bulk").foreach {
          bulk => {
            val field_names = Utilities.getCaseClassParameter[AccountForm]
            val account_data_list = Utilities.bulkImport(bulk, field_names)
            for (((account_data, action), index) <- account_data_list.zipWithIndex) {
              accountForm.bind(account_data.map(kv => (kv._1, kv._2)).toMap).fold(
                formWithErrors => {
                  error_list += "Row " + index.toString + ": " + formWithErrors.errorsAsJson.toString
                },
                account => {
                  setPendingCache(
                    Left(request),
                    pendingCache(Left(request)) :+ PendingCacheStructure(
                      id = pendingCache(Left(request)).length + 1,
                      changeType = ChangeType.withName(action.toUpperCase),
                      trafficSource = TrafficSource.MSN,
                      changeCategory = ChangeCategory.ACCOUNT,
                      changeData = accountFormToDbo(account)
                    )
                  )
                }
              )
            }
          }
        }
        if (error_list.nonEmpty) {
          BadRequest(views.html.msn.customer.account.new_account(
            accountForm,
            error_list.toList
          ))
        } else {
          Redirect(controllers.msn.customer.account.routes.AccountController.accounts())
        }
      }
    }
  }
}


