package controllers.msn.customer.account.campaign

import javax.inject.Inject

import Shared.Shared._
import be.objectify.deadbolt.scala.cache.HandlerCache
import be.objectify.deadbolt.scala.{ActionBuilders, DeadboltActions}
import com.microsoft.bingads.campaignmanagement.Campaign
import com.mongodb.casbah.Imports._
import helpers.msn.customer.account.campaign.CampaignControllerHelper
import models.mongodb._
import models.mongodb.msn.Msn._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc._
import security.HandlerKeys

import scala.collection.immutable.List
import scala.collection.mutable.ListBuffer

class CampaignController @Inject()(val messagesApi: MessagesApi, deadbolt: DeadboltActions, handlers: HandlerCache, actionBuilder: ActionBuilders) extends Controller with I18nSupport {

  import CampaignControllerHelper._

  def campaigns(page: Int, pageSize: Int, orderBy: Int, filter: String) = deadbolt.Dynamic(name=PermissionGroup.MSNRead.entryName, handler=handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        Ok(views.html.msn.customer.account.campaign.campaigns(
          msnCampaignCollection.find(
            DBObject(),
            DBObject("campaign" -> DBObject("$slice" -> -1))
          ).skip(page * pageSize).limit(pageSize).toList.map(dboToMsnEntity[Campaign](_, "campaign", None)),
          page,
          pageSize,
          orderBy,
          filter,
          msnCampaignCollection.count(),
          pendingCache(Left(request))
            .filter(
              x =>
                x.trafficSource == TrafficSource.MSN && x.changeCategory == ChangeCategory.CAMPAIGN
            )
        ))
    }
  }


  def newCampaign = deadbolt.Dynamic(name=PermissionGroup.MSNWrite.entryName, handler=handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request => Ok(views.html.msn.customer.account.campaign.new_campaign(campaignForm, List()))
    }
  }


  def editCampaign(id: String) = deadbolt.Dynamic(name=PermissionGroup.MSNWrite.entryName, handler=handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        msnCampaignCollection.findOne(DBObject("campaignApiId" -> id), DBObject("campaign" -> DBObject("$slice" -> -1))) match {
          case Some(x) =>
            val campaign = dboToMsnEntity[Campaign](x, "campaign", None)
            Ok(views.html.msn.customer.account.campaign.edit_campaign(
              id,
              campaignForm.fill(
                CampaignForm(
                  name = campaign.getName,
                  description = Some(campaign.getDescription),
                  budgetType = Some(campaign.getBudgetType.toString),
                  daylightSaving = campaign.getDaylightSaving,
                  dailyBudget = Some(campaign.getDailyBudget),
                  monthlyBudget = Some(campaign.getMonthlyBudget),
                  status = campaign.getStatus.toString,
                  timeZone = campaign.getTimeZone
                )
              )
            ))
          case None =>
            BadRequest("Not Found")
        }
    }
  }


  def createCampaign = deadbolt.Dynamic(name=PermissionGroup.MSNWrite.entryName, handler=handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        campaignForm.bindFromRequest.fold(
          formWithErrors => BadRequest(views.html.msn.customer.account.campaign.new_campaign(formWithErrors, List())),
          campaign => {
            setPendingCache(
              Left(request),
              pendingCache(Left(request)) :+ PendingCacheStructure(
                id = pendingCache(Left(request)).length + 1,
                changeType = ChangeType.NEW,
                trafficSource = TrafficSource.MSN,
                changeCategory = ChangeCategory.CAMPAIGN,
                changeData = campaignFormToDbo(campaign)
              )
            )
            Redirect(controllers.msn.customer.account.campaign.routes.CampaignController.campaigns())
          }
        )
    }
  }

  def saveCampaign(id: String) = deadbolt.Dynamic(name=PermissionGroup.MSNWrite.entryName, handler=handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        campaignForm.bindFromRequest.fold(
          formWithErrors => {
            BadRequest(
              views.html.msn.customer.account.campaign.edit_campaign(
                id,
                formWithErrors
              )
            )
          },
          campaign => {
            setPendingCache(
              Left(request),
              pendingCache(Left(request)) :+ PendingCacheStructure(
                id = pendingCache(Left(request)).length + 1,
                changeType = ChangeType.UPDATE,
                trafficSource = TrafficSource.MSN,
                changeCategory = ChangeCategory.CAMPAIGN,
                changeData = campaignFormToDbo(campaign)
              )
            )
            Redirect(controllers.msn.customer.account.campaign.routes.CampaignController.campaigns())
          }
        )
    }
  }


  def deleteCampaign(id: String) = deadbolt.Dynamic(name=PermissionGroup.MSNWrite.entryName, handler=handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        setPendingCache(
          Left(request),
          pendingCache(Left(request)) :+ PendingCacheStructure(
            id = pendingCache(Left(request)).length + 1,
            changeType = ChangeType.DELETE,
            trafficSource = TrafficSource.MSN,
            changeCategory = ChangeCategory.CAMPAIGN,
            changeData = DBObject("apiId" -> id)
          )
        )
        Redirect(controllers.msn.customer.account.campaign.routes.CampaignController.campaigns())
    }
  }

  def bulkNewCampaign = deadbolt.Dynamic(name = PermissionGroup.MSNWrite.entryName, handler = handlers(HandlerKeys.defaultHandler)) {
    Action(parse.multipartFormData) {
      implicit request => {
        var error_list = new ListBuffer[String]()
        request.body.file("bulk").foreach {
          bulk => {
            val field_names = Utilities.getCaseClassParameter[com.microsoft.bingads.campaignmanagement.Campaign]
            val campaign_data_list = Utilities.bulkImport(bulk, field_names)
            for (((campaign_data, action), index) <- campaign_data_list.zipWithIndex) {
              campaignForm.bind(campaign_data.map(kv => (kv._1, kv._2)).toMap).fold(
                formWithErrors => {
                  error_list += "Row " + index.toString + ": " + formWithErrors.errorsAsJson.toString
                },
                campaign => {
                  setPendingCache(
                    Left(request),
                    pendingCache(Left(request)) :+ PendingCacheStructure(
                      id = pendingCache(Left(request)).length + 1,
                      changeType = ChangeType.withName(action.toUpperCase),
                      trafficSource = TrafficSource.MSN,
                      changeCategory = ChangeCategory.CAMPAIGN,
                      changeData = campaignFormToDbo(campaign)
                    )
                  )
                }
              )
            }
          }
        }
        if (error_list.nonEmpty) {
          BadRequest(views.html.msn.customer.account.campaign.new_campaign(
            campaignForm,
            error_list.toList
          ))
        } else {
          Redirect(controllers.msn.customer.account.campaign.routes.CampaignController.campaigns())
        }
      }
    }
  }
}


