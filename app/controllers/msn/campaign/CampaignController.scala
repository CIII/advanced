package controllers.msn.campaign

import Shared.Shared._
import models.mongodb._
import models.mongodb.msn.collections._
import play.api.data.Form
import play.api.data.Forms._
import play.api.libs.json.{JsObject, Json}
import play.api.mvc._
import play.modules.reactivemongo.MongoController
import play.api.data.format.Formats._
import reactivemongo.api.QueryOpts
import reactivemongo.core.commands.Count
import scala.collection.immutable.List
import scala.collection.mutable.ListBuffer
import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._


object CampaignController extends Controller with MongoController {

  def campaigns(page: Int, pageSize: Int, orderBy: Int, filter: String) = Action.async {
    implicit request =>
      campaignCollection.find(Json.obj())
        .options(QueryOpts(skipN=page * pageSize))
        .cursor[JsObject]
        .collect[List](pageSize)
        .map { campaigns_obj =>
          Ok(views.html.msn.campaign.campaigns(
            campaigns_obj.map(x => gson.fromJson(
              Json.stringify(x),
              classOf[bingads.campaignmanagement.Campaign]
            )),
            page,
            pageSize,
            orderBy,
            filter,
            Await.result(db.command(Count(campaignCollection.name, None)), 30 seconds),
            pendingCache(request)
              .filter(
                x =>
                  x.traffic_source == traffic_source.MSN && x.change_category == change_category.CAMPAIGN
              )
          ))
      }.recover{
        case e =>
          BadRequest("Error -> %s".format(e.toString))
      }
  }


  def newCampaign = Action {
    implicit request => Ok(views.html.msn.campaign.new_campaign(campaignForm, List()))
  }


  def editCampaign(id: String) = Action.async {
    implicit request =>
      campaignCollection.find(Json.obj("id" -> id)).one[JsObject].map {
        case Some(x) =>
          val campaign = gson.fromJson(
            Json.stringify(x),
            classOf[bingads.campaignmanagement.Campaign]
          )
          Ok(views.html.msn.campaign.edit_campaign(
            id,
            campaignForm.fill(
              CampaignForm(
                name=campaign.getName,
                description=Some(campaign.getDescription),
                budget_type=Some(campaign.getBudgetType.toString),
                daylight_saving=campaign.getDaylightSaving,
                daily_budget=Some(campaign.getDailyBudget),
                monthly_budget=Some(campaign.getMonthlyBudget),
                status=campaign.getStatus.toString,
                time_zone=campaign.getTimeZone
              )
            )
          ))
        case None =>
          BadRequest("Not Found")
      }.recover{
        case e =>
          BadRequest("Error -> %s".format(e.toString))
      }
  }


  def createCampaign = Action {
    implicit request =>
      campaignForm.bindFromRequest.fold(
        formWithErrors => BadRequest(views.html.msn.campaign.new_campaign(formWithErrors, List())),
        campaign => {
          setPendingCache(
            request,
            pendingCache(request) :+ pending_cache_structure(
              id=pendingCache(request).length+1,
              change_type=change_type.NEW,
              traffic_source=traffic_source.MSN,
              change_category=change_category.CAMPAIGN,
              change_data=campaign
            )
          )
            Redirect(controllers.msn.campaign.routes.CampaignController.campaigns())
        }
      )
  }


  def saveCampaign(id: String) = Action {
    implicit request =>
      campaignForm.bindFromRequest.fold(
        formWithErrors => {
          BadRequest(
            views.html.msn.campaign.edit_campaign(
              id,
              formWithErrors
            )
          )
        },
        campaign => {
          setPendingCache(
            request,
            pendingCache(request) :+ pending_cache_structure(
              id=pendingCache(request).length+1,
              change_type=change_type.UPDATE,
              traffic_source=traffic_source.MSN,
              change_category=change_category.CAMPAIGN,
              change_data=campaign
            )
          )
          Redirect(controllers.msn.campaign.routes.CampaignController.campaigns())
        }
      )
  }


  def deleteCampaign(id: String) = Action {
    implicit request =>
      setPendingCache(
        request,
        pendingCache(request) :+ pending_cache_structure(
          id=pendingCache(request).length+1,
          change_type=change_type.DELETE,
          traffic_source=traffic_source.MSN,
          change_category=change_category.CAMPAIGN,
          change_data=id
        )
      )
      Redirect(controllers.msn.campaign.routes.CampaignController.campaigns())
  }


  def bulkNewCampaign = Action(parse.multipartFormData) {
    implicit request => {
      var error_list = new ListBuffer[String]()
      request.body.file("bulk").map {
        bulk => {
          val field_names = Utilities.getCaseClassParameter[bingads.campaignmanagement.Campaign]
          val campaign_data_list = Utilities.bulkImport(bulk, field_names)
          for (((campaign_data, action), index) <- campaign_data_list.zipWithIndex) {
            campaignForm.bind(campaign_data.map(kv => (kv._1, kv._2)).toMap).fold(
              formWithErrors => {
                error_list += "Row " + index.toString + ": " + formWithErrors.errorsAsJson.toString
              },
              campaign => {
                setPendingCache(
                  request,
                  pendingCache(request) :+ pending_cache_structure(
                    id=pendingCache(request).length+1,
                    change_type=change_type.withName(action.toUpperCase),
                    traffic_source=traffic_source.MSN,
                    change_category=change_category.CAMPAIGN,
                    change_data=campaign
                  )
                )
              }
            )
          }
        }
      }
      if (error_list.size > 0) {
        BadRequest(views.html.msn.campaign.new_campaign(
          campaignForm,
          error_list.toList
        ))
      } else {
        Redirect(controllers.msn.campaign.routes.CampaignController.campaigns())
      }
    }
  }

  case class CampaignForm(
    name: String,
    description: Option[String],
    budget_type: Option[String],
    daylight_saving: Boolean,
    daily_budget: Option[Double],
    monthly_budget: Option[Double],
    status: String,
    time_zone: String
  )

  def campaignForm: Form[CampaignForm] = Form(
    mapping(
      "name" -> nonEmptyText,
      "description" -> optional(text),
      "budget_type" -> optional(text),
      "daylight_saving" -> boolean,
      "daily_budget" -> optional(of[Double]),
      "monthly_budget" -> optional(of[Double]),
      "status" -> nonEmptyText,
      "time_zone" -> nonEmptyText
    )(CampaignForm.apply)(CampaignForm.unapply)
  )
}
