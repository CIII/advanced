package controllers.msn.adgroup

import Shared.Shared._
import models.mongodb._
import models.mongodb.msn.collections.adGroupCollection
import play.api.data.Form
import play.api.data.Forms._
import play.api.data.format.Formats._
import play.api.libs.json.{JsObject, Json}
import play.api.mvc._
import play.modules.reactivemongo.MongoController
import reactivemongo.api.QueryOpts
import reactivemongo.core.commands.Count

import scala.collection.immutable.List
import scala.collection.mutable.ListBuffer
import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._


object AdGroupController extends Controller with MongoController {

  def adgroups(page: Int, pageSize: Int, orderBy: Int, filter: String) = Action.async {
    implicit request =>
      adGroupCollection.find(Json.obj())
        .options(QueryOpts(skipN=page * pageSize))
        .cursor[JsObject]
        .collect[List](pageSize)
        .map { adgroups_obj =>
          Ok(views.html.msn.adgroup.adgroups(
            adgroups_obj.map(x => gson.fromJson(
              Json.stringify(x),
              classOf[bingads.campaignmanagement.AdGroup]
            )),
            page,
            pageSize,
            orderBy,
            filter,
            Await.result(db.command(Count(adGroupCollection.name, None)), 30 seconds),
            pendingCache(request)
              .filter(
                x =>
                  x.traffic_source == traffic_source.MSN && x.change_category == change_category.AD_GROUP
              )
          ))
      }.recover{
        case e =>
          BadRequest("Error -> %s".format(e.toString))
      }
  }


  def newAdGroup = Action {
    implicit request => Ok(views.html.msn.adgroup.new_adgroup(adGroupForm, List()))
  }


  def getBid(bid: bingads.campaignmanagement.Bid): Option[Double] = bid match {
        case x =>
          Some(x.getAmount)
        case _ =>
          None
    }

  def editAdGroup(id: String) = Action.async {
    implicit request =>
      adGroupCollection.find(Json.obj("id" -> id)).one[JsObject].map {
        case Some(x) =>
          val adgroup = gson.fromJson(
            Json.stringify(x),
            classOf[bingads.campaignmanagement.AdGroup]
          )
          Ok(views.html.msn.adgroup.edit_adgroup(
            id,
            adGroupForm.fill(
              AdGroupForm(
                id=Some(adgroup.getId),
                name=adgroup.getName,
                start_date=Some(
                  new org.joda.time.DateTime(
                    adgroup.getStartDate.getYear,
                    adgroup.getStartDate.getMonth,
                    adgroup.getStartDate.getDay,
                    0,
                    0
                  )
                ),
                end_date=Some(
                  new org.joda.time.DateTime(
                    adgroup.getEndDate.getYear,
                    adgroup.getEndDate.getMonth,
                    adgroup.getEndDate.getDay,
                    0,
                    0
                  )
                ),
                ad_distribution=adgroup.getAdDistribution.toList,
                ad_rotation_type=adgroup.getAdRotation.toString,
                bidding_model=adgroup.getBiddingModel.toString,
                broad_match_bid=getBid(adgroup.getBroadMatchBid),
                content_match_bid=getBid(adgroup.getContentMatchBid),
                phrase_match_bid=getBid(adgroup.getPhraseMatchBid),
                exact_match_bid=getBid(adgroup.getExactMatchBid),
                network=Some(adgroup.getNetwork.toString),
                pricing_model=Some(adgroup.getPricingModel.toString),
                language=adgroup.getLanguage,
                status=adgroup.getStatus.toString
              )
            )
          ))
        case None =>
          BadRequest("Not Found")
      }.recover{
        case e =>
          BadRequest("Error -> %s".format(e.toString))
      }
  }


  def createAdGroup = Action {
    implicit request =>
      adGroupForm.bindFromRequest.fold(
        formWithErrors => BadRequest(views.html.msn.adgroup.new_adgroup(formWithErrors, List())),
        adgroup => {
          setPendingCache(
            request,
            pendingCache(request) :+ pending_cache_structure(
              id=pendingCache(request).length+1,
              change_type=change_type.NEW,
              traffic_source=traffic_source.MSN,
              change_category=change_category.AD_GROUP,
              change_data=adgroup
            )
          )
            Redirect(controllers.msn.campaign.routes.CampaignController.campaigns())
        }
      )
  }


  def saveAdGroup(id: String) = Action {
    implicit request =>
      adGroupForm.bindFromRequest.fold(
        formWithErrors => {
          BadRequest(
            views.html.msn.adgroup.edit_adgroup(
              id,
              formWithErrors
            )
          )
        },
        campaign => {
          setPendingCache(
            request,
            pendingCache(request) :+ pending_cache_structure(
              id=pendingCache(request).length+1,
              change_type=change_type.UPDATE,
              traffic_source=traffic_source.MSN,
              change_category=change_category.AD_GROUP,
              change_data=campaign
            )
          )
          Redirect(controllers.msn.adgroup.routes.AdGroupController.adgroups())
        }
      )
  }


  def deleteAdGroup(id: String) = Action {
    implicit request =>
      setPendingCache(
        request,
        pendingCache(request) :+ pending_cache_structure(
          id=pendingCache(request).length+1,
          change_type=change_type.DELETE,
          traffic_source=traffic_source.MSN,
          change_category=change_category.AD_GROUP,
          change_data=id
        )
      )
      Redirect(controllers.msn.adgroup.routes.AdGroupController.adgroups())
  }


  def bulkNewAdGroup = Action(parse.multipartFormData) {
    implicit request => {
      var error_list = new ListBuffer[String]()
      request.body.file("bulk").map {
        bulk => {
          val field_names = Utilities.getCaseClassParameter[bingads.campaignmanagement.Campaign]
          val campaign_data_list = Utilities.bulkImport(bulk, field_names)
          for (((campaign_data, action), index) <- campaign_data_list.zipWithIndex) {
            adGroupForm.bind(campaign_data.map(kv => (kv._1, kv._2)).toMap).fold(
              formWithErrors => {
                error_list += "Row " + index.toString + ": " + formWithErrors.errorsAsJson.toString
              },
              adgroup => {
                setPendingCache(
                  request,
                  pendingCache(request) :+ pending_cache_structure(
                    id=pendingCache(request).length+1,
                    change_type=change_type.withName(action.toUpperCase),
                    traffic_source=traffic_source.MSN,
                    change_category=change_category.AD_GROUP,
                    change_data=adgroup
                  )
                )
              }
            )
          }
        }
      }
      if (error_list.size > 0) {
        BadRequest(views.html.msn.adgroup.new_adgroup(
          adGroupForm,
          error_list.toList
        ))
      } else {
        Redirect(controllers.msn.adgroup.routes.AdGroupController.adgroups())
      }
    }
  }

  case class AdGroupForm(
    id: Option[Long],
    name: String,
    start_date: Option[org.joda.time.DateTime],
    end_date: Option[org.joda.time.DateTime],
    ad_distribution: List[String],
    ad_rotation_type: String,
    bidding_model: String,
    broad_match_bid: Option[Double],
    content_match_bid: Option[Double],
    phrase_match_bid: Option[Double],
    exact_match_bid: Option[Double],
    network: Option[String],
    pricing_model: Option[String],
    language: String,
    status: String
  )

  def adGroupForm: Form[AdGroupForm] = Form(
    mapping(
      "id" -> optional(longNumber),
      "name" -> nonEmptyText,
      "start_date" -> optional(jodaDate),
      "end_date" -> optional(jodaDate),
      "ad_distribution" -> list(text),
      "ad_rotation_type" -> text,
      "bidding_model" -> text,
      "broad_match_bid" -> optional(of[Double]),
      "content_match_bid" -> optional(of[Double]),
      "phrase_match_bid" -> optional(of[Double]),
      "exact_match_bid" -> optional(of[Double]),
      "network" -> optional(text),
      "pricing_model" -> optional(text),
      "language" -> nonEmptyText,
      "status" -> nonEmptyText
    )(AdGroupForm.apply)(AdGroupForm.unapply)
  )
}
