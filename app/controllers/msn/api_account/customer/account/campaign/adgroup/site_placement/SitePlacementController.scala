package controllers.msn.api_account.customer.account.campaign.adgroup.site_placement

import javax.inject.Inject

import Shared.Shared._
import be.objectify.deadbolt.scala.cache.HandlerCache
import be.objectify.deadbolt.scala.{ActionBuilders, DeadboltActions}
import com.microsoft.bingads.campaignmanagement.SitePlacement
import com.microsoft.bingads.customermanagement.Customer
import com.mongodb.casbah.Imports._
import helpers.msn.api_account.customer.account.campaign.adgroup.site_placement.SitePlacementControllerHelper
import models.mongodb.msn.Msn._
import models.mongodb.{PermissionGroup, Utilities}
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc.{Action, Controller}
import security.HandlerKeys
import sync.shared.Msn._

import scala.collection.immutable.List
import scala.collection.mutable.ListBuffer

class SitePlacementController @Inject()(val messagesApi: MessagesApi, deadbolt: DeadboltActions, handlers: HandlerCache, actionBuilder: ActionBuilders) extends Controller with I18nSupport {

  import SitePlacementControllerHelper._

  def sitePlacements(page: Int, pageSize: Int, orderBy: Int, filter: String) = deadbolt.Dynamic(name=PermissionGroup.MSNWrite.entryName, handler=handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        Ok(views.html.msn.api_account.customer.customers(
          msnSitePlacementCollection.find(
            DBObject(),
            DBObject("sitePlacement" -> DBObject("$slice" -> -1))
          ).skip(page * pageSize).limit(pageSize).toList.map(dboToMsnEntity[Customer](_, "sitePlacement", None)),
          page,
          pageSize,
          orderBy,
          filter,
          msnSitePlacementCollection.count(),
          pendingCache(Left(request))
            .filter(
              x =>
                x.trafficSource == TrafficSource.MSN && x.changeCategory == ChangeCategory.CUSTOMER
            )
        ))
    }
  }


  def newSitePlacement = deadbolt.Dynamic(name=PermissionGroup.MSNWrite.entryName, handler=handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request => Ok(views.html.msn.api_account.customer.account_info.campaign.adgroup.site_placement.new_site_placement(sitePlacementForm, List()))
    }
  }


  def editSitePlacement(id: String) = deadbolt.Dynamic(name=PermissionGroup.MSNWrite.entryName, handler=handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        msnSitePlacementCollection.findOne(DBObject("sitePlacementApiId" -> id)) match {
          case Some(site_placement_obj) =>
            val site_placement = dboToMsnEntity[SitePlacement](site_placement_obj, "sitePlacement", None)
            Ok(views.html.msn.api_account.customer.account_info.campaign.adgroup.site_placement.edit_site_placement(
              id,
              sitePlacementForm.fill(
                SitePlacementForm(
                  apiId = Some(site_placement.getId),
                  sitePlacementApiId = Some(site_placement.getPlacementId),
                  bid = Some(Bid(amount = site_placement.getBid.getAmount)),
                  status = Some(site_placement.getStatus.value),
                  url = site_placement.getUrl
                )
              )
            ))
          case None =>
            BadRequest("Not Found")
        }
    }
  }


  def createSitePlacements = deadbolt.Dynamic(name=PermissionGroup.MSNWrite.entryName, handler=handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        sitePlacementForm.bindFromRequest.fold(
          formWithErrors => BadRequest(views.html.msn.api_account.customer.account_info.campaign.adgroup.site_placement.new_site_placement(formWithErrors, List())),
          site_placement_form => {
            setPendingCache(
              Left(request),
              pendingCache(Left(request)) :+ PendingCacheStructure(
                id = pendingCache(Left(request)).length + 1,
                changeType = ChangeType.NEW,
                trafficSource = TrafficSource.MSN,
                changeCategory = ChangeCategory.SITE_PLACEMENT,
                changeData = sitePlacementFormToDbo(site_placement_form)
              )
            )
            Redirect(controllers.msn.api_account.customer.account.campaign.adgroup.site_placement.routes.SitePlacementController.sitePlacements())
          }
        )
    }
  }


  def saveSitePlacement(id: String) = deadbolt.Dynamic(name=PermissionGroup.MSNWrite.entryName, handler=handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        sitePlacementForm.bindFromRequest.fold(
          formWithErrors => {
            BadRequest(
              views.html.msn.api_account.customer.account_info.campaign.adgroup.site_placement.edit_site_placement(
                id,
                formWithErrors
              )
            )
          },
          site_placement => {
            setPendingCache(
              Left(request),
              pendingCache(Left(request)) :+ PendingCacheStructure(
                id = pendingCache(Left(request)).length + 1,
                changeType = ChangeType.UPDATE,
                trafficSource = TrafficSource.MSN,
                changeCategory = ChangeCategory.SITE_PLACEMENT,
                changeData = sitePlacementFormToDbo(site_placement)
              )
            )
            Redirect(controllers.msn.api_account.customer.account.campaign.adgroup.site_placement.routes.SitePlacementController.sitePlacements())
          }
        )
    }
  }


  def deleteSitePlacement(id: String) = deadbolt.Dynamic(name=PermissionGroup.MSNWrite.entryName, handler=handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        setPendingCache(
          Left(request),
          pendingCache(Left(request)) :+ PendingCacheStructure(
            id = pendingCache(Left(request)).length + 1,
            changeType = ChangeType.DELETE,
            trafficSource = TrafficSource.MSN,
            changeCategory = ChangeCategory.SITE_PLACEMENT,
            changeData = DBObject("apiId" -> id)
          )
        )
        Redirect(controllers.msn.api_account.customer.account.campaign.adgroup.site_placement.routes.SitePlacementController.sitePlacements())
    }
  }


  def bulkNewSitePlacement = deadbolt.Dynamic(name = PermissionGroup.MSNWrite.entryName, handler = handlers(HandlerKeys.defaultHandler)) {
    Action(parse.multipartFormData) {
      implicit request => {
        var error_list = new ListBuffer[String]()
        request.body.file("bulk").foreach {
          bulk => {
            val field_names = Utilities.getCaseClassParameter[SitePlacementForm]
            val site_placement_data_list = Utilities.bulkImport(bulk, field_names)
            for (((customer_data, action), index) <- site_placement_data_list.zipWithIndex) {
              sitePlacementForm.bind(customer_data.map(kv => (kv._1, kv._2)).toMap).fold(
                formWithErrors => {
                  error_list += "Row " + index.toString + ": " + formWithErrors.errorsAsJson.toString
                },
                site_placement => {
                  setPendingCache(
                    Left(request),
                    pendingCache(Left(request)) :+ PendingCacheStructure(
                      id = pendingCache(Left(request)).length + 1,
                      changeType = ChangeType.withName(action.toUpperCase),
                      trafficSource = TrafficSource.MSN,
                      changeCategory = ChangeCategory.SITE_PLACEMENT,
                      changeData = sitePlacementFormToDbo(site_placement)
                    )
                  )
                }
              )
            }
          }
        }
        if (error_list.nonEmpty) {
          BadRequest(views.html.msn.api_account.customer.account_info.campaign.adgroup.site_placement.new_site_placement(
            sitePlacementForm,
            error_list.toList
          ))
        } else {
          Redirect(controllers.msn.api_account.customer.account.campaign.adgroup.site_placement.routes.SitePlacementController.sitePlacements())
        }
      }
    }
  }
}

