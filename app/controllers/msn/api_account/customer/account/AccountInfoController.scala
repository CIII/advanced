package controllers.msn.api_account.customer.account

import javax.inject.Inject

import Shared.Shared._
import be.objectify.deadbolt.scala.{ActionBuilders, DeadboltActions}
import be.objectify.deadbolt.scala.cache.HandlerCache
import com.microsoft.bingads.customermanagement.{AccountInfoWithCustomerData, User}
import com.mongodb.casbah.Imports._
import helpers.msn.api_account.ApiAccountControllerHelper._
import models.mongodb.{Utilities, PermissionGroup}
import models.mongodb.msn.Msn._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc.BodyParsers.parse
import play.api.mvc.{Action, Controller}
import security.HandlerKeys

import scala.collection.immutable.List
import scala.collection.mutable.ListBuffer

class AccountInfoController @Inject()(val messagesApi: MessagesApi, deadbolt: DeadboltActions, handlers: HandlerCache, actionBuilder: ActionBuilders) extends Controller with I18nSupport {

  def account_infos(page: Int, pageSize: Int, orderBy: Int, filter: String) = deadbolt.Dynamic(name = PermissionGroup.MSNRead.entryName, handler = handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        Ok(views.html.msn.api_account.customer.account_info.account_infos(
          msnAccountInfoCollection.find(
            DBObject(),
            DBObject("account_info" -> DBObject("$slice" -> -1))
          ).toList.map { x =>
            Tuple2(
              dboToMsnEntity[User](x, "account", Some("user")),
              dboToMsnEntity[AccountInfoWithCustomerData](x, "account", Some("accountInfoWithCustomerData"))
            )
          },
          page,
          pageSize,
          orderBy,
          filter,
          msnAccountInfoCollection.count()
        ))
    }
  }
}