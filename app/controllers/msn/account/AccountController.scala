package controllers.msn.account

import Shared.Shared._
import models.mongodb._
import models.mongodb.google.collections._
import reactivemongo.api.QueryOpts
import scala.concurrent.duration._
import play.api.data.Form
import play.api.data.Forms._
import play.api.libs.json.{JsObject, Json}
import play.api.mvc._
import play.modules.reactivemongo.MongoController
import reactivemongo.core.commands.Count

import scala.collection.immutable.List
import scala.collection.mutable.ListBuffer
import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global


object AccountController extends Controller with MongoController {

  def accounts(page: Int, pageSize: Int, orderBy: Int, filter: String) = Action.async {
    implicit request =>
      accountCollection.find(Json.obj())
        .options(QueryOpts(skipN=page * pageSize))
        .cursor[JsObject]
        .collect[List](pageSize)
        .map { accounts_obj =>
          Ok(views.html.msn.account.accounts(
            accounts_obj.map( x =>
              Tuple3(
                gson.fromJson(
                  Json.stringify(x.\("User")),
                  classOf[bingads.customermanagement.entities.User]
                ),
                gson.fromJson(
                  Json.stringify(x.\("AccountInfoWithCustomerData")),
                  classOf[bingads.customermanagement.entities.AccountInfoWithCustomerData]
                ),
                Json.stringify(x.\("DeveloperToken"))
              )
            ).toList,
            page,
            pageSize,
            orderBy,
            filter,
            Await.result(db.command(Count(accountCollection.name, None)), 30 seconds),
            pendingCache(request)
              .filter(
                x =>
                  x.traffic_source == traffic_source.MSN && x.change_category == change_category.ACCOUNT
              )
          ))
      }.recover{
        case e =>
          BadRequest("Error -> %s".format(e.toString))
      }
  }


  def newAccount = Action {
    implicit request => Ok(views.html.msn.account.new_account(accountForm, List()))
  }


  def editAccount(id: String) = Action.async {
    implicit request =>
      accountCollection.find(Json.obj("id" -> id)).one[JsObject].map {
        case Some(account_obj) =>
          val user = gson.fromJson(
            Json.stringify(account_obj.\("User")),
            classOf[bingads.customermanagement.entities.User]
          )
          val account_info_with_customer_data = gson.fromJson(
            Json.stringify(account_obj.\("AccountInfoWithCustomerData")),
            classOf[bingads.customermanagement.entities.AccountInfoWithCustomerData]
          )
          Ok(views.html.msn.account.edit_account(
            id,
            accountForm.fill(
              AccountForm(
                user_name=user.getUserName,
                password=user.getPassword,
                developer_token = Json.stringify(account_obj.\("DeveloperToken")),
                customer_id = Some(account_info_with_customer_data.getCustomerId),
                customer_account_id = Some(account_info_with_customer_data.getAccountId)
              )
            )
          ))
        case None =>
          BadRequest("Not Found")
      }.recover{
        case e =>
          BadRequest("Error -> %s".format(e.toString))
      }
  }


  def createAccount = Action {
    implicit request =>
      accountForm.bindFromRequest.fold(
        formWithErrors => BadRequest(views.html.msn.account.new_account(formWithErrors, List())),
        account => {
          setPendingCache(
            request,
            pendingCache(request) :+ pending_cache_structure(
              id=pendingCache(request).length+1,
              change_type=change_type.NEW,
              traffic_source=traffic_source.MSN,
              change_category=change_category.ACCOUNT,
              change_data=account
            )
          )
            Redirect(controllers.msn.account.routes.AccountController.accounts())
        }
      )
  }


  def saveAccount(id: String) = Action {
    implicit request =>
      accountForm.bindFromRequest.fold(
        formWithErrors => {
          BadRequest(
            views.html.msn.account.edit_account(
              id,
              formWithErrors
            )
          )
        },
        account => {
          setPendingCache(
            request,
            pendingCache(request) :+ pending_cache_structure(
              id=pendingCache(request).length+1,
              change_type=change_type.UPDATE,
              traffic_source=traffic_source.MSN,
              change_category=change_category.ACCOUNT,
              change_data=account
            )
          )
          Redirect(controllers.msn.account.routes.AccountController.accounts())
        }
      )
  }


  def deleteAccount(id: String) = Action {
    implicit request =>
      setPendingCache(
        request,
        pendingCache(request) :+ pending_cache_structure(
          id=pendingCache(request).length+1,
          change_type=change_type.DELETE,
          traffic_source=traffic_source.MSN,
          change_category=change_category.ACCOUNT,
          change_data=id
        )
      )
      Redirect(controllers.msn.account.routes.AccountController.accounts())
  }


  def bulkNewAccount = Action(parse.multipartFormData) {
    implicit request => {
      var error_list = new ListBuffer[String]()
      request.body.file("bulk").map {
        bulk => {
          val field_names = Utilities.getCaseClassParameter[AccountForm]
          val account_data_list = Utilities.bulkImport(bulk, field_names)
          for (((account_data, action), index) <- account_data_list.zipWithIndex) {
            accountForm.bind(account_data.map(kv => (kv._1, kv._2)).toMap).fold(
              formWithErrors => {
                error_list += "Row " + index.toString + ": " + formWithErrors.errorsAsJson.toString
              },
              account => {
                setPendingCache(
                  request,
                  pendingCache(request) :+ pending_cache_structure(
                    id=pendingCache(request).length + 1,
                    change_type=change_type.withName(action.toUpperCase),
                    traffic_source=traffic_source.MSN,
                    change_category=change_category.ACCOUNT,
                    change_data=account
                  )
                )
              }
            )
          }
        }
      }
      if (error_list.size > 0) {
        BadRequest(views.html.msn.account.new_account(
          accountForm,
          error_list.toList
        ))
      } else {
        Redirect(controllers.msn.account.routes.AccountController.accounts())
      }
    }
  }

  case class AccountForm(
    user_name: String,
    password: String,
    developer_token: String,
    customer_id: Option[Long],
    customer_account_id: Option[Long]
  )

  def accountForm: Form[AccountForm] = Form(
    mapping(
      "user_name" -> nonEmptyText,
      "password" -> nonEmptyText,
      "developer_token" -> nonEmptyText,
      "customer_id" -> optional(longNumber),
      "customer_account_id" -> optional(longNumber)
    )(AccountForm.apply)(AccountForm.unapply)
  )
}
