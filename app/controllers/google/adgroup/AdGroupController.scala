package controllers.google.adgroup

import Shared.Shared._
import com.google.api.ads.adwords.axis.v201409.cm.{AdGroup, Campaign}
import controllers.google.campaign.CampaignController._
import models.mongodb._
import play.api.Play.current
import play.api.cache.Cache
import play.api.data.Forms._
import play.api.data._
import play.api.libs.json.{JsObject, Json}
import play.api.mvc._
import play.modules.reactivemongo.MongoController
import models.mongodb.google.collections._
import reactivemongo.api.QueryOpts
import reactivemongo.core.commands.Count
import scala.collection.mutable.ListBuffer
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._


object AdGroupController extends Controller with MongoController {

  def adGroups(page: Int, pageSize: Int, orderBy: Int, filter: String) = Action.async {
    implicit request =>
      adGroupCollection.find(Json.obj())
        .options(QueryOpts(skipN=page * pageSize))
        .cursor[JsObject].collect[List](pageSize).map{ ad_groups =>
          Ok(views.html.google.adgroup.ad_groups(
            ad_groups.map(y => gson.fromJson(Json.stringify(y), classOf[AdGroup])),
            page,
            pageSize,
            orderBy,
            filter,
            Await.result(db.command(Count(adGroupCollection.name, None)), 30 seconds),
            pendingCache(request)
              .filter(
                x =>
                  x.traffic_source == traffic_source.GOOGLE
                  && x.change_category == change_category.AD_GROUP
              )
          ))
      }.recover{
        case e =>
          BadRequest("Error -> %s".format(e.toString))
      }
  }

  def newAdGroup = Action.async {
    implicit request =>
      campaignCollection.find(Json.obj()).cursor[JsObject].collect[List]().map { campaigns =>
        Ok(views.html.google.adgroup.new_ad_group(
          adGroupForm,
          campaigns.map(y => gson.fromJson(Json.stringify(y), classOf[Campaign])),
          pendingCache(request)
            .filter(
              x =>
                x.traffic_source == traffic_source.GOOGLE
                && x.change_category == change_category.CAMPAIGN
                && x.change_type == change_type.NEW
            )
            .map(_.change_data.asInstanceOf[controllers.google.campaign.CampaignController.CampaignForm]),
          List()
        ))
      }
  }

  def createAdGroup = Action.async {
    implicit request =>
      val current_cache = Cache.get(pendingCacheKey(request)).getOrElse(List()).asInstanceOf[List[pending_cache_structure]]
      campaignCollection.find(Json.obj()).cursor[JsObject].collect[List]().map { campaigns =>
        adGroupForm.bindFromRequest.fold(
          formWithErrors => {
            BadRequest(
              views.html.google.adgroup.new_ad_group(
                formWithErrors,
                campaigns.map(y => gson.fromJson(Json.stringify(y), classOf[Campaign])),
                pendingCache(request)
                  .filter(
                    x =>
                      x.traffic_source == traffic_source.GOOGLE
                      && x.change_category == change_category.CAMPAIGN
                      && x.change_type == change_type.NEW
                  )
                  .map(_.change_data.asInstanceOf[controllers.google.campaign.CampaignController.CampaignForm]),
                List()
              )
            )
          },
          ad_group => {
            Cache.set(
              pendingCacheKey(request),
              current_cache :+ pending_cache_structure(
                id = current_cache.length + 1,
                change_type = change_type.NEW,
                traffic_source = traffic_source.GOOGLE,
                change_category = change_category.AD_GROUP,
                change_data = ad_group
              )
            )
            Redirect(controllers.google.adgroup.routes.AdGroupController.adGroups())
          }
        )
      }
  }


  def bulkNewAdGroup = Action.async(parse.multipartFormData) {
    implicit request => {
      var error_list = new ListBuffer[String]()
      campaignCollection.find(Json.obj()).cursor[JsObject].collect[List]().map { campaigns =>
        request.body.file("bulk").map {
          bulk => {
            val field_names = Utilities.getCaseClassParameter[AdGroup]
            val ad_group_data_list = Utilities.bulkImport(bulk, field_names)
            for (((ad_group_data, action), index) <- ad_group_data_list.zipWithIndex) {
              adGroupForm.bind(ad_group_data.map(kv => (kv._1, kv._2)).toMap).fold(
                formWithErrors => {
                  error_list += "Row " + index.toString + ": " + formWithErrors.errorsAsJson.toString
                },
                ad_group => {
                  setPendingCache(
                    request,
                    pendingCache(request) :+ pending_cache_structure(
                      id=pendingCache(request).length+1,
                      change_type=change_type.withName(action),
                      traffic_source=traffic_source.GOOGLE,
                      change_category=change_category.AD_GROUP,
                      change_data=ad_group
                    )
                  )
                }
              )
            }
          }
        }
        if (error_list.size > 0) {
          BadRequest(views.html.google.adgroup.new_ad_group(
            adGroupForm,
            campaigns.map(y => gson.fromJson(Json.stringify(y), classOf[Campaign])),
            pendingCache(request)
              .filter(x =>
                x.traffic_source == traffic_source.GOOGLE
                && x.change_category == change_category.AD_GROUP
                && x.change_type == change_type.NEW
              )
              .map(_.change_data.asInstanceOf[controllers.google.campaign.CampaignController.CampaignForm]),
            error_list.toList
          ))
        } else {
          Redirect(controllers.google.adgroup.routes.AdGroupController.adGroups())
        }
      }
    }
  }

  def editAdGroup(api_id: Long) = Action.async {
    implicit request =>
      adGroupCollection.find(Json.obj("id" -> api_id)).one[JsObject].flatMap {
        case None => Future{Ok(views.html.dashboard())}
        case Some(ad_group_obj) =>
          campaignCollection.find(Json.obj()).cursor[JsObject].collect[List]().map { campaigns =>
            def ad_group = gson.fromJson(Json.stringify(ad_group_obj), classOf[AdGroup])
            Ok(views.html.google.adgroup.edit_ad_group(
              ad_group.getId,
              adGroupForm.fill(
                AdGroupForm(
                  api_id = Some(ad_group.getId),
                  campaign_api_id = ad_group.getCampaignId,
                  name = ad_group.getName,
                  status = ad_group.getStatus.toString,
                  content_bid_criterion_type_group = Some(ad_group.getContentBidCriterionTypeGroup.toString)
                )),
              campaigns.map(y => gson.fromJson(Json.stringify(y), classOf[Campaign])),
              pendingCache(request)
                .filter(x =>
                  x.traffic_source == traffic_source.GOOGLE.toString
                  && x.change_category == change_category.CAMPAIGN.toString
                  && x.change_type == change_type.NEW.toString
                )
                .map(_.change_data.asInstanceOf[CampaignForm])
            ))
          }.recover{
            case e =>
              BadRequest("Error -> %s".format(e.toString))
          }
      }.recover{
        case e =>
          BadRequest("Error -> %s".format(e.toString))
      }
  }

  def saveAdGroup(api_id: Long) = Action.async {
    implicit request =>
      campaignCollection.find(Json.obj()).cursor[JsObject].collect[List]().map { campaigns =>
        adGroupForm.bindFromRequest.fold(formWithErrors => {
          BadRequest(
            views.html.google.adgroup.edit_ad_group(
              api_id,
              formWithErrors,
              campaigns.map(y => gson.fromJson(Json.stringify(y), classOf[Campaign])),
              pendingCache(request)
                .filter(x =>
                  x.traffic_source == traffic_source.GOOGLE.toString
                  && x.change_category == change_category.CAMPAIGN.toString
                  && x.change_type == change_type.NEW
                )
                .map(_.change_data.asInstanceOf[controllers.google.campaign.CampaignController.CampaignForm])
            )
          )
        },
          ad_group => {
            setPendingCache(
              request,
              pendingCache(request) :+ pending_cache_structure(
                id=pendingCache(request).length+1,
                change_type=change_type.UPDATE,
                traffic_source=traffic_source.GOOGLE,
                change_category=change_category.AD_GROUP,
                change_data=ad_group
              )
            )
            Redirect(controllers.google.adgroup.routes.AdGroupController.adGroups())
          }
        )
      }
  }

  def deleteAdGroup(api_id: Long) = Action {
    implicit request =>
      setPendingCache(
        request,
        pendingCache(request) :+ pending_cache_structure(
          id=pendingCache(request).length+1,
          change_type=change_type.DELETE,
          traffic_source=traffic_source.GOOGLE,
          change_category=change_category.AD_GROUP,
          change_data=api_id
        )
      )
      Redirect(controllers.google.adgroup.routes.AdGroupController.adGroups())
  }

  case class AdGroupForm(
    var api_id: Option[Long],
    var campaign_api_id: Long,
    var name: String,
    var status: String,
    var content_bid_criterion_type_group: Option[String]
  )

  def adGroupForm: Form[AdGroupForm] = Form(
    mapping(
      "api_id" -> optional(longNumber),
      "campaign_api_id" -> longNumber,
      "name" -> nonEmptyText,
      "status" -> text,
      "content_bid_criterion_type_group" -> optional(text)
    )(AdGroupForm.apply)(AdGroupForm.unapply)
  )
}
