package controllers.google.adgroup.criterion

import Shared.Shared._
import com.google.api.ads.adwords.axis.v201409.cm._
import models.mongodb._
import models.mongodb.google.collections._
import play.api.data.Forms._
import play.api.data._
import play.api.data.format.Formats._
import play.api.libs.json.{JsObject, Json}
import play.api.mvc._
import play.modules.reactivemongo.MongoController
import reactivemongo.api.QueryOpts
import reactivemongo.core.commands.Count

import scala.collection.mutable.ListBuffer
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

object KeywordController extends Controller with MongoController {

  def keywords(page: Int, pageSize: Int, orderBy: Int, filter: String) = Action.async {
    implicit request =>
      adGroupCriterionCollection
        .find(Json.obj("criterion.properties.criterionType" -> "Keyword"))
        .options(QueryOpts(skipN=page * pageSize))
        .cursor[JsObject]
        .collect[List](pageSize)
        .map(keywords =>
          Ok(views.html.google.adgroup.criterion.keyword.keywords(
            keywords.map(y =>
                gson.fromJson(Json.stringify(y), classOf[AdGroupCriterion])
            ),
            page,
            pageSize,
            orderBy,
            filter,
            Await.result(db.command(Count(adGroupCriterionCollection.name, None)), 30 seconds),
            pendingCache(request)
              .filter(x =>
                x.traffic_source == traffic_source.GOOGLE
                && x.change_category == change_category.KEYWORD
              )
          ))
      ).recover {
        case e =>
          BadRequest("Error -> %s".format(e.toString))
      }
  }

  def newKeyword = Action.async {
    implicit request =>
      adGroupCollection.genericQueryBuilder.cursor[JsObject].collect[List]().map{ ad_groups =>
        Ok(views.html.google.adgroup.criterion.keyword.new_keyword(
          keywordForm,
          ad_groups.map(y => gson.fromJson(Json.stringify(y), classOf[AdGroup])),
          pendingCache(request)
            .filter(x =>
              x.change_type == change_type.NEW
              && x.traffic_source == traffic_source.GOOGLE
              && x.change_category == change_category.AD_GROUP
            )
            .map(_.change_data.asInstanceOf[controllers.google.adgroup.AdGroupController.AdGroupForm]),
          List()
        ))
    }.recover{
        case e =>
          BadRequest("Error -> %s".format(e.toString))
      }
  }

  def createKeyword = Action.async {
    implicit request =>
      adGroupCollection.genericQueryBuilder.cursor[JsObject].collect[List]().map { ad_groups =>
        keywordForm.bindFromRequest.fold(
          formWithErrors => {
            BadRequest(views.html.google.adgroup.criterion.keyword.new_keyword(
              formWithErrors,
              ad_groups.map(y => gson.fromJson(Json.stringify(y), classOf[AdGroup])),
              pendingCache(request)
                .filter(x =>
                  x.change_type == change_type.NEW
                  && x.traffic_source == traffic_source.GOOGLE
                  && x.change_category == change_category.AD_GROUP
                )
                .map(_.change_data.asInstanceOf[controllers.google.adgroup.AdGroupController.AdGroupForm]),
              List()
            ))
          },
          keyword => {
            setPendingCache(
              request,
              pendingCache(request) :+ pending_cache_structure(
                id=pendingCache(request).length+1,
                change_type=change_type.NEW,
                traffic_source=traffic_source.GOOGLE,
                change_category=change_category.KEYWORD,
                change_data=keyword
              )
            )
            Redirect(controllers.google.adgroup.criterion.routes.KeywordController.keywords())
          }
        )
      }
  }

  def bulkNewKeyword = Action.async(parse.multipartFormData) {
    implicit request =>
      var error = false
      var error_list = new ListBuffer[String]()
      adGroupCollection.genericQueryBuilder.cursor[JsObject].collect[List]().map { ad_groups =>
        request.body.file("bulk").map {
          bulk => {
            val field_names = Utilities.getCaseClassParameter[KeywordForm]
            val keyword_data_list = Utilities.bulkImport(bulk, field_names)
            for (((keyword_data, action), index) <- keyword_data_list.zipWithIndex) {
              keywordForm.bind(keyword_data.map(kv => (kv._1, kv._2)).toMap).fold(
                formWithErrors => {
                  error = true
                  error_list += "Row " + index.toString + ": " + formWithErrors.errorsAsJson.toString
                },
                keyword =>
                  setPendingCache(
                    request,
                    pendingCache(request) :+ pending_cache_structure(
                      id=pendingCache(request).length+1,
                      change_type=change_type.withName(action.toUpperCase),
                      traffic_source=traffic_source.GOOGLE,
                      change_category=change_category.KEYWORD,
                      change_data=keyword
                    )
                  )
              )
            }
          }
        }
        if (error) {
          BadRequest(views.html.google.adgroup.criterion.keyword.new_keyword(
            keywordForm,
            ad_groups.map(y => gson.fromJson(Json.stringify(y), classOf[AdGroup])),
            pendingCache(request)
              .filter(x =>
                x.change_type == change_type.NEW
                && x.traffic_source == traffic_source.GOOGLE
                && x.change_category == change_category.AD_GROUP
              )
              .map(_.change_data.asInstanceOf[controllers.google.adgroup.AdGroupController.AdGroupForm]),
            error_list.toList
          ))
        } else {
          Redirect(controllers.google.adgroup.criterion.routes.KeywordController.keywords())
        }
      }
  }

  def editKeyword(id: Long) = Action.async {
    implicit request =>
      adGroupCriterionCollection.find(Json.obj("criterion.properties.id" -> id)).one[JsObject].flatMap {
        case None => Future{Ok(views.html.dashboard())}
        case Some(keyword) =>
          adGroupCollection.genericQueryBuilder.cursor[JsObject].collect[List]().map { ad_groups =>
            def adGroupCriterion = gson.fromJson(Json.stringify(keyword), classOf[AdGroupCriterion])
            adGroupCriterion match {
              case b: BiddableAdGroupCriterion =>
                Ok(views.html.google.adgroup.criterion.keyword.edit_keyword(
                  id,
                  keywordForm.fill(KeywordForm(
                    api_id = Some(adGroupCriterion.getCriterion.getId),
                    ad_group_api_id = adGroupCriterion.getAdGroupId,
                    criterion_use = adGroupCriterion.getCriterionUse.toString,
                    text = adGroupCriterion.getCriterion.asInstanceOf[Keyword].getText,
                    match_type = adGroupCriterion.getCriterion.asInstanceOf[Keyword].getMatchType.toString,
                    user_status = Some(b.getUserStatus.toString),
                    system_serving_status = Some(b.getSystemServingStatus.toString),
                    approval_status = Some(b.getApprovalStatus.toString),
                    disapproval_reasons = Some(b.getDisapprovalReasons.toList),
                    destination_url = Some(b.getDestinationUrl),
                    first_page_cpc_amount = Some(b.getFirstPageCpc.getAmount.getMicroAmount),
                    top_of_page_cpc_amount = Some(b.getTopOfPageCpc.getAmount.getMicroAmount),
                    bid_modifier = Some(b.getBidModifier)
                  )),
                  ad_groups.map(y => gson.fromJson(Json.stringify(y), classOf[AdGroup])),
                  pendingCache(request)
                    .filter(x =>
                      x.change_type == change_type.NEW
                      && x.traffic_source == traffic_source.GOOGLE
                      && x.change_category == change_category.AD_GROUP
                    )
                    .map(_.change_data.asInstanceOf[controllers.google.adgroup.AdGroupController.AdGroupForm])
                ))
              case c: NegativeAdGroupCriterion =>
                Ok(views.html.google.adgroup.criterion.keyword.edit_keyword(
                  id,
                  keywordForm.fill(KeywordForm(
                    api_id = Some(adGroupCriterion.getCriterion.getId),
                    ad_group_api_id = adGroupCriterion.getAdGroupId,
                    criterion_use = adGroupCriterion.getCriterionUse.toString,
                    text = adGroupCriterion.getCriterion.asInstanceOf[Keyword].getText,
                    match_type = adGroupCriterion.getCriterion.asInstanceOf[Keyword].getMatchType.toString,
                    None, None, None, None, None, None, None, None
                  )),
                  ad_groups.map(y => gson.fromJson(Json.stringify(y), classOf[AdGroup])),
                  pendingCache(request)
                    .filter(x =>
                      x.change_type == change_type.NEW
                      && x.traffic_source == traffic_source.GOOGLE
                      && x.change_category == change_category.AD_GROUP
                    )
                    .map(_.change_data.asInstanceOf[controllers.google.adgroup.AdGroupController.AdGroupForm])
                ))
            }
          }.recover{
            case e =>
              BadRequest("Error -> %s".format(e.toString))
          }
      }.recover{
        case e =>
          BadRequest("Error -> %s".format(e.toString))
      }
  }

  def saveKeyword(id: Long) = Action.async {
    implicit request =>
      adGroupCollection.genericQueryBuilder.cursor[JsObject].collect[List]().map { ad_groups =>
        keywordForm.bindFromRequest.fold(
          formWithErrors => BadRequest(views.html.google.adgroup.criterion.keyword.edit_keyword(
            id,
            formWithErrors,
            ad_groups.map(y => gson.fromJson(Json.stringify(y), classOf[AdGroup])),
            pendingCache(request)
              .filter(x =>
                x.change_type == change_type.NEW
                && x.traffic_source == traffic_source.GOOGLE
                && x.change_category == change_category.AD_GROUP
              )
              .map(_.change_data.asInstanceOf[controllers.google.adgroup.AdGroupController.AdGroupForm])
          )),
          keyword => {
            setPendingCache(
              request,
              pendingCache(request) :+ pending_cache_structure(
                id=pendingCache(request).length+1,
                change_type=change_type.UPDATE,
                traffic_source=traffic_source.GOOGLE,
                change_category=change_category.KEYWORD,
                change_data=keyword
              )
            )
            Redirect(controllers.google.adgroup.criterion.routes.KeywordController.keywords())
          }
        )
      }
  }

  def deleteKeyword(id: Long) = Action {
    implicit request =>
      setPendingCache(
        request,
        pendingCache(request) :+ pending_cache_structure(
          id=pendingCache(request).length+1,
          change_type=change_type.DELETE,
          traffic_source=traffic_source.GOOGLE,
          change_category=change_category.KEYWORD,
          change_data=id
        )
      )
      Redirect(controllers.google.adgroup.criterion.routes.KeywordController.keywords())
  }

  case class KeywordForm(
    var api_id: Option[Long],
    var ad_group_api_id: Long,
    var criterion_use: String,
    var text: String,
    var match_type: String,
    var user_status: Option[String],
    var system_serving_status: Option[String],
    var approval_status: Option[String],
    var disapproval_reasons: Option[List[String]],
    var destination_url: Option[String],
    var first_page_cpc_amount: Option[Long],
    var top_of_page_cpc_amount: Option[Long],
    var bid_modifier: Option[Double]
  )

  def keywordForm: Form[KeywordForm] = Form(
    mapping(
      "api_id" -> optional(longNumber),
      "ad_group_api_id" -> longNumber,
      "criterion_use" -> text,
      "text" -> nonEmptyText,
      "match_type" -> nonEmptyText,
      "user_status" -> optional(text),
      "system_serving_status" -> optional(text),
      "approval_status" -> optional(text),
      "disapproval_reasons" -> optional(list(text)),
      "destination_url" -> optional(text),
      "first_page_cpc_amount" -> optional(longNumber),
      "top_of_page_cpc_amount" -> optional(longNumber),
      "bid_modifier" -> optional(Forms.of[Double])
    )(KeywordForm.apply)(KeywordForm.unapply)
  )
}