package controllers.google.mcc

import javax.inject.Inject

import Shared.Shared._
import be.objectify.deadbolt.scala.cache.HandlerCache
import be.objectify.deadbolt.scala.{ActionBuilders, DeadboltActions}
import com.mongodb.casbah.Imports._
import helpers.google.mcc.MccControllerHelper._
import models.mongodb._
import models.mongodb.google.Google._
import play.api.Play.current
import play.api.cache.Cache
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc._
import security.HandlerKeys

import scala.collection.immutable.List
import scala.collection.mutable.ListBuffer

class MccController @Inject()(val messagesApi: MessagesApi, deadbolt: DeadboltActions, handlers: HandlerCache, actionBuilder: ActionBuilders) extends Controller with I18nSupport {
  def json = Action {
    implicit request =>
      val mcc = googleMccCollection.find(DBObject()).toList
      Ok(com.mongodb.util.JSON.serialize(mcc.toString()))
  }

  def mcc(page: Int, pageSize: Int, orderBy: Int, filter: String) = deadbolt.Dynamic(name=PermissionGroup.GoogleRead.entryName, handler=handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        Ok(views.html.google.mcc.mcc(
          googleMccCollection.find().skip(page * pageSize).limit(pageSize).toList.map(google.Google.dboToMcc),
          page,
          pageSize,
          orderBy,
          filter,
          googleMccCollection.count(),
          pendingCache(Left(request))
            .filter(
              x =>
                x.trafficSource == TrafficSource.GOOGLE && x.changeCategory == ChangeCategory.MCC
            )
        ))
    }
  }


  def newMcc = deadbolt.Dynamic(name=PermissionGroup.GoogleWrite.entryName, handler=handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request => Ok(views.html.google.mcc.new_mcc(mccForm, List()))
    }
  }


  def editMcc(id: String) = deadbolt.Dynamic(name=PermissionGroup.GoogleWrite.entryName, handler=handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        googleMccCollection.findOne(DBObject("_id" -> id)) match {
          case Some(mcc_obj) =>
            def mcc = google.Google.dboToMcc(mcc_obj)
            Ok(views.html.google.mcc.edit_mcc(
              id,
              mccForm.fill(
                Mcc(
                  _id = mcc._id,
                  name = mcc.name,
                  developerToken = mcc.developerToken,
                  oAuthClientId = mcc.oAuthClientId,
                  oAuthClientSecret = mcc.oAuthClientSecret,
                  oAuthRefreshToken = mcc.oAuthRefreshToken
                )
              )
            ))
          case None =>
            BadRequest("Not Found")
        }
    }
  }


  def createMcc = deadbolt.Dynamic(name=PermissionGroup.GoogleWrite.entryName, handler=handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        mccForm.bindFromRequest.fold(
          formWithErrors => BadRequest(views.html.google.mcc.new_mcc(formWithErrors, List())),
          mcc => {
            setPendingCache(
              Left(request),
              pendingCache(Left(request)) :+ PendingCacheStructure(
                id = pendingCache(Left(request)).length + 1,
                changeType = ChangeType.NEW,
                trafficSource = TrafficSource.GOOGLE,
                changeCategory = ChangeCategory.MCC,
                changeData = mccToDBObject(mcc)
              )
            )
            Redirect(controllers.google.mcc.routes.MccController.mcc(0, 10, 2, ""))
          }
        )
    }
  }


  def saveMcc(id: String) = deadbolt.Dynamic(name=PermissionGroup.GoogleWrite.entryName, handler=handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        val pending_cache_key = request.session.get(Security.username).get + cache_ext
        val current_cache = Cache.get(pending_cache_key).getOrElse(List()).asInstanceOf[List[PendingCacheStructure]]
        mccForm.bindFromRequest.fold(
          formWithErrors => {
            BadRequest(
              views.html.google.mcc.edit_mcc(
                id,
                formWithErrors
              )
            )
          },
          mcc => {
            Cache.set(
              pending_cache_key,
              current_cache :+ PendingCacheStructure(
                id = current_cache.length + 1,
                changeType = ChangeType.UPDATE,
                trafficSource = TrafficSource.GOOGLE,
                changeCategory = ChangeCategory.MCC,
                changeData = com.mongodb.util.JSON.serialize(mcc).asInstanceOf[DBObject]
              )
            )
            Redirect(controllers.google.mcc.routes.MccController.mcc())
          }
        )
    }
  }


  def deleteMcc(id: String) = deadbolt.Dynamic(name=PermissionGroup.GoogleWrite.entryName, handler=handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        val pending_cache_key = request.session.get(Security.username).get + cache_ext
        val current_cache = Cache.get(pending_cache_key).getOrElse(List()).asInstanceOf[List[PendingCacheStructure]]
        Cache.set(
          pending_cache_key,
          current_cache :+ PendingCacheStructure(
            id = current_cache.length + 1,
            changeType = ChangeType.DELETE,
            trafficSource = TrafficSource.GOOGLE,
            changeCategory = ChangeCategory.MCC,
            changeData = DBObject("mccObjId" -> id)
          )
        )
        Redirect(controllers.google.mcc.routes.MccController.mcc())
    }
  }


  def bulkNewMcc = deadbolt.Dynamic(name=PermissionGroup.GoogleWrite.entryName, handler=handlers(HandlerKeys.defaultHandler)) {
    Action(parse.multipartFormData) {
      implicit request => {
        var error_list = new ListBuffer[String]()
        val pending_cache_key = request.session.get(Security.username).get + cache_ext
        val current_cache = Cache.get(pending_cache_key).getOrElse(List()).asInstanceOf[List[PendingCacheStructure]]
        request.body.file("bulk").foreach {
          bulk => {
            val field_names = Utilities.getCaseClassParameter[Mcc]
            val mcc_data_list = Utilities.bulkImport(bulk, field_names)
            for (((mcc_data, action), index) <- mcc_data_list.zipWithIndex) {
              mccForm.bind(mcc_data.map(kv => (kv._1, kv._2)).toMap).fold(
                formWithErrors => {
                  error_list += "Row " + index.toString + ": " + formWithErrors.errorsAsJson.toString
                },
                mcc => {
                  Cache.set(
                    pending_cache_key,
                    current_cache :+ PendingCacheStructure(
                      id = current_cache.length + 1,
                      changeType = ChangeType.withName(action.toUpperCase),
                      trafficSource = TrafficSource.GOOGLE,
                      changeCategory = ChangeCategory.MCC,
                      changeData = mccToDBObject(mcc)
                    )
                  )
                }
              )
            }
          }
        }
        if (error_list.nonEmpty) {
          BadRequest(views.html.google.mcc.new_mcc(
            mccForm,
            error_list.toList
          ))
        } else {
          Redirect(controllers.google.mcc.routes.MccController.mcc())
        }
      }
    }
  }
}