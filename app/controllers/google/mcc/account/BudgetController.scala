package controllers.google.mcc.account

import javax.inject.Inject

import Shared.Shared._
import be.objectify.deadbolt.scala.cache.HandlerCache
import be.objectify.deadbolt.scala.{ActionBuilders, DeadboltActions}
import com.google.api.ads.adwords.axis.v201509.cm.Budget
import com.mongodb.casbah.Imports._
import helpers.google.mcc.account.BudgetControllerHelper._
import models.mongodb._
import models.mongodb.google.Google._
import play.api.Play.current
import play.api.cache.Cache
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc._
import security.HandlerKeys

import scala.collection.mutable.ListBuffer

class BudgetController @Inject()(val messagesApi: MessagesApi, deadbolt: DeadboltActions, handlers: HandlerCache, actionBuilder: ActionBuilders) extends Controller with I18nSupport {
  def json = Action {
    implicit request =>
      Ok(controllers.json(
        request,
        List(
          "mccObjId",
          "mccApiId",
          "customerObjId",
          "customerApiId",
          "tsecs"
        ),
        "budget",
        googleBudgetCollection
      ))
  }

  def budgets(page: Int, pageSize: Int, orderBy: Int, filter: String) = deadbolt.Dynamic(name = PermissionGroup.GoogleRead.entryName, handler = handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        val budgets = googleBudgetCollection.find(
          DBObject(),
          DBObject("budget" -> DBObject("$slice" -> -1))
        ).skip(page * pageSize).limit(pageSize).toList
        Ok(views.html.google.mcc.account.budget.budgets(
          budgets.map(dboToGoogleEntity[Budget](_, "budget", None)),
          page,
          pageSize,
          orderBy,
          filter,
          googleBudgetCollection.count(),
          Cache.get(pendingCacheKey(Left(request)))
            .getOrElse(List())
            .asInstanceOf[List[PendingCacheStructure]]
            .filter(x => x.trafficSource == TrafficSource.GOOGLE && x.changeCategory == ChangeCategory.BUDGET)
        ))
    }
  }

  def newBudget = deadbolt.Dynamic(name = PermissionGroup.GoogleWrite.entryName, handler = handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        Ok(views.html.google.mcc.account.budget.new_budget(
          budgetForm,
          List()
        ))
    }
  }

  def editBudget(api_id: Long) = deadbolt.Dynamic(name = PermissionGroup.GoogleWrite.entryName, handler = handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        val budgetDBObject = googleBudgetCollection.findOne(
          DBObject("apiId" -> api_id),
          DBObject("budget" -> DBObject("$slice" -> -1))
        )
        budgetDBObject match {
          case Some(budgetObj) =>
            def budget = dboToGoogleEntity[Budget](budgetObj, "budget", None)
            Ok(views.html.google.mcc.account.budget.edit_budget(
              api_id,
              budgetForm.fill(
                BudgetForm(
                  apiId = Some(budget.getBudgetId),
                  name = budget.getName,
                  period = Some(budget.getPeriod.toString),
                  amount = Some(budget.getAmount.getMicroAmount),
                  deliveryMethod = Some(budget.getDeliveryMethod.toString),
                  isExplicitlyShared = Some(budget.getIsExplicitlyShared),
                  status = Some(budget.getStatus.toString)
                )
              )
            ))
          case None =>
            Redirect(controllers.google.mcc.account.routes.BudgetController.budgets())
        }
    }
  }

  def createBudget = deadbolt.Dynamic(name = PermissionGroup.GoogleWrite.entryName, handler = handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        val current_cache = Cache.get(pendingCacheKey(Left(request))).getOrElse(List()).asInstanceOf[List[PendingCacheStructure]]
        budgetForm.bindFromRequest.fold(
          formWithErrors => {
            BadRequest(
              views.html.google.mcc.account.budget.new_budget(
                formWithErrors,
                List()
              )
            )
          },
          budget => {
            Cache.set(
              pendingCacheKey(Left(request)),
              current_cache :+ PendingCacheStructure(
                id = current_cache.length + 1,
                changeType = ChangeType.NEW,
                trafficSource = TrafficSource.GOOGLE,
                changeCategory = ChangeCategory.BUDGET,
                changeData = budgetFormToDbo(budget)
              )
            )
            Redirect(controllers.google.mcc.account.routes.BudgetController.budgets())
          }
        )
    }
  }

  def bulkNewBudget = deadbolt.Dynamic(name = PermissionGroup.GoogleWrite.entryName, handler = handlers(HandlerKeys.defaultHandler)) {
    Action(parse.multipartFormData) {
      implicit request => {
        var error_list = new ListBuffer[String]()
        request.body.file("bulk").foreach {
          bulk => {
            val field_names = Utilities.getCaseClassParameter[Budget]
            val budget_data_list = Utilities.bulkImport(bulk, field_names)
            for (((budget_data, action), index) <- budget_data_list.zipWithIndex) {
              budgetForm.bind(budget_data.map(kv => (kv._1, kv._2)).toMap).fold(
                formWithErrors => {
                  error_list += "Row " + index.toString + ": " + formWithErrors.errorsAsJson.toString
                },
                budget => {
                  setPendingCache(
                    Left(request),
                    pendingCache(Left(request)) :+ PendingCacheStructure(
                      id = pendingCache(Left(request)).length + 1,
                      changeType = ChangeType.withName(action.toUpperCase),
                      trafficSource = TrafficSource.GOOGLE,
                      changeCategory = ChangeCategory.BUDGET,
                      changeData = budgetFormToDbo(budget)
                    )
                  )
                }
              )
            }
          }
        }
        if (error_list.nonEmpty) {
          BadRequest(views.html.google.mcc.account.budget.new_budget(
            budgetForm,
            error_list.toList
          ))
        } else {
          Redirect(controllers.google.mcc.account.routes.BudgetController.budgets())
        }
      }
    }
  }


  def saveBudget(api_id: Long) = deadbolt.Dynamic(name = PermissionGroup.GoogleWrite.entryName, handler = handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        budgetForm.bindFromRequest.fold(
          formWithErrors => {
            BadRequest(
              views.html.google.mcc.account.budget.edit_budget(
                api_id,
                formWithErrors
              )
            )
          },
          budget => {
            setPendingCache(
              Left(request),
              pendingCache(Left(request)) :+ PendingCacheStructure(
                id = pendingCache(Left(request)).length + 1,
                changeType = ChangeType.UPDATE,
                trafficSource = TrafficSource.GOOGLE,
                changeCategory = ChangeCategory.BUDGET,
                changeData = budgetFormToDbo(budget)
              )
            )
            Redirect(controllers.google.mcc.account.routes.BudgetController.budgets())
          }
        )
    }
  }


  def deleteBudget(api_id: Long) = deadbolt.Dynamic(name = PermissionGroup.GoogleWrite.entryName, handler = handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        setPendingCache(
          Left(request),
          pendingCache(Left(request)) :+ PendingCacheStructure(
            id = pendingCache(Left(request)).length + 1,
            changeType = ChangeType.DELETE,
            trafficSource = TrafficSource.GOOGLE,
            changeCategory = ChangeCategory.BUDGET,
            changeData = DBObject("apiId" -> api_id)
          )
        )
        Redirect(controllers.google.mcc.account.routes.BudgetController.budgets())
    }
  }
}
