package controllers.google.mcc.account

import javax.inject.Inject

import be.objectify.deadbolt.scala.cache.HandlerCache
import be.objectify.deadbolt.scala.{ActionBuilders, DeadboltActions}
import com.google.api.ads.adwords.axis.v201509.mcm.Customer
import com.mongodb.casbah.Imports._
import models.mongodb.PermissionGroup
import models.mongodb.google.Google._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc._
import security.HandlerKeys

class AccountController @Inject()(val messagesApi: MessagesApi, deadbolt: DeadboltActions, handlers: HandlerCache, actionBuilder: ActionBuilders) extends Controller with I18nSupport {

  def json = Action {
    implicit request =>
      Ok(controllers.json(
        request,
        List(
          "mccObjId",
          "mccApiId",
          "customerObjId",
          "customerApiId",
          "tsecs"
        ),
        "customer",
        googleCustomerCollection
      ))
  }

  def accounts(page: Int, pageSize: Int, orderBy: Int, filter: String) = deadbolt.Dynamic(name = PermissionGroup.GoogleRead.entryName, handler = handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        Ok(
          views.html.google.mcc.account.accounts(
            googleCustomerCollection.find(
              DBObject(),
              DBObject("customer" -> DBObject("$slice" -> -1))
            ).skip(page * pageSize).limit(pageSize).toList.map(
              dboToGoogleEntity[Customer](_, "customer", None)
            ),
            page,
            pageSize,
            orderBy,
            filter,
            googleCustomerCollection.count()
          )
        )
    }
  }
}
