package controllers.google.mcc.account.campaign

import javax.inject.Inject

import Shared.Shared._
import be.objectify.deadbolt.scala.cache.HandlerCache
import be.objectify.deadbolt.scala.{ActionBuilders, DeadboltActions}
import com.google.api.ads.adwords.axis.v201509.cm.{Budget, Campaign}
import com.mongodb.casbah.Imports._
import helpers.google.mcc.account.BudgetControllerHelper._
import helpers.google.mcc.account.campaign.CampaignControllerHelper._
import models.mongodb._
import models.mongodb.google.Google._
import play.api.Play.current
import play.api.cache.Cache
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc._
import security.HandlerKeys

import scala.collection.immutable.List
import scala.collection.mutable.ListBuffer

class CampaignController @Inject()(val messagesApi: MessagesApi, deadbolt: DeadboltActions, handlers: HandlerCache, actionBuilder: ActionBuilders) extends Controller with I18nSupport {
  def json = Action {
    implicit request =>
      Ok(controllers.json(
        request,
        List(
          "mccObjId",
          "customerObjId",
          "customerApiId",
          "campaignObjId",
          "campaignApiId"
        ),
        "campaign",
        googleCampaignCollection
      ))
  }

  def campaigns(page: Int, pageSize: Int, orderBy: Int, filter: String) = deadbolt.Dynamic(name = PermissionGroup.GoogleRead.entryName, handler = handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        Ok(views.html.google.mcc.account.campaign.campaigns(
          googleCampaignCollection.find(
            DBObject(),
            DBObject("campaign" -> DBObject("$slice" -> -1))
          ).skip(page * pageSize).limit(pageSize).toList.map(dboToGoogleEntity[Campaign](_, "campaign", None)),
          page,
          pageSize,
          orderBy,
          filter,
          googleCampaignCollection.count(),
          pendingCache(Left(request))
            .filter(
              x =>
                x.trafficSource == TrafficSource.GOOGLE && x.changeCategory == ChangeCategory.CAMPAIGN
            )
        ))
    }
  }

  def newCampaign = deadbolt.Dynamic(name = PermissionGroup.GoogleWrite.entryName, handler = handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        val pending_cache_key = request.session.get(Security.username).get + cache_ext
        Ok(views.html.google.mcc.account.campaign.new_campaign(
          campaignForm,
          googleBudgetCollection.find(
            DBObject(),
            DBObject("budget" -> DBObject("$slice" -> -1))
          ).toList.map(dboToGoogleEntity[Budget](_, "budget", None)),
          Cache.get(pending_cache_key)
            .getOrElse(List()).asInstanceOf[List[PendingCacheStructure]]
            .filter(x => x.changeType == ChangeType.NEW && x.trafficSource == TrafficSource.GOOGLE && x.changeCategory == ChangeCategory.BUDGET)
            .map(x => dboToBudgetForm(x.changeData.asDBObject)),
          List()
        ))
    }
  }

  def createCampaign = deadbolt.Dynamic(name = PermissionGroup.GoogleWrite.entryName, handler = handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        campaignForm.bindFromRequest.fold(
          formWithErrors => {
            BadRequest(views.html.google.mcc.account.campaign.new_campaign(
              formWithErrors,
              googleBudgetCollection.find(
                DBObject(),
                DBObject("budget" -> DBObject("$slice" -> -1))
              ).toList.map(dboToGoogleEntity[Budget](_, "budget", None)),
              pendingCache(Left(request))
                .filter(x => x.changeType == ChangeType.NEW && x.trafficSource == TrafficSource.GOOGLE && x.changeCategory == ChangeCategory.BUDGET)
                .map(x => dboToBudgetForm(x.changeData.asDBObject)),
              List()
            ))
          },
          campaign => {
            setPendingCache(
              Left(request),
              pendingCache(Left(request)) :+ PendingCacheStructure(
                id = pendingCache(Left(request)).length + 1,
                changeType = ChangeType.NEW,
                trafficSource = TrafficSource.GOOGLE,
                changeCategory = ChangeCategory.CAMPAIGN,
                changeData = campaignFormToDbo(campaign)
              )
            )
            Redirect(controllers.google.mcc.account.campaign.routes.CampaignController.campaigns())
          }
        )
    }
  }

  def bulkNewCampaign = deadbolt.Dynamic(name = PermissionGroup.GoogleWrite.entryName, handler = handlers(HandlerKeys.defaultHandler)) {
    Action(parse.multipartFormData) {
      implicit request =>
        var error_list = new ListBuffer[String]()
        request.body.file("bulk").foreach {
          bulk => {
            val field_names = Utilities.getCaseClassParameter[CampaignForm]
            val campaign_data_list = Utilities.bulkImport(bulk, field_names)
            for (((campaign_data, action), index) <- campaign_data_list.zipWithIndex) {
              campaignForm.bind(campaign_data.map(kv => (kv._1, kv._2)).toMap).fold(
                formWithErrors => {
                  error_list += "Row " + index.toString + ": " + formWithErrors.errorsAsJson.toString
                },
                campaign =>
                  setPendingCache(
                    Left(request),
                    pendingCache(Left(request)) :+ PendingCacheStructure(
                      id = pendingCache(Left(request)).length + 1,
                      changeType = ChangeType.withName(action.toUpperCase),
                      trafficSource = TrafficSource.GOOGLE,
                      changeCategory = ChangeCategory.CAMPAIGN,
                      changeData = campaignFormToDbo(campaign)
                    )
                  )
              )
            }
          }
        }
        if (error_list.nonEmpty) {
          BadRequest(views.html.google.mcc.account.campaign.new_campaign(
            campaignForm,
            googleCustomerCollection.find(
              DBObject(),
              DBObject("budget" -> DBObject("$slice" -> -1))
            ).toList.map(dboToGoogleEntity[Budget](_, "budget", None)),
            pendingCache(Left(request))
              .filter(x => x.changeType == ChangeType.NEW && x.trafficSource == TrafficSource.GOOGLE && x.changeCategory == ChangeCategory.BUDGET)
              .map(x => dboToBudgetForm(x.changeData.asDBObject)),
            error_list.toList
          ))
        } else {
          Redirect(controllers.google.mcc.account.campaign.routes.CampaignController.campaigns())
        }
    }
  }

  def editCampaign(api_id: Long) = deadbolt.Dynamic(name = PermissionGroup.GoogleWrite.entryName, handler = handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        googleCampaignCollection.findOne(
          DBObject("apiId" -> api_id),
          DBObject("campaign" -> DBObject("$slice" -> -1))
        ) match {
          case None => Ok(views.html.dashboard(List()))
          case Some(campaign_obj) =>
            val campaign = dboToGoogleEntity[Campaign](campaign_obj, "campaign", None)
            val budgets = googleBudgetCollection.find(
              DBObject("customerApiId" -> campaign_obj.getAsOrElse[Option[Long]]("customerApiId", None)),
              DBObject("budget" -> DBObject("$slice" -> -1))
            ).toList
            Ok(views.html.google.mcc.account.campaign.edit_campaign(
              campaign.getId,
              campaignForm.fill(
                CampaignForm(
                  CampaignParent(
                    mccObjId = campaign_obj.getAsOrElse[Option[String]]("mccObjId", None),
                    customerApiId = campaign_obj.getAsOrElse[Option[Long]]("customerApiId", None)
                  ),
                  apiId = Some(campaign.getId),
                  name = campaign.getName,
                  status = Some(campaign.getStatus.toString),
                  servingStatus = Some(campaign.getServingStatus.toString),
                  startDate = Some(campaign.getStartDate),
                  endDate = Some(campaign.getEndDate),
                  budgetApiId = Some(campaign.getBudget.getBudgetId),
                  advertisingChannelType = Some(campaign.getAdvertisingChannelType.toString),
                  adServingOptimizationStatus = Some(campaign.getAdServingOptimizationStatus.toString),
                  frequencyCapImpressions = try {
                    Option(campaign.getFrequencyCap.getImpressions)
                  } catch {
                    case _ => None
                  },
                  frequencyCapTimeUnit = try {
                    Option(campaign.getFrequencyCap.getTimeUnit.toString)
                  } catch {
                    case _ => None
                  },
                  frequencyCapLevel = try {
                    Option(campaign.getFrequencyCap.getLevel.toString)
                  } catch {
                    case _ => None
                  },
                  targetGoogleSearch = Some(campaign.getNetworkSetting.getTargetGoogleSearch),
                  targetSearchNetwork = Some(campaign.getNetworkSetting.getTargetSearchNetwork),
                  targetContentNetwork = Some(campaign.getNetworkSetting.getTargetContentNetwork),
                  targetPartnerSearchNetwork = Some(campaign.getNetworkSetting.getTargetPartnerSearchNetwork)
                )
              ),
              budgets.map(dboToGoogleEntity[Budget](_, "budget", None))
            ))
        }
    }
  }


  def saveCampaign(api_id: Long) = deadbolt.Dynamic(name = PermissionGroup.GoogleWrite.entryName, handler = handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        campaignForm.bindFromRequest.fold(
          formWithErrors => {
            BadRequest(views.html.google.mcc.account.campaign.edit_campaign(
              api_id,
              formWithErrors,
              googleBudgetCollection.find(
                DBObject(),
                DBObject("budget" -> DBObject("$slice" -> -1))
              ).toList.map(dboToGoogleEntity[Budget](_, "budget", None))
            ))
          },
          campaign => {
            setPendingCache(
              Left(request),
              pendingCache(Left(request)) :+ PendingCacheStructure(
                id = pendingCache(Left(request)).length + 1,
                changeType = ChangeType.UPDATE,
                trafficSource = TrafficSource.GOOGLE,
                changeCategory = ChangeCategory.CAMPAIGN,
                changeData = campaignFormToDbo(campaign)
              )
            )
            Redirect(controllers.google.mcc.account.campaign.routes.CampaignController.campaigns())
          }
        )
    }
  }

  def deleteCampaign(api_id: Long) = deadbolt.Dynamic(name = PermissionGroup.GoogleWrite.entryName, handler = handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        setPendingCache(
          Left(request),
          pendingCache(Left(request)) :+ PendingCacheStructure(
            id = pendingCache(Left(request)).length + 1,
            changeType = ChangeType.DELETE,
            trafficSource = TrafficSource.GOOGLE,
            changeCategory = ChangeCategory.CAMPAIGN,
            changeData = DBObject("apiId" -> api_id)
          )
        )
        Redirect(controllers.google.mcc.account.campaign.routes.CampaignController.campaigns())
    }
  }
}
