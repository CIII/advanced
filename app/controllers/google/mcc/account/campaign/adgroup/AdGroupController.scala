package controllers.google.mcc.account.campaign.adgroup

import javax.inject.Inject

import Shared.Shared._
import be.objectify.deadbolt.scala.cache.HandlerCache
import be.objectify.deadbolt.scala.{ActionBuilders, DeadboltActions}
import com.google.api.ads.adwords.axis.v201509.cm.{AdGroup, Campaign}
import com.mongodb.casbah.Imports._
import helpers.google.mcc.account.campaign.CampaignControllerHelper._
import helpers.google.mcc.account.campaign.adgroup.AdGroupControllerHelper._
import models.mongodb._
import models.mongodb.google.Google._
import play.api.Play.current
import play.api.cache.Cache
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc._
import security.HandlerKeys

import scala.collection.immutable.List
import scala.collection.mutable.ListBuffer

class AdGroupController @Inject()(val messagesApi: MessagesApi, deadbolt: DeadboltActions, handlers: HandlerCache, actionBuilder: ActionBuilders) extends Controller with I18nSupport {
  def json = Action {
    implicit request =>
      Ok(controllers.json(
        request,
        List(
          "mccObjId",
          "customerObjId",
          "customerApiId",
          "campaignObjId",
          "campaignApiId",
          "adGroupObjId",
          "adGroupApiId"
        ),
        "adGroup",
        googleAdGroupCollection
      ))
  }

  def adGroups(page: Int, pageSize: Int, orderBy: Int, filter: String) = deadbolt.Dynamic(name = PermissionGroup.GoogleRead.entryName, handler = handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        Ok(views.html.google.mcc.account.campaign.adgroup.ad_groups(
          googleAdGroupCollection.find(
            DBObject(),
            DBObject("adGroup" -> DBObject("$slice" -> -1))
          ).skip(page * pageSize).limit(pageSize).toList.map(dboToGoogleEntity[AdGroup](_, "adGroup", None)),
          page,
          pageSize,
          orderBy,
          filter,
          googleAdGroupCollection.count(),
          pendingCache(Left(request))
            .filter(
              x =>
                x.trafficSource == TrafficSource.GOOGLE
                  && x.changeCategory == ChangeCategory.AD_GROUP
            )
        ))
    }
  }

  def newAdGroup = deadbolt.Dynamic(name = PermissionGroup.GoogleWrite.entryName, handler = handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        Ok(views.html.google.mcc.account.campaign.adgroup.new_ad_group(
          adGroupForm,
          googleCampaignCollection.find(
            DBObject(),
            DBObject("campaign" -> DBObject("$slice" -> -1))
          ).toList.map(dboToGoogleEntity[Campaign](_, "campaign", None)),
          pendingCache(Left(request))
            .filter(
              x =>
                x.trafficSource == TrafficSource.GOOGLE
                  && x.changeCategory == ChangeCategory.CAMPAIGN
                  && x.changeType == ChangeType.NEW
            )
            .map(x => dboToCampaignForm(x.changeData.asDBObject)),
          List()
        ))
    }
  }

  def createAdGroup = deadbolt.Dynamic(name = PermissionGroup.GoogleWrite.entryName, handler = handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        val current_cache = Cache.get(pendingCacheKey(Left(request))).getOrElse(List()).asInstanceOf[List[PendingCacheStructure]]
        adGroupForm.bindFromRequest.fold(
          formWithErrors => {
            BadRequest(
              views.html.google.mcc.account.campaign.adgroup.new_ad_group(
                formWithErrors,
                googleCampaignCollection.find(
                  DBObject(),
                  DBObject("campaign" -> DBObject("$slice" -> -1))
                ).toList.map(dboToGoogleEntity[Campaign](_, "campaign", None)),
                pendingCache(Left(request))
                  .filter(x =>
                    x.trafficSource == TrafficSource.GOOGLE
                      && x.changeCategory == ChangeCategory.CAMPAIGN
                      && x.changeType == ChangeType.NEW
                  )
                  .map(x => dboToCampaignForm(x.changeData.asDBObject)),
                List()
              )
            )
          },
          ad_group => {
            Cache.set(
              pendingCacheKey(Left(request)),
              current_cache :+ PendingCacheStructure(
                id = current_cache.length + 1,
                changeType = ChangeType.NEW,
                trafficSource = TrafficSource.GOOGLE,
                changeCategory = ChangeCategory.AD_GROUP,
                changeData = adGroupFormToDbo(ad_group)
              )
            )
            Redirect(controllers.google.mcc.account.campaign.adgroup.routes.AdGroupController.adGroups())
          }
        )
    }
  }


  def bulkNewAdGroup = deadbolt.Dynamic(name = PermissionGroup.GoogleWrite.entryName, handler = handlers(HandlerKeys.defaultHandler)) {
    Action(parse.multipartFormData) {
      implicit request => {
        var error_list = new ListBuffer[String]()
        request.body.file("bulk").foreach {
          bulk => {
            val field_names = Utilities.getCaseClassParameter[AdGroup]
            val ad_group_data_list = Utilities.bulkImport(bulk, field_names)
            for (((ad_group_data, action), index) <- ad_group_data_list.zipWithIndex) {
              adGroupForm.bind(ad_group_data.map(kv => (kv._1, kv._2)).toMap).fold(
                formWithErrors => {
                  error_list += "Row " + index.toString + ": " + formWithErrors.errorsAsJson.toString
                },
                ad_group => {
                  setPendingCache(
                    Left(request),
                    pendingCache(Left(request)) :+ PendingCacheStructure(
                      id = pendingCache(Left(request)).length + 1,
                      changeType = ChangeType.withName(action),
                      trafficSource = TrafficSource.GOOGLE,
                      changeCategory = ChangeCategory.AD_GROUP,
                      changeData = adGroupFormToDbo(ad_group)
                    )
                  )
                }
              )
            }
          }
        }
        if (error_list.nonEmpty) {
          BadRequest(views.html.google.mcc.account.campaign.adgroup.new_ad_group(
            adGroupForm,
            googleCampaignCollection.find(
              DBObject(),
              DBObject("campaign" -> DBObject("$slice" -> -1))
            ).toList.map(dboToGoogleEntity[Campaign](_, "campaign", None)),
            pendingCache(Left(request))
              .filter(x =>
                x.trafficSource == TrafficSource.GOOGLE
                  && x.changeCategory == ChangeCategory.AD_GROUP
                  && x.changeType == ChangeType.NEW
              )
              .map(x => dboToCampaignForm(x.changeData.asDBObject)),
            error_list.toList
          ))
        } else {
          Redirect(controllers.google.mcc.account.campaign.adgroup.routes.AdGroupController.adGroups())
        }
      }
    }
  }

  def editAdGroup(api_id: Long) = deadbolt.Dynamic(name = PermissionGroup.GoogleWrite.entryName, handler = handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        googleAdGroupCollection.findOne(DBObject("adGroupApiId" -> api_id)) match {
          case None => Ok(views.html.dashboard(dashboardChartData))
          case Some(ad_group_obj) =>
            val campaigns = googleCampaignCollection.find(
              DBObject(),
              DBObject("campaign" -> DBObject("$slice" -> -1))
            ).toList
            def ad_group = dboToGoogleEntity[AdGroup](ad_group_obj, "adGroup", None)
            Ok(views.html.google.mcc.account.campaign.adgroup.edit_ad_group(
              ad_group.getId,
              adGroupForm.fill(
                AdGroupForm(
                  AdGroupParent(
                    mccObjId = ad_group_obj.getAsOrElse[Option[String]]("mccObjId", None),
                    customerApiId = ad_group_obj.getAsOrElse[Option[Long]]("customerApiId", None),
                    campaignApiId = ad_group_obj.getAsOrElse[Option[Long]]("campaignApiId", None)
                  ),
                  apiId = Some(ad_group.getId),
                  name = ad_group.getName,
                  status = ad_group.getStatus.toString,
                  contentBidCriterionTypeGroup = Some(ad_group.getContentBidCriterionTypeGroup.toString)
                )),
              campaigns.map(dboToGoogleEntity[Campaign](_, "campaign", None)),
              pendingCache(Left(request)).filter(x =>
                x.trafficSource == TrafficSource.GOOGLE
                  && x.changeCategory == ChangeCategory.CAMPAIGN
                  && x.changeType == ChangeType.NEW
              ).map(x => dboToCampaignForm(x.changeData.asDBObject))
            ))
        }
    }
  }

  def saveAdGroup(api_id: Long) = deadbolt.Dynamic(name = PermissionGroup.GoogleWrite.entryName, handler = handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        adGroupForm.bindFromRequest.fold(formWithErrors => {
          BadRequest(
            views.html.google.mcc.account.campaign.adgroup.edit_ad_group(
              api_id,
              formWithErrors,
              googleCampaignCollection.find(
                DBObject(),
                DBObject("campaign" -> DBObject("$slice" -> -1))
              ).toList.map(dboToGoogleEntity[Campaign](_, "campaign", None)),
              pendingCache(Left(request))
                .filter(x =>
                  x.trafficSource == TrafficSource.GOOGLE
                    && x.changeCategory == ChangeCategory.CAMPAIGN
                    && x.changeType == ChangeType.NEW
                )
                .map(x => dboToCampaignForm(x.changeData.asDBObject))
            )
          )
        },
          ad_group => {
            setPendingCache(
              Left(request),
              pendingCache(Left(request)) :+ PendingCacheStructure(
                id = pendingCache(Left(request)).length + 1,
                changeType = ChangeType.UPDATE,
                trafficSource = TrafficSource.GOOGLE,
                changeCategory = ChangeCategory.AD_GROUP,
                changeData = adGroupFormToDbo(ad_group)
              )
            )
            Redirect(controllers.google.mcc.account.campaign.adgroup.routes.AdGroupController.adGroups())
          }
        )
    }
  }

  def deleteAdGroup(api_id: Long) = deadbolt.Dynamic(name = PermissionGroup.GoogleWrite.entryName, handler = handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        setPendingCache(
          Left(request),
          pendingCache(Left(request)) :+ PendingCacheStructure(
            id = pendingCache(Left(request)).length + 1,
            changeType = ChangeType.DELETE,
            trafficSource = TrafficSource.GOOGLE,
            changeCategory = ChangeCategory.AD_GROUP,
            changeData = DBObject("apiId" -> api_id)
          )
        )
        Redirect(controllers.google.mcc.account.campaign.adgroup.routes.AdGroupController.adGroups())
    }
  }
}
