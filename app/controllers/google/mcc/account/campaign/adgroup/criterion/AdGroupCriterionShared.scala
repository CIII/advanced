package controllers.google.mcc.account.campaign.adgroup.criterion

import com.mongodb.casbah.Imports._

object AdGroupCriterionShared {
  case class AdGroupCriterionParent(
    var mccObjId: Option[String],
    var customerApiId: Option[Long],
    var campaignApiId: Option[Long],
    var adGroupApiId: Option[Long]
  )

  def dboToAdGroupCriterionParent(dbo: DBObject) = AdGroupCriterionParent(
    mccObjId=dbo.getAsOrElse[Option[String]]("mccObjId", None),
    customerApiId=dbo.getAsOrElse[Option[Long]]("customerApiId", None),
    campaignApiId=dbo.getAsOrElse[Option[Long]]("campaignApiId", None),
    adGroupApiId=dbo.getAsOrElse[Option[Long]]("adGroupApiId", None)
  )

  def adGroupCriterionParentToDbo(agcp: AdGroupCriterionParent) = DBObject(
    "mccObjId" -> agcp.mccObjId.get,
    "customerApiId" -> agcp.customerApiId.get,
    "campaignApiId" -> agcp.campaignApiId.get,
    "adGroupApiId" -> agcp.adGroupApiId.get
  )

  case class CustomParameter(
    key: String,
    value: Option[String]
  )

  def dboToCustomParameter(dbo: DBObject) = CustomParameter(
    key=dbo.getAs[String]("key").get,
    value=dbo.getAs[String]("value")
  )

  def customParameterToDbo(cp: CustomParameter) = DBObject(
    "key" -> cp.key,
    "value" -> cp.value.get
  )

}
