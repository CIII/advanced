package controllers.google.mcc.account.campaign.adgroup.ad

import com.mongodb.casbah.Imports._
import models.mongodb.Utilities

object AdGroupAdShared {
  case class AdGroupAdParent (
    var mccObjId: Option[String],
    var customerApiId: Option[Long],
    var campaignApiId: Option[Long],
    var adGroupApiId: Option[Long]
  )

  abstract class AdGroupAdForm{
    var parent: AdGroupAdParent
    var apiId: Option[Long]
    var status: Option[String]
  }

  def dboToAdGroupAdParent(dbo: DBObject) = AdGroupAdParent(
    mccObjId=dbo.getAsOrElse[Option[String]]("mccObjId", None),
    customerApiId=dbo.getAsOrElse[Option[Long]]("customerApiId", None),
    campaignApiId=dbo.getAsOrElse[Option[Long]]("campaignApiId", None),
    adGroupApiId=dbo.getAsOrElse[Option[Long]]("adGroupApiId", None)
  )

  def adGroupAdParentToDbo(agap: AdGroupAdParent): DBObject = {
    var dbo = DBObject.newBuilder
    for((name, idx) <- Utilities.getCaseClassParameter[AdGroupAdParent].zipWithIndex) {
      dbo += (Utilities.getMethodName(name) -> agap.productElement(idx))
    }
    dbo.result
  }
}
