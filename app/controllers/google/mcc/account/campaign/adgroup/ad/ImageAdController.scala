package controllers.google.mcc.account.campaign.adgroup.ad

import javax.inject.Inject

import Shared.Shared._
import be.objectify.deadbolt.scala.cache.HandlerCache
import be.objectify.deadbolt.scala.{ActionBuilders, DeadboltActions}
import com.google.api.ads.adwords.axis.v201509.cm.{AdGroupAd, ImageAd}
import com.mongodb.casbah.Imports._
import helpers.google.mcc.account.campaign.adgroup.ad.ImageAdControllerHelper._
import models.mongodb._
import models.mongodb.google.Google._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc._
import security.HandlerKeys

import scala.collection.immutable.List
import scala.collection.mutable.ListBuffer

class ImageAdController @Inject()(
  val messagesApi: MessagesApi,
  deadbolt: DeadboltActions,
  handlers: HandlerCache,
  actionBuilder: ActionBuilders
) extends Controller with I18nSupport {

  def json = Action {
    implicit request =>
      Ok(controllers.json(
        request,
        List(
          "mccObjId",
          "customerObjId",
          "customerApiId",
          "campaignObjId",
          "campaignApiId",
          "adGroupObjId",
          "adGroupApiId",
          "adObjId",
          "adApiId"
        ),
        "ad",
        googleAdCollection,
        Some("adType" -> "ImageAd")
      ))
  }

  def imageAds(page: Int, pageSize: Int, orderBy: Int, filter: String) = deadbolt.Dynamic(name = PermissionGroup.GoogleRead.entryName, handler = handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        Ok(views.html.google.mcc.account.campaign.adgroup.ad.image.image_ads(
          googleAdCollection.find(
            DBObject("adType" -> "ImageAd"),
            DBObject("ad" -> DBObject("$slice" -> -1))
          ).skip(page * pageSize).limit(pageSize).toList.map(y =>
            gson.fromJson(
              com.mongodb.util.JSON.serialize(y.as[MongoDBList]("ad").head.asInstanceOf[DBObject].as[DBObject]("object")),
              classOf[AdGroupAd]
            )
          ),
          page,
          pageSize,
          orderBy,
          filter,
          googleAdCollection.count(DBObject("adType" -> "ImageAd")),
          pendingCache(Left(request))
            .filter(x =>
              x.trafficSource == TrafficSource.GOOGLE
                && x.changeCategory == ChangeCategory.IMAGE_AD
            )
        ))
    }
  }

  def newImageAd = deadbolt.Dynamic(name = PermissionGroup.GoogleWrite.entryName, handler = handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        Ok(views.html.google.mcc.account.campaign.adgroup.ad.image.new_image_ad(
          imageAdForm,
          List()
        ))
    }
  }

  def editImageAd(api_id: Long) = deadbolt.Dynamic(name = PermissionGroup.GoogleWrite.entryName, handler = handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        googleAdCollection.findOne(DBObject("adApiId" -> api_id)) match {
          case Some(image_ad_obj) =>
            val image_ad = dboToGoogleEntity[AdGroupAd](image_ad_obj, "ad", None)
            Ok(views.html.google.mcc.account.campaign.adgroup.ad.image.edit_image_ad(
              image_ad.getAd.getId,
              imageAdForm.fill(
                ImageAdForm(
                  controllers.Google.AdGroupAdParent(
                    mccObjId = image_ad_obj.getAsOrElse[Option[String]]("mccObjId", None),
                    customerApiId = image_ad_obj.getAsOrElse[Option[Long]]("customerApiId", None),
                    campaignApiId = image_ad_obj.getAsOrElse[Option[Long]]("campaignApiId", None),
                    adGroupApiId = image_ad_obj.getAsOrElse[Option[Long]]("adGroupApiId", None)
                  ),
                  apiId = Some(image_ad.getAd.getId),
                  url = Some(image_ad.getAd.getDisplayUrl),
                  displayUrl = Some(image_ad.getAd.getDisplayUrl),
                  devicePreference = Some(image_ad.getAd.getDevicePreference),
                  imageName = image_ad.getAd.asInstanceOf[ImageAd].getName,
                  imageFileSize = Some(image_ad.getAd.asInstanceOf[ImageAd].getImage.getFileSize),
                  imageData = Some(image_ad.getAd.asInstanceOf[ImageAd].getImage.getData),
                  status = Some(image_ad.getStatus.toString),
                  approvalStatus = Some(image_ad.getApprovalStatus.toString),
                  disapprovalReasons = Some(image_ad.getDisapprovalReasons.toList),
                  trademarkDisapproved = Some(image_ad.getTrademarkDisapproved)
                )
              )
            ))
          case None =>
            BadRequest("NOT FOUND")
        }
    }
  }


  def image(byteString: String) = deadbolt.Dynamic(name = PermissionGroup.GoogleWrite.entryName, handler = handlers(HandlerKeys.defaultHandler)) {
    Action {
      val MimeType = "image/png"
      try {
        val imageData: Array[Byte] = byteString.getBytes("UTF-8")
        Ok(imageData).as(MimeType)
      }
      catch {
        case e: IllegalArgumentException =>
          BadRequest("Could not generate image. Error: " + e.toString)
      }
    }
  }


  def createImageAd = deadbolt.Dynamic(name = PermissionGroup.GoogleWrite.entryName, handler = handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        imageAdForm.bindFromRequest.fold(
          formWithErrors => {
            BadRequest(
              views.html.google.mcc.account.campaign.adgroup.ad.image.new_image_ad(
                formWithErrors,
                List()
              )
            )
          },
          image_ad => {
            setPendingCache(
              Left(request),
              pendingCache(Left(request)) :+ PendingCacheStructure(
                id = pendingCache(Left(request)).length + 1,
                changeType = ChangeType.NEW,
                trafficSource = TrafficSource.GOOGLE,
                changeCategory = ChangeCategory.IMAGE_AD,
                changeData = imageAdFormToDbo(image_ad)
              )
            )
            Redirect(controllers.google.mcc.account.campaign.adgroup.ad.routes.ImageAdController.imageAds())
          }
        )
    }
  }


  def bulkNewImageAd = deadbolt.Dynamic(name = PermissionGroup.GoogleWrite.entryName, handler = handlers(HandlerKeys.defaultHandler)) {
    Action(parse.multipartFormData) {
      implicit request =>
        var error_list = new ListBuffer[String]()
        request.body.file("bulk").foreach {
          bulk => {
            val field_names = Utilities.getCaseClassParameter[ImageAdForm]
            val image_ad_data_list = Utilities.bulkImport(bulk, field_names)
            for (((image_ad_data, action), index) <- image_ad_data_list.zipWithIndex) {
              imageAdForm.bind(image_ad_data.map(kv => (kv._1, kv._2)).toMap).fold(
                formWithErrors => {
                  error_list += "Row " + index.toString + ": " + formWithErrors.errorsAsJson.toString
                },
                image_ad =>
                  setPendingCache(
                    Left(request),
                    pendingCache(Left(request)) :+ PendingCacheStructure(
                      id = pendingCache(Left(request)).length + 1,
                      changeType = ChangeType.withName(action.toUpperCase),
                      trafficSource = TrafficSource.GOOGLE,
                      changeCategory = ChangeCategory.IMAGE_AD,
                      changeData = imageAdFormToDbo(image_ad)
                    )
                  )
              )
            }
          }
        }
        if (error_list.nonEmpty) {
          BadRequest(views.html.google.mcc.account.campaign.adgroup.ad.image.new_image_ad(
            imageAdForm,
            error_list.toList
          ))
        } else {
          Redirect(controllers.google.mcc.account.campaign.adgroup.ad.routes.ImageAdController.imageAds())
        }
    }
  }


  def saveImageAd(api_id: Long) = deadbolt.Dynamic(name = PermissionGroup.GoogleWrite.entryName, handler = handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        imageAdForm.bindFromRequest.fold(
          formWithErrors => {
            BadRequest(
              views.html.google.mcc.account.campaign.adgroup.ad.image.edit_image_ad(
                api_id,
                formWithErrors
              )
            )
          },
          image_ad => {
            setPendingCache(
              Left(request),
              pendingCache(Left(request)) :+ PendingCacheStructure(
                id = pendingCache(Left(request)).length + 1,
                changeType = ChangeType.UPDATE,
                trafficSource = TrafficSource.GOOGLE,
                changeCategory = ChangeCategory.IMAGE_AD,
                changeData = imageAdFormToDbo(image_ad)
              )
            )
            Redirect(controllers.google.mcc.account.campaign.adgroup.ad.routes.ImageAdController.imageAds())
          }
        )
    }
  }


  def deleteImageAd(api_id: Long) = deadbolt.Dynamic(name = PermissionGroup.GoogleWrite.entryName, handler = handlers(HandlerKeys.defaultHandler)) {
    Action {
      implicit request =>
        setPendingCache(
          Left(request),
          pendingCache(Left(request)) :+ PendingCacheStructure(
            id = pendingCache(Left(request)).length + 1,
            changeType = ChangeType.DELETE,
            trafficSource = TrafficSource.GOOGLE,
            changeCategory = ChangeCategory.IMAGE_AD,
            changeData = DBObject("apiId" -> api_id)
          )
        )
        Redirect(controllers.google.mcc.account.campaign.adgroup.ad.routes.ImageAdController.imageAds())
    }
  }
}


