package controllers.google.mcc.account.campaign.criterion

import com.mongodb.casbah.Imports._

object CampaignCriterionShared {
  case class CampaignCriterionParent(
    mccObjId: Option[String],
    customerApiId: Option[Long],
    campaignApiId: Option[Long]
  )

  def campaignCriterionParentToDbo(p: CampaignCriterionParent): DBObject = {
    DBObject(
      "mccObjId" -> p.mccObjId,
      "customerApiId" -> p.customerApiId,
      "campaignApiId" -> p.campaignApiId
    )
  }

  def dboToCampaignCriterionParent(dbo: DBObject): CampaignCriterionParent = {
    CampaignCriterionParent(
      mccObjId = dbo.getAsOrElse[Option[String]]("mccObjId", None),
      customerApiId = dbo.getAsOrElse[Option[Long]]("customerApiId", None),
      campaignApiId = dbo.getAsOrElse[Option[Long]]("campaignApiId", None)
    )
  }
}
