package controllers.google.reporting

import javax.inject.Inject

import be.objectify.deadbolt.scala.cache.HandlerCache
import be.objectify.deadbolt.scala.{ActionBuilders, DeadboltActions}
import com.google.api.ads.adwords.lib.jaxb.v201509.ReportDefinitionReportType
import com.mongodb.casbah.Imports._
import models.mongodb.google.Google._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.libs.json.{Json, JsString, JsObject}
import play.api.mvc.{Action, Controller}
import helpers.google.reporting.ReportingControllerHelper._
import Shared.Shared._

class ReportingController @Inject()(val messagesApi: MessagesApi, deadbolt: DeadboltActions, handlers: HandlerCache, actionBuilder: ActionBuilders) extends Controller with I18nSupport {
  def json(
    reportType: String,
    campaignId: String,
    adGroupId: String,
    adId: String,
    keywordId: String,
    startDate: String,
    endDate: String
  ) = Action {
    implicit request =>
      Ok(
        com.mongodb.util.JSON.serialize(
          MongoDBList(
            googleReportCollection(ReportDefinitionReportType.fromValue(reportType.toUpperCase)).find(buildQry(campaignId, adGroupId, adId, keywordId, startDate, endDate)).toList: _*
          )
        )
      )
  }

  def csv(
    reportType: String,
    campaignId: String,
    adGroupId: String,
    adId: String,
    keywordId: String,
    startDate: String,
    endDate: String
  ) = Action {
    implicit request =>
      Ok(
        mongoToCsv(
          googleReportCollection(ReportDefinitionReportType.fromValue(reportType)).find(buildQry(campaignId, adGroupId, adId, keywordId, startDate, endDate))
            .toList
        )
      )
  }
}