package controllers.google.account

import Shared.Shared._
import com.google.api.ads.adwords.axis.v201409.cm.Budget
import models.mongodb._
import play.api.Play.current
import play.api.cache.Cache
import play.api.data.Forms._
import play.api.data._
import play.api.libs.json.{JsObject, Json}
import play.api.mvc._
import play.modules.reactivemongo.MongoController
import models.mongodb.google.collections._
import reactivemongo.core.commands.Count
import scala.collection.mutable.ListBuffer
import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

object BudgetController extends Controller with MongoController {

  def budgets(page: Int, pageSize: Int, orderBy: Int, filter: String) = Action.async {
    implicit request =>
      budgetCollection.genericQueryBuilder.cursor[JsObject].collect[List]().map ( budgets =>
        Ok(views.html.google.budget.budgets(
          budgets.map(y => gson.fromJson(Json.stringify(y), classOf[Budget])),
          page,
          pageSize,
          orderBy,
          filter,
          Await.result(db.command(Count(budgetCollection.name, None)), 30 seconds),
          Cache.get(pendingCacheKey(request))
            .getOrElse(List())
            .asInstanceOf[List[pending_cache_structure]]
            .filter(x =>
              x.traffic_source == traffic_source.GOOGLE && x.change_category == change_category.BUDGET
            )
        ))
      ).recover{
        case e =>
          BadRequest("Error -> %s".format(e.toString))
      }
  }

  def newBudget = Action {
    implicit request =>
      Ok(views.html.google.budget.new_budget(
        budgetForm,
        List()
      ))
  }

  def editBudget(api_id: Long) = Action.async {
    implicit request =>
      budgetCollection.find(Json.obj("budgetId" -> api_id)).one[JsObject].map {
        case Some(budgetObj) =>
          def budget = gson.fromJson(budgetObj.toString(), classOf[Budget])
            Ok(views.html.google.budget.edit_budget(
              api_id,
              budgetForm.fill(
                BudgetForm(
                  api_id = Some(budget.getBudgetId),
                  name = budget.getName,
                  period_amount = Some(budget.getAmount.getMicroAmount),
                  delivery_method = Some(budget.getDeliveryMethod.toString),
                  is_explicitly_shared = Some(budget.getIsExplicitlyShared),
                  status = Some(budget.getStatus.toString)
              )
            )
            ))
        case None =>
          Redirect(routes.BudgetController.budgets())
      }.recover {
        case e =>
          BadRequest("Error -> %s".format(e.toString))
      }
  }


  def createBudget = Action {
    implicit request =>
      val current_cache = Cache.get(pendingCacheKey(request)).getOrElse(List()).asInstanceOf[List[pending_cache_structure]]
      budgetForm.bindFromRequest.fold(
        formWithErrors => {
          BadRequest(
            views.html.google.budget.new_budget(
              formWithErrors,
              List()
            )
          )
        },
        budget => {
          Cache.set(
            pendingCacheKey(request),
            current_cache :+ pending_cache_structure(
              id=current_cache.length+1,
              change_type=change_type.NEW,
              traffic_source=traffic_source.GOOGLE,
              change_category=change_category.BUDGET,
              change_data=budget
            )
          )
          Redirect(routes.BudgetController.budgets())
        }
      )
  }


  def bulkNewBudget = Action(parse.multipartFormData) {
    implicit request => {
      var error_list = new ListBuffer[String]()
      request.body.file("bulk").map {
        bulk => {
          val field_names = Utilities.getCaseClassParameter[Budget]
          val budget_data_list = Utilities.bulkImport(bulk, field_names)
          for (((budget_data, action), index) <- budget_data_list.zipWithIndex) {
            budgetForm.bind(budget_data.map(kv => (kv._1, kv._2)).toMap).fold(
              formWithErrors => {
                error_list += "Row " + index.toString + ": " + formWithErrors.errorsAsJson.toString
              },
              budget => {
                setPendingCache(
                  request,
                  pendingCache(request) :+ pending_cache_structure(
                    id=pendingCache(request).length+1,
                    change_type=change_type.withName(action.toUpperCase),
                    traffic_source=traffic_source.GOOGLE,
                    change_category=change_category.BUDGET,
                    change_data=budget
                  )
                )
              }
            )
          }
        }
      }
      if (error_list.size > 0) {
        BadRequest(views.html.google.budget.new_budget(
          budgetForm,
          error_list.toList
        ))
      } else {
        Redirect(routes.BudgetController.budgets())
      }
    }
  }


  def saveBudget(api_id: Long) = Action {
    implicit request =>
      budgetForm.bindFromRequest.fold(
        formWithErrors => {
          BadRequest(
            views.html.google.budget.edit_budget(
              api_id,
              formWithErrors
            )
          )
        },
        budget => {
          setPendingCache(
            request,
            pendingCache(request) :+ pending_cache_structure(
              id=pendingCache(request).length+1,
              change_type=change_type.UPDATE,
              traffic_source=traffic_source.GOOGLE,
              change_category=change_category.BUDGET,
              change_data=budget
            )
          )
          Redirect(routes.BudgetController.budgets())
        }
      )
  }


  def deleteBudget(api_id: Long) = Action {
    implicit request =>
      setPendingCache(
        request,
        pendingCache(request) :+ pending_cache_structure(
          id=pendingCache(request).length+1,
          change_type=change_type.DELETE,
          traffic_source=traffic_source.GOOGLE,
          change_category=change_category.BUDGET,
          change_data=api_id
        )
      )
      Redirect(routes.BudgetController.budgets())
  }


  case class BudgetForm(
    var api_id: Option[Long],
    var name: String,
    var period_amount: Option[Long],
    var delivery_method: Option[String],
    var is_explicitly_shared: Option[Boolean],
    var status: Option[String]
  )

  def budgetForm: Form[BudgetForm] = Form(
    mapping(
      "api_id" -> optional(longNumber),
      "name" -> text,
      "period_amount" -> optional(longNumber),
      "delivery_method" -> optional(text),
      "is_explicitly_shared" -> optional(boolean),
      "status" -> optional(text)
    )(BudgetForm.apply)(BudgetForm.unapply)
  )
}
