package controllers.google.account

import Shared.Shared._
import com.google.api.ads.adwords.axis.v201409.mcm.ManagedCustomer
import models.mongodb.google.collections._
import play.api.libs.json.{JsObject, Json}
import play.api.mvc._
import play.modules.reactivemongo.MongoController
import reactivemongo.api.QueryOpts
import reactivemongo.core.commands.Count

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

object AccountController extends Controller with MongoController {

  def accounts(page: Int, pageSize: Int, orderBy: Int, filter: String) = Action.async {
    implicit request =>
      accountCollection.genericQueryBuilder
        .options(QueryOpts(skipN=page * pageSize))
        .cursor[JsObject].collect[List](pageSize)
        .map (accounts =>
          Ok(
            views.html.google.account.accounts(
              accounts.map(y =>
                gson.fromJson(
                  Json.stringify(y),
                  classOf[ManagedCustomer]
                )
              ),
              page,
              pageSize,
              orderBy,
              filter,
              Await.result(db.command(Count(accountCollection.name, None)), 30 seconds)
            )
          )
      ).recover {
        case e =>
          BadRequest("Error -> %s".format(e.toString))
      }
  }
}