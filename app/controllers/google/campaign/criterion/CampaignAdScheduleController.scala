package controllers.google.campaign.criterion

import Shared.Shared._
import com.google.api.ads.adwords.axis.v201409.cm.{AdSchedule, Campaign, CampaignCriterion}
import controllers.google.campaign.CampaignController._
import models.mongodb._
import play.api.Play.current
import play.api.cache._
import play.api.data.Forms._
import play.api.data._
import play.api.data.format.Formats._
import play.api.libs.json.{JsObject, Json}
import play.api.mvc._
import play.modules.reactivemongo.MongoController
import models.mongodb.google.collections._
import reactivemongo.api.QueryOpts
import reactivemongo.core.commands.Count
import scala.collection.immutable._
import scala.collection.mutable.ListBuffer
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

object CampaignAdScheduleController extends Controller with MongoController {

  def campaignAdSchedule(page: Int, pageSize: Int, orderBy: Int, filter: String) = Action.async {
    implicit request =>
      campaignCriterionCollection.find(Json.obj("criterion.criterionType" -> "AdSchedule"))
        .options(QueryOpts(skipN=page * pageSize))
        .cursor[JsObject]
        .collect[List](pageSize)
        .map { campaign_criterion =>
          Ok(views.html.google.campaign.criterion.ad_schedule.campaign_ad_schedule(
            campaign_criterion.map(y => gson.fromJson(Json.stringify(y), classOf[CampaignCriterion])),
            page,
            pageSize,
            orderBy,
            filter,
            Await.result(db.command(Count(campaignCriterionCollection.name, None)), 30 seconds),
            pendingCache(request)
              .filter(x =>
                x.traffic_source == traffic_source.GOOGLE
                && x.change_category == change_category.CAMPAIGN_AD_SCHEDULE
              )
          ))
      }.recover{
        case e =>
          BadRequest("Error -> %s".format(e.toString))
      }
  }

  def newCampaignAdSchedule = Action.async {
    implicit request =>
      campaignCollection.find(Json.obj()).cursor[JsObject].collect[List]().map { campaign =>
        Ok(views.html.google.campaign.criterion.ad_schedule.new_campaign_ad_schedule(
          campaignAdScheduleForm,
          campaign.map(y => gson.fromJson(Json.stringify(y), classOf[Campaign])),
          pendingCache(request)
            .filter(x =>
              x.change_type == change_type.NEW
              && x.traffic_source == traffic_source.GOOGLE
              && x.change_category == change_category.CAMPAIGN
            )
            .map(
              _.change_data.asInstanceOf[controllers.google.campaign.CampaignController.CampaignForm]
            ),
          List()
        ))
      }.recover{
        case e =>
          BadRequest("Error -> %s".format(e.toString))
      }
  }

  def createCampaignAdSchedule = Action.async {
    implicit request =>
      campaignCollection.find(Json.obj()).cursor[JsObject].collect[List]().map { campaign =>
        campaignAdScheduleForm.bindFromRequest.fold(
          formWithErrors => {
            BadRequest(views.html.google.campaign.criterion.ad_schedule.new_campaign_ad_schedule(
              formWithErrors,
              campaign.map(y => gson.fromJson(Json.stringify(y), classOf[Campaign])),
              pendingCache(request)
                .filter(x =>
                  x.change_type == change_type.NEW
                  && x.traffic_source == traffic_source.GOOGLE
                  && x.change_category == change_category.CAMPAIGN
                )
                .map(_.change_data.asInstanceOf[controllers.google.campaign.CampaignController.CampaignForm]),
              List()
            ))
          },
          campaign_ad_schedule => {
            setPendingCache(
              request,
              pendingCache(request) :+ pending_cache_structure(
                id=pendingCache(request).length+1,
                change_type=change_type.NEW,
                traffic_source=traffic_source.GOOGLE,
                change_category=change_category.CAMPAIGN_AD_SCHEDULE,
                change_data=campaign_ad_schedule
              )
            )
            Redirect(
              controllers.google.campaign.criterion.routes.CampaignAdScheduleController.campaignAdSchedule()
            )
          }
        )
      }.recover{
        case e =>
          BadRequest("Error -> %s".format(e.toString))
      }
  }

  def bulkNewCampaignAdSchedule = Action.async(parse.multipartFormData) {
    implicit request =>
      var error_list = new ListBuffer[String]()
      campaignCollection.find(Json.obj()).cursor[JsObject].collect[List]().map { campaign =>
        request.body.file("bulk").map {
          bulk => {
            val field_names = Utilities.getCaseClassParameter[CampaignAdScheduleForm]
            val ad_schedule_data_list = Utilities.bulkImport(bulk, field_names)
            for (((ad_schedule_data, action), index) <- ad_schedule_data_list.zipWithIndex) {
              campaignAdScheduleForm.bind(ad_schedule_data.map(kv => (kv._1, kv._2)).toMap).fold(
                formWithErrors => {
                  error_list += "Row " + index.toString + ": " + formWithErrors.errorsAsJson.toString
                },
                campaign_ad_schedule =>
                  setPendingCache(
                    request,
                    pendingCache(request) :+ pending_cache_structure(
                      id=pendingCache(request).length+1,
                      change_type=change_type.withName(action.toUpperCase),
                      traffic_source=traffic_source.GOOGLE,
                      change_category=change_category.CAMPAIGN_KEYWORD,
                      change_data=campaign_ad_schedule
                    )
                  )
              )
            }
          }
        }
        if (error_list.size > 0) {
          BadRequest(views.html.google.campaign.criterion.ad_schedule.new_campaign_ad_schedule(
            campaignAdScheduleForm,
            campaign.map(y => gson.fromJson(Json.stringify(y), classOf[Campaign])),
            pendingCache(request)
              .filter(x =>
                x.change_type == change_type.NEW
                && x.traffic_source == traffic_source.GOOGLE
                && x.change_category == change_category.CAMPAIGN
              )
              .map(_.change_data.asInstanceOf[controllers.google.campaign.CampaignController.CampaignForm]),
            error_list.toList
          ))
        } else {
          Redirect(
            controllers.google.campaign.criterion.routes.CampaignAdScheduleController.campaignAdSchedule()
          )
        }
      }.recover{
        case e =>
          BadRequest("Error -> %s".format(e.toString))
      }
  }

  def editCampaignAdSchedule(api_id: Long) = Action.async {
    implicit request =>
      campaignCriterionCollection.find(Json.obj("criterion.properties.id" -> api_id)).one[JsObject].flatMap {
        case None => Future{Ok(views.html.dashboard())}
        case Some(campaign_criterion_obj) =>
          campaignCollection.find(Json.obj()).cursor[JsObject].collect[List]().map { campaigns =>
            def campaign_ad_schedule = gson.fromJson(Json.stringify(campaign_criterion_obj), classOf[CampaignCriterion])
            Ok(views.html.google.campaign.criterion.ad_schedule.edit_campaign_ad_schedule(
              api_id,
              campaignAdScheduleForm.fill(
                CampaignAdScheduleForm(
                  api_id = Some(campaign_ad_schedule.getCriterion.getId),
                  campaign_api_id = campaign_ad_schedule.getCampaignId,
                  is_negative = Some(campaign_ad_schedule.getIsNegative),
                  day_of_week = campaign_ad_schedule.getCriterion.asInstanceOf[AdSchedule].getDayOfWeek.toString,
                  start_hour = campaign_ad_schedule.getCriterion.asInstanceOf[AdSchedule].getStartHour,
                  start_minute = campaign_ad_schedule.getCriterion.asInstanceOf[AdSchedule].getStartMinute.toString,
                  end_hour = campaign_ad_schedule.getCriterion.asInstanceOf[AdSchedule].getEndHour,
                  end_minute = campaign_ad_schedule.getCriterion.asInstanceOf[AdSchedule].getEndMinute.toString,
                  bid_modifier = campaign_ad_schedule.getBidModifier
                )),
              campaigns.map(y => gson.fromJson(Json.stringify(y), classOf[Campaign])),
              pendingCache(request)
                .filter(x =>
                  x.change_type == change_type.NEW
                  && x.traffic_source == traffic_source.GOOGLE
                  && x.change_category == change_category.CAMPAIGN
                )
                .map(_.change_data.asInstanceOf[CampaignForm])
            ))
          }.recover{
            case e =>
              BadRequest("Error -> %s".format(e.toString))
          }
      }.recover {
        case e =>
          BadRequest("Error -> %s".format(e.toString))
      }
    }

  def saveCampaignAdSchedule(api_id: Long) = Action.async {
    implicit request =>
      campaignCollection.find(Json.obj()).cursor[JsObject].collect[List]().map { campaigns =>
        campaignAdScheduleForm.bindFromRequest.fold(
          formWithErrors => BadRequest(views.html.google.campaign.criterion.ad_schedule.edit_campaign_ad_schedule(
            api_id,
            formWithErrors,
            campaigns.map(y => gson.fromJson(Json.stringify(y), classOf[Campaign])),
            Cache.get(pendingCacheKey(request))
              .getOrElse(List()).asInstanceOf[List[pending_cache_structure]]
              .filter(x =>
                x.change_type == change_type.NEW
                && x.traffic_source == traffic_source.GOOGLE
                && x.change_category == change_category.CAMPAIGN
              )
              .map(_.change_data.asInstanceOf[controllers.google.campaign.CampaignController.CampaignForm])
          )),
          campaign_ad_schedule => {
            setPendingCache(
              request,
              pendingCache(request) :+ pending_cache_structure(
                id=pendingCache(request).length+1,
                change_type=change_type.UPDATE,
                traffic_source=traffic_source.GOOGLE,
                change_category=change_category.CAMPAIGN_AD_SCHEDULE,
                change_data=campaign_ad_schedule
              )
            )
            Redirect(controllers.google.campaign.criterion.routes.CampaignAdScheduleController.campaignAdSchedule())
          }
        )
      }
  }

  def deleteCampaignAdSchedule(api_id: Long) = Action {
    implicit request =>
      setPendingCache(
        request,
        pendingCache(request) :+ pending_cache_structure(
          id=pendingCache(request).length+1,
          change_type=change_type.DELETE,
          traffic_source=traffic_source.GOOGLE,
          change_category=change_category.CAMPAIGN_AD_SCHEDULE,
          change_data=api_id
        )
      )
      Redirect(controllers.google.campaign.criterion.routes.CampaignAdScheduleController.campaignAdSchedule())
  }


  case class CampaignAdScheduleForm(
    var api_id: Option[Long],
    var campaign_api_id: Long,
    var is_negative: Option[Boolean],
    var day_of_week: String,
    var start_hour: Int,
    var start_minute: String,
    var end_hour: Int,
    var end_minute: String,
    var bid_modifier: Double
  )

  def campaignAdScheduleForm: Form[CampaignAdScheduleForm] = Form(
    mapping(
      "api_id" -> optional(longNumber),
      "campaign_api_id" -> longNumber,
      "is_negative" -> optional(boolean),
      "day_of_week" -> text,
      "start_hour" -> number,
      "start_minute" -> text,
      "end_hour" -> number,
      "end_minute" -> text,
      "bid_modifier" -> of[Double]
    )(CampaignAdScheduleForm.apply)(CampaignAdScheduleForm.unapply)
  )
}
