package controllers.google.campaign.criterion

import Shared.Shared._
import com.google.api.ads.adwords.axis.v201409.cm.{Campaign, CampaignCriterion, Keyword}
import models.mongodb._
import models.mongodb.google.collections._
import play.api.data.Forms._
import play.api.data._
import play.api.data.format.Formats._
import play.api.libs.json.{JsObject, Json}
import play.api.mvc._
import play.modules.reactivemongo.MongoController
import reactivemongo.api.QueryOpts
import reactivemongo.core.commands.Count

import scala.collection.mutable.ListBuffer
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

object CampaignKeywordController extends Controller with MongoController {

  def campaignKeywords(page: Int, pageSize: Int, orderBy: Int, filter: String) = Action.async {
    implicit request =>
      campaignCriterionCollection.find(Json.obj("criterion.criterionType" -> "Keyword"))
        .options(QueryOpts(skipN=page * pageSize))
        .cursor[JsObject]
        .collect[List](pageSize).map { campaign_criterion =>
          Ok(views.html.google.campaign.criterion.keyword.campaign_keywords(
            campaign_criterion.map(y => gson.fromJson(Json.stringify(y), classOf[CampaignCriterion])),
            page,
            pageSize,
            orderBy,
            filter,
            Await.result(db.command(Count(campaignCriterionCollection.name, None)), 30 seconds),
            pendingCache(request)
              .filter(x =>
                x.traffic_source == traffic_source.GOOGLE
                && x.change_category == change_category.KEYWORD
              )
          ))
      }.recover{
        case e =>
          BadRequest("Error -> %s".format(e.toString))
      }
  }

  def newCampaignKeyword = Action.async {
    implicit request =>
      campaignCollection.find(Json.obj()).cursor[JsObject].collect[List]().map { campaigns =>
        Ok(views.html.google.campaign.criterion.keyword.new_campaign_keyword(
          campaignKeywordForm,
          campaigns.map(y => gson.fromJson(Json.stringify(y), classOf[Campaign])),
          pendingCache(request)
            .filter(x =>
              x.change_type == change_type.NEW
              && x.traffic_source == traffic_source.GOOGLE
              && x.change_category == change_category.CAMPAIGN
            )
            .map(_.change_data.asInstanceOf[controllers.google.campaign.CampaignController.CampaignForm]),
          List()
        ))
      }.recover{
        case e =>
          BadRequest("Error -> %s".format(e.toString))
      }
  }

  def createCampaignKeyword = Action.async {
    implicit request =>
      campaignCollection.find(Json.obj()).cursor[JsObject].collect[List]().map { campaigns =>
        campaignKeywordForm.bindFromRequest.fold(
          formWithErrors => {
            BadRequest(views.html.google.campaign.criterion.keyword.new_campaign_keyword(
              formWithErrors,
              campaigns.map(y => gson.fromJson(Json.stringify(y), classOf[Campaign])),
              pendingCache(request)
                .filter(x =>
                  x.change_type == change_type.NEW
                  && x.traffic_source == traffic_source.GOOGLE
                  && x.change_category == change_category.CAMPAIGN
                )
                .map(_.change_data.asInstanceOf[controllers.google.campaign.CampaignController.CampaignForm]),
              List()
            ))
          },
          campaign_keyword => {
            setPendingCache(
              request,
              pendingCache(request) :+ pending_cache_structure(
                id=pendingCache(request).length+1,
                change_type=change_type.NEW,
                traffic_source=traffic_source.GOOGLE,
                change_category=change_category.CAMPAIGN_KEYWORD,
                change_data=campaign_keyword
              )
            )
            Redirect(controllers.google.campaign.criterion.routes.CampaignKeywordController.campaignKeywords())
          }
        )
      }.recover{
        case e =>
          BadRequest("Error -> %s".format(e.toString))
      }
  }

  def bulkNewCampaignKeyword = Action.async(parse.multipartFormData) {
    implicit request =>
      campaignCollection.find(Json.obj()).cursor[JsObject].collect[List]().map { campaigns =>
        var error_list = new ListBuffer[String]()
        request.body.file("bulk").map {
          bulk => {
            val field_names = Utilities.getCaseClassParameter[CampaignKeywordForm]
            val keyword_data_list = Utilities.bulkImport(bulk, field_names)
            for (((keyword_data, action), index) <- keyword_data_list.zipWithIndex) {
              campaignKeywordForm.bind(keyword_data.map(kv => (kv._1, kv._2)).toMap).fold(
                formWithErrors => {
                  error_list += "Row " + index.toString + ": " + formWithErrors.errorsAsJson.toString
                },
                campaign_keyword =>
                  setPendingCache(
                    request,
                    pendingCache(request) :+ pending_cache_structure(
                      id=pendingCache(request).length+1,
                      change_type=change_type.withName(action.toUpperCase),
                      traffic_source=traffic_source.GOOGLE,
                      change_category=change_category.CAMPAIGN_KEYWORD,
                      change_data=campaign_keyword
                    )
                  )
              )
            }
          }
        }
        if (error_list.size > 0) {
          BadRequest(views.html.google.campaign.criterion.keyword.new_campaign_keyword(
            campaignKeywordForm,
            campaigns.map(y => gson.fromJson(Json.stringify(y), classOf[Campaign])),
            pendingCache(request)
              .filter(x =>
                x.change_type == change_type.NEW
                && x.traffic_source == traffic_source.GOOGLE
                && x.change_category == change_category.CAMPAIGN
              )
              .map(_.change_data.asInstanceOf[controllers.google.campaign.CampaignController.CampaignForm]),
            error_list.toList
          ))
        } else {
          Redirect(controllers.google.campaign.criterion.routes.CampaignKeywordController.campaignKeywords())
        }
      }.recover{
        case e =>
          BadRequest("Error -> %s".format(e.toString))
      }
  }

  def editCampaignKeyword(api_id: Long) = Action.async {
    implicit request =>
      campaignCriterionCollection.find(Json.obj("criterion.properties.id" -> api_id)).one[JsObject].flatMap {
        case None => Future{Ok(views.html.dashboard())}
        case Some(campaign_criterion_obj) =>
          campaignCollection.find(Json.obj()).cursor[JsObject].collect[List]().map { campaigns =>
            def campaign_keyword = gson.fromJson(Json.stringify(campaign_criterion_obj), classOf[CampaignCriterion])
            Ok(views.html.google.campaign.criterion.keyword.edit_campaign_keyword(
              api_id,
              campaignKeywordForm.fill(
                CampaignKeywordForm(
                  api_id = Some(campaign_keyword.getCriterion.getId),
                  campaign_api_id = campaign_keyword.getCampaignId,
                  is_negative = Some(campaign_keyword.getIsNegative),
                  text = campaign_keyword.getCriterion.asInstanceOf[Keyword].getText,
                  match_type = campaign_keyword.getCriterion.asInstanceOf[Keyword].getMatchType.toString,
                  bid_modifier = Some(campaign_keyword.getBidModifier)
                )
              ),
              campaigns.map(y => gson.fromJson(Json.stringify(y), classOf[Campaign])),
              pendingCache(request)
                .filter(x =>
                  x.change_type == change_type.NEW
                  && x.traffic_source == traffic_source.GOOGLE
                  && x.change_category == change_category.CAMPAIGN
                )
                .map(_.change_data.asInstanceOf[controllers.google.campaign.CampaignController.CampaignForm])
            ))
          }.recover{
            case e =>
              BadRequest("Error -> %s".format(e.toString))
          }
      }.recover{
        case e =>
          BadRequest("Error -> %s".format(e.toString))
      }
  }

  def saveCampaignKeyword(api_id: Long) = Action.async {
    implicit request =>
      campaignCollection.find(Json.obj()).cursor[JsObject].collect[List]().map { campaigns =>
        campaignKeywordForm.bindFromRequest.fold(
          formWithErrors => BadRequest(views.html.google.campaign.criterion.keyword.edit_campaign_keyword(
            api_id,
            formWithErrors,
            campaigns.map(y => gson.fromJson(Json.stringify(y), classOf[Campaign])),
            pendingCache(request)
              .filter(x =>
                x.change_type == change_type.NEW
                && x.traffic_source == traffic_source.GOOGLE
                && x.change_category == change_category.CAMPAIGN
              )
              .map(_.change_data.asInstanceOf[controllers.google.campaign.CampaignController.CampaignForm])
          )),
          campaign_keyword => {
            setPendingCache(
              request,
              pendingCache(request) :+ pending_cache_structure(
                id=pendingCache(request).length+1,
                change_type=change_type.UPDATE,
                traffic_source=traffic_source.GOOGLE,
                change_category=change_category.CAMPAIGN_KEYWORD,
                change_data=campaign_keyword
              )
            )
            Redirect(controllers.google.campaign.criterion.routes.CampaignKeywordController.campaignKeywords())
          }
        )
      }
  }

  def deleteCampaignKeyword(api_id: Long) = Action {
    implicit request =>
      setPendingCache(
        request,
        pendingCache(request) :+ pending_cache_structure(
          id=pendingCache(request).length+1,
          change_type=change_type.DELETE,
          traffic_source=traffic_source.GOOGLE,
          change_category=change_category.CAMPAIGN_KEYWORD,change_data=api_id
        )
      )
      Redirect(controllers.google.campaign.criterion.routes.CampaignKeywordController.campaignKeywords())
  }


  case class CampaignKeywordForm(
    var api_id: Option[Long],
    var campaign_api_id: Long,
    var is_negative: Option[Boolean],
    var text: String,
    var match_type: String,
    var bid_modifier: Option[Double]
  )


  def campaignKeywordForm: Form[CampaignKeywordForm] = Form(
    mapping(
      "api_id" -> optional(longNumber),
      "campaign_api_id" -> longNumber,
      "is_negative" -> optional(boolean),
      "text" -> nonEmptyText,
      "match_type" -> nonEmptyText,
      "bid_modifier" -> optional(of[Double])
    )(CampaignKeywordForm.apply)(CampaignKeywordForm.unapply)
  )
}
