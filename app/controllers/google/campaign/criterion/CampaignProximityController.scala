package controllers.google.campaign.criterion

import Shared.Shared._
import com.google.api.ads.adwords.axis.v201409.cm.{Campaign, CampaignCriterion, Proximity}
import models.mongodb._
import models.mongodb.google.collections._
import play.api.data.Forms._
import play.api.data._
import play.api.data.format.Formats._
import play.api.libs.json.{JsObject, Json}
import play.api.mvc._
import play.modules.reactivemongo.MongoController
import reactivemongo.api.QueryOpts
import reactivemongo.core.commands.Count

import scala.collection.mutable.ListBuffer
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

object CampaignProximityController extends Controller with MongoController {

  def campaignProximity(page: Int, pageSize: Int, orderBy: Int, filter: String) = Action.async {
    implicit request =>
      campaignCriterionCollection.find(Json.obj("criterion.criterionType" -> "Proximity"))
        .options(QueryOpts(skipN=page * pageSize))
        .cursor[JsObject]
        .collect[List](pageSize).map { proximity =>
          Ok(views.html.google.campaign.criterion.proximity.campaign_proximity(
            proximity.map(y => gson.fromJson(Json.stringify(y), classOf[CampaignCriterion])),
            page,
            pageSize,
            orderBy,
            filter,
            Await.result(db.command(Count(campaignCriterionCollection.name, None)), 30 seconds),
            pendingCache(request)
              .filter(x =>
                x.traffic_source == traffic_source.GOOGLE.toString
                && x.change_category == change_category.CAMPAIGN_PROXIMITY.toString
              )
          ))
      }
  }

  def newCampaignProximity = Action.async {
    implicit request =>
      campaignCollection.find(Json.obj()).cursor[JsObject].collect[List]().map { campaigns =>
        Ok(views.html.google.campaign.criterion.proximity.new_campaign_proximity(
          campaignProximityForm,
          campaigns.map(y => gson.fromJson(Json.stringify(y), classOf[Campaign])),
          pendingCache(request)
            .filter(x =>
              x.change_type == change_type.NEW.toString
              && x.traffic_source == traffic_source.GOOGLE.toString
              && x.change_category == change_category.CAMPAIGN.toString
            )
            .map(_.change_data.asInstanceOf[controllers.google.campaign.CampaignController.CampaignForm]),
          List()
        ))
      }
  }

  def createCampaignProximity = Action.async {
    implicit request =>
      campaignCollection.find(Json.obj()).cursor[JsObject].collect[List]().map { campaigns =>
        campaignProximityForm.bindFromRequest.fold(
          formWithErrors => {
            BadRequest(views.html.google.campaign.criterion.proximity.new_campaign_proximity(
              formWithErrors,
              campaigns.map(y => gson.fromJson(Json.stringify(y), classOf[Campaign])),
              pendingCache(request)
                .filter(x =>
                  x.change_type == change_type.NEW.toString &&
                  x.traffic_source == traffic_source.GOOGLE.toString &&
                  x.change_category == change_category.CAMPAIGN.toString
                )
                .map(_.change_data.asInstanceOf[controllers.google.campaign.CampaignController.CampaignForm]),
              List()
            ))
          },
          campaign_keyword => {
            setPendingCache(
              request,
              pendingCache(request) :+ pending_cache_structure(
                id=pendingCache(request).length+1,
                change_type=change_type.NEW,
                traffic_source=traffic_source.GOOGLE,
                change_category=change_category.CAMPAIGN_PROXIMITY,
                change_data=campaign_keyword
              )
            )
            Redirect(controllers.google.campaign.criterion.routes.CampaignProximityController.campaignProximity())
          }
        )
      }
  }

  def bulkNewCampaignProximity = Action.async(parse.multipartFormData) {
    implicit request =>
      var error_list = new ListBuffer[String]()
      campaignCollection.find(Json.obj()).cursor[JsObject].collect[List]().map { campaigns =>
        request.body.file("bulk").map {
          bulk => {
            val field_names = Utilities.getCaseClassParameter[CampaignCriterion]
            val proximity_data_list = Utilities.bulkImport(bulk, field_names)
            for (((proximity_data, action), index) <- proximity_data_list.zipWithIndex) {
              campaignProximityForm.bind(proximity_data.map(kv => (kv._1, kv._2)).toMap).fold(
                formWithErrors => {
                  error_list += "Row " + index.toString + ": " + formWithErrors.errorsAsJson.toString
                },
                campaign_proximity =>
                  setPendingCache(
                    request,
                    pendingCache(request) :+ pending_cache_structure(
                      id=pendingCache(request).length+1,
                      change_type=change_type.withName(action.toUpperCase),
                      traffic_source=traffic_source.GOOGLE,
                      change_category=change_category.CAMPAIGN_KEYWORD,
                      change_data=campaign_proximity
                    )
                  )
              )
            }
          }
        }
        if (error_list.size > 0) {
          BadRequest(views.html.google.campaign.criterion.proximity.new_campaign_proximity(
            campaignProximityForm,
            campaigns.map(y => gson.fromJson(Json.stringify(y), classOf[Campaign])),
            pendingCache(request)
              .filter(x =>
                x.change_type == change_type.NEW
                && x.traffic_source == traffic_source.GOOGLE
                && x.change_category == change_category.CAMPAIGN
              )
              .map(_.change_data.asInstanceOf[controllers.google.campaign.CampaignController.CampaignForm]),
            error_list.toList
          ))
        } else {
          Redirect(controllers.google.campaign.criterion.routes.CampaignProximityController.campaignProximity())
        }
      }
  }

  def editCampaignProximity(api_id: Long) = Action.async {
    implicit request =>
      campaignCriterionCollection.find(Json.obj("criterion.id" -> api_id)).one[JsObject].flatMap {
        case None => Future{Ok(views.html.dashboard())}
        case Some(campaign_criterion_obj) =>
          campaignCollection.find(Json.obj()).cursor[JsObject].collect[List]().map { campaigns =>
            def campaign_proximity = gson.fromJson(Json.stringify(campaign_criterion_obj), classOf[CampaignCriterion])
            Ok(views.html.google.campaign.criterion.proximity.edit_campaign_proximity(
              api_id,
              campaignProximityForm.fill(
                CampaignProximityForm(
                  api_id = Some(campaign_proximity.getCriterion.getId),
                  campaign_api_id = campaign_proximity.getCampaignId,
                  is_negative = Some(campaign_proximity.getIsNegative),
                  latitude_in_micro_degrees = Some(campaign_proximity.getCriterion.asInstanceOf[Proximity].getGeoPoint.getLatitudeInMicroDegrees),
                  longitude_in_micro_degrees = Some(campaign_proximity.getCriterion.asInstanceOf[Proximity].getGeoPoint.getLongitudeInMicroDegrees),
                  radius_distance_units = campaign_proximity.getCriterion.asInstanceOf[Proximity].getRadiusDistanceUnits.toString,
                  radius_in_units = campaign_proximity.getCriterion.asInstanceOf[Proximity].getRadiusInUnits,
                  street_address = Some(campaign_proximity.getCriterion.asInstanceOf[Proximity].getAddress.getStreetAddress),
                  street_address2 = Some(campaign_proximity.getCriterion.asInstanceOf[Proximity].getAddress.getStreetAddress2),
                  city_name = Some(campaign_proximity.getCriterion.asInstanceOf[Proximity].getAddress.getCityName),
                  province_code = Some(campaign_proximity.getCriterion.asInstanceOf[Proximity].getAddress.getProvinceCode),
                  province_name = Some(campaign_proximity.getCriterion.asInstanceOf[Proximity].getAddress.getProvinceName),
                  postal_code = Some(campaign_proximity.getCriterion.asInstanceOf[Proximity].getAddress.getPostalCode),
                  country_code = Some(campaign_proximity.getCriterion.asInstanceOf[Proximity].getAddress.getCountryCode),
                  bid_modifier = Some(campaign_proximity.getBidModifier)
                )
              ),
              campaigns.map(y => gson.fromJson(Json.stringify(y), classOf[Campaign])),
              pendingCache(request)
                .filter(x =>
                  x.change_type == change_type.NEW
                  && x.traffic_source == traffic_source.GOOGLE
                  && x.change_category == change_category.CAMPAIGN
                )
                .map(_.change_data.asInstanceOf[controllers.google.campaign.CampaignController.CampaignForm])
            ))
          }.recover{
            case e =>
              BadRequest("Error -> %s".format(e.toString))
          }
      }.recover{
        case e =>
        BadRequest("Error -> %s".format(e.toString))
      }
  }

  def saveCampaignProximity(api_id: Long) = Action.async {
    implicit request =>
      campaignCollection.find(Json.obj()).cursor[JsObject].collect[List]().map { campaigns =>
        campaignProximityForm.bindFromRequest.fold(
          formWithErrors => BadRequest(views.html.google.campaign.criterion.proximity.edit_campaign_proximity(
            api_id,
            formWithErrors,
            campaigns.map(y => gson.fromJson(Json.stringify(y), classOf[Campaign])),
            pendingCache(request)
              .filter(x =>
                x.change_type == change_type.NEW
                && x.traffic_source == traffic_source.GOOGLE
                && x.change_category == change_category.CAMPAIGN
              )
              .map(_.change_data.asInstanceOf[controllers.google.campaign.CampaignController.CampaignForm])
          )),
          campaign_proximity => {
            setPendingCache(
              request,
              pendingCache(request) :+ pending_cache_structure(
                id=pendingCache(request).length+1,
                change_type=change_type.UPDATE,
                traffic_source=traffic_source.GOOGLE,
                change_category=change_category.CAMPAIGN_PROXIMITY,
                change_data=campaign_proximity
              )
            )
            Redirect(controllers.google.campaign.criterion.routes.CampaignProximityController.campaignProximity())
          }
        )
      }
  }

  def deleteCampaignProximity(api_id: Long) = Action {
    implicit request =>
      setPendingCache(
        request,
        pendingCache(request) :+ pending_cache_structure(
          id=pendingCache(request).length+1,
          change_type=change_type.DELETE,
          traffic_source=traffic_source.GOOGLE,
          change_category=change_category.CAMPAIGN_PROXIMITY,
          change_data=api_id
        )
      )
      Redirect(controllers.google.campaign.criterion.routes.CampaignProximityController.campaignProximity())
  }

  case class CampaignProximityForm(
    var api_id: Option[Long],
    var campaign_api_id: Long,
    var is_negative: Option[Boolean],
    var latitude_in_micro_degrees: Option[Int],
    var longitude_in_micro_degrees: Option[Int],
    var radius_distance_units: String,
    var radius_in_units: Double,
    var street_address: Option[String],
    var street_address2: Option[String],
    var city_name: Option[String],
    var province_code: Option[String],
    var province_name: Option[String],
    var postal_code: Option[String],
    var country_code: Option[String],
    var bid_modifier: Option[Double]
  )

  def campaignProximityForm: Form[CampaignProximityForm] = Form(
    mapping(
      "api_id" -> optional(longNumber),
      "campaign_api_id" -> longNumber,
      "is_negative" -> optional(boolean),
      "latitude_in_micro_degrees" -> optional(number),
      "longitude_in_micro_degrees" -> optional(number),
      "radius_distance_units" -> text,
      "radius_in_units" -> of[Double],
      "street_address" -> optional(text),
      "street_address2" -> optional(text),
      "city_name" -> optional(text),
      "province_code" -> optional(text),
      "province_name" -> optional(text),
      "postal_code" -> optional(text),
      "country_code" -> optional(text),
      "bid_modifier" -> optional(of[Double])
    )(CampaignProximityForm.apply)(CampaignProximityForm.unapply)
  )
}
