package controllers.google.campaign

import Shared.Shared._
import com.google.api.ads.adwords.axis.v201409.cm.{Budget, Campaign}
import models.mongodb._
import models.mongodb.google.collections._
import play.api.Play.current
import play.api.cache.Cache
import play.api.data.Forms._
import play.api.data._
import play.api.libs.json.{JsObject, Json}
import play.api.mvc._
import play.modules.reactivemongo.MongoController
import reactivemongo.api.QueryOpts
import reactivemongo.core.commands.Count

import scala.collection.immutable.List
import scala.collection.mutable.ListBuffer
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._


object CampaignController extends Controller with MongoController {

  def campaigns(page: Int, pageSize: Int, orderBy: Int, filter: String) = Action.async {
    implicit request =>
      campaignCollection.find(Json.obj())
        .options(QueryOpts(skipN=page * pageSize))
        .cursor[JsObject]
        .collect[List](pageSize).map { campaigns =>
          Ok(views.html.google.campaign.campaigns(
            campaigns.map(y => gson.fromJson(Json.stringify(y), classOf[Campaign])),
            page,
            pageSize,
            orderBy,
            filter,
            Await.result(db.command(Count(campaignCollection.name, None)), 30 seconds),
            pendingCache(request)
              .filter(
                x =>
                  x.traffic_source == traffic_source.GOOGLE && x.change_category == change_category.CAMPAIGN
              )
          ))
      }.recover{
        case e =>
          BadRequest("Error -> %s".format(e.toString))
      }
  }

  def newCampaign = Action.async {
    implicit request =>
      val pending_cache_key = request.session.get(Security.username).get + cache_ext
      budgetCollection.find(Json.obj()).cursor[JsObject].collect[List]().map { budgets =>
        Ok(views.html.google.campaign.new_campaign(
          campaignForm,
          budgets.map(y => gson.fromJson(Json.stringify(y), classOf[Budget])),
          Cache.get(pending_cache_key)
            .getOrElse(List()).asInstanceOf[List[pending_cache_structure]]
            .filter(x => x.change_type == change_type.NEW && x.traffic_source == traffic_source.GOOGLE && x.change_category == change_category.BUDGET)
            .map(_.change_data.asInstanceOf[controllers.google.account.BudgetController.BudgetForm]),
          List()
        ))
      }
  }

  def createCampaign = Action.async {
    implicit request =>
      budgetCollection.find(Json.obj()).cursor[JsObject].collect[List]().map { budgets =>
        campaignForm.bindFromRequest.fold(
          formWithErrors => {
            BadRequest(views.html.google.campaign.new_campaign(
              formWithErrors,
              budgets.map(y => gson.fromJson(Json.stringify(y), classOf[Budget])),
              pendingCache(request)
                .filter(x => x.change_type == change_type.NEW && x.traffic_source == traffic_source.GOOGLE && x.change_category == change_category.BUDGET)
                .map(_.change_data.asInstanceOf[controllers.google.account.BudgetController.BudgetForm]),
              List()
            ))
          },
          campaign => {
            setPendingCache(
              request,
              pendingCache(request) :+ pending_cache_structure(
                id=pendingCache(request).length+1,
                change_type=change_type.NEW,
                traffic_source=traffic_source.GOOGLE,
                change_category=change_category.CAMPAIGN,
                change_data=campaign
              )
            )
            Redirect(controllers.google.campaign.routes.CampaignController.campaigns())
          }
        )
      }
  }


  def bulkNewCampaign = Action.async(parse.multipartFormData) {
    implicit request =>
      budgetCollection.find(Json.obj()).cursor[JsObject].collect[List]().map { budgets =>
        var error_list = new ListBuffer[String]()
        request.body.file("bulk").map {
          bulk => {
            val field_names = Utilities.getCaseClassParameter[CampaignForm]
            val campaign_data_list = Utilities.bulkImport(bulk, field_names)
            for (((campaign_data, action), index) <- campaign_data_list.zipWithIndex) {
              campaignForm.bind(campaign_data.map(kv => (kv._1, kv._2)).toMap).fold(
                formWithErrors => {
                  error_list += "Row " + index.toString + ": " + formWithErrors.errorsAsJson.toString
                },
                campaign =>
                  setPendingCache(
                    request,
                    pendingCache(request) :+ pending_cache_structure(
                      id=pendingCache(request).length+1,
                      change_type=change_type.withName(action.toUpperCase),
                      traffic_source=traffic_source.GOOGLE,
                      change_category=change_category.CAMPAIGN,
                      change_data=campaign
                    )
                  )
              )
            }
          }
        }
        if (error_list.size > 0) {
          BadRequest(views.html.google.campaign.new_campaign(
            campaignForm,
            budgets.map(y => gson.fromJson(Json.stringify(y), classOf[Budget])),
            pendingCache(request)
              .filter(x => x.change_type == change_type.NEW && x.traffic_source == traffic_source.GOOGLE && x.change_category == change_category.BUDGET)
              .map(_.change_data.asInstanceOf[controllers.google.account.BudgetController.BudgetForm]),
            error_list.toList
          ))
        } else {
          Redirect(controllers.google.campaign.routes.CampaignController.campaigns())
        }
      }
  }

  def editCampaign(api_id: Long) = Action.async {
    implicit request =>
      campaignCollection.find(Json.obj("id" -> api_id)).one[JsObject].flatMap {
        case None => Future{Ok(views.html.dashboard())}
        case Some(campaign_obj) =>
          def campaign = gson.fromJson(Json.stringify(campaign_obj), classOf[Campaign])
          budgetCollection.find(Json.obj()).cursor[JsObject].collect[List]().map { budgets =>
            Ok(views.html.google.campaign.edit_campaign(
              campaign.getId,
              campaignForm.fill(
                CampaignForm(
                  api_id=Some(campaign.getId),
                  name=campaign.getName,
                  status=Some(campaign.getStatus.toString),
                  serving_status=Some(campaign.getServingStatus.toString),
                  start_date=Some(campaign.getStartDate),
                  end_date=Some(campaign.getEndDate),
                  budget_api_id=Some(campaign.getBudget.getBudgetId),
                  advertising_channel_type=Some(campaign.getAdvertisingChannelType.toString)
              )),
              budgets.map(y => gson.fromJson(Json.stringify(y), classOf[Budget]))
            ))
          }.recover{
            case e =>
              BadRequest("Error -> %s".format(e.toString))
          }
      }.recover{
        case e =>
          BadRequest("Error -> %s".format(e.toString))
      }
  }

  def saveCampaign(api_id: Long) = Action.async {
    implicit request =>
      budgetCollection.find(Json.obj()).cursor[JsObject].collect[List]().map { budgets =>
        campaignForm.bindFromRequest.fold(
          formWithErrors => {
            BadRequest(views.html.google.campaign.edit_campaign(
              api_id,
              formWithErrors,
              budgets.map(y => gson.fromJson(Json.stringify(y), classOf[Budget]))
            ))
          },
          campaign => {
            setPendingCache(
              request,
              pendingCache(request) :+ pending_cache_structure(
                id=pendingCache(request).length+1,
                change_type=change_type.UPDATE,
                traffic_source=traffic_source.GOOGLE,
                change_category=change_category.CAMPAIGN,
                change_data=campaign
              )
            )
            Redirect(controllers.google.campaign.routes.CampaignController.campaigns())
          }
        )
      }
  }

  def deleteCampaign(api_id: Long) = Action {
    implicit request =>
      setPendingCache(
        request,
        pendingCache(request) :+ pending_cache_structure(
          id=pendingCache(request).length+1,
          change_type=change_type.DELETE,
          traffic_source=traffic_source.GOOGLE,
          change_category=change_category.CAMPAIGN,
          change_data=api_id
        )
      )
      Redirect(controllers.google.campaign.routes.CampaignController.campaigns())
  }

  case class CampaignForm(
    var api_id: Option[Long],
    var name: String,
    var status: Option[String],
    var serving_status: Option[String],
    var start_date: Option[String],
    var end_date: Option[String],
    var budget_api_id: Option[Long],
    var advertising_channel_type: Option[String]
  )

  def campaignForm: Form[CampaignForm] = Form(
    mapping(
      "api_id" -> optional(longNumber),
      "name" -> nonEmptyText,
      "status" -> optional(text),
      "serving_status" -> optional(text),
      "start_date" -> optional(text),
      "end_date" -> optional(text),
      "budget_api_id" -> optional(longNumber),
      "advertising_channel_type" -> optional(text)
    )(CampaignForm.apply)(CampaignForm.unapply)
  )
}