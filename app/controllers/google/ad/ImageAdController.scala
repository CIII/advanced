package controllers.google.ad

import Shared.Shared._
import com.google.api.ads.adwords.axis.v201409.cm.{AdGroupAd, ImageAd}
import models.mongodb._
import models.mongodb.google.collections._
import play.api.data.Forms._
import play.api.data._
import play.api.libs.json.{JsObject, Json}
import play.api.mvc._
import play.modules.reactivemongo.MongoController
import reactivemongo.api.QueryOpts
import reactivemongo.core.commands.Count

import scala.collection.mutable.ListBuffer
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent._
import scala.concurrent.duration._

object ImageAdController extends Controller with MongoController {

  def imageAds(page: Int, pageSize: Int, orderBy: Int, filter: String) = Action.async {
    implicit request =>
      adGroupAdCollection.find(Json.obj("ad.properties.adType" -> "ImageAd"))
        .options(QueryOpts(skipN=page * pageSize))
        .cursor[JsObject].collect[List](pageSize).map(image_ads =>
          Ok(views.html.google.ad.image.image_ads(
            image_ads.map(y => gson.fromJson(Json.stringify(y), classOf[AdGroupAd])),
            page,
            pageSize,
            orderBy,
            filter,
            Await.result(db.command(Count(adGroupAdCollection.name, None)), 30 seconds),
            pendingCache(request)
              .filter(x =>
                x.traffic_source == traffic_source.GOOGLE
                && x.change_category == change_category.IMAGE_AD
              )
          ))
      ).recover {
        case e =>
          BadRequest("Error -> %s".format(e.toString))
      }
  }

  def newImageAd = Action {
    implicit request =>
      Ok(views.html.google.ad.image.new_image_ad(
        imageAdForm,
        List()
      ))
  }

  def editImageAd(api_id: Long) = Action.async {
    implicit request =>
      adGroupAdCollection.find(Json.obj("ad.properties.id" -> api_id)).one[JsObject].map {
        case Some(image_ad_obj) =>
          def image_ad = gson.fromJson(Json.stringify(image_ad_obj), classOf[AdGroupAd])
          Ok(views.html.google.ad.image.edit_image_ad(
            image_ad.getAd.getId,
            imageAdForm.fill(
              ImageAdForm(
                api_id = Some(image_ad.getAd.getId),
                ad_group_api_id = image_ad.getAdGroupId,
                url = Some(image_ad.getAd.getDisplayUrl),
                display_url = Some(image_ad.getAd.getDisplayUrl),
                device_preference = Some(image_ad.getAd.getDevicePreference),
                image_name = image_ad.getAd.asInstanceOf[ImageAd].getName,
                image_file_size = Some(image_ad.getAd.asInstanceOf[ImageAd].getImage.getFileSize),
                image_data = Some(image_ad.getAd.asInstanceOf[ImageAd].getImage.getData),
                status = Some(image_ad.getStatus.toString),
                approval_status = Some(image_ad.getApprovalStatus.toString),
                disapproval_reasons = Some(image_ad.getDisapprovalReasons.toList),
                trademark_disapproved = Some(image_ad.getTrademarkDisapproved)
              )
            )
          ))
        case None =>
          BadRequest("NOT FOUND")
      }.recover {
        case e =>
          BadRequest("Error -> %s".format(e.toString))
      }
  }


  def image(byteString: String) = Action {

    val MimeType = "image/png"
    try {
      val imageData: Array[Byte] = byteString.getBytes("UTF-8")
      Ok(imageData).as(MimeType)
    }
    catch {
      case e: IllegalArgumentException =>
        BadRequest("Could not generate image. Error: " + e.toString)
    }
  }


  def createImageAd = Action {
    implicit request =>
      imageAdForm.bindFromRequest.fold(
        formWithErrors => {
          BadRequest(
            views.html.google.ad.image.new_image_ad(
              formWithErrors,
              List()
            )
          )
        },
        image_ad => {
          setPendingCache(
            request,
            pendingCache(request) :+ pending_cache_structure(
              id=pendingCache(request).length+1,
              change_type=change_type.NEW,
              traffic_source=traffic_source.GOOGLE,
              change_category=change_category.IMAGE_AD,
              change_data=image_ad
            )
          )
          Redirect(controllers.google.ad.routes.ImageAdController.imageAds())
        }
      )
  }


  def bulkNewImageAd = Action(parse.multipartFormData) {
    implicit request =>
      var error_list = new ListBuffer[String]()
      request.body.file("bulk").map {
        bulk => {
          val field_names = Utilities.getCaseClassParameter[ImageAdForm]
          val image_ad_data_list = Utilities.bulkImport(bulk, field_names)
          for (((image_ad_data, action), index) <- image_ad_data_list.zipWithIndex) {
            imageAdForm.bind(image_ad_data.map(kv => (kv._1, kv._2)).toMap).fold(
              formWithErrors => {
                error_list += "Row " + index.toString + ": " + formWithErrors.errorsAsJson.toString
              },
              image_ad =>
                setPendingCache(
                  request,
                  pendingCache(request) :+ pending_cache_structure(
                    id=pendingCache(request).length+1,
                    change_type=change_type.withName(action.toUpperCase),
                    traffic_source=traffic_source.GOOGLE,
                    change_category=change_category.IMAGE_AD,
                    change_data=image_ad
                  )
                )
            )
          }
        }
      }
      if (error_list.size > 0) {
        BadRequest(views.html.google.ad.image.new_image_ad(
          imageAdForm,
          error_list.toList
        ))
      } else {
        Redirect(controllers.google.ad.routes.ImageAdController.imageAds())
      }
  }


  def saveImageAd(api_id: Long) = Action {
    implicit request =>
      imageAdForm.bindFromRequest.fold(
        formWithErrors => {
          BadRequest(
            views.html.google.ad.image.edit_image_ad(
              api_id,
              formWithErrors
            )
          )
        },
        image_ad => {
          setPendingCache(
            request,
            pendingCache(request) :+ pending_cache_structure(
              id=pendingCache(request).length+1,
              change_type=change_type.UPDATE,
              traffic_source=traffic_source.GOOGLE,
              change_category=change_category.IMAGE_AD,
              change_data=image_ad
            )
          )
          Redirect(controllers.google.ad.routes.ImageAdController.imageAds())
        }
      )
  }


  def deleteImageAd(api_id: Long) = Action {
    implicit request =>
      setPendingCache(
        request,
        pendingCache(request) :+ pending_cache_structure(
          id=pendingCache(request).length+1,
          change_type=change_type.DELETE,
          traffic_source=traffic_source.GOOGLE,
          change_category=change_category.IMAGE_AD,
          change_data=api_id
        )
      )
      Redirect(controllers.google.ad.routes.ImageAdController.imageAds())
  }

  case class ImageAdForm(
    var api_id: Option[Long],
    var ad_group_api_id: Long,
    var url: Option[String],
    var display_url: Option[String],
    var device_preference: Option[Long],
    var image_name: String,
    var image_file_size: Option[Long],
    var image_data: Option[Array[Byte]],
    var status: Option[String],
    var approval_status: Option[String],
    var disapproval_reasons: Option[List[String]],
    var trademark_disapproved: Option[Boolean]
  )

  def imageAdForm: Form[ImageAdForm] = Form(
    mapping(
      "api_id" -> optional(longNumber),
      "ad_group_api_id" -> longNumber,
      "url" -> optional(text),
      "display_url" -> optional(text),
      "device_preference" -> optional(longNumber),
      "image_name" -> nonEmptyText,
      "image_file_size" -> optional(longNumber),
      "image_data" -> optional(ignored(Array[Byte]())),
      "status" -> optional(text),
      "approval_status" -> optional(text),
      "disapproval_reasons" -> optional(list(text)),
      "trademark_disapproved" -> optional(boolean)
      )(ImageAdForm.apply)(ImageAdForm.unapply)
  )
}
