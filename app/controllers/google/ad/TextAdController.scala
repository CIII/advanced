package controllers.google.ad

import Shared.Shared._
import com.google.api.ads.adwords.axis.v201409.cm.AdGroupAd
import models.mongodb._
import models.mongodb.google.collections._
import play.api.data.Forms._
import play.api.data._
import play.api.libs.json.{JsObject, Json}
import play.api.mvc._
import play.modules.reactivemongo.MongoController
import reactivemongo.api.QueryOpts
import reactivemongo.core.commands.Count

import scala.collection.mutable.ListBuffer
import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

object TextAdController extends Controller with MongoController {

  def textAds(page: Int, pageSize: Int, orderBy: Int, filter: String) = Action.async {
    implicit request =>
      adGroupAdCollection.find(Json.obj("ad.properties.adType" -> "TextAd"))
        .options(QueryOpts(skipN=page * pageSize))
        .cursor[JsObject].collect[List](pageSize).map(text_ads =>
          Ok(views.html.google.ad.text.text_ads(
            text_ads.map(y => gson.fromJson(Json.stringify(y), classOf[AdGroupAd])),
            page,
            pageSize,
            orderBy,
            filter,
            Await.result(db.command(Count(adGroupAdCollection.name, None)), 30 seconds),
            pendingCache(request)
              .filter(x =>
                x.traffic_source == traffic_source.GOOGLE
                && x.change_category == change_category.TEXT_AD
              )
          ))
      ).recover {
        case e =>
          BadRequest("Error -> %s".format(e.toString))
      }
  }

  def newTextAd = Action {
    implicit request =>
      Ok(views.html.google.ad.text.new_text_ad(
        textAdForm,
        List()
      ))
  }

  def editTextAd(api_id: Long) = Action.async {
    implicit request =>
      adGroupAdCollection.find(Json.obj("ad.properties.id" -> api_id)).one[JsObject].map {
        case Some(text_ad_obj) =>
          def text_ad = gson.fromJson(Json.stringify(text_ad_obj), classOf[AdGroupAd])
          Ok(views.html.google.ad.text.edit_text_ad(
            api_id,
            textAdForm.fill(
              TextAdForm(
                api_id = Some(text_ad.getAd.getId),
                ad_group_api_id = text_ad.getAdGroupId,
                url = Some(text_ad.getAd.getUrl),
                display_url = Some(text_ad.getAd.getDisplayUrl),
                device_preference = Some(text_ad.getAd.getDevicePreference),
                headline = Some(text_ad.getAd.asInstanceOf[com.google.api.ads.adwords.axis.v201409.cm.TextAd].getHeadline),
                description1 = Some(text_ad.getAd.asInstanceOf[com.google.api.ads.adwords.axis.v201409.cm.TextAd].getDescription1),
                description2 = Some(text_ad.getAd.asInstanceOf[com.google.api.ads.adwords.axis.v201409.cm.TextAd].getDescription2),
                status = Some(text_ad.getStatus.toString),
                approval_status = Some(text_ad.getApprovalStatus.toString),
                disapproval_reasons = Some(text_ad.getDisapprovalReasons.toList),
                trademark_disapproved = Some(text_ad.getTrademarkDisapproved)
              )
            )
          ))
        case None =>
          BadRequest("Not Found")
      }.recover{
        case e =>
          e.printStackTrace()
          BadRequest("Error -> %s".format(e.getMessage))
      }
  }


  def createTextAd = Action {
    implicit request =>
      textAdForm.bindFromRequest.fold(
        formWithErrors => {
          BadRequest(
            views.html.google.ad.text.new_text_ad(
              formWithErrors,
              List()
            )
          )
        },
        text_ad => {
          setPendingCache(
            request,
            pendingCache(request) :+ pending_cache_structure(
              id=pendingCache(request).length+1,
              change_type=change_type.NEW,
              traffic_source=traffic_source.GOOGLE,
              change_category=change_category.TEXT_AD,
              change_data=text_ad
            )
          )
          Redirect(controllers.google.ad.routes.TextAdController.textAds())
        }
      )
  }


  def bulkNewTextAd = Action(parse.multipartFormData) {
    implicit request => {
      var error_list = new ListBuffer[String]()
      request.body.file("bulk").map {
        bulk => {
          val field_names = Utilities.getCaseClassParameter[TextAdForm]
          val text_ad_data_list = Utilities.bulkImport(bulk, field_names)
          for (((text_ad_data, action), index) <- text_ad_data_list.zipWithIndex) {
            textAdForm.bind(text_ad_data.map(kv => (kv._1, kv._2)).toMap).fold(
              formWithErrors => {
                error_list += "Row " + index.toString + ": " + formWithErrors.errorsAsJson.toString
              },
              text_ad => {
                setPendingCache(
                  request,
                  pendingCache(request) :+ pending_cache_structure(
                    id=pendingCache(request).length+1,
                    change_type=change_type.withName(action.toUpperCase),
                    traffic_source=traffic_source.GOOGLE,
                    change_category=change_category.TEXT_AD,
                    change_data=text_ad
                  )
                )
              }
            )
          }
        }
      }
      if (error_list.size > 0) {
        BadRequest(views.html.google.ad.text.new_text_ad(
          textAdForm,
          error_list.toList
        ))
      } else {
        Redirect(controllers.google.ad.routes.TextAdController.textAds())
      }
    }
  }


  def saveTextAd(api_id: Long) = Action {
    implicit request =>
      textAdForm.bindFromRequest.fold(
        formWithErrors => BadRequest(views.html.google.ad.text.edit_text_ad(api_id, formWithErrors)),
        text_ad => {
          setPendingCache(
            request,
            pendingCache(request) :+ pending_cache_structure(
              id=pendingCache(request).length+1,
              change_type=change_type.UPDATE,
              traffic_source=traffic_source.GOOGLE,
              change_category=change_category.TEXT_AD,
              change_data=text_ad
            )
          )
          Redirect(controllers.google.ad.routes.TextAdController.textAds())
        }
      )
  }


  def deleteTextAd(api_id: Long) = Action {
    implicit request =>
      setPendingCache(
        request,
        pendingCache(request) :+ pending_cache_structure(
          id=pendingCache(request).length + 1,
          change_type=change_type.DELETE,
          traffic_source=traffic_source.GOOGLE,
          change_category=change_category.TEXT_AD,
          change_data=api_id
        )
      )
      Redirect(controllers.google.ad.routes.TextAdController.textAds())
  }


  case class TextAdForm(
    var api_id: Option[Long],
    var ad_group_api_id: Long,
    var url: Option[String],
    var display_url: Option[String],
    var device_preference: Option[Long],
    var headline: Option[String],
    var description1: Option[String],
    var description2: Option[String],
    var status: Option[String],
    var approval_status: Option[String],
    var disapproval_reasons: Option[List[String]],
    var trademark_disapproved: Option[Boolean]
  )

  def textAdForm: Form[TextAdForm] = Form(
    mapping(
      "api_id" -> optional(longNumber),
      "ad_group_object_id" -> longNumber,
      "url" -> optional(text),
      "display_url" -> optional(text),
      "device_preference" -> optional(longNumber),
      "headline" -> optional(text),
      "description1" -> optional(text),
      "description2" -> optional(text),
      "status" -> optional(text),
      "approval_status" -> optional(text),
      "disapproval_reasons" -> optional(list(text)),
      "trademark_disapproved" -> optional(boolean)
    )(TextAdForm.apply)(TextAdForm.unapply)
  )
}
