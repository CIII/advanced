package controllers

import javax.inject.Inject

import Shared.Shared._
import be.objectify.deadbolt.scala.cache.HandlerCache
import be.objectify.deadbolt.scala.{ActionBuilders, DeadboltActions}
import com.mongodb.casbah.Imports._
import models.mongodb.Task
import org.joda.time.DateTime
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc._
import sync.shared.Google._
import sync.shared.Msn._
import sync.shared.Yahoo._
import sync.tasks.TaskStatus

import scala.collection.immutable.List

class TaskController @Inject()(val messagesApi: MessagesApi, deadbolt: DeadboltActions, handlers: HandlerCache, actionBuilder: ActionBuilders) extends Controller with I18nSupport {
  def tasks = WebSocket.tryAccept[String] { request =>
    TaskStatus.attach(request.session.get(Security.username).get)
  }

  def run(id: String) = Action {
    implicit request =>
      val user = request.session.get(Security.username).get
      Task.taskCollection.findOne(DBObject("user" -> user, "task.id" -> id.toLong)) match {
        case Some(task_obj) =>
          setPendingCache(Left(request), pendingCache(Left(request)) ::: dboToTaskStructure(task_obj.as[DBObject]("task")).data)
        case _ =>
      }
      Redirect(routes.TaskController.view(id))
  }

  def view(id: String) = Action {
    implicit request =>
      val user = request.session.get(Security.username).get
      val runningTasks = taskCache(Right(user))
      Task.taskCollection.findOne(DBObject("user" -> user, "task.id" -> id.toLong)) match {
        case Some(task_obj) =>
          Ok(views.html.task.view_task(id.toLong, dboToTaskStructure(task_obj.as[DBObject]("task")), runningTasks))
        case _ =>
          NotFound
      }
  }

  def pending_changes = Action {
    implicit request =>
      Ok(views.html.pending(pendingCache(Left(request))))
  }

  def cancel_change(index: String) = Action {
    implicit request =>
      setTaskCache(Left(request), taskCache(Left(request)).filterNot(_.id == index.toInt))
      Ok(views.html.pending(pendingCache(Left(request))))
  }

  def confirm_changes = Action {
    implicit request =>
      val user = request.session.get(Security.username).get
      val task = TaskStructure(
        id=Task.taskCollection.count(DBObject("user" -> user))+1,
        data=pendingCache(Left(request)),
        startTime=new DateTime(),
        complete=false,
        completeTime=None,
        processes=List()
      )
      var subprocess_count = 0
      for(item <- pendingCache(Left(request))) {
        item.trafficSource match {
          case TrafficSource.GOOGLE =>
            (item.changeCategory match {
                case ChangeCategory.MCC =>
                  googleManagementActor
                case ChangeCategory.CAMPAIGN =>
                  googleCampaignActor
                case ChangeCategory.AD_GROUP =>
                  googleAdGroupActor
              }
            ) ! PendingCacheMessage(cache=Some(item), request=Some(request))
          case TrafficSource.MSN =>
            (item.changeCategory match {
              case ChangeCategory.API_ACCOUNT =>
                msnApiAccountActor
              case ChangeCategory.ACCOUNT_INFO =>
                msnAccountInfoActor
            }) ! PendingCacheMessage(cache=Some(item), request=Some(request))
                subprocess_count = 5
          case TrafficSource.YAHOO =>
            (
              item.changeCategory match {
                case ChangeCategory.API_ACCOUNT =>
                  yahooApiAccountActor
              }
            ) ! PendingCacheMessage(cache=Some(item), request=Some(request))
        }
        setPendingCache(
          Left(request),
          pendingCache(Left(request)).filterNot (_.id == item.id)
        )
        task.processes = task.processes :+ Process(changeDataId=item.id, subProcesses=subprocess_count, completedSubProcesses=0)
      }
      setTaskCache(Left(request), taskCache(Left(request)) :+ task)
      Task.taskCollection.insert(
        DBObject(
          "user" -> user,
          "task" -> taskStructureToDbo(task)
        )
      )
      Redirect(routes.DashboardController.dashboard())
  }
}
