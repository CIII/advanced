package controllers

import play.api.data.Form
import play.api.data.Forms._
import reactivemongo.bson.BSONObjectID

object UserSettingController {
  case class UserSettingForm(
    var id: BSONObjectID,
    var username: String,
    var show_alerts_on_dashboard: Boolean,
    var alert_types: List[String],
    var alert_severity: List[String]
  )

  def userSettingForm(id: BSONObjectID = BSONObjectID.generate, username: String) = Form(
    mapping(
      "id" -> ignored(id),
      "username" -> ignored(username),
      "show_alerts_on_dashboard" -> boolean,
      "alert_types" -> list(text),
      "alert_severity" -> list(text)
    )(UserSettingForm.apply)(UserSettingForm.unapply)
  )
}
