/**
 *
 */
package security

import be.objectify.deadbolt.scala.{DeadboltHandler, DynamicResourceHandler}
import com.mongodb.casbah.Imports._
import models.mongodb.{PermissionGroup, UserAccount}
import play.api.mvc.{Request, Security}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future


class MyDynamicResourceHandler extends DynamicResourceHandler{

  def isAllowed[A](name: String, meta: String, handler: DeadboltHandler, request: Request[A]) = {
    request.session.get(Security.username) match {
      case Some(username) =>
        UserAccount.userAccountCollection.findOne(DBObject("userName" -> username)) match {
          case Some(userAccount) =>
            Future(UserAccount.dboToUserAccount(userAccount).getPermissions.contains(PermissionGroup.withName(name)))
          case None =>
            Future(false)
        }
      case None =>
        Future(false)
    }
  }

  def checkPermission[A](permissionValue: String, deadboltHandler: DeadboltHandler, request: Request[A]) = Future(false)
}