/**
 *
 */
package handlers

import java.lang.ProcessBuilder.Redirect

import be.objectify.deadbolt.scala.DeadboltHandler
import be.objectify.deadbolt.scala.DynamicResourceHandler
import controllers.routes
import models.mongodb.{PermissionGroup, UserAccount}
import play.api.libs.json.Json
import play.api.mvc.{Controller, Security, Request}
import com.novus.salat._
import com.novus.salat.global._
import com.mongodb.casbah.Imports._


class MyDynamicResourceHandler extends DynamicResourceHandler{

  def isAllowed[A](name: String, meta: String, handler: DeadboltHandler, request: Request[A]) = {
    request.session.get(Security.username) match {
      case Some(username) =>
        UserAccount.userAccountCollection.findOne(DBObject("userName" -> username)) match {
          case Some(userAccount) =>
            UserAccount.dboToUserAccount(userAccount).getPermissions.contains(PermissionGroup.withName(name))
          case None =>
            false
        }
      case None =>
        false
    }
  }

  def checkPermission[A](permissionValue: String, deadboltHandler: DeadboltHandler, request: Request[A]) = {
    println(" MyDynamicResourceHandler checkPermission")
    false
  }
}