/**
 *
 */
package handlers

import controllers.routes
import models.mongodb.UserAccount
import play.api.libs.json.{JsObject, Json}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent._

import be.objectify.deadbolt.core.models.Subject
import be.objectify.deadbolt.scala.DeadboltHandler
import be.objectify.deadbolt.scala.DynamicResourceHandler
import play.api.mvc._
import com.novus.salat._
import com.novus.salat.global._
import com.mongodb.casbah.Imports._

import scala.concurrent.duration.Duration


class MyDeadboltHandler(dynamicResourceHandler: Option[DynamicResourceHandler] = None) extends Controller with DeadboltHandler{

  def beforeAuthCheck[A](request: Request[A]) = None

  override def getDynamicResourceHandler[A](request: Request[A]): Option[DynamicResourceHandler] = {
    if (dynamicResourceHandler.isDefined) dynamicResourceHandler
    else Some(new MyDynamicResourceHandler())
  }

  override def getSubject[A](request: Request[A]): Option[Subject] = {
    request.session.get(Security.username) match {
      case Some(username) =>
        Some(
          UserAccount.dboToUserAccount(
            UserAccount.userAccountCollection.findOne(DBObject("userName" -> username)).get.asDBObject
          )
        )
      case None =>
        Option.empty
    }
  }

  def onAuthFailure[A](request: play.api.mvc.Request[A]): Future[play.api.mvc.Result] = {
    Future{Redirect(routes.DashboardController.permission_denied())}
  }
}