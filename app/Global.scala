import Shared.Shared._
import play.api.mvc.RequestHeader
import play.api.{Application, GlobalSettings}

import scala.concurrent.duration._

object Global extends GlobalSettings {

  override def onStart(app: Application) = {
    googleReportingDaemon(
      reportType = com.google.api.ads.adwords.lib.jaxb.v201509.ReportDefinitionReportType.KEYWORDS_PERFORMANCE_REPORT,
      fields = Some(List(
        "Date",
        "Id",
        "AdGroupId",
        "AdGroupName",
        "ApprovalStatus",
        "AveragePosition",
        "CampaignId",
        "CampaignName",
        "Clicks",
        "ConvertedClicks",
        "Cost",
        "CostPerConvertedClick",
        "Device",
        "FirstPageCpc",
        "Impressions",
        "KeywordMatchType",
        "Criteria",
        "CriteriaDestinationUrl",
        "CpcBid",
        "CpmBid",
        "QualityScore"
      )),
      enableZeroImpressions = true,
      initial_delay = 0 hours,
      interval = 24 hours
    )

    googleReportingDaemon(
      reportType = com.google.api.ads.adwords.lib.jaxb.v201509.ReportDefinitionReportType.GEO_PERFORMANCE_REPORT,
      fields = Some(List(
        "Date",
        "AccountDescriptiveName",
        "CountryCriteriaId",
        "RegionCriteriaId",
        "CampaignId",
        "CampaignName",
        "AdGroupId",
        "AdGroupName",
        "Device",
        "AverageCpc",
        "AveragePosition",
        "Impressions",
        "Clicks",
        "Cost",
        "ConvertedClicks",
        "ClickConversionRate",
        "CostPerConvertedClick",
        "Ctr"
      )),
      enableZeroImpressions = true,
      initial_delay = 0 hours,
      interval = 24 hours
    )

    googleReportingDaemon(
      reportType = com.google.api.ads.adwords.lib.jaxb.v201509.ReportDefinitionReportType.AD_PERFORMANCE_REPORT,
      fields = Some(List(
        "Date",
        "AccountDescriptiveName",
        "CampaignId",
        "CampaignName",
        "AdGroupId",
        "AdGroupName",
        "KeywordId",
        "Id",
        "Device",
        "Impressions",
        "Clicks",
        "Cost",
        "AveragePosition",
        "AdType"
      )),
      enableZeroImpressions = true,
      initial_delay = 0 hours,
      interval = 24 hours
    )

    List(
      GeminiReportType.ad_extension_details,
      GeminiReportType.adjustment_stats,
      GeminiReportType.audience,
      GeminiReportType.conversion_rules_stats,
      GeminiReportType.keyword_stats,
      GeminiReportType.performance_stats,
      GeminiReportType.search_stats
    ).foreach { reportType =>
      yahooReportingDaemon(
        reportType,
        GeminiReportFieldMapping(reportType),
        initial_delay = 0 hours,
        interval = 24 hours
      )
    }

    msnReportingDaemon(MsnReportType.KeywordPerformanceReportRequest, initial_delay=0 hours, interval=24 hours)

    miscDaemon(initial_delay=0 hours, interval=24 hours)

    googleManagementDaemon(initial_delay=0 hours, interval=24 hours)

    uberReportingDaemon(initial_delay=0 hours, interval=24 hours)
  }

  override def onError(request: RequestHeader, ex: Throwable) = {
    ex.printStackTrace()
    super.onError(request, ex)
  }
}