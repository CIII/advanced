package helpers

import play.api.data.Form
import play.api.data.Forms._

import scala.collection.immutable.List

/**
 * Created by clarencewilliams on 11/30/15.
 */
object UserProfileControllerHelper {
  case class UserProfileForm(
    _id: Option[String],
    var username: String,
    var email: String,
    var security_roles: List[String]
  )

  def userProfileForm: Form[UserProfileForm] = Form(
    mapping(
      "_id" -> optional(text),
      "username" -> text,
      "email" -> email,
      "security_roles" -> list(text)
    )(UserProfileForm.apply)(UserProfileForm.unapply)
  )
}
