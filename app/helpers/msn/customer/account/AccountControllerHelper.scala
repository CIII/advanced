package helpers.msn.customer.account

import com.mongodb.casbah.Imports._
import play.api.data.Form
import play.api.data.Forms._

/**
 * Created by clarencewilliams on 11/30/15.
 */
object AccountControllerHelper {
  case class AccountForm(
    userName: String,
    password: String,
    developerToken: String,
    customerApiId: Option[Long],
    customerAccountApiId: Option[Long]
  )

  def accountFormToDbo(af: AccountForm) = DBObject(
    "userName" -> af.userName,
    "password" -> af.password,
    "developerToken" -> af.developerToken,
    "customerApiId" -> af.customerApiId.get,
    "customerAccountApiId" -> af.customerAccountApiId
  )


  def dboToAccountForm(dbo: DBObject) = AccountForm(
    userName=dbo.getAsOrElse[String]("userName", ""),
    password=dbo.getAsOrElse[String]("password", ""),
    developerToken=dbo.getAsOrElse[String]("developerToken", ""),
    customerApiId=dbo.getAsOrElse[Option[Long]]("customerApiId", None),
    customerAccountApiId=dbo.getAsOrElse[Option[Long]]("customerAccountApiId", None)
  )

  def accountForm: Form[AccountForm] = Form(
    mapping(
      "userName" -> nonEmptyText,
      "password" -> nonEmptyText,
      "developerToken" -> nonEmptyText,
      "customerApiId" -> optional(longNumber),
      "customerAccountApiId" -> optional(longNumber)
    )(AccountForm.apply)(AccountForm.unapply)
  )
}
