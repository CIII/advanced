name := """adVanced"""

version := "1.0-SNAPSHOT"

scalaVersion := "2.11.7"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

routesGenerator := InjectedRoutesGenerator

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"

libraryDependencies ++= Seq(
  jdbc,
  cache,
  ws,
  specs2 % Test,
  "org.squeryl" % "squeryl_2.11" % "0.9.6-RC4",
  "mysql" % "mysql-connector-java" % "5.1.10",
  "com.unboundid" % "unboundid-ldapsdk" % "3.0.0",
  "com.google.code.gson" % "gson" % "2.5",
  "org.json" % "json" % "20140107",
  "org.scala-lang.modules" %% "scala-async" % "0.9.2",
  "be.objectify" % "deadbolt-scala_2.11" % "2.4.2",
  "com.google.api-ads" % "ads-lib" % "2.5.0",
  "com.google.api-ads" % "adwords-axis" % "2.5.0",
  "com.beachape" %% "enumeratum" % "1.3.2",
  "com.beachape" %% "enumeratum-play" % "1.3.2",
  "org.mongodb" %% "casbah" % "2.8.1",
  "com.novus" %% "salat" % "1.9.9",
  "com.microsoft.bingads" % "microsoft.bingads" % "10.4.2",
  "com.github.marklister" % "product-collections_2.11" % "1.4.2"
)